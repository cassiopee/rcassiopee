# rcassiopee: The Cassiopee R package

This R package is a wrapper for [JalHyd the Cassiopee library calculation](https://gitlab.irstea.fr/cassiopee/jalhyd) used in the [Cassiopee software](https://cassiopee.g-eau.fr).

The Cassiopée software was developed by the ecohydraulic R&D pole gathering [OFB (French Office for Biodiversity)](https://ofb.gouv.fr/) and [IMFT (Fluids Mechanics Institute of Toulouse)(https://www.imft.fr/)] and by [UMR G-EAU (Joint Research Unit "Water Management, Actors, Territories")](http://g-eau.fr/index.php/en/).

It includes tools for designing fish crossing devices for upstream and downstream migrations, and hydraulic calculation tools useful for environmental and agricultural engineering.

## Installation

You can clone this repository or download its content and type `Rcmd.exe INSTALL rcassiopee` in the terminal.

In an R session, thanks to the `remote` R package, you can also directly type: `remotes::install_gitlab("cassiopee/rcassiopee", host = "gitlab.irstea.fr")`.

## Usage

First, you should load the library in V8 and load a session file previously recorded from [Cassiopee](https://cassiopee.g-eau.fr).

``` r
# Load JalHyd in a v8 context
sv8 <- Cassiopee()

# Load a session of cassiopee
LoadSession(sv8, "session.json")
```

Then you should determine which module you want to use in the session. Each module is identified by a unique identifier (uid) which appears in the address bar of your browser when you select the module. E.g.: For this URL, `https://cassiopee.g-eau.fr/#/calculator/cWp0bT` the uid is "cWp0bT".

``` r
# The nub to be calculated is identified by a unique identifier which appears at the end of the URL of https://cassiopee.g-eau.fr
uid <- "cWp0bT"

# Run the calculation of a nub
CalcNub(sv8, uid)

# Get the result of the calculation as a numeric vector
GetResult(sv8, uid)
```

## Instructions for updating JalHyd version

If a new version of JalHyd is available, you can update it manually in the R package.
You need to have a recent version of `nodejs` and `npm` installed on your system, preferably Linux (not tested on Windows).

At the root of the repository, type in a terminal:

```
npm install
npm start
```

This will update the file `inst/jalhyd/bundle.js` with the last version of [jalhyd published on NPM](https://www.npmjs.com/package/jalhyd).

Then reinstall the R package on your system with RStudio or in the terminal, type `R CMD INSTALL ../rcassiopee`.
