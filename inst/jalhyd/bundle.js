(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function (global){
global.jalhyd = require('jalhyd');

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"jalhyd":14}],2:[function(require,module,exports){
(function (global){
/*! http://mths.be/base64 v0.1.0 by @mathias | MIT license */
;(function(root) {

	// Detect free variables `exports`.
	var freeExports = typeof exports == 'object' && exports;

	// Detect free variable `module`.
	var freeModule = typeof module == 'object' && module &&
		module.exports == freeExports && module;

	// Detect free variable `global`, from Node.js or Browserified code, and use
	// it as `root`.
	var freeGlobal = typeof global == 'object' && global;
	if (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal) {
		root = freeGlobal;
	}

	/*--------------------------------------------------------------------------*/

	var InvalidCharacterError = function(message) {
		this.message = message;
	};
	InvalidCharacterError.prototype = new Error;
	InvalidCharacterError.prototype.name = 'InvalidCharacterError';

	var error = function(message) {
		// Note: the error messages used throughout this file match those used by
		// the native `atob`/`btoa` implementation in Chromium.
		throw new InvalidCharacterError(message);
	};

	var TABLE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
	// http://whatwg.org/html/common-microsyntaxes.html#space-character
	var REGEX_SPACE_CHARACTERS = /[\t\n\f\r ]/g;

	// `decode` is designed to be fully compatible with `atob` as described in the
	// HTML Standard. http://whatwg.org/html/webappapis.html#dom-windowbase64-atob
	// The optimized base64-decoding algorithm used is based on @atk’s excellent
	// implementation. https://gist.github.com/atk/1020396
	var decode = function(input) {
		input = String(input)
			.replace(REGEX_SPACE_CHARACTERS, '');
		var length = input.length;
		if (length % 4 == 0) {
			input = input.replace(/==?$/, '');
			length = input.length;
		}
		if (
			length % 4 == 1 ||
			// http://whatwg.org/C#alphanumeric-ascii-characters
			/[^+a-zA-Z0-9/]/.test(input)
		) {
			error(
				'Invalid character: the string to be decoded is not correctly encoded.'
			);
		}
		var bitCounter = 0;
		var bitStorage;
		var buffer;
		var output = '';
		var position = -1;
		while (++position < length) {
			buffer = TABLE.indexOf(input.charAt(position));
			bitStorage = bitCounter % 4 ? bitStorage * 64 + buffer : buffer;
			// Unless this is the first of a group of 4 characters…
			if (bitCounter++ % 4) {
				// …convert the first 8 bits to a single ASCII character.
				output += String.fromCharCode(
					0xFF & bitStorage >> (-2 * bitCounter & 6)
				);
			}
		}
		return output;
	};

	// `encode` is designed to be fully compatible with `btoa` as described in the
	// HTML Standard: http://whatwg.org/html/webappapis.html#dom-windowbase64-btoa
	var encode = function(input) {
		input = String(input);
		if (/[^\0-\xFF]/.test(input)) {
			// Note: no need to special-case astral symbols here, as surrogates are
			// matched, and the input is supposed to only contain ASCII anyway.
			error(
				'The string to be encoded contains characters outside of the ' +
				'Latin1 range.'
			);
		}
		var padding = input.length % 3;
		var output = '';
		var position = -1;
		var a;
		var b;
		var c;
		var d;
		var buffer;
		// Make sure any padding is handled outside of the loop.
		var length = input.length - padding;

		while (++position < length) {
			// Read three bytes, i.e. 24 bits.
			a = input.charCodeAt(position) << 16;
			b = input.charCodeAt(++position) << 8;
			c = input.charCodeAt(++position);
			buffer = a + b + c;
			// Turn the 24 bits into four chunks of 6 bits each, and append the
			// matching character for each of them to the output.
			output += (
				TABLE.charAt(buffer >> 18 & 0x3F) +
				TABLE.charAt(buffer >> 12 & 0x3F) +
				TABLE.charAt(buffer >> 6 & 0x3F) +
				TABLE.charAt(buffer & 0x3F)
			);
		}

		if (padding == 2) {
			a = input.charCodeAt(position) << 8;
			b = input.charCodeAt(++position);
			buffer = a + b;
			output += (
				TABLE.charAt(buffer >> 10) +
				TABLE.charAt((buffer >> 4) & 0x3F) +
				TABLE.charAt((buffer << 2) & 0x3F) +
				'='
			);
		} else if (padding == 1) {
			buffer = input.charCodeAt(position);
			output += (
				TABLE.charAt(buffer >> 2) +
				TABLE.charAt((buffer << 4) & 0x3F) +
				'=='
			);
		}

		return output;
	};

	var base64 = {
		'encode': encode,
		'decode': decode,
		'version': '0.1.0'
	};

	// Some AMD build optimizers, like r.js, check for specific condition patterns
	// like the following:
	if (
		typeof define == 'function' &&
		typeof define.amd == 'object' &&
		define.amd
	) {
		define(function() {
			return base64;
		});
	}	else if (freeExports && !freeExports.nodeType) {
		if (freeModule) { // in Node.js or RingoJS v0.8.0+
			freeModule.exports = base64;
		} else { // in Narwhal or RingoJS v0.7.0-
			for (var key in base64) {
				base64.hasOwnProperty(key) && (freeExports[key] = base64[key]);
			}
		}
	} else { // in Rhino or a web browser
		root.base64 = base64;
	}

}(this));

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.capitalize = exports.formattedValue = exports.floatDivAndMod = exports.isGreaterThan = exports.isLowerThan = exports.isEqual = exports.isNumeric = exports.round = exports.BoolIdentity = exports.XOR = exports.Debug = void 0;
class Debug {
    /**
     * @param _DBG Flag de débuggage
     */
    constructor(_DBG) {
        this._DBG = _DBG;
    }
    /**
     * Affiche un message dans la console si le flag this.DBG est à true
     * @param s Message à afficher dans la console
     */
    debug(...args) {
        // tslint:disable-next-line:no-console
        if (this._DBG) {
            // tslint:disable-next-line:no-console
            console.log(...args);
        }
    }
    get DBG() {
        return this._DBG;
    }
    set DBG(d) {
        this._DBG = d;
    }
}
exports.Debug = Debug;
/**
 * Méthode simulant l'opérateur booléen xor
 * @see http://www.howtocreate.co.uk/xor.html
 */
function XOR(a, b) {
    return (a || b) && !(a && b);
}
exports.XOR = XOR;
/**
 * Méthode simulant l'opérateur booléen identité (vrai si a=b)
 */
function BoolIdentity(a, b) {
    return (a && b) || (!a && !b);
}
exports.BoolIdentity = BoolIdentity;
/**
 * arrondi d'un nombre avec une précision donnée
 * @param val nombre à arrondir
 * @param prec nombre de chiffres
 */
function round(val, prec) {
    const m = Math.pow(10, prec || 0);
    return Math.round(val * m) / m;
}
exports.round = round;
function isNumeric(s) {
    if (s !== undefined && s !== null) {
        if (typeof s === "string") {
            const trimmed = s.trim();
            return trimmed !== "" && !isNaN(Number(trimmed));
        }
        else if (typeof s === "number") {
            return !isNaN(s);
        }
    }
    return false;
}
exports.isNumeric = isNumeric;
/**
 * Retourne true si a et b sont égaux à e près (en gros)
 */
function isEqual(a, b, e = 1E-7) {
    if (a === 0 && b === 0) {
        return true;
    }
    if (b === 0) { // ne pas diviser par 0
        [a, b] = [b, a];
    }
    if (a === 0) { // éviter d'avoir un quotient toujours nul
        return (Math.abs(b) < e);
    }
    // cas général
    return (Math.abs((a / b) - 1) < e);
}
exports.isEqual = isEqual;
/** Retourne true si a < b à e près */
function isLowerThan(a, b, e = 1E-7) {
    return (b - a) > e; // enfin (a + e) < b quoi
}
exports.isLowerThan = isLowerThan;
/** Retourne true si a > b à e près */
function isGreaterThan(a, b, e = 1E-7) {
    return (a - b) > e;
}
exports.isGreaterThan = isGreaterThan;
/**
 * Division entière de a par b, plus modulo, avec une tolérance e
 * pour gérer le cas des nombres réels
 */
function floatDivAndMod(a, b, e = 1E-7) {
    let q = Math.floor(a / b);
    let r = a % b;
    // si le modulo est bon à un pouïème, pas de reste
    if (isEqual(r, b, e)) {
        r = 0;
        // si la division entière n'est pas tombée juste, +1 au quotient
        if (q !== (a / b)) {
            q++;
        }
    }
    // si le modulo est nul à un pouïème, il est nul
    if (isEqual(r, 0, e)) {
        r = 0;
    }
    return { q, r };
}
exports.floatDivAndMod = floatDivAndMod;
/**
 * Formats (rounds) the given number (or the value of the given parameter) with the
 * number of decimals specified in app preferences; if given number is too low and
 * more decimals would be needed, keep as much significative digits as the number of
 * decimals specified in app preferences, except potential trailing zeroes;
 * returns "-" to point out an error if value is not a number or is undefined
 *
 * ex. with 3 decimals:
 * 1             => 1.000
 * 65431         => 65431.000
 * 0.002         => 0.002
 * 0.00004521684 => 0.0000452
 * 0.000001      => 0.000001
 * 5.00004521684 => 5.000
 * 5.000001      => 5.000
 */
function formattedValue(value, nDigits) {
    // bulletproof
    nDigits = Math.max(nDigits, 1);
    nDigits = Math.min(nDigits, 100);
    if (typeof value !== "number" || value === undefined) {
        return "-";
    }
    const minRenderableNumber = Number("1E-" + nDigits);
    // if required precision is too low, avoid rendering only zeroes
    if (value < minRenderableNumber) {
        return String(Number(value.toPrecision(nDigits))); // double casting avoids trailing zeroes
    }
    else {
        return value.toFixed(nDigits);
    }
}
exports.formattedValue = formattedValue;
function capitalize(s) {
    return s.charAt(0).toUpperCase() + s.slice(1);
}
exports.capitalize = capitalize;

},{}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChildNub = void 0;
const nub_1 = require("./nub");
/**
 * A Nub that is meant to exist within a parent only
 */
class ChildNub extends nub_1.Nub {
    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Structure ones
     */
    unsetCalculatedParam(except) {
        if (this.parent) {
            this.parent.unsetCalculatedParam(except);
        }
    }
    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Structure ones
     */
    get calculatedParam() {
        if (this.parent) {
            return this.parent.calculatedParam;
        }
        // For testing purpose without ParallelStructure
        return this._calculatedParam;
    }
    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Structure ones
     */
    set calculatedParam(p) {
        if (this.parent) {
            this.parent.calculatedParam = p;
        }
        else {
            // For testing purpose without ParallelStructure
            this._calculatedParam = p;
        }
    }
    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Structure ones
     */
    findFirstCalculableParameter(otherThan) {
        if (this.parent) {
            return this.parent.findFirstCalculableParameter(otherThan);
        }
        return undefined;
    }
    /**
     * Forwards to parent, that has visibility over
     * all the parameters, including the Structure ones
     */
    resetDefaultCalculatedParam(requirer) {
        if (this.parent) {
            this.parent.resetDefaultCalculatedParam(requirer);
        }
    }
}
exports.ChildNub = ChildNub;

},{"./nub":33}],5:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ComputeNode = exports.SectionType = exports.CalculatorType = void 0;
const base_1 = require("./base");
const jalhyd_object_1 = require("./jalhyd_object");
/**
 * type de calculette
 */
var CalculatorType;
(function (CalculatorType) {
    CalculatorType[CalculatorType["ConduiteDistributrice"] = 0] = "ConduiteDistributrice";
    CalculatorType[CalculatorType["LechaptCalmon"] = 1] = "LechaptCalmon";
    CalculatorType[CalculatorType["SectionParametree"] = 2] = "SectionParametree";
    CalculatorType[CalculatorType["RegimeUniforme"] = 3] = "RegimeUniforme";
    CalculatorType[CalculatorType["CourbeRemous"] = 4] = "CourbeRemous";
    CalculatorType[CalculatorType["PabDimensions"] = 5] = "PabDimensions";
    CalculatorType[CalculatorType["PabPuissance"] = 6] = "PabPuissance";
    CalculatorType[CalculatorType["Structure"] = 7] = "Structure";
    CalculatorType[CalculatorType["ParallelStructure"] = 8] = "ParallelStructure";
    CalculatorType[CalculatorType["Dever"] = 9] = "Dever";
    CalculatorType[CalculatorType["Cloisons"] = 10] = "Cloisons";
    CalculatorType[CalculatorType["MacroRugo"] = 11] = "MacroRugo";
    CalculatorType[CalculatorType["PabChute"] = 12] = "PabChute";
    CalculatorType[CalculatorType["PabNombre"] = 13] = "PabNombre";
    CalculatorType[CalculatorType["Section"] = 14] = "Section";
    CalculatorType[CalculatorType["Pab"] = 15] = "Pab";
    CalculatorType[CalculatorType["CloisonAval"] = 16] = "CloisonAval";
    CalculatorType[CalculatorType["MacroRugoCompound"] = 17] = "MacroRugoCompound";
    CalculatorType[CalculatorType["Jet"] = 18] = "Jet";
    CalculatorType[CalculatorType["Grille"] = 19] = "Grille";
    CalculatorType[CalculatorType["Pente"] = 20] = "Pente";
    CalculatorType[CalculatorType["Bief"] = 21] = "Bief";
    CalculatorType[CalculatorType["Solveur"] = 22] = "Solveur";
    CalculatorType[CalculatorType["YAXB"] = 23] = "YAXB";
    CalculatorType[CalculatorType["Trigo"] = 24] = "Trigo";
    CalculatorType[CalculatorType["SPP"] = 25] = "SPP";
    CalculatorType[CalculatorType["YAXN"] = 26] = "YAXN";
    CalculatorType[CalculatorType["ConcentrationBlocs"] = 27] = "ConcentrationBlocs";
    CalculatorType[CalculatorType["Par"] = 28] = "Par";
    CalculatorType[CalculatorType["ParSimulation"] = 29] = "ParSimulation";
    CalculatorType[CalculatorType["PreBarrage"] = 30] = "PreBarrage";
    CalculatorType[CalculatorType["PbCloison"] = 31] = "PbCloison";
    CalculatorType[CalculatorType["PbBassin"] = 32] = "PbBassin";
    CalculatorType[CalculatorType["Espece"] = 33] = "Espece";
    CalculatorType[CalculatorType["Verificateur"] = 34] = "Verificateur"; // Vérificateur de contraintes sur une passe pour une ou plusieurs espèces
})(CalculatorType = exports.CalculatorType || (exports.CalculatorType = {}));
/** types de sections */
var SectionType;
(function (SectionType) {
    SectionType[SectionType["SectionCercle"] = 0] = "SectionCercle";
    SectionType[SectionType["SectionRectangle"] = 1] = "SectionRectangle";
    SectionType[SectionType["SectionTrapeze"] = 2] = "SectionTrapeze";
    SectionType[SectionType["SectionPuissance"] = 3] = "SectionPuissance";
})(SectionType = exports.SectionType || (exports.SectionType = {}));
/**
 * noeud de calcul
 */
class ComputeNode extends jalhyd_object_1.JalhydObject {
    constructor(prms, dbg = false) {
        super();
        this._debug = new base_1.Debug(dbg);
        this._prms = prms;
        this.debug("PARAMS", prms);
        // important for Param uid
        this._prms.parent = this;
        this.setParametersCalculability();
        this.setResultsUnits();
        this.exposeResults();
    }
    get prms() {
        return this._prms;
    }
    /**
     * Retrieves a parameter from its symbol (unique in a given Nub)
     * @WARNING also retrieves **extra results** and returns them as ParamDefinition !
     */
    getParameter(name) {
        for (const p of this.parameterIterator) {
            if (p.symbol === name) {
                return p;
            }
        }
        return undefined;
    }
    getFirstAnalyticalParameter() {
        for (const p of this.parameterIterator) {
            if (p.isAnalytical()) {
                return p;
            }
        }
        return undefined;
    }
    /**
     * Returns an iterator over own parameters (this._prms)
     */
    get parameterIterator() {
        return this._prms.iterator;
    }
    // interface IDebug
    debug(...args) {
        this._debug.debug(...args);
    }
    get DBG() {
        return this._debug.DBG;
    }
    set DBG(d) {
        this._debug.DBG = d;
    }
    /**
     * Define ParamFamily (@see ParamDefinition) for extra results; also
     * used to declare extra results that are not linkable but may be
     * used by Solveur : set family to undefined
     */
    exposeResults() {
        this._resultsFamilies = {};
    }
    get resultsFamilies() {
        return this._resultsFamilies;
    }
    setResultsUnits() {
        this._resultsUnits = {};
    }
    get resultsUnits() {
        return this._resultsUnits;
    }
}
exports.ComputeNode = ComputeNode;

},{"./base":3,"./jalhyd_object":15}],6:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
/**
 * JaLHyd configuration file
 */
exports.config = {
    serialisation: {
        fileFormatVersion: "1.3"
    }
};

},{}],7:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.jalhydVersion = exports.jalhydDateRev = void 0;
exports.jalhydDateRev = "2021-02-17";
exports.jalhydVersion = "stable";

},{}],8:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Grille = exports.GrilleType = exports.GrilleProfile = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const interval_1 = require("../util/interval");
const message_1 = require("../util/message");
var GrilleProfile;
(function (GrilleProfile) {
    GrilleProfile[GrilleProfile["Rectangular"] = 0] = "Rectangular";
    GrilleProfile[GrilleProfile["Hydrodynamic"] = 1] = "Hydrodynamic";
    GrilleProfile[GrilleProfile["Custom"] = 2] = "Custom";
})(GrilleProfile = exports.GrilleProfile || (exports.GrilleProfile = {}));
var GrilleType;
(function (GrilleType) {
    GrilleType[GrilleType["Conventional"] = 0] = "Conventional";
    GrilleType[GrilleType["Oriented"] = 1] = "Oriented";
    GrilleType[GrilleType["Inclined"] = 2] = "Inclined";
})(GrilleType = exports.GrilleType || (exports.GrilleType = {}));
class Grille extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Grille;
        this._props.addObserver(this);
        this.type = GrilleType.Inclined;
        this.profile = GrilleProfile.Rectangular;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    get gridType() {
        return this.properties.getPropValue("gridType");
    }
    get gridProfile() {
        return this.properties.getPropValue("gridProfile");
    }
    set type(type) {
        this.properties.setPropValue("gridType", type);
    }
    set profile(profile) {
        this.properties.setPropValue("gridProfile", profile);
    }
    /** Coefficient de forme des barreaux a */
    get a() {
        let v;
        const p = this.gridProfile;
        const t = this.gridType;
        if (p === GrilleProfile.Custom) {
            v = this.prms.a.v;
        }
        else {
            if (t === GrilleType.Inclined) {
                if (p === GrilleProfile.Rectangular) {
                    v = 3.85;
                }
                else if (p === GrilleProfile.Hydrodynamic) {
                    v = 2.10;
                }
            }
            else {
                if (p === GrilleProfile.Rectangular) {
                    v = 2.89;
                }
                else if (p === GrilleProfile.Hydrodynamic) {
                    v = 1.70;
                }
            }
        }
        return v;
    }
    /** Coefficient de forme des barreaux c */
    get c() {
        let v;
        const p = this.gridProfile;
        const t = this.gridType;
        if (t === GrilleType.Oriented) {
            if (p === GrilleProfile.Rectangular) {
                v = 1.69;
            }
            else if (p === GrilleProfile.Hydrodynamic) {
                v = 2.78;
            }
            else if (p === GrilleProfile.Custom) {
                v = this.prms.c.v;
            }
        }
        else {
            throw new Error("Grille.c() : only oriented grids have a c coefficient");
        }
        return v;
    }
    Calc() {
        this.currentResult = this.Equation();
        return this.result;
    }
    Equation() {
        const r = this._result;
        const t = this.gridType;
        // Préconisations d'inclinaison
        if (this.gridPlanParamsOK) {
            if (t === GrilleType.Oriented) {
                if (this.prms.Alpha.v > 45) {
                    r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_GRILLE_ALPHA_GREATER_THAN_45));
                }
            }
            if (t === GrilleType.Inclined) {
                if (this.prms.Beta.v > 26) {
                    r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_GRILLE_BETA_GREATER_THAN_26));
                }
            }
        }
        // 1st calculation step
        // Hauteur d'eau
        const H = this.prms.CEau.v - this.prms.CRad.v;
        r.resultElement.values.H = H;
        // Hauteur de grille
        const HG = this.prms.CSomGrille.v - this.prms.CRad.v;
        r.resultElement.values.HG = HG;
        // Section d'approche de la prise d'eau
        const S = H * this.prms.B.v;
        r.resultElement.values.S = S;
        // Section d'approche du plan de grille
        const SPDG = HG * this.prms.B.v;
        r.resultElement.values.SPDG = SPDG;
        // Vitesse d'approche moyenne pour le débit maximum turbiné
        const VA = this.prms.QMax.v / S;
        r.resultElement.values.VA = VA;
        // Vitesse d'approche moyenne pour le débit maximum turbiné, en soustr. la partie sup. évent. obturée
        const VAPDG = this.prms.QMax.v / SPDG;
        r.resultElement.values.VAPDG = VAPDG;
        // 2nd calculation step
        if (this.gridPlanParamsOK) {
            let VN;
            if (t === GrilleType.Conventional || t === GrilleType.Inclined) {
                // Longueur de grille immergée
                const LG = HG / Math.sin(this.prms.Beta.v * Math.PI / 180);
                r.resultElement.values.LG = LG;
                // Distance longitudinale entre le point émergent du plan de grille et le pied de grille
                const D = H / Math.tan(this.prms.Beta.v * Math.PI / 180);
                r.resultElement.values.D = D;
                // Distance longitudinale entre le sommet immergé et le pied de grille
                const DG = HG / Math.tan(this.prms.Beta.v * Math.PI / 180);
                r.resultElement.values.DG = DG;
                // Surface de grille immergée
                const SG = LG * this.prms.B.v;
                r.resultElement.values.SG = SG;
                // Vitesse normale moyenne pour le débit maximum turbiné
                VN = this.prms.QMax.v / SG;
                r.resultElement.values.VN = VN;
            }
            if (t === GrilleType.Oriented) {
                // Largeur du plan de grille
                const BG = this.prms.B.v / Math.sin(this.prms.Alpha.v * Math.PI / 180);
                // ... BG is not exposed as a result !
                // Surface de grille immergée
                const SG = BG * HG;
                r.resultElement.values.SG = SG;
                // Vitesse normale moyenne pour le débit maximum turbiné
                VN = this.prms.QMax.v / SG;
                r.resultElement.values.VN = VN;
            }
            // Préconisation pour éviter le placage des poissons sur le plan de grille
            if (VN > 0.5) {
                r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_GRILLE_VN_GREATER_THAN_05));
            }
        }
        // 3rd calculation step
        if (this.gridPlanParamsOK && this.gridParamsOK) {
            // Rapport de forme des barreaux
            const RFB = this.prms.b.v / this.prms.p.v;
            r.resultElement.values.RFB = RFB;
            // Rapport espacement/épaisseur des barreaux
            const REEB = this.prms.e.v / this.prms.b.v;
            r.resultElement.values.REEB = REEB;
            // Obstruction due aux barreaux seuls
            const OB = this.prms.b.v / (this.prms.b.v + this.prms.e.v);
            r.resultElement.values.OB = OB;
            // L'obstruction totale est-elle au moins égale à l'obstruction due aux barreaux ?
            if (this.prms.O.v < r.resultElement.values.OB) {
                r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_GRILLE_O_LOWER_THAN_OB));
            }
            // perte de charge
            this.calcDH(r, VAPDG);
        }
        return r;
    }
    // interface Observer
    update(sender, data) {
        if (data.action === "propertyChange") {
            switch (data.name) {
                case "gridType":
                    this.updateBarsCoefficientsDisplay();
                    if (data.value === GrilleType.Conventional) {
                        // adjust hidden params
                        this.prms.Beta.visible = true;
                        this.prms.Alpha.visible = false;
                        this.prms.O.visible = true;
                        this.prms.Ob.visible = false;
                        this.prms.OEntH.visible = false;
                        this.prms.cIncl.visible = false;
                        // set recommended values
                        this.prms.Alpha.singleValue = 90;
                        // adjust definition domains
                        this.prms.Beta.domain.interval.setInterval(new interval_1.Interval(45, 90));
                        this.prms.Alpha.domain.interval.setInterval(new interval_1.Interval(0, 90));
                        this.prms.O.domain.interval.setInterval(new interval_1.Interval(0.2, 0.6));
                    }
                    if (data.value === GrilleType.Oriented) {
                        // adjust hidden params
                        this.prms.Beta.visible = false;
                        this.prms.Alpha.visible = true;
                        this.prms.O.visible = true;
                        this.prms.Ob.visible = false;
                        this.prms.OEntH.visible = false;
                        this.prms.cIncl.visible = false;
                        // set recommended values
                        this.prms.Beta.singleValue = 90;
                        // adjust definition domains
                        this.prms.Beta.domain.interval.setInterval(new interval_1.Interval(0, 90));
                        this.prms.Alpha.domain.interval.setInterval(new interval_1.Interval(30, 90));
                        this.prms.O.domain.interval.setInterval(new interval_1.Interval(0.35, 0.6));
                    }
                    if (data.value === GrilleType.Inclined) {
                        // adjust hidden params
                        this.prms.Beta.visible = true;
                        this.prms.Alpha.visible = false;
                        this.prms.O.visible = false;
                        this.prms.Ob.visible = true;
                        this.prms.OEntH.visible = true;
                        this.prms.cIncl.visible = true;
                        // set recommended values
                        this.prms.Alpha.singleValue = 90;
                        // adjust definition domains
                        this.prms.Beta.domain.interval.setInterval(new interval_1.Interval(15, 90));
                        this.prms.Alpha.domain.interval.setInterval(new interval_1.Interval(0, 90));
                    }
                    break;
                case "gridProfile":
                    this.updateBarsCoefficientsDisplay();
                    break;
            }
        }
    }
    updateBarsCoefficientsDisplay() {
        this.prms.a.visible = (this.gridProfile === GrilleProfile.Custom);
        this.prms.c.visible = (this.gridProfile === GrilleProfile.Custom && this.gridType === GrilleType.Oriented);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.QMax.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.CRad.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.CEau.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.CSomGrille.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.B.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Beta.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Alpha.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.b.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.p.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.e.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.a.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.c.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.O.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Ob.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.OEntH.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.cIncl.calculability = param_definition_1.ParamCalculability.FIXED;
    }
    setResultsUnits() {
        this._resultsUnits = {
            H: "m",
            HG: "m",
            S: "m²",
            SPDG: "m²",
            VA: "m/s",
            VAPDG: "m/s",
            LG: "m",
            D: "m",
            DG: "m",
            SG: "m²",
            VN: "m/s",
            DH00: "cm",
            DH05: "cm",
            DH10: "cm",
            DH15: "cm",
            DH20: "cm",
            DH25: "cm",
            DH30: "cm",
            DH35: "cm",
            DH40: "cm",
            DH45: "cm",
            DH50: "cm",
            DH55: "cm",
            DH60: "cm"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            H: undefined,
            HG: undefined,
            S: undefined,
            SPDG: undefined,
            VA: undefined,
            VAPDG: undefined,
            LG: undefined,
            D: undefined,
            DG: undefined,
            VN: undefined,
            SG: undefined,
            RFB: undefined,
            REEB: undefined,
            OB: undefined,
            a: undefined,
            c: undefined
        };
    }
    // no calculated param
    findCalculatedParameter() {
        return undefined;
    }
    // no calculated param
    doCalc(computedSymbol, rInit) {
        return this.Calc();
    }
    /**
     * Returns true if every "grid plan" parameter (2nd fieldset in NgHyd)
     * has a defined value; allows to perform 2nd calculation step
     */
    get gridPlanParamsOK() {
        if (this.gridType === GrilleType.Oriented) {
            return this.prms.Alpha.singleValue !== undefined;
        }
        else {
            return this.prms.Beta.singleValue !== undefined;
        }
    }
    /**
     * Returns true if every "grid" parameter (3rd fieldset in NgHyd)
     * has a defined value; allows to perform 3rd calculation step
     */
    get gridParamsOK() {
        let optionalFieldsOk = true;
        if (this.gridProfile === GrilleProfile.Custom) {
            optionalFieldsOk = optionalFieldsOk && (this.prms.a.singleValue !== undefined);
            if (this.gridType === GrilleType.Oriented) {
                optionalFieldsOk = optionalFieldsOk && (this.prms.c.singleValue !== undefined);
            }
        }
        if (this.gridType === GrilleType.Inclined) {
            optionalFieldsOk = (optionalFieldsOk
                && (this.prms.Ob.singleValue !== undefined)
                && (this.prms.OEntH.singleValue !== undefined)
                && (this.prms.cIncl.singleValue !== undefined));
        }
        else { // type "Conventional" and "Oriented"
            optionalFieldsOk = (optionalFieldsOk
                && (this.prms.O.singleValue !== undefined));
        }
        return (optionalFieldsOk
            // mandatory fields
            && this.prms.b.singleValue !== undefined
            && this.prms.p.singleValue !== undefined
            && this.prms.e.singleValue !== undefined);
    }
    /**
     * Perte de charge pour un taux de colmatage allant de 0 à 60% par pas de 5%.
     * @param r Result to complete with calculated DH values
     */
    calcDH(r, VAPDG) {
        const t = this.gridType;
        const g = 9.81;
        // add automatic bars profile coeficients to results ?
        if (this.gridProfile !== GrilleProfile.Custom) {
            r.resultElement.values.a = this.a;
            if (this.gridType === GrilleType.Oriented) {
                r.resultElement.values.c = this.c;
            }
        }
        for (const pct of [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]) {
            let KO;
            let XI;
            const VCSDG = (VAPDG * VAPDG) / (2 * g);
            switch (t) {
                case GrilleType.Conventional:
                    if (pct === 0) {
                        KO = Math.pow(this.prms.O.v / (1 - this.prms.O.v), 1.6);
                    }
                    else {
                        // Obstruction totale
                        const OC = this.prms.O.v + ((1 - this.prms.O.v) * (pct / 100));
                        KO = Math.pow(OC / (1 - OC), 1.6);
                    }
                    const KB = Math.pow(1 - Math.cos(this.prms.Beta.v * Math.PI / 180), 0.39);
                    XI = this.a * KO * KB;
                    break;
                case GrilleType.Oriented:
                    let KA;
                    if (pct === 0) {
                        KO = Math.pow(this.prms.O.v / (1 - this.prms.O.v), 1.6);
                        KA = 1 + (this.c * Math.pow((90 - this.prms.Alpha.v) / 90, 2.35)
                            * Math.pow((1 - this.prms.O.v) / this.prms.O.v, 3));
                    }
                    else {
                        // Obstruction totale
                        const OC = this.prms.O.v + ((1 - this.prms.O.v) * (pct / 100));
                        KO = Math.pow(OC / (1 - OC), 1.6);
                        KA = 1 + (this.c * Math.pow((90 - this.prms.Alpha.v) / 90, 2.35)
                            * Math.pow((1 - OC) / OC, 3));
                    }
                    XI = this.a * KO * KA;
                    break;
                case GrilleType.Inclined:
                    let KOB;
                    const SINBC = Math.pow(Math.sin(this.prms.Beta.v * Math.PI / 180), 2);
                    const KOENTH = Math.pow(this.prms.OEntH.v / (1 - this.prms.OEntH.v), 0.77);
                    if (pct === 0) {
                        KOB = Math.pow(this.prms.Ob.v / (1 - this.prms.Ob.v), 1.65);
                    }
                    else {
                        // Obstruction totale
                        const OC = this.prms.Ob.v + ((1 - this.prms.Ob.v) * (pct / 100));
                        KOB = Math.pow(OC / (1 - OC), 1.65);
                    }
                    XI = this.a * KOB * SINBC + this.prms.cIncl.v * KOENTH;
                    break;
            }
            const DH = 100 * XI * VCSDG;
            // store as multiple result values
            let valueKey = String(pct);
            if (valueKey.length === 1) { // clodo padStart() for ES < 2017
                valueKey = "0" + valueKey;
            }
            valueKey = "DH" + valueKey;
            r.resultElement.values[valueKey] = DH;
        }
    }
}
exports.Grille = Grille;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/interval":148,"../util/message":151}],9:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GrilleParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class GrilleParams extends params_equation_1.ParamsEquation {
    constructor(rQMax, rCRad, rCEau, rCSomGrille, rB, rBeta, rAlpha, rb, rp, re, ra, rc, rO, rOb, rOEntH, rcIncl) {
        super();
        this._QMax = new param_definition_1.ParamDefinition(this, "QMax", param_domain_1.ParamDomainValue.POS_NULL, "m³/s", rQMax, param_definition_1.ParamFamily.FLOWS);
        this._CRad = new param_definition_1.ParamDefinition(this, "CRad", param_domain_1.ParamDomainValue.ANY, "m", rCRad, param_definition_1.ParamFamily.ELEVATIONS);
        this._CEau = new param_definition_1.ParamDefinition(this, "CEau", param_domain_1.ParamDomainValue.ANY, "m", rCEau, param_definition_1.ParamFamily.ELEVATIONS);
        this._CSomGrille = new param_definition_1.ParamDefinition(this, "CSomGrille", param_domain_1.ParamDomainValue.ANY, "m", rCSomGrille, param_definition_1.ParamFamily.ELEVATIONS);
        this._B = new param_definition_1.ParamDefinition(this, "B", param_domain_1.ParamDomainValue.POS, "m", rB, param_definition_1.ParamFamily.WIDTHS);
        this._Beta = new param_definition_1.ParamDefinition(this, "Beta", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 15, 90), "°", rBeta);
        this._Alpha = new param_definition_1.ParamDefinition(this, "Alpha", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 30, 90), "°", rAlpha);
        this._b = new param_definition_1.ParamDefinition(this, "b", param_domain_1.ParamDomainValue.POS, "mm", rb);
        this._p = new param_definition_1.ParamDefinition(this, "p", param_domain_1.ParamDomainValue.POS, "mm", rp);
        this._e = new param_definition_1.ParamDefinition(this, "e", param_domain_1.ParamDomainValue.POS, "mm", re);
        this._a = new param_definition_1.ParamDefinition(this, "a", param_domain_1.ParamDomainValue.POS, "", ra, undefined, false);
        this._c = new param_definition_1.ParamDefinition(this, "c", param_domain_1.ParamDomainValue.POS, "", rc, undefined, false);
        this._O = new param_definition_1.ParamDefinition(this, "O", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 1), "", rO);
        this._Ob = new param_definition_1.ParamDefinition(this, "Ob", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0.28, 0.53), "", rOb, undefined, false);
        this._OEntH = new param_definition_1.ParamDefinition(this, "OEntH", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 0.28), "", rOEntH, undefined, false);
        this._cIncl = new param_definition_1.ParamDefinition(this, "cIncl", param_domain_1.ParamDomainValue.POS, "", rcIncl);
        this.addParamDefinition(this._QMax);
        this.addParamDefinition(this._CRad);
        this.addParamDefinition(this._CEau);
        this.addParamDefinition(this._CSomGrille);
        this.addParamDefinition(this._B);
        this.addParamDefinition(this._Beta);
        this.addParamDefinition(this._Alpha);
        this.addParamDefinition(this._b);
        this.addParamDefinition(this._p);
        this.addParamDefinition(this._e);
        this.addParamDefinition(this._a);
        this.addParamDefinition(this._c);
        this.addParamDefinition(this._O);
        this.addParamDefinition(this._Ob);
        this.addParamDefinition(this._OEntH);
        this.addParamDefinition(this._cIncl);
    }
    get QMax() {
        return this._QMax;
    }
    get CRad() {
        return this._CRad;
    }
    get CEau() {
        return this._CEau;
    }
    get CSomGrille() {
        return this._CSomGrille;
    }
    get B() {
        return this._B;
    }
    get Beta() {
        return this._Beta;
    }
    get Alpha() {
        return this._Alpha;
    }
    get b() {
        return this._b;
    }
    get p() {
        return this._p;
    }
    get e() {
        return this._e;
    }
    get a() {
        return this._a;
    }
    get c() {
        return this._c;
    }
    get O() {
        return this._O;
    }
    get Ob() {
        return this._Ob;
    }
    get OEntH() {
        return this._OEntH;
    }
    get cIncl() {
        return this._cIncl;
    }
}
exports.GrilleParams = GrilleParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],10:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Jet = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const param_value_mode_1 = require("../param/param-value-mode");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
class Jet extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        /** steps for generating the trajectory */
        this.precision = 50;
        this._calcType = compute_node_1.CalculatorType.Jet;
        this._defaultCalculatedParam = prms.D;
        this.resetDefaultCalculatedParam();
    }
    /** paramètres castés au bon type */
    get prms() {
        return this._prms;
    }
    Calc(sVarCalc, rInit) {
        this.currentResult = super.Calc(sVarCalc, rInit);
        // omit extra results if calculation failed
        if (this.result.vCalc !== undefined) {
            // H: chute
            this.result.resultElement.values.H = this.prms.ZJ.v - this.prms.ZW.v;
            // Y: profondeur
            this.result.resultElement.values.Y = this.prms.ZW.v - this.prms.ZF.v;
            // YH: rapport profondeur/chute
            this.result.resultElement.values.YH = this.result.resultElement.values.Y / this.result.resultElement.values.H;
            // t: temps de vol
            this.result.resultElement.values.t = this.prms.D.V / Math.cos(this.alpha) / this.prms.V0.V;
            // Vx: vitesse horizontale à l'impact
            this.result.resultElement.values.Vx = this.prms.V0.V * Math.cos(this.alpha);
            // Vz: vitesse verticale à l'impact
            this.result.resultElement.values.Vz =
                this.prms.V0.V * Math.sin(this.alpha) - this.result.resultElement.values.t * 9.81;
            // Vt: vitesse à l'impact
            this.result.resultElement.values.Vt = Math.sqrt(Math.pow(this.result.resultElement.values.Vx, 2)
                + Math.pow(this.result.resultElement.values.Vz, 2));
        }
        let ZF = this.prms.ZF.v;
        let ZW = this.prms.ZW.v;
        let ZJ = this.prms.ZJ.v;
        if (this.calculatedParam === this.prms.ZF) {
            ZF = this.result.resultElement.vCalc;
        }
        if (this.calculatedParam === this.prms.ZW) {
            ZW = this.result.resultElement.vCalc;
        }
        if (this.calculatedParam === this.prms.ZJ) {
            ZJ = this.result.resultElement.vCalc;
        }
        // y a-t-il de l'eau au dessus du sol ?
        if (ZF > ZW) {
            this.result.resultElement.log.add(new message_1.Message(message_1.MessageCode.WARNING_JET_WATER_ELEVATION_UNDERGROUND));
        }
        // le jet est-il bien au dessus du sol ?
        if (ZF > ZJ) {
            this.result.resultElement.log.add(new message_1.Message(message_1.MessageCode.WARNING_JET_START_ELEVATION_UNDERGROUND));
        }
        // le jet est-il bien émergé ?
        if (ZW > ZJ) {
            this.result.resultElement.log.add(new message_1.Message(message_1.MessageCode.WARNING_JET_START_SUBMERGED));
        }
        return this.result;
    }
    Equation(sVarCalc) {
        const g = 9.81;
        let v;
        let h;
        switch (sVarCalc) {
            case ("ZJ"):
                h = this.CalcH();
                v = h + this.prms.ZW.v;
                break;
            case ("ZW"):
                h = this.CalcH();
                v = this.prms.ZJ.v - h;
                break;
            case ("D"):
                h = (this.prms.ZJ.v - this.prms.ZW.v);
                const sqrtArg = Math.pow(this.prms.V0.v * Math.sin(this.alpha), 2) + 2 * g * h;
                if (sqrtArg < 0) {
                    return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_JET_SUBMERGED_NO_SOLUTION), this);
                }
                v = this.prms.V0.v / g * Math.cos(this.alpha)
                    * (this.prms.V0.v * Math.sin(this.alpha)
                        + Math.sqrt(sqrtArg));
                break;
            default:
                throw new Error("Jet.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v);
    }
    /** clone casting */
    clone() {
        return super.clone();
    }
    /**
     * Returns an array of trajectories built from the current Nub state.
     * A trajectory is a list of coordinate pairs representing the fall height (y),
     * for each abscissa (x) between 0 and the impact abscissa (D).
     * A coordinate pair is a list of 2 numbers [ x, y ].
     * If no parameter is varying, result will contain only 1 element.
     * Trajectory calculation uses a copy of the current Nub to calculate ZW from D.
     */
    generateTrajectories() {
        const trajectories = [];
        // clone Nub so that ZW calculation will not impact current state
        const nub = this.clone();
        // is anything varying ?
        if (this.resultHasMultipleValues()) {
            const valuesLists = {};
            const length = this.variatingLength();
            // reset clone params to SINGLE mode
            nub.prms.V0.valueMode = param_value_mode_1.ParamValueMode.SINGLE;
            nub.prms.S.valueMode = param_value_mode_1.ParamValueMode.SINGLE;
            nub.prms.D.valueMode = param_value_mode_1.ParamValueMode.SINGLE;
            // H will be calculated
            // 1. find all extended values lists; ignore ZW (will be calculated) and D (will be reaffected)
            for (const symbol of ["S", "V0", "ZJ"]) {
                const p = this.getParameter(symbol);
                valuesLists[symbol] = [];
                if (this.calculatedParam.symbol === symbol) { // calculated
                    for (let i = 0; i < length; i++) {
                        valuesLists[symbol].push(this.result.resultElements[i].vCalc);
                    }
                }
                else if (p.hasMultipleValues) { // variating
                    const iter = p.getExtendedValuesIterator(length);
                    while (iter.hasNext) {
                        const nv = iter.next();
                        valuesLists[symbol].push(nv.value);
                    }
                }
                else { // single
                    for (let i = 0; i < length; i++) {
                        valuesLists[symbol].push(p.singleValue);
                    }
                }
            }
            // 2. build one series for each variating step
            for (let i = 0; i < length; i++) {
                // exclude iteration if calculation has failed
                if (this.result.resultElements[i].ok) {
                    // set clone params values; ignore ZW (will be calculated)
                    // and D (will be reaffected by getDAbscissae)
                    for (const symbol of ["S", "V0", "ZJ"]) {
                        const val = valuesLists[symbol][i];
                        nub.getParameter(symbol).v = val;
                    }
                    // compute series
                    trajectories.push(this.buildSeriesForIteration(nub, i));
                }
                else {
                    // mark failed calculation using empty list
                    trajectories.push([]);
                }
            }
        }
        else { // nothing is varying
            for (const symbol of ["S", "V0", "ZJ"]) {
                // init .v of clone
                nub.getParameter(symbol).v = nub.getParameter(symbol).singleValue;
            }
            trajectories.push(this.buildSeriesForIteration(nub, 0));
        }
        return trajectories;
    }
    CalcH() {
        const g = 9.81;
        return (0.5 * g * Math.pow(this.prms.D.v, 2)
            / (Math.pow(Math.cos(this.alpha), 2) * Math.pow(this.prms.V0.v, 2)) - Math.tan(this.alpha) * this.prms.D.v);
    }
    /**
     * Build a trajectory data series for a calculation iteration
     */
    buildSeriesForIteration(nub, i) {
        const traj = [];
        const xs = this.getDAbscissae(i);
        for (const x of xs) {
            // compute H for D = x
            nub.prms.D.v = x;
            // console.log("__computing H for x =", x, nub.prms.D.v);
            const h = nub.Calc("ZW");
            traj.push([x, h.vCalc]);
        }
        return traj;
    }
    /**
     * Returns a list of abscissae from 0 to D (number of steps is this.precision)
     * @param variatingIndex if D is variating, index of the D value to fetch
     */
    getDAbscissae(variatingIndex = 0) {
        const abs = [];
        // divide impact abscissa into steps
        let D;
        if (this.calculatedParam.symbol === "D") {
            D = this.result.resultElements[variatingIndex].vCalc;
        }
        else if (this.prms.D.hasMultipleValues) {
            const length = this.variatingLength();
            const valsD = [];
            const iter = this.prms.D.getExtendedValuesIterator(length);
            while (iter.hasNext) {
                const nv = iter.next();
                valsD.push(nv.value);
            }
            D = valsD[variatingIndex];
        }
        else {
            D = this.prms.D.V;
        }
        const step = D / this.precision;
        // zero-abscissa
        let x = 0;
        abs.push(x);
        // abscissae in ]0,D[
        for (let i = 0; i < this.precision - 1; i++) {
            x += step;
            abs.push(x);
        }
        // D-abscissa
        abs.push(D);
        return abs;
    }
    setParametersCalculability() {
        this.prms.V0.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.S.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.ZJ.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.ZW.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.ZF.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.D.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
    setResultsUnits() {
        this._resultsUnits = {
            H: "m",
            Y: "m",
            t: "s",
            Vx: "m/s",
            Vz: "m/s",
            Vt: "m/s"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            H: param_definition_1.ParamFamily.TOTALFALLS,
            Y: param_definition_1.ParamFamily.HEIGHTS,
            YH: undefined,
            t: undefined,
            Vx: undefined,
            Vz: undefined,
            Vt: undefined
        };
    }
    get alpha() {
        return Math.atan(this.prms.S.v);
    }
}
exports.Jet = Jet;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../param/param-value-mode":89,"../util/message":151,"../util/result":155}],11:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JetParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class JetParams extends params_equation_1.ParamsEquation {
    constructor(V0, S, ZJ, ZW, ZF, D) {
        super();
        this._V0 = new param_definition_1.ParamDefinition(this, "V0", param_domain_1.ParamDomainValue.POS, "m", V0, param_definition_1.ParamFamily.SPEEDS);
        this.addParamDefinition(this._V0);
        this._S = new param_definition_1.ParamDefinition(this, "S", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, -0.99, 0.99), "m/m", S, param_definition_1.ParamFamily.SLOPES);
        this.addParamDefinition(this._S);
        this._ZJ = new param_definition_1.ParamDefinition(this, "ZJ", param_domain_1.ParamDomainValue.ANY, "m", ZJ, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this._ZJ);
        this._ZW = new param_definition_1.ParamDefinition(this, "ZW", param_domain_1.ParamDomainValue.ANY, "m", ZW, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this._ZW);
        this._ZF = new param_definition_1.ParamDefinition(this, "ZF", param_domain_1.ParamDomainValue.ANY, "m", ZF, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this._ZF);
        this._D = new param_definition_1.ParamDefinition(this, "D", param_domain_1.ParamDomainValue.POS_NULL, "m", D, param_definition_1.ParamFamily.LENGTHS);
        this.addParamDefinition(this._D);
    }
    get V0() {
        return this._V0;
    }
    get S() {
        return this._S;
    }
    get ZJ() {
        return this._ZJ;
    }
    get ZW() {
        return this._ZW;
    }
    get ZF() {
        return this._ZF;
    }
    get D() {
        return this._D;
    }
}
exports.JetParams = JetParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],12:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dichotomie = void 0;
const base_1 = require("./base");
const param_domain_1 = require("./param/param-domain");
const session_settings_1 = require("./session_settings");
const interval_1 = require("./util/interval");
const message_1 = require("./util/message");
const result_1 = require("./util/result");
const search_interval_1 = require("./util/search_interval");
/**
 * calcul par dichotomie
 * principe : y=f(x1,x2,...), on connait une valeur de y,
 * on cherche par ex le x2 correspondant, les autres xi étant fixés.
 * la méthode Equation() calcule analytiquement y=f(xi)
 */
class Dichotomie extends base_1.Debug {
    /**
     * Construction de la classe.
     * @param nub Noeud de calcul contenant la méthode de calcul Equation
     * @param sVarCalc Nom de la variable à calculer
     */
    constructor(nub, sVarCalc, dbg = false, func) {
        super(dbg);
        this.nub = nub;
        this.sVarCalc = sVarCalc;
        /**
         * nombre d'étapes de recherche de l'intervalle de départ
         */
        this._startIntervalMaxSteps = 50;
        this._paramX = this.nub.getParameter(this.sVarCalc);
        if (func !== undefined) {
            this._func = func;
        }
        else {
            this._func = this.nub.EquationFirstAnalyticalParameter;
            this.analyticalSymbol = this.nub.firstAnalyticalPrmSymbol;
        }
    }
    set vX(vCalc) {
        this._paramX.v = vCalc;
    }
    /**
     * étapes de recherche de l'intervalle de départ
     */
    get startIntervalMaxSteps() {
        return this._startIntervalMaxSteps;
    }
    set startIntervalMaxSteps(n) {
        this._startIntervalMaxSteps = n;
    }
    CalculX(x) {
        this.vX = x;
        return this.Calcul();
    }
    /**
     * trouve la valeur x pour y=f(x) avec un y donné
     * @param rTarget valeur de y
     * @param rTol tolérance pour l'arrêt de la recherche
     * @param rInit valeur initiale approximative de x
     */
    Dichotomie(rTarget, rTol, rInit) {
        this._target = rTarget;
        // console.log("-----");
        // for (let x = 0; x <= 1; x += 0.1)
        //     console.log(this.CalculX(x).vCalc);
        // console.log("-----");
        // recherche de l'intervalle de départ
        this.debug("Valeur cible de " + this.analyticalSymbol + "=" + rTarget
            + ", valeur initiale de " + this.sVarCalc + "=" + rInit);
        try {
            const r = this.getStartInterval(rTarget, rInit);
            if (r.ok) {
                const inter = r.intSearch;
                // Recherche du zero par la méthode de Brent
                const s = this.uniroot(this.CalcZero, this, inter.min, inter.max, rTol);
                if (s.found) {
                    this.debug(`${this.analyticalSymbol}(${this.sVarCalc}=${s.value}) = ${rTarget}`);
                    return new result_1.Result(s.value);
                }
                else {
                    this.debug("Non convergence");
                    return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_DICHO_CONVERGE, {
                        lastApproximation: s.value
                    }));
                }
            }
            else {
                return new result_1.Result(r.res);
            }
        }
        catch (e) {
            // un appel à Calcul() a généré une erreur
            return this.nub.result;
        }
    }
    debug(s) {
        if (this.DBG) {
            super.debug("Dichotomie: " + s);
        }
    }
    /**
     * Calcul de l'équation analytique.
     * @note Wrapper vers this.nub.Equation pour simplifier le code.
     * On utilise la première variable du tableau des variables pouvant être calculée analytiquement
     * Il faudra s'assurer que cette première variable correspond à la méthode de calcul la plus rapide
     */
    Calcul() {
        // return this.nub.Equation(this.analyticalSymbol).vCalc;
        return this._func.call(this.nub);
    }
    /**
     * Détermine si une fonction est croissante ou décroissante.
     * @param x point auquel on calcul la dérivée
     * @param dom domaine de définition de la variable
     */
    isIncreasingFunction(x, dom) {
        let epsilon = 1e-8;
        for (let i = 0; i < 20; i++) {
            const bounds = new interval_1.Interval(x - epsilon, x + epsilon);
            bounds.setInterval(bounds.intersect(dom)); // au cas où l'on sorte du domaine de la variable de la fonction
            const y1 = this.CalculX(bounds.min);
            const y2 = this.CalculX(bounds.max);
            if (Math.abs(y2 - y1) > 1E-6)
                return y2 > y1;
            epsilon *= 10;
        }
        return true;
    }
    /**
     * recherche l'intervalle contenant la valeur cible
     * @param rTarget valeur cible
     * @param intSearch intervalle de départ
     * @param intMax intervalle maximum (domaine de définition de la variable)
     */
    searchTarget(rTarget, intSearch, intMax) {
        this.debug(`searchTarget debut: Target ${this.analyticalSymbol}=${rTarget} dans`
            + ` ${this.sVarCalc}=${intSearch.toString()}`);
        let n = 0;
        let ok = false;
        let bAllowRestart = false;
        do {
            intSearch.setInterval(intSearch.intersect(intMax));
            if (intSearch.length === 0) {
                this.debug(`searchTarget length= 0: ${this.sVarCalc}=${intSearch.toString()}`);
                break;
            }
            if (intSearch.hasTargetValue(rTarget)) {
                ok = true;
                break;
            }
            intSearch.growStep(2);
            intSearch.next();
            if (bAllowRestart) {
                intSearch.checkDirection();
            }
            else if (n > this._startIntervalMaxSteps / 2) {
                bAllowRestart = true;
                intSearch.reInit();
            }
        } while (n++ < this._startIntervalMaxSteps);
        this.debug(`searchTarget fin: Target ${this.analyticalSymbol}=${rTarget} dans`
            + ` ${this.sVarCalc}=${intSearch.toString()}`);
        return { ok, intSearch };
    }
    /**
     * Détermine l'intervalle de recherche initial
     * @param rTarget valeur cible de la fonction
     * @param rInit  valeur initiale de la variable
     */
    getStartInterval(rTarget, rInit) {
        const prmDom = this._paramX.domain;
        const min = prmDom.minValue;
        const max = prmDom.maxValue;
        if (prmDom.domain === param_domain_1.ParamDomainValue.NOT_NULL) {
            if (rInit === 0) {
                rInit = 1e-8;
            }
        }
        const intMax = new interval_1.Interval(min, max);
        try {
            intMax.checkValue(rInit);
        }
        catch (m) {
            return { ok: false, res: m };
        }
        // sens de variation de la fonction
        const inc = this.isIncreasingFunction(rInit, intMax);
        let step = 0.001;
        if (base_1.BoolIdentity(this.CalculX(rInit) > rTarget, inc)) {
            step = -step;
            // par ex, la fonction est croissante et la valeur initiale
            // de la variable a une image par la fonction > valeur cible
        }
        // initialisation de l'intervalle de recherche
        const intSearch1 = new search_interval_1.SearchInterval(this, rInit, step, this.DBG);
        // on cherche dans une première direction
        let a = this.searchTarget(rTarget, intSearch1, intMax);
        if (a.ok) {
            return a;
        }
        // il se peut que la fonction ne soit pas monotone et qu'il faille chercher dans l'autre direction
        const intSearch2 = new search_interval_1.SearchInterval(this, rInit, -step, this.DBG);
        a = this.searchTarget(rTarget, intSearch2, intMax);
        if (a.ok) {
            return a;
        }
        // gestion de l'erreur
        // la valeur cible de la fonction est elle trouvable ?
        let res;
        let errDomain = false;
        switch (prmDom.domain) {
            case param_domain_1.ParamDomainValue.INTERVAL:
                const si = new search_interval_1.SearchInterval(this, intMax.min, intMax.max - intMax.min);
                errDomain = !si.hasTargetValue(rTarget);
                break;
            case param_domain_1.ParamDomainValue.POS:
            case param_domain_1.ParamDomainValue.POS_NULL:
                const y = this.CalculX(1e-8);
                errDomain = inc && (rTarget < y);
                break;
            case param_domain_1.ParamDomainValue.ANY:
                break;
            default:
                throw new Error("unsupported parameter domain value");
        }
        if (errDomain) {
            res = new message_1.Message(message_1.MessageCode.ERROR_DICHO_INIT_DOMAIN);
            res.extraVar.targetSymbol = this.analyticalSymbol; // symbole de la variable calculée par la fonction
            res.extraVar.targetValue = rTarget; // valeur cible pour la fonction
            // intervalle de valeurs pour la variable d'entrée de la fonction
            res.extraVar.variableInterval = intMax.toString();
            res.extraVar.variableSymbol = this._paramX.symbol; // symbole de la variable d'entrée de la fonction
        }
        else {
            // Fusion des infos des deux intervalles explorés
            const intFus = []; // Signification des indices : 0,1 : minmax target; 2, 3 : variables associées
            if (isNaN(intSearch1.targets.max) || isNaN(intSearch2.targets.max)) {
                // bug targets en NaN en prod mais pas en debug pas à pas en explorant les variables
                intSearch1.updateTargets();
                intSearch2.updateTargets();
            }
            if (intSearch1.targets.max > intSearch2.targets.max) {
                intFus[1] = intSearch1.targets.max;
                intFus[3] = intSearch1.getVal(intSearch1.targets.maxIndex);
            }
            else {
                intFus[1] = intSearch2.targets.max;
                intFus[3] = intSearch2.getVal(intSearch2.targets.maxIndex);
            }
            if (intSearch1.targets.min < intSearch2.targets.min) {
                intFus[0] = intSearch1.targets.min;
                intFus[2] = intSearch1.getVal(intSearch1.targets.minIndex);
            }
            else {
                intFus[0] = intSearch2.targets.min;
                intFus[2] = intSearch2.getVal(intSearch2.targets.minIndex);
            }
            if (intFus[1] < rTarget) {
                res = new message_1.Message(message_1.MessageCode.ERROR_DICHO_TARGET_TOO_HIGH);
                res.extraVar.extremeTarget = intFus[1];
                res.extraVar.variableExtremeValue = intFus[3];
            }
            else {
                res = new message_1.Message(message_1.MessageCode.ERROR_DICHO_TARGET_TOO_LOW);
                res.extraVar.extremeTarget = intFus[0];
                res.extraVar.variableExtremeValue = intFus[2];
            }
            res.extraVar.variableSymbol = this._paramX.symbol; // symbole de la variable de la fonction
            res.extraVar.targetSymbol = this.analyticalSymbol; // symbole de la variable calculée par la fonction
            res.extraVar.targetValue = rTarget; // valeur cible pour la fonction
        }
        return { ok: false, res };
    }
    CalcZero(x) {
        this.vX = x;
        return this.Calcul() - this._target;
    }
    /**
     * Searches the interval from <tt>lowerLimit</tt> to <tt>upperLimit</tt>
     * for a root (i.e., zero) of the function <tt>func</tt> with respect to
     * its first argument using Brent's method root-finding algorithm.
     *
     * Translated from zeroin.c in http://www.netlib.org/c/brent.shar.
     *
     * Copyright (c) 2012 Borgar Thorsteinsson <borgar@borgar.net>
     * MIT License, http://www.opensource.org/licenses/mit-license.php
     *
     * @param {function} func function for which the root is sought.
     * @param {number} lowerlimit the lower point of the interval to be searched.
     * @param {number} upperlimit the upper point of the interval to be searched.
     * @param {number} errorTol the desired accuracy (convergence tolerance).
     * @param {number} maxIter the maximum number of iterations.
     * @returns an estimate for the root within accuracy.
     *
     */
    uniroot(func, thisArg, lowerLimit, upperLimit, errorTol = 0, maxIter) {
        let a = lowerLimit;
        let b = upperLimit;
        let c = a;
        let fa = func.call(thisArg, a);
        let fb = func.call(thisArg, b);
        let fc = fa;
        let tolAct; // Actual tolerance
        let newStep; // Step at this iteration
        let prevStep; // Distance from the last but one to the last approximation
        let p; // Interpolation step is calculated in the form p/q; division is delayed until the last moment
        let q;
        if (maxIter === undefined) {
            maxIter = session_settings_1.SessionSettings.maxIterations;
        }
        while (maxIter-- > 0) {
            prevStep = b - a;
            if (Math.abs(fc) < Math.abs(fb)) {
                // Swap data for b to be the best approximation
                a = b, b = c, c = a;
                fa = fb, fb = fc, fc = fa;
            }
            tolAct = 1e-15 * Math.abs(b) + errorTol / 2;
            newStep = (c - b) / 2;
            if (Math.abs(newStep) <= tolAct
                && (Math.abs(fb) < errorTol
                    || Math.abs(fb - fc) < errorTol * 1E5)) {
                // Acceptable approx. is found
                return {
                    found: true,
                    value: b
                };
            }
            // Decide if the interpolation can be tried
            if (Math.abs(prevStep) >= tolAct && Math.abs(fa) > Math.abs(fb)) {
                // If prev_step was large enough and was in true direction, Interpolatiom may be tried
                let t1;
                let cb;
                let t2;
                cb = c - b;
                if (a === c) { // If we have only two distinct points linear interpolation can only be applied
                    t1 = fb / fa;
                    p = cb * t1;
                    q = 1.0 - t1;
                }
                else { // Quadric inverse interpolation
                    q = fa / fc, t1 = fb / fc, t2 = fb / fa;
                    p = t2 * (cb * q * (q - t1) - (b - a) * (t1 - 1));
                    q = (q - 1) * (t1 - 1) * (t2 - 1);
                }
                if (p > 0) {
                    q = -q; // p was calculated with the opposite sign; make p positive
                }
                else {
                    p = -p; // and assign possible minus to q
                }
                if (p < (0.75 * cb * q - Math.abs(tolAct * q) / 2) &&
                    p < Math.abs(prevStep * q / 2)) {
                    // If (b + p / q) falls in [b,c] and isn't too large it is accepted
                    newStep = p / q;
                }
                // If p/q is too large then the bissection procedure can reduce [b,c] range to more extent
            }
            if (Math.abs(newStep) < tolAct) { // Adjust the step to be not less than tolerance
                newStep = (newStep > 0) ? tolAct : -tolAct;
            }
            a = b, fa = fb; // Save the previous approx.
            b += newStep, fb = func.call(thisArg, b); // Do step to a new approxim.
            if ((fb > 0 && fc > 0) || (fb < 0 && fc < 0)) {
                c = a, fc = fa; // Adjust c for it to have a sign opposite to that of b
            }
        }
        // No acceptable approximation was found
        return {
            found: false,
            value: b
        };
    }
}
exports.Dichotomie = Dichotomie;

},{"./base":3,"./param/param-domain":87,"./session_settings":105,"./util/interval":148,"./util/message":151,"./util/result":155,"./util/search_interval":157}],13:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FishPass = void 0;
const nub_1 = require("./nub");
/**
 * An intermediate class for all fish passes (Pab, Par, MacroRugo[Compound]…),
 * just for the sake of typing
 */
class FishPass extends nub_1.Nub {
}
exports.FishPass = FishPass;

},{"./nub":33}],14:[function(require,module,exports){
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(require("./base"), exports);
__exportStar(require("./config"), exports);
__exportStar(require("./param/param-definition"), exports);
__exportStar(require("./param/param-domain"), exports);
__exportStar(require("./param/params-equation"), exports);
__exportStar(require("./param/param-values"), exports);
__exportStar(require("./param/param-value-mode"), exports);
__exportStar(require("./param/param-value-iterator"), exports);
__exportStar(require("./compute-node"), exports);
__exportStar(require("./nub"), exports);
__exportStar(require("./session"), exports);
__exportStar(require("./session_settings"), exports);
__exportStar(require("./props"), exports);
__exportStar(require("./open-channel/methode-resolution"), exports);
__exportStar(require("./open-channel/regime_uniforme"), exports);
__exportStar(require("./open-channel/remous"), exports);
__exportStar(require("./open-channel/remous_params"), exports);
__exportStar(require("./open-channel/section/section_parametree"), exports);
__exportStar(require("./open-channel/section/section_type"), exports);
__exportStar(require("./open-channel/section/section_trapez"), exports);
__exportStar(require("./open-channel/section/section_trapez_params"), exports);
__exportStar(require("./open-channel/section/section_rectang"), exports);
__exportStar(require("./open-channel/section/section_rectang_params"), exports);
__exportStar(require("./open-channel/section/section_circulaire"), exports);
__exportStar(require("./open-channel/section/section_circulaire_params"), exports);
__exportStar(require("./open-channel/section/section_puissance"), exports);
__exportStar(require("./open-channel/section/section_puissance_params"), exports);
__exportStar(require("./util/message"), exports);
__exportStar(require("./util/log"), exports);
__exportStar(require("./util/result"), exports);
__exportStar(require("./util/resultelement"), exports);
__exportStar(require("./util/interval"), exports);
__exportStar(require("./util/observer"), exports);
__exportStar(require("./util/array_reverse_iterator"), exports);
__exportStar(require("./util/map_iterator"), exports);
__exportStar(require("./util/number_array_iterator"), exports);
__exportStar(require("./util/number_array_reverse_iterator"), exports);
__exportStar(require("./util/enum"), exports);
__exportStar(require("./pab/cloisons"), exports);
__exportStar(require("./structure/parallel_structure"), exports);
__exportStar(require("./structure/parallel_structure_params"), exports);
__exportStar(require("./structure/structure"), exports);
__exportStar(require("./structure/structure_params"), exports);
__exportStar(require("./structure/factory_structure"), exports);
__exportStar(require("./structure/structure_props"), exports);
__exportStar(require("./linked-value"), exports);
__exportStar(require("./jalhyd_object"), exports);
__exportStar(require("./date_revision"), exports);
__exportStar(require("./open-channel/section/section_nub"), exports);
__exportStar(require("./pab/pab"), exports);
__exportStar(require("./pab/pab_params"), exports);
__exportStar(require("./pab/pab_chute"), exports);
__exportStar(require("./pab/pab_chute_params"), exports);
__exportStar(require("./pab/pab_dimension"), exports);
__exportStar(require("./pab/pab_dimensions_params"), exports);
__exportStar(require("./pab/pab_nombre"), exports);
__exportStar(require("./pab/pab_nombre_params"), exports);
__exportStar(require("./pab/pab_puissance"), exports);
__exportStar(require("./pab/pab_puissance_params"), exports);
__exportStar(require("./pab/cloison_aval"), exports);
__exportStar(require("./pipe_flow/lechaptcalmon"), exports);
__exportStar(require("./lc-material"), exports);
__exportStar(require("./structure/dever"), exports);
__exportStar(require("./macrorugo/macrorugo"), exports);
__exportStar(require("./macrorugo/macrorugo_compound"), exports);
__exportStar(require("./macrorugo/mrc-inclination"), exports);
__exportStar(require("./devalaison/jet"), exports);
__exportStar(require("./devalaison/grille"), exports);
__exportStar(require("./open-channel/bief"), exports);
__exportStar(require("./open-channel/bief_params"), exports);
__exportStar(require("./solveur/solveur"), exports);
__exportStar(require("./solveur/solveur_params"), exports);
__exportStar(require("./math/yaxb"), exports);
__exportStar(require("./math/yaxb_params"), exports);
__exportStar(require("./math/trigo"), exports);
__exportStar(require("./math/trigo_params"), exports);
__exportStar(require("./math/spp"), exports);
__exportStar(require("./math/spp_params"), exports);
__exportStar(require("./math/yaxn"), exports);
__exportStar(require("./math/yaxn_params"), exports);
__exportStar(require("./child_nub"), exports);
__exportStar(require("./macrorugo/concentration_blocs"), exports);
__exportStar(require("./macrorugo/concentration_blocs_params"), exports);
__exportStar(require("./variated-details"), exports);
__exportStar(require("./par/par"), exports);
__exportStar(require("./par/par_params"), exports);
__exportStar(require("./par/par_simulation"), exports);
__exportStar(require("./par/par_simulation_params"), exports);
__exportStar(require("./verification/espece"), exports);
__exportStar(require("./verification/espece_params"), exports);
__exportStar(require("./verification/verificateur"), exports);
__exportStar(require("./verification/verificateur_params"), exports);
__exportStar(require("./verification/fish_species"), exports);
__exportStar(require("./verification/diving-jet-support"), exports);
__exportStar(require("./fish_pass"), exports);
__exportStar(require("./prebarrage/pre_barrage"), exports);
__exportStar(require("./prebarrage/pre_barrage_params"), exports);
__exportStar(require("./prebarrage/pb_cloison"), exports);
__exportStar(require("./prebarrage/pb_bassin"), exports);
__exportStar(require("./prebarrage/pb_bassin_params"), exports);

},{"./base":3,"./child_nub":4,"./compute-node":5,"./config":6,"./date_revision":7,"./devalaison/grille":8,"./devalaison/jet":10,"./fish_pass":13,"./jalhyd_object":15,"./lc-material":16,"./linked-value":17,"./macrorugo/concentration_blocs":18,"./macrorugo/concentration_blocs_params":19,"./macrorugo/macrorugo":20,"./macrorugo/macrorugo_compound":21,"./macrorugo/mrc-inclination":24,"./math/spp":25,"./math/spp_params":26,"./math/trigo":27,"./math/trigo_params":28,"./math/yaxb":29,"./math/yaxb_params":30,"./math/yaxn":31,"./math/yaxn_params":32,"./nub":33,"./open-channel/bief":34,"./open-channel/bief_params":35,"./open-channel/methode-resolution":36,"./open-channel/regime_uniforme":39,"./open-channel/remous":40,"./open-channel/remous_params":41,"./open-channel/section/section_circulaire":47,"./open-channel/section/section_circulaire_params":48,"./open-channel/section/section_nub":49,"./open-channel/section/section_parametree":50,"./open-channel/section/section_puissance":52,"./open-channel/section/section_puissance_params":53,"./open-channel/section/section_rectang":54,"./open-channel/section/section_rectang_params":55,"./open-channel/section/section_trapez":56,"./open-channel/section/section_trapez_params":57,"./open-channel/section/section_type":58,"./pab/cloison_aval":60,"./pab/cloisons":62,"./pab/pab":64,"./pab/pab_chute":65,"./pab/pab_chute_params":66,"./pab/pab_dimension":67,"./pab/pab_dimensions_params":68,"./pab/pab_nombre":69,"./pab/pab_nombre_params":70,"./pab/pab_params":71,"./pab/pab_puissance":72,"./pab/pab_puissance_params":73,"./par/par":74,"./par/par_params":75,"./par/par_simulation":76,"./par/par_simulation_params":77,"./param/param-definition":86,"./param/param-domain":87,"./param/param-value-iterator":88,"./param/param-value-mode":89,"./param/param-values":90,"./param/params-equation":92,"./pipe_flow/lechaptcalmon":96,"./prebarrage/pb_bassin":98,"./prebarrage/pb_bassin_params":99,"./prebarrage/pb_cloison":100,"./prebarrage/pre_barrage":101,"./prebarrage/pre_barrage_params":102,"./props":103,"./session":104,"./session_settings":105,"./solveur/solveur":106,"./solveur/solveur_params":107,"./structure/dever":108,"./structure/factory_structure":110,"./structure/parallel_structure":111,"./structure/parallel_structure_params":112,"./structure/structure":115,"./structure/structure_params":125,"./structure/structure_props":126,"./util/array_reverse_iterator":146,"./util/enum":147,"./util/interval":148,"./util/log":149,"./util/map_iterator":150,"./util/message":151,"./util/number_array_iterator":152,"./util/number_array_reverse_iterator":153,"./util/observer":154,"./util/result":155,"./util/resultelement":156,"./variated-details":158,"./verification/diving-jet-support":159,"./verification/espece":160,"./verification/espece_params":161,"./verification/fish_species":162,"./verification/verificateur":163,"./verification/verificateur_params":164}],15:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JalhydObject = void 0;
const base64 = require("base-64");
class JalhydObject {
    constructor() {
        this._uid = JalhydObject.nextUID;
    }
    get uid() {
        return this._uid;
    }
    static get nextUID() {
        return base64.encode(Math.random().toString(36).substring(2)).substring(0, 6);
    }
    /**
     * @WARNING utiliser uniquement pour conserver l'ID lorsqu'on charge des sessions
     */
    setUid(uid) {
        this._uid = uid;
    }
}
exports.JalhydObject = JalhydObject;

},{"base-64":2}],16:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LCMaterial = void 0;
/** materials for Lechapt-Calmon */
var LCMaterial;
(function (LCMaterial) {
    LCMaterial[LCMaterial["UnlinedCastIronCoarseConcrete"] = 0] = "UnlinedCastIronCoarseConcrete";
    LCMaterial[LCMaterial["CastSteelOrUncoatedCoarseConcrete"] = 1] = "CastSteelOrUncoatedCoarseConcrete";
    LCMaterial[LCMaterial["CastSteelOrCementCoating"] = 2] = "CastSteelOrCementCoating";
    LCMaterial[LCMaterial["CastIronOrSteelCoatingBitumen"] = 3] = "CastIronOrSteelCoatingBitumen";
    LCMaterial[LCMaterial["RolledSteelSmoothConcrete"] = 4] = "RolledSteelSmoothConcrete";
    LCMaterial[LCMaterial["CastIronOrSteelCoatingCentrifuged"] = 5] = "CastIronOrSteelCoatingCentrifuged";
    LCMaterial[LCMaterial["PVCPolyethylene"] = 6] = "PVCPolyethylene";
    LCMaterial[LCMaterial["HydraulicallySmoothPipe005D02"] = 7] = "HydraulicallySmoothPipe005D02";
    LCMaterial[LCMaterial["HydraulicallySmoothPipe025D1"] = 8] = "HydraulicallySmoothPipe025D1";
})(LCMaterial = exports.LCMaterial || (exports.LCMaterial = {}));

},{}],17:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LinkedValue = void 0;
const param_definition_1 = require("./param/param-definition");
const param_value_mode_1 = require("./param/param-value-mode");
const param_values_1 = require("./param/param-values");
class LinkedValue {
    constructor(nub, element, symbol, meta = {}) {
        this._nub = nub;
        this._element = element;
        this._symbol = symbol;
        this.meta = meta;
    }
    get nub() { return this._nub; }
    get element() { return this._element; }
    get symbol() { return this._symbol; }
    /**
     * Returns true if targetted value is a ParamDefinition (in CALC mode or not),
     * and not an extra result
     */
    isParameter() {
        return (this.element instanceof param_definition_1.ParamDefinition);
    }
    /**
     * Returns true if targetted value is a ParamDefinition in CALC mode
     * (might not have any value yet)
     */
    isResult() {
        return (this.isParameter()
            && this.element.valueMode === param_value_mode_1.ParamValueMode.CALCUL);
    }
    /**
     * Returns true if targetted value is a ParamDefinition in CALC mode,
     * or in LINK mode targetting a result (might not have any value yet)
     */
    isCalculated() {
        return (this.isResult()
            || this.isExtraResult()
            || (this.isParameter()
                && this.element.valueMode === param_value_mode_1.ParamValueMode.LINK
                && this.element.referencedValue.isCalculated() // recursion
            ));
    }
    /**
     * Returns true if targetted value is an extra result
     * (might not have any value yet)
     */
    isExtraResult() {
        return (this.nub !== undefined
            && this.symbol !== undefined
            && !this.isParameter());
    }
    /**
     * Returns true if v and the current objects have the same :
     *  - Nub UID
     *  - symbol
     *  - value type (Parameter / Result)
     * (ignores metadata)
     * @param v
     */
    equals(v) {
        return (v
            && (v.nub.uid === this.nub.uid)
            && (v.symbol === this.symbol)
            && ((v.element === undefined && this.element === undefined)
                || (v.element !== undefined
                    && this.element !== undefined
                    && v.element.constructor.name === this.element.constructor.name)));
    }
    /**
     * Returs the ParamValues for the linked Parameter if any, or a
     * fake ParamValues object if the targetted element is a Result
     * or extra result.
     * If target is a result and triggerChainComputation is true,
     * triggers a chain computation to obtain the result
     */
    getParamValues(triggerChainComputation = false) {
        let ret;
        // trigger computation ?
        if ((this.isExtraResult() || this.isResult())
            && triggerChainComputation) {
            this.nub.CalcSerie();
        }
        if (this.isParameter()) {
            const targetParam = this.element;
            // target might be in CALC mode
            if (targetParam.valueMode === param_value_mode_1.ParamValueMode.CALCUL) {
                // if already computed, expose handmade fake param values for iterability
                if (this.nub.result) {
                    if (!this._paramValues) {
                        this._paramValues = new param_values_1.ParamValues();
                        // populate
                        if (targetParam.hasMultipleValues) {
                            const multipleRes = this.nub.result.getCalculatedValues();
                            this._paramValues.setValues(multipleRes);
                        }
                        else {
                            const singleRes = this.nub.result.vCalc;
                            this._paramValues.setValues(singleRes);
                        }
                    }
                    // return local cache
                    ret = this._paramValues;
                }
                else {
                    throw new Error("LinkedValue.getParamValues() - result not available");
                }
            }
            else {
                // simple proxy to target parameter values; if target parameter is
                // also a LINK, ParamDefinition.paramValues will recursively call
                // LinkedValue.getParamValues() (this method)
                ret = targetParam.paramValues;
            }
        }
        else if (this.isExtraResult()) {
            // is result available ?
            if (this.nub.result) {
                // expose handmade fake param values for iterability
                if (!this._paramValues) {
                    this._paramValues = new param_values_1.ParamValues();
                    // populate
                    if (this.nub.resultHasMultipleValues()) {
                        const multipleExtraRes = this.getExtraResults(this.symbol);
                        this._paramValues.setValues(multipleExtraRes);
                    }
                    else {
                        const singleExtraRes = this.nub.result.getValue(this.symbol);
                        this._paramValues.setValues(singleExtraRes);
                    }
                }
                // return local cache
                ret = this._paramValues;
            }
            else {
                throw new Error("LinkedValue.getParamValues() - extra result not available");
            }
        }
        else {
            throw new Error("LinkedValue.getParamValues() - cannot determine nature of target");
        }
        return ret;
    }
    /**
     * Returns the single value of this.paramValues; triggers a chain computation
     * if required to obtain ParamValues
     */
    getValue(triggerChainComputation = false) {
        const pv = this.getParamValues(triggerChainComputation);
        return pv.singleValue;
    }
    /**
     * Returns true if
     * - a parameter is targetted and it has multiple values
     * - a result / extra result is targetted and it has more than 1 value
     */
    hasMultipleValues() {
        if (this.isParameter()) { // possibly in CALC mode, but ParamDefinition.hasMultipleValues takes care of it
            return this.element.hasMultipleValues;
        }
        else if (this.isResult() || this.isExtraResult()) {
            // guess if parent Nub has any variating parameter (linked or not)
            return this.nub.resultHasMultipleValues();
        }
        return false;
    }
    /**
     * Returns true if target value is available
     */
    isDefined() {
        if (this.isExtraResult()) {
            return (this.nub.result !== undefined);
        }
        else {
            return this.element.isDefined;
        }
    }
    /**
     * When invalidating a Nub's result, parameters pointing to this result should
     * invalidate their proxy paramValues too
     */
    invalidateParamValues() {
        this._paramValues = undefined;
    }
    getExtraResults(symbol) {
        const res = [];
        for (const re of this.nub.result.resultElements) {
            const er = re.getValue(symbol);
            if (er !== undefined) {
                res.push(er);
            }
        }
        return res;
    }
}
exports.LinkedValue = LinkedValue;

},{"./param/param-definition":86,"./param/param-value-mode":89,"./param/param-values":90}],18:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConcentrationBlocs = void 0;
const base_1 = require("../base");
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
class ConcentrationBlocs extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.ConcentrationBlocs;
        this._defaultCalculatedParam = prms.C;
        this.resetDefaultCalculatedParam();
    }
    get prms() {
        return this._prms;
    }
    /**
     * L / N = ax = D / sqrt(C)
     */
    Equation(sVarCalc) {
        let rounded = false;
        if (sVarCalc !== "N" && !Number.isInteger(this.prms.N.v)) {
            this.prms.N.v = Math.round(this.prms.N.v);
            rounded = true;
        }
        let v;
        let ax; // pattern size
        let R; // rest
        let NB;
        let axB;
        let cB;
        let NH;
        let axH;
        let cH;
        switch (sVarCalc) {
            case "C":
                ax = this.prms.L.v / this.prms.N.v;
                v = Math.pow(this.prms.D.v / ax, 2);
                break;
            case "N":
                ax = this.prms.D.v / Math.sqrt(this.prms.C.v);
                const divAndMod = base_1.floatDivAndMod(this.prms.L.v, ax);
                v = divAndMod.q;
                R = divAndMod.r;
                // parfois la largeur résiduelle peut être à peine en dessous de la largeur de motif (modulo foireux)
                if (base_1.isEqual(R, ax, 1e-2)) {
                    R -= ax;
                    v++;
                }
                // harmonisation ?
                if (!base_1.isEqual(R, 0, 1e-2)) {
                    // vers le bas
                    if (v > 0) {
                        NB = v;
                        axB = this.prms.L.v / NB;
                        cB = Math.pow(this.prms.D.v / axB, 2);
                    }
                    // vers le haut
                    NH = v + 1;
                    axH = this.prms.L.v / NH;
                    cH = Math.pow(this.prms.D.v / axH, 2);
                }
                break;
            case "L":
                ax = this.prms.D.v / Math.sqrt(this.prms.C.v);
                v = ax * this.prms.N.v;
                break;
            case "D":
                ax = this.prms.L.v / this.prms.N.v;
                v = ax * Math.sqrt(this.prms.C.v);
                break;
            default:
                throw new Error("ConcentrationBlocs.Equation() : invalid variable name " + sVarCalc);
        }
        const r = new result_1.Result(v, this);
        // extra results
        r.resultElement.values.ax = ax;
        if (R !== undefined) {
            r.resultElement.values.R = R;
        }
        if (NB !== undefined) {
            r.resultElement.values.NB = NB;
        }
        if (axB !== undefined) {
            r.resultElement.values.AXB = axB;
        }
        if (cB !== undefined) {
            r.resultElement.values.CB = cB;
        }
        if (NH !== undefined) {
            r.resultElement.values.NH = NH;
        }
        if (axH !== undefined) {
            r.resultElement.values.AXH = axH;
        }
        if (cH !== undefined) {
            r.resultElement.values.CH = cH;
        }
        if (rounded) {
            const m = new message_1.Message(message_1.MessageCode.WARNING_VALUE_ROUNDED_TO_INTEGER);
            m.extraVar.symbol = "N";
            m.extraVar.rounded = this.prms.N.v;
            r.resultElement.log.add(m);
        }
        return r;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.C.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.N.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.L.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.D.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
    setResultsUnits() {
        this._resultsUnits = {
            ax: "m",
            R: "m",
            AXB: "m",
            AXH: "m"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            ax: undefined,
            R: undefined,
            NB: undefined,
            AXB: undefined,
            CB: undefined,
            NH: undefined,
            AXH: undefined,
            CH: undefined
        };
    }
}
exports.ConcentrationBlocs = ConcentrationBlocs;

},{"../base":3,"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/message":151,"../util/result":155}],19:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConcentrationBlocsParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class ConcentrationBlocsParams extends params_equation_1.ParamsEquation {
    constructor(rC, rN, rL, rD) {
        super();
        this._C = new param_definition_1.ParamDefinition(this, "C", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 1), undefined, rC);
        this._N = new param_definition_1.ParamDefinition(this, "N", param_domain_1.ParamDomainValue.POS, undefined, rN);
        this._L = new param_definition_1.ParamDefinition(this, "L", param_domain_1.ParamDomainValue.POS, "m", rL, param_definition_1.ParamFamily.WIDTHS);
        this._D = new param_definition_1.ParamDefinition(this, "D", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 2), "m", rD);
        this.addParamDefinition(this._C);
        this.addParamDefinition(this._N);
        this.addParamDefinition(this._L);
        this.addParamDefinition(this._D);
    }
    /** Concentration de blocs */
    get C() {
        return this._C;
    }
    /** Nombre de motifs */
    get N() {
        return this._N;
    }
    /** Largeur de la passe */
    get L() {
        return this._L;
    }
    /** Diamètre des plots */
    get D() {
        return this._D;
    }
}
exports.ConcentrationBlocsParams = ConcentrationBlocsParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],20:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MacroRugo = exports.MacroRugoFlowType = void 0;
const compute_node_1 = require("../compute-node");
const dichotomie_1 = require("../dichotomie");
const index_1 = require("../index");
const param_definition_1 = require("../param/param-definition");
const param_value_mode_1 = require("../param/param-value-mode");
const session_settings_1 = require("../session_settings");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const mrc_inclination_1 = require("./mrc-inclination");
const fish_pass_1 = require("../fish_pass");
var MacroRugoFlowType;
(function (MacroRugoFlowType) {
    MacroRugoFlowType[MacroRugoFlowType["EMERGENT"] = 0] = "EMERGENT";
    MacroRugoFlowType[MacroRugoFlowType["QUASI_EMERGENT"] = 1] = "QUASI_EMERGENT";
    MacroRugoFlowType[MacroRugoFlowType["SUBMERGED"] = 2] = "SUBMERGED";
})(MacroRugoFlowType = exports.MacroRugoFlowType || (exports.MacroRugoFlowType = {}));
class MacroRugo extends fish_pass_1.FishPass {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._cache = {};
        this._calcType = compute_node_1.CalculatorType.MacroRugo;
        this._defaultCalculatedParam = this.prms.Q;
        this.resetDefaultCalculatedParam();
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer
     * @param rInit Valeur initiale
     */
    Calc(sVarCalc, rInit) {
        // Si on ne force pas à CALCUL, les tests à la 5e décimale ne passent plus (macrorugo.spec.ts)
        const originalValueMode = this.getParameter(sVarCalc).valueMode;
        this.getParameter(sVarCalc).valueMode = param_value_mode_1.ParamValueMode.CALCUL;
        const r = super.Calc(sVarCalc, rInit);
        this.getParameter(sVarCalc).valueMode = originalValueMode;
        // La largeur de la rampe est-elle adéquate par rapport à la largeur de motif ax ?
        // (si le parent est une MacroRugoCompound avec radier incliné, on ignore ces problèmes)
        if (this.parent === undefined
            || (this.parent instanceof index_1.MacrorugoCompound
                && this.parent.properties.getPropValue("inclinedApron") === mrc_inclination_1.MRCInclination.NOT_INCLINED)) {
            const ax = this.prms.PBD.v / Math.sqrt(this.prms.C.v);
            if (this.prms.B.v < ax - 0.001) { // B < ax, with a little tolerance
                const m = new message_1.Message(message_1.MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
                m.extraVar.pattern = ax;
                r.resultElement.log.add(m);
            }
            else if (this.prms.B.v % (ax / 2) > 0.001 && this.prms.B.v % (ax / 2) < ax / 2 - 0.001) {
                const m = new message_1.Message(message_1.MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
                m.extraVar.halfPattern = ax / 2;
                m.extraVar.lower = Math.floor(this.prms.B.v / ax * 2) * (ax / 2);
                m.extraVar.higher = Math.ceil(this.prms.B.v / ax * 2) * (ax / 2);
                r.resultElement.log.add(m);
            }
        }
        // La concentration est-elle dans les valeurs admissibles 8-20% (#284)
        if (this.parent === undefined) {
            if (this.prms.C.V < 0.08 || this.prms.C.V > 0.2) {
                r.resultElement.log.add(new message_1.Message(message_1.MessageCode.WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS));
            }
        }
        // Ajout des résultats complémentaires
        // Cote de fond aval
        r.resultElement.values.ZF2 = this.prms.ZF1.v - this.prms.If.v * this.prms.L.v;
        // Vitesse débitante
        let resVdeb = this.prms.Q.V / this.prms.B.v / this.prms.Y.v;
        if (isNaN(resVdeb)) {
            resVdeb = 0;
        }
        r.resultElement.values.Vdeb = resVdeb;
        if (this.flowType !== MacroRugoFlowType.SUBMERGED) {
            // Froude
            r.resultElement.values.Vg = r.resultElement.values.Vdeb / (1 - Math.sqrt(MacroRugo.fracAxAy * this.prms.C.v));
            let resFr = r.resultElement.values.Vg / Math.sqrt(MacroRugo.g * this.prms.Y.v);
            if (isNaN(resFr)) { // if Y == 0
                resFr = 0;
            }
            r.resultElement.values.Fr = resFr;
            // Vitesse maximale
            r.resultElement.values.Vmax = this.r * r.resultElement.values.Vg * this.CalcfFr(resVdeb);
        }
        // Puissance dissipée
        r.resultElement.values.PV = 1000 * MacroRugo.g * r.resultElement.values.Vdeb * this.prms.If.v;
        // Type d'écoulement
        if (this.prms.Y.v / this.prms.PBH.v < 1) {
            r.resultElement.values.ENUM_MacroRugoFlowType = MacroRugoFlowType.EMERGENT;
        }
        else if (this.prms.Y.v / this.prms.PBH.v < MacroRugo.limitSubmerg) {
            r.resultElement.values.ENUM_MacroRugoFlowType = MacroRugoFlowType.QUASI_EMERGENT;
        }
        else {
            r.resultElement.values.ENUM_MacroRugoFlowType = MacroRugoFlowType.SUBMERGED;
        }
        // Vitesse et débit du guide technique
        /* let cQ: [number, number, number, number];
        let cV: [number, number, number];
        let hdk: number;
        if (r.resultElement.values.ENUM_MacroRugoFlowType === MacroRugoFlowType.SUBMERGED) {
            cQ = [0.955, 2.282, 0.466, -0.23];
            hdk = this.prms.PBH.v;
        } else {
            hdk = this.prms.PBD.v;
            if (Math.abs(this.prms.Cd0.v - 2) < 0.05) {
                cQ = [0.648, 1.084, 0.56, -0.456];
                cV = [3.35, 0.27, 0.53];
            } else {
                cQ = [0.815, 1.45, 0.557, -0.456];
                cV = [4.54, 0.32, 0.56];
            }
        } */
        if (this.prms.Y.v > 0 && this.prms.If.v > 0) {
            r.resultElement.values.Strickler =
                this.prms.Q.V / (Math.pow(this.prms.Y.v, 5 / 3) * this.prms.B.v * Math.pow(this.prms.If.v, 0.5));
        }
        else {
            r.resultElement.values.Strickler = 0;
        }
        return r;
    }
    Equation(sVarCalc) {
        const Q = this.prms.Q.v;
        const q0 = Math.sqrt(2 * MacroRugo.g * this.prms.If.v * this.prms.PBD.v * (1 - (this.sigma * this.prms.C.v)) /
            (this.Cx * this.prms.C.v)) * this.prms.Y.v * this.prms.B.v;
        let r;
        if (q0 > 0) {
            this.setFlowType();
            const dicho = new dichotomie_1.Dichotomie(this, "Q", false, this.resolveQ);
            r = dicho.Dichotomie(0, session_settings_1.SessionSettings.precision, q0);
        }
        else {
            r = new result_1.Result(0);
        }
        this.prms.Q.v = Q;
        return r;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.ZF1.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.L.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Ks.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.B.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.If.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Y.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.C.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.PBD.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.PBH.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Cd0.calculability = param_definition_1.ParamCalculability.FREE;
    }
    setResultsUnits() {
        this._resultsUnits = {
            PV: "W/m³",
            Vdeb: "m/s",
            Vmax: "m/s",
            Vg: "m/s",
            ZF2: "m",
            Strickler: "SI"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            ZF2: param_definition_1.ParamFamily.ELEVATIONS,
            Vdeb: param_definition_1.ParamFamily.SPEEDS,
            Vmax: param_definition_1.ParamFamily.SPEEDS,
            Vg: param_definition_1.ParamFamily.SPEEDS,
            Fr: undefined,
            PV: undefined,
            Strickler: param_definition_1.ParamFamily.STRICKLERS
        };
    }
    setFlowType() {
        const hstar = this.prms.Y.v / this.prms.PBH.v;
        if (hstar > MacroRugo.limitSubmerg) {
            this.flowType = MacroRugoFlowType.SUBMERGED;
        }
        else if (hstar < 1) {
            this.flowType = MacroRugoFlowType.EMERGENT;
        }
        else {
            this.flowType = MacroRugoFlowType.QUASI_EMERGENT;
        }
    }
    /**
     * Equation from Cassan, L., Laurens, P., 2016. Design of emergent and submerged rock-ramp fish passes.
     * Knowledge & Management of Aquatic Ecosystems 45.
     * @param sVarCalc Variable à calculer
     */
    resolveQ() {
        // Reset cached variables depending on Q (or not...)
        this._cache = {};
        /** adimensional water depth */
        const hstar = this.prms.Y.v / this.prms.PBH.v;
        switch (this.flowType) {
            case MacroRugoFlowType.SUBMERGED:
                return this.resolveQSubmerged();
            case MacroRugoFlowType.EMERGENT:
                return this.resolveQEmergent();
            case MacroRugoFlowType.QUASI_EMERGENT:
                const a = (hstar - 1) / (MacroRugo.limitSubmerg - 1);
                return (1 - a) * this.resolveQEmergent() + a * this.resolveQSubmerged();
        }
    }
    /**
     * Averaged velocity (m.s-1)
     */
    get U0() {
        if (this._cache.U0 === undefined) {
            this._cache.U0 = this.prms.Q.v / this.prms.B.v / this.prms.Y.v;
        }
        return this._cache.U0;
    }
    get CdChD() {
        if (this._cache.CdChD === undefined) {
            this._cache.CdChD = this.Cd * this.prms.C.v * this.prms.PBH.v / this.prms.PBD.v;
        }
        return this._cache.CdChD;
    }
    /**
     * sigma ratio between the block area in the x, y plane and D2
     */
    get sigma() {
        if (this._cache.sigma === undefined) {
            if (this.prms.Cd0.v >= 2) {
                this._cache.sigma = 1;
            }
            else {
                this._cache.sigma = Math.PI / 4;
            }
        }
        return this._cache.sigma;
    }
    get R() {
        if (this._cache.R === undefined) {
            this._cache.R = (1 - this.sigma * this.prms.C.v);
        }
        return this._cache.R;
    }
    /**
     * Bed friction coefficient Equation (3) (Cassan et al., 2016)
     * @param Y Water depth (m)
     */
    calcCf(Y) {
        if (this.prms.Ks.v < 1E-9) {
            // Between Eq (8) and (9) (Cassan et al., 2016)
            const reynolds = this.U0 * this.prms.Y.v / MacroRugo.nu;
            return 0.3164 / 4. * Math.pow(reynolds, -0.25);
        }
        else {
            // Equation (3) (Cassan et al., 2016)
            return 2 / Math.pow(5.1 * Math.log10(Y / this.prms.Ks.v) + 6, 2);
        }
    }
    /**
     * Interpolation of Cd0 for Cd from calibrated Cd0 (See #291)
     * Cx = 1.1 for Cd0 = 1 and Cx = 2.592 for Cd0 = 2
     */
    get Cx() {
        if (this._cache.Cx === undefined) {
            this._cache.Cx = this.prms.Cd0.v * 1.4917 - 0.3914;
        }
        return this._cache.Cx;
    }
    /**
     * Calculation of Cd : drag coefficient of a block under the actual flow conditions
     */
    get Cd() {
        if (this._cache.Cd === undefined) {
            this._cache.Cd = this.Cx * Math.min(3, (1 + 1 / Math.pow(this.prms.Y.v / this.prms.PBD.v, 2)));
        }
        return this._cache.Cd;
    }
    /**
     * Calcul de Beta force ratio between drag and turbulent stress (Cassan et al. 2016 eq(8))
     * \Beta = (k / alpha_t) (C_d C k / D) / (1 - \sigma C)
     * @param alpha \alpha_t turbulent length scale (m) within the blocks layer
     */
    calcBeta(alpha) {
        return Math.min(100, Math.sqrt(this.prms.PBH.v * this.CdChD / alpha / this.R));
    }
    /**
     * Averaged velocity at a given vertical position (m.s-1)
     * @param alpha turbulent length scale (m) within the blocks layer
     * @param z dimensionless vertical position z / k
     */
    calcUz(alpha, z = 1) {
        const beta = this.calcBeta(alpha);
        // Equation (9) Cassan et al., 2016
        return this.u0 * Math.sqrt(beta * (this.prms.Y.v / this.prms.PBH.v - 1) * Math.sinh(beta * z) / Math.cosh(beta) + 1);
    }
    get ustar() {
        if (this._cache.ustar === undefined) {
            this._cache.ustar = Math.sqrt(MacroRugo.g * this.prms.If.v * (this.prms.Y.v - this.prms.PBH.v));
        }
        return this._cache.ustar;
    }
    resolveAlpha_t(alpha) {
        /** s: minimum distance between blocks */
        const s = this.prms.PBD.v * (1 / Math.sqrt(this.prms.C.v) - 1);
        /** Equation(11) Cassan et al., 2016 */
        const l0 = Math.min(s, 0.15 * this.prms.PBH.v);
        // Equation(10) Cassan et al., 2016
        return alpha * this.calcUz(alpha) - l0 * this.ustar;
    }
    resolveQSubmerged() {
        /** Tirant d'eau (m) */
        const h = this.prms.Y.v;
        /** Concentration de blocs (-) */
        const C = this.prms.C.v;
        /** Paramètre de bloc : Diamètre (m) */
        const D = this.prms.PBD.v;
        /** Paramètre de bloc : Hauteur (m) */
        const k = this.prms.PBH.v;
        /** Slope (m/m) */
        const S = this.prms.If.v;
        /** Accélération gravité (m/s²) */
        const g = MacroRugo.g;
        /** Constante von Karman */
        const kappa = 0.41;
        /** Velocity at the bed §2.3.2 Cassan et al., 2016 */
        this.u0 = Math.sqrt(k * 2 * g * S * this.R
            / (this.Cd * C * k / D + this.calcCf(k) * this.R));
        /** turbulent length scale (m) within the blocks layer (alpha_t) */
        const alpha = uniroot(this.resolveAlpha_t, this, 1E-3, 10);
        /** averaged velocity at the top of blocks (m.s-1) */
        const uk = this.calcUz(alpha);
        /** Equation (13) Cassan et al., 2016 */
        const d = k - 1 / kappa * alpha * uk / this.ustar;
        /** Equation (14) Cassan et al., 2016 */
        const z0 = (k - d) * Math.exp(-kappa * uk / this.ustar);
        /** Integral of Equation (12) Cassan et al., 2016 */
        // tslint:disable-next-line:variable-name
        let Qsup;
        if (z0 > 0) {
            Qsup = this.ustar / kappa * ((h - d) * (Math.log((h - d) / z0) - 1)
                - ((k - d) * (Math.log((k - d) / z0) - 1)));
        }
        else {
            Qsup = 0;
        }
        // calcul intégrale dans la canopée----
        // tslint:disable-next-line:variable-name
        let Qinf = this.u0;
        let u = 0;
        let uOld;
        const step = 0.01;
        const zMax = 1 + step / 2;
        for (let z = step; z < zMax; z += step) {
            uOld = u;
            u = this.calcUz(alpha, z);
            Qinf += (uOld + u);
        }
        Qinf = Qinf / 2 * step * k;
        // Calcul de u moyen
        return this.U0 - (Qinf + Qsup) / h;
    }
    resolveQEmergent() {
        // tslint:disable-next-line: variable-name
        const Cd = this.Cd * this.fFr;
        /** N from Cassan 2016 eq(2) et Cassan 2014 eq(12) */
        const N = (1 * this.calcCf(this.prms.Y.v)) / (this.prms.Y.v / this.prms.PBD.v * Cd * this.prms.C.v);
        // const U0i = Math.sqrt(
        //     2 * MacroRugo.g * this.prms.If.v * this.prms.PBD.v *
        //     (1 - this.sigma * this.prms.C.v) / (Cd * this.prms.C.v * (1 + N))
        // );
        return this.U0 - uniroot(this.resolveU0Complete, this, 1E-6, 100);
    }
    resolveU0Complete(U0) {
        const fFr = this.CalcfFr(U0);
        const alpha = 1 - Math.pow(1 * this.prms.C.v, 0.5) - 0.5 * this.sigma * this.prms.C.v;
        const N = (alpha * this.calcCf(this.prms.Y.v)) /
            (this.prms.Y.v / this.prms.PBD.v * this.Cd * fFr * this.prms.C.v);
        return U0 - Math.sqrt(2 * MacroRugo.g * this.prms.If.v * this.prms.PBD.v *
            (1 - this.sigma * this.prms.C.v) / (this.Cd * fFr * this.prms.C.v * (1 + N)));
    }
    /**
     * Calcul du ratio entre la vitesse moyenne à l'aval d'un block et la vitesse maximale
     * r = 1.1 pour un plot circulaire Cd0​=1.1 et r = 1.5 pour un plot à face plane Cd0​=2.6
     * Voir #283
     */
    get r() {
        if (this._cache.r === undefined) {
            this._cache.r = 0.4 * this.prms.Cd0.v + 0.7;
        }
        return this._cache.r;
    }
    /**
     * Froude correction function (Cassan et al. 2014, Eq. 19)
     */
    get fFr() {
        if (this._cache.fFr === undefined) {
            this._cache.fFr = this.CalcfFr(this.U0);
        }
        return this._cache.fFr;
    }
    /**
     * Calculation of Froude correction function (Cassan et al. 2014, Eq. 19)
     */
    CalcfFr(U0) {
        // tslint:disable-next-line:variable-name
        const Fr = U0 /
            (1 - Math.sqrt(MacroRugo.fracAxAy * this.prms.C.v)) /
            Math.sqrt(MacroRugo.g * this.prms.Y.v);
        if (Fr < 1.3) {
            return Math.pow(Math.min(1 / (1 - Math.pow(Fr, 2) / 4), Math.pow(Fr, -2 / 3)), 2);
        }
        else {
            return Math.pow(Fr, -4 / 3);
        }
    }
}
exports.MacroRugo = MacroRugo;
MacroRugo.g = 9.81;
/** nu: water kinematic viscosity  */
MacroRugo.nu = 1E-6;
// Water at 20 °C has a kinematic viscosity of about 10−6 m2·s−1
// (https://en.wikipedia.org/wiki/Viscosity#Kinematic_viscosity,_%CE%BD)
/** Ratio between the width (perpendicular to flow) and the length (parallel to flow) of a cell (-) */
MacroRugo.fracAxAy = 1;
/** Limit between emergent and submerged flow */
MacroRugo.limitSubmerg = 1.1;
/**
 * Searches the interval from <tt>lowerLimit</tt> to <tt>upperLimit</tt>
 * for a root (i.e., zero) of the function <tt>func</tt> with respect to
 * its first argument using Brent's method root-finding algorithm.
 *
 * Translated from zeroin.c in http://www.netlib.org/c/brent.shar.
 *
 * Copyright (c) 2012 Borgar Thorsteinsson <borgar@borgar.net>
 * MIT License, http://www.opensource.org/licenses/mit-license.php
 *
 * @param {function} func function for which the root is sought.
 * @param {number} lowerlimit the lower point of the interval to be searched.
 * @param {number} upperlimit the upper point of the interval to be searched.
 * @param {number} errorTol the desired accuracy (convergence tolerance).
 * @param {number} maxIter the maximum number of iterations.
 * @returns an estimate for the root within accuracy.
 *
 */
function uniroot(func, thisArg, lowerLimit, upperLimit, errorTol = 0, maxIter = 1000) {
    let a = lowerLimit;
    let b = upperLimit;
    let c = a;
    let fa = func.call(thisArg, a);
    let fb = func.call(thisArg, b);
    let fc = fa;
    let tolAct; // Actual tolerance
    let newStep; // Step at this iteration
    let prevStep; // Distance from the last but one to the last approximation
    let p; // Interpolation step is calculated in the form p/q; division is delayed until the last moment
    let q;
    while (maxIter-- > 0) {
        prevStep = b - a;
        if (Math.abs(fc) < Math.abs(fb)) {
            // Swap data for b to be the best approximation
            a = b, b = c, c = a;
            fa = fb, fb = fc, fc = fa;
        }
        tolAct = 1e-15 * Math.abs(b) + errorTol / 2;
        newStep = (c - b) / 2;
        if (Math.abs(newStep) <= tolAct || fb === 0) {
            return b; // Acceptable approx. is found
        }
        // Decide if the interpolation can be tried
        if (Math.abs(prevStep) >= tolAct && Math.abs(fa) > Math.abs(fb)) {
            // If prev_step was large enough and was in true direction, Interpolatiom may be tried
            let t1;
            let cb;
            let t2;
            cb = c - b;
            if (a === c) { // If we have only two distinct points linear interpolation can only be applied
                t1 = fb / fa;
                p = cb * t1;
                q = 1.0 - t1;
            }
            else { // Quadric inverse interpolation
                q = fa / fc, t1 = fb / fc, t2 = fb / fa;
                p = t2 * (cb * q * (q - t1) - (b - a) * (t1 - 1));
                q = (q - 1) * (t1 - 1) * (t2 - 1);
            }
            if (p > 0) {
                q = -q; // p was calculated with the opposite sign; make p positive
            }
            else {
                p = -p; // and assign possible minus to q
            }
            if (p < (0.75 * cb * q - Math.abs(tolAct * q) / 2) &&
                p < Math.abs(prevStep * q / 2)) {
                // If (b + p / q) falls in [b,c] and isn't too large it is accepted
                newStep = p / q;
            }
            // If p/q is too large then the bissection procedure can reduce [b,c] range to more extent
        }
        if (Math.abs(newStep) < tolAct) { // Adjust the step to be not less than tolerance
            newStep = (newStep > 0) ? tolAct : -tolAct;
        }
        a = b, fa = fb; // Save the previous approx.
        b += newStep, fb = func.call(thisArg, b); // Do step to a new approxim.
        if ((fb > 0 && fc > 0) || (fb < 0 && fc < 0)) {
            c = a, fc = fa; // Adjust c for it to have a sign opposite to that of b
        }
    }
    return undefined;
}

},{"../compute-node":5,"../dichotomie":12,"../fish_pass":13,"../index":14,"../param/param-definition":86,"../param/param-value-mode":89,"../session_settings":105,"../util/message":151,"../util/result":155,"./mrc-inclination":24}],21:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MacrorugoCompound = void 0;
const base_1 = require("../base");
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const props_1 = require("../props");
const session_1 = require("../session");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const macrorugo_1 = require("./macrorugo");
const mrc_inclination_1 = require("./mrc-inclination");
class MacrorugoCompound extends macrorugo_1.MacroRugo {
    /** enfants castés au bon type */
    get children() {
        return this._children;
    }
    /** parameters cast to the right type */
    get prms() {
        return this._prms;
    }
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.MacroRugoCompound;
        this._props.addObserver(this);
        this.inclinedApron = mrc_inclination_1.MRCInclination.NOT_INCLINED;
        this._childrenType = "MacroRugo";
    }
    get inclinedApron() {
        return this.properties.getPropValue("inclinedApron");
    }
    set inclinedApron(i) {
        this.properties.setPropValue("inclinedApron", i);
    }
    CalcSerie(rInit) {
        if (this.properties.getPropValue("inclinedApron") === mrc_inclination_1.MRCInclination.INCLINED) {
            // important to regenerate it here, at every iteration of CalcSerie()
            this.generateInclinedFishway();
        }
        // calculate and store depths once for all
        for (const child of this.children) {
            // do we have a series of values ?
            if (this.prms.Z1.hasMultipleValues || child.prms.ZF1.hasMultipleValues) {
                let valsZ1;
                let valsChildZF1;
                if (this.prms.Z1.hasMultipleValues) {
                    valsZ1 = this.prms.Z1.getInferredValuesList();
                }
                else {
                    valsZ1 = new Array(child.prms.ZF1.getInferredValuesList().length)
                        .fill(this.prms.Z1.singleValue);
                }
                if (child.prms.ZF1.hasMultipleValues) {
                    valsChildZF1 = child.prms.ZF1.getInferredValuesList();
                }
                else {
                    valsChildZF1 = new Array(this.prms.Z1.getInferredValuesList().length)
                        .fill(child.prms.ZF1.singleValue);
                }
                // adjust lengths (should never happen when only one of the paramters is varying)
                if (valsZ1.length > valsChildZF1.length) {
                    valsChildZF1 = child.prms.ZF1.getInferredValuesList(valsZ1.length);
                }
                else if (valsZ1.length < valsChildZF1.length) {
                    valsZ1 = this.prms.Z1.getInferredValuesList(valsChildZF1.length);
                }
                // define calculated values list on child
                const valsY = [];
                for (let i = 0; i < valsZ1.length; i++) {
                    valsY.push(Math.max(valsZ1[i] - valsChildZF1[i], 0));
                }
                child.prms.Y.setValues(valsY);
            }
            else {
                // or just a single value ?
                child.prms.Y.setValue(Math.max(this.prms.Z1.singleValue - child.prms.ZF1.singleValue, 0));
            }
        }
        return super.CalcSerie(rInit);
    }
    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer
     * @param rInit Valeur initiale
     */
    Calc(sVarCalc, rInit) {
        if (sVarCalc !== "Q") {
            throw new Error("MacrorugoCompound.Calc() : invalid parameter " + sVarCalc);
        }
        this.copyPrmsToChildren();
        this.currentResult = this.Equation(sVarCalc);
        // lateral inclination for inclined aprons
        if (this.properties.getPropValue("inclinedApron") === mrc_inclination_1.MRCInclination.INCLINED) {
            // extraResult : inclination
            this.result.resultElement.values.LIncl = (this.prms.ZRL.v - this.prms.ZRR.v) / this.prms.BR.v;
            // La largeur de la rampe inclinée est-elle adéquate par rapport à la largeur de motif ax ?
            const ax = this.prms.PBD.v / Math.sqrt(this.prms.C.v);
            if (this.prms.BR.v - ax < -0.001) { // BR < ax, with a little tolerance
                const m = new message_1.Message(message_1.MessageCode.WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH);
                m.extraVar.pattern = ax;
                this._result.resultElement.log.add(m);
            }
            else if (this.prms.BR.v % (ax / 2) > 0.001 && this.prms.BR.v % (ax / 2) < ax / 2 - 0.001) {
                const m = new message_1.Message(message_1.MessageCode.WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH);
                m.extraVar.halfPattern = ax / 2;
                m.extraVar.lower = Math.floor(this.prms.BR.v / ax * 2) * (ax / 2);
                m.extraVar.higher = Math.ceil(this.prms.BR.v / ax * 2) * (ax / 2);
                this._result.resultElement.log.add(m);
            }
        }
        // Check block concentration bounds
        if (this.prms.C.v < 0.08 || this.prms.C.v > 0.2) {
            this._result.resultElement.log.add(new message_1.Message(message_1.MessageCode.WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS));
        }
        return this._result;
    }
    Equation(sVarCalc) {
        let B = 0;
        const res = new result_1.Result(0);
        for (const child of this.children) {
            child.Calc(sVarCalc);
            if (!child.result.ok) {
                const m = new message_1.Message(message_1.MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                m.extraVar.number = String(child.findPositionInParent() + 1);
                return new result_1.Result(m);
            }
            res.vCalc += child.result.vCalc;
            child.result.values.xCenter = B + child.prms.B.v / 2;
            B += child.prms.B.v;
            // la cote de fond aval n'a pas de sens dans la MR-Complexe car on
            // ne précise pas la longueur
            delete child.result.values.ZF2;
        }
        return res;
    }
    addChild(child, after) {
        super.addChild(child, after);
        for (const p of child.parameterIterator) {
            p.visible = false;
        }
        child.prms.ZF1.visible = true;
        child.prms.B.visible = true;
        child.prms.B.calculability = param_definition_1.ParamCalculability.FREE;
    }
    addDefaultChild(after) {
        this.addChild(session_1.Session.getInstance().createNub(new props_1.Props({ calcType: compute_node_1.CalculatorType.MacroRugo })), after);
    }
    // interface Observer
    update(sender, data) {
        if (data.action === "propertyChange" && data.name === "inclinedApron") {
            if (data.value === mrc_inclination_1.MRCInclination.INCLINED) { // switch to inclined apron mode
                this.prms.ZRL.visible = true;
                this.prms.ZRR.visible = true;
                this.prms.BR.visible = true;
                this.prms.PBD.calculability = param_definition_1.ParamCalculability.FIXED;
                this.prms.C.calculability = param_definition_1.ParamCalculability.FIXED;
            }
            else { // switch to multiple aprons mode
                this.prms.ZRL.visible = false;
                this.prms.ZRR.visible = false;
                this.prms.BR.visible = false;
                this.prms.PBD.calculability = param_definition_1.ParamCalculability.FREE;
                this.prms.C.calculability = param_definition_1.ParamCalculability.FREE;
            }
        }
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.ZF1.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.L.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Ks.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.B.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.If.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Y.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.C.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.PBD.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.PBH.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Cd0.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.ZRL.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.ZRR.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.BR.calculability = param_definition_1.ParamCalculability.FIXED;
    }
    exposeResults() {
        // overload MacroRugo.exposeResults() with empty method, just
        // to avoid exposing inherited results that are not calculated here
        this._resultsFamilies = {
            LIncl: undefined
        };
    }
    copyPrmsToChildren() {
        for (const child of this.children) {
            // Copy common parameters with MacrorugoCompound
            for (const v of ["Ks", "If", "C", "PBD", "PBH", "Cd0"]) {
                child.getParameter(v).v = this.getParameter(v).v;
            }
            // Calculate Length and depth from other parameters
            child.prms.L.v = this.prms.DH.v / this.prms.If.v;
        }
    }
    /**
     * Discrétisation d'un radier incliné en radiers horizontaux
     * Le pas de discrétisationest fixée à D / C^0.5.
     * Les cellules discrétisées sont centrées sur la passe, on crée
     * les cellules manquantes aux extrémités si le reste > ax / 2
     * sinon les cellules aux extrémités sont étendues jusqu'aux bords.
     * La cote de radier de chaque cellule est calculée au milieu de celle-ci.
     */
    generateInclinedFishway() {
        // Calcul de la position des cellules
        // Taille d'une cellule de calcul
        const ax = this.prms.PBD.singleValue / Math.sqrt(this.prms.C.singleValue);
        // Nombre entier de cellules et reste
        const nCells = Math.floor(this.prms.BR.singleValue / ax);
        const xRest = this.prms.BR.singleValue % ax;
        const xCenters = [];
        let lastBorder = 0;
        // Position du centre des cellules
        for (let i = 0; i < nCells; i++) {
            if (i === nCells - 1 && xRest <= ax / 2) {
                // Agrandissement de la première cellule (taille normale + xRest) si
                // le reste était trop faible pour en rajouter une
                xCenters.push(lastBorder + (ax + xRest) / 2);
                lastBorder += ax + xRest;
            }
            else {
                xCenters.push(lastBorder + ax / 2);
                lastBorder += ax;
            }
        }
        // Ajout éventuel d'une cellule à droite (en haut), de taille xRest
        if (xRest > ax / 2) {
            xCenters.push(lastBorder + xRest / 2);
        }
        // Génération des radiers
        // Suppression des radiers existants
        this.deleteAllChildren();
        // Ajout des radiers et calcul de leur cote amont et de leur largeur
        lastBorder = 0;
        for (const xCenter of xCenters) {
            this.addDefaultChild();
            this.children[this.children.length - 1].prms.ZF1.singleValue = base_1.round(this.prms.ZRR.singleValue + xCenter / this.prms.BR.singleValue
                * (this.prms.ZRL.singleValue - this.prms.ZRR.singleValue), 3);
            this.children[this.children.length - 1].prms.B.singleValue = base_1.round((xCenter - lastBorder) * 2, 3);
            lastBorder += this.children[this.children.length - 1].prms.B.singleValue;
        }
    }
}
exports.MacrorugoCompound = MacrorugoCompound;

},{"../base":3,"../compute-node":5,"../param/param-definition":86,"../props":103,"../session":104,"../util/message":151,"../util/result":155,"./macrorugo":20,"./mrc-inclination":24}],22:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MacrorugoCompoundParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const macrorugo_params_1 = require("./macrorugo_params");
class MacrorugoCompoundParams extends macrorugo_params_1.MacrorugoParams {
    /**
     *
     * @param rZ1 Cote de l'eau amont (m)
     * @param rZRL Cote de radier gauche (m) (Radier incliné seulement)
     * @param rZRR Cote de radier droit (m) (Radier incliné seulement)
     * @param rB Largeur (m) (Radier incliné seulement)
     * @param rDH Chute (m)
     * @param rIf Pente (m/m)
     * @param rY Tirant d'eau (m)
     * @param rRF Rugosité de fond (m)
     * @param rCB Concentration de blocs (m)
     * @param rPBD Paramètre de bloc : Diamètre (m)
     * @param rPBH Paramètre de bloc : Hauteur (m)
     * @param rCd0 Paramètre de bloc : Forme (1 pour rond, 2 pour carré)
     */
    constructor(rZ1, rZRL, rZRR, rB, rDH, rIf, rRF, rCB, rPBD, rPBH, rCd0) {
        super((rZRL + rZRR) / 2, 1, rDH / rIf, rIf, 1, 1, rRF, rCB, rPBD, rPBH, rCd0);
        this._Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this._Z1);
        this._ZRL = new param_definition_1.ParamDefinition(this, "ZRL", param_domain_1.ParamDomainValue.ANY, "m", rZRL, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZRL);
        this._ZRR = new param_definition_1.ParamDefinition(this, "ZRR", param_domain_1.ParamDomainValue.ANY, "m", rZRR, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this._ZRR);
        this._BR = new param_definition_1.ParamDefinition(this, "BR", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 100), "m", rB, param_definition_1.ParamFamily.WIDTHS);
        this.addParamDefinition(this._BR);
        this._DH = new param_definition_1.ParamDefinition(this, "DH", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 100), "m", rDH, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this._DH);
        // Width, water depth, and Bottom elevation are defined in Macrorugo children
        this.B.visible = false;
        this.ZF1.visible = false;
        this.Y.visible = false;
        this.Q.visible = false;
    }
    get Z1() {
        return this._Z1;
    }
    get ZRL() {
        return this._ZRL;
    }
    get ZRR() {
        return this._ZRR;
    }
    get BR() {
        return this._BR;
    }
    get DH() {
        return this._DH;
    }
}
exports.MacrorugoCompoundParams = MacrorugoCompoundParams;

},{"../param/param-definition":86,"../param/param-domain":87,"./macrorugo_params":23}],23:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MacrorugoParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class MacrorugoParams extends params_equation_1.ParamsEquation {
    /**
     *
     * @param rZF1 Cote de fond amont (m)
     * @param rL Longueur (m)
     * @param rB Largeur (m)
     * @param rIf Pente (m/m)
     * @param rQ Débit (m3/s)
     * @param rY Tirant d'eau (m)
     * @param rRF Rugosité de fond (m)
     * @param rCB Concentration de blocs (m)
     * @param rPBD Paramètre de bloc : Diamètre (m)
     * @param rPBH Paramètre de bloc : Hauteur (m)
     * @param rCd0 Paramètre de bloc : Forme (1 pour rond, 2 pour carré)
     */
    constructor(rZF1, rL, rB, rIf, rQ, rY, rRF, rCB, rPBD, rPBH, rCd0) {
        super();
        this._ZF1 = new param_definition_1.ParamDefinition(this, "ZF1", param_domain_1.ParamDomainValue.ANY, "m", rZF1, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this._ZF1);
        this._L = new param_definition_1.ParamDefinition(this, "L", param_domain_1.ParamDomainValue.POS, "m", rL, param_definition_1.ParamFamily.LENGTHS);
        this.addParamDefinition(this._L);
        this._B = new param_definition_1.ParamDefinition(this, "B", param_domain_1.ParamDomainValue.POS, "m", rB, param_definition_1.ParamFamily.WIDTHS);
        this.addParamDefinition(this._B);
        this._If = new param_definition_1.ParamDefinition(this, "If", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 0.5), "m/m", rIf, param_definition_1.ParamFamily.SLOPES);
        this.addParamDefinition(this._If);
        this._Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.POS, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this.addParamDefinition(this._Q);
        this._Y = new param_definition_1.ParamDefinition(this, "Y", param_domain_1.ParamDomainValue.POS_NULL, "m", rY, param_definition_1.ParamFamily.HEIGHTS);
        this.addParamDefinition(this._Y);
        this._Ks = new param_definition_1.ParamDefinition(this, "Ks", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 1), "m", rRF);
        this.addParamDefinition(this._Ks);
        this._C = new param_definition_1.ParamDefinition(this, "C", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 1), "", rCB);
        this.addParamDefinition(this._C);
        this._PBD = new param_definition_1.ParamDefinition(this, "PBD", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 2), "m", rPBD);
        this.addParamDefinition(this._PBD);
        this._PBH = new param_definition_1.ParamDefinition(this, "PBH", param_domain_1.ParamDomainValue.POS, "m", rPBH, param_definition_1.ParamFamily.HEIGHTS);
        this.addParamDefinition(this._PBH);
        this._Cd0 = new param_definition_1.ParamDefinition(this, "Cd0", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0.5, 3), undefined, rCd0);
        this.addParamDefinition(this._Cd0);
    }
    /**
     * Cote de fond amont (m)
     * @return {ParamDefinition}
     */
    get ZF1() {
        return this._ZF1;
    }
    /**
     * Longueur (m)
     * @return {ParamDefinition}
     */
    get L() {
        return this._L;
    }
    /**
     * Largeur (m)
     * @return {ParamDefinition}
     */
    get B() {
        return this._B;
    }
    /**
     * Pente (m/m)
     * @return {ParamDefinition}
     */
    get If() {
        return this._If;
    }
    /**
     * Débit (m3/s)
     * @return {ParamDefinition}
     */
    get Q() {
        return this._Q;
    }
    /**
     * Tirant d'eau (m)
     * @return {ParamDefinition}
     */
    get Y() {
        return this._Y;
    }
    /**
     * Rugosité de fond (m)
     * @return {ParamDefinition}
     */
    get Ks() {
        return this._Ks;
    }
    /**
     * Concentration de blocs (-)
     * @return {ParamDefinition}
     */
    get C() {
        return this._C;
    }
    /**
     * Paramètre de bloc : Diamètre (m)
     * @return {ParamDefinition}
     */
    get PBD() {
        return this._PBD;
    }
    /**
     * Paramètre de bloc : Hauteur (m)
     * @return {ParamDefinition}
     */
    get PBH() {
        return this._PBH;
    }
    /**
     * Paramètre de bloc : Forme (1 pour rond, 2 pour carré)
     * @return {ParamDefinition}
     */
    get Cd0() {
        return this._Cd0;
    }
}
exports.MacrorugoParams = MacrorugoParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],24:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MRCInclination = void 0;
/** Inclination state for MacroRugoCompound */
var MRCInclination;
(function (MRCInclination) {
    MRCInclination[MRCInclination["NOT_INCLINED"] = 0] = "NOT_INCLINED";
    MRCInclination[MRCInclination["INCLINED"] = 1] = "INCLINED";
})(MRCInclination = exports.MRCInclination || (exports.MRCInclination = {}));

},{}],25:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SPP = exports.SPPOperation = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
var SPPOperation;
(function (SPPOperation) {
    SPPOperation[SPPOperation["SUM"] = 0] = "SUM";
    SPPOperation[SPPOperation["PRODUCT"] = 1] = "PRODUCT"; // Produit
})(SPPOperation = exports.SPPOperation || (exports.SPPOperation = {}));
/**
 * Somme et produit de puissances
 * Y = Σ ou π (a1.x1^p1, … an.xn^pn)
 */
class SPP extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.SPP;
        this._defaultCalculatedParam = prms.Y;
        this._childrenType = "Puissance";
        this.resetDefaultCalculatedParam();
        this.operation = SPPOperation.SUM;
    }
    /** paramètres castés au bon type */
    get prms() {
        return this._prms;
    }
    get operation() {
        return this.properties.getPropValue("sppOperation");
    }
    set operation(o) {
        this.properties.setPropValue("sppOperation", o);
    }
    /**
     * @param sVarCalc Nom du paramètre à calculer : "Y", ou
     *  { uid: "abcdef", symbol: "X" } avec "abcdef" l'UID du module YAXN et "X" son paramètre
     * @param rInit Valeur initiale
     */
    Calc(sVarCalc, rInit) {
        // if Calc() is called outside of CalcSerie(), _result might not be initialized
        if (!this.result) {
            this.initNewResultElement();
        }
        switch (sVarCalc) {
            case "Y":
                this.currentResult = super.Calc(sVarCalc, rInit);
                if (this.result.ok) {
                    this.getParameter(sVarCalc).v = this.result.resultElement.vCalc;
                }
                break;
            default:
                if (typeof sVarCalc === "string") {
                    throw new Error("SPP.Calc() : unknow parameter to calculate " + sVarCalc);
                }
                let vPartielle;
                // 1. calcul de la somme / du produit des modules YAXN, sauf celui qui est en calcul;
                // 2. soustraction / division de Y par le résultat de 1.
                if (this.operation === SPPOperation.SUM) {
                    vPartielle = 0;
                    for (const c of this._children) {
                        if (c.uid !== sVarCalc.uid) {
                            const cRes = c.Calc("Y");
                            if (cRes.ok) {
                                vPartielle += cRes.vCalc;
                            }
                            else {
                                const m = new message_1.Message(message_1.MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                                m.extraVar.number = String(c.findPositionInParent() + 1); // String to avoid decimals
                                this.result.log.add(m);
                                this.result.log.addLog(cRes.log);
                                return this.result;
                            }
                        }
                    }
                    vPartielle = this.prms.Y.v - vPartielle;
                }
                else if (this.operation === SPPOperation.PRODUCT) {
                    vPartielle = 1;
                    for (const c of this._children) {
                        if (c.uid !== sVarCalc.uid) {
                            const cRes = c.Calc("Y");
                            if (cRes.ok) {
                                vPartielle *= cRes.vCalc;
                            }
                            else {
                                const m = new message_1.Message(message_1.MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                                m.extraVar.number = String(c.findPositionInParent() + 1); // String to avoid decimals
                                this.result.log.add(m);
                                this.result.log.addLog(cRes.log);
                                return this.result;
                            }
                        }
                    }
                    if (vPartielle === 0) {
                        const m = new message_1.Message(message_1.MessageCode.ERROR_DIVISION_BY_ZERO);
                        m.extraVar.symbol = "vPartielle";
                        this.result.resultElement.addMessage(m);
                        return this.result;
                    }
                    vPartielle = this.prms.Y.v / vPartielle;
                }
                // 3. injection de vPartielle dans le Y du module YAXN en calcul
                const yaxnEnCalcul = this.getChildren()[this.getIndexForChild(sVarCalc.uid)];
                yaxnEnCalcul.prms.Y.v = vPartielle;
                const r = yaxnEnCalcul.Calc(sVarCalc.symbol, rInit); // sVarCalc.symbol should always be "X"
                // "copy" result
                this.result.symbol = r.symbol;
                this.result.vCalc = r.vCalc;
                this.result.log.addLog(r.log);
                this.result.globalLog.addLog(r.globalLog);
        }
        return this.result;
    }
    Equation(sVarCalc) {
        let v;
        switch (sVarCalc) {
            case "Y":
                if (this.operation === SPPOperation.SUM) {
                    v = 0;
                    for (const c of this._children) {
                        const cRes = c.Calc("Y");
                        if (cRes.ok) {
                            v += cRes.vCalc;
                        }
                        else {
                            const m = new message_1.Message(message_1.MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                            m.extraVar.number = String(c.findPositionInParent() + 1); // String to avoid decimals
                            this.result.log.add(m);
                            this.result.log.addLog(cRes.log);
                            return this.result;
                        }
                    }
                }
                else if (this.operation === SPPOperation.PRODUCT) {
                    v = 1;
                    for (const c of this._children) {
                        const cRes = c.Calc("Y");
                        if (cRes.ok) {
                            v *= cRes.vCalc;
                        }
                        else {
                            const m = new message_1.Message(message_1.MessageCode.ERROR_SOMETHING_FAILED_IN_CHILD);
                            m.extraVar.number = String(c.findPositionInParent() + 1); // String to avoid decimals
                            this.result.log.add(m);
                            this.result.log.addLog(cRes.log);
                            return this.result;
                        }
                    }
                }
                break;
            default:
                throw new Error("YAXN.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    /** paramétrage de la calculabilité des paramètres */
    setParametersCalculability() {
        this.prms.Y.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.SPP = SPP;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/message":151,"../util/result":155}],26:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SPPParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * Y = Σ ou π (a1.x1^p1, … an.xn^pn)
 */
class SPPParams extends params_equation_1.ParamsEquation {
    constructor(rY) {
        super();
        this._Y = new param_definition_1.ParamDefinition(this, "Y", param_domain_1.ParamDomainValue.ANY, undefined, rY, param_definition_1.ParamFamily.ANY);
        this.addParamDefinition(this._Y);
    }
    get Y() {
        return this._Y;
    }
}
exports.SPPParams = SPPParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],27:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Trigo = exports.TrigoUnit = exports.TrigoOperation = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const result_1 = require("../util/result");
var TrigoOperation;
(function (TrigoOperation) {
    TrigoOperation[TrigoOperation["COS"] = 0] = "COS";
    TrigoOperation[TrigoOperation["SIN"] = 1] = "SIN";
    TrigoOperation[TrigoOperation["TAN"] = 2] = "TAN";
    TrigoOperation[TrigoOperation["COSH"] = 3] = "COSH";
    TrigoOperation[TrigoOperation["SINH"] = 4] = "SINH";
    TrigoOperation[TrigoOperation["TANH"] = 5] = "TANH";
})(TrigoOperation = exports.TrigoOperation || (exports.TrigoOperation = {}));
var TrigoUnit;
(function (TrigoUnit) {
    TrigoUnit[TrigoUnit["DEG"] = 0] = "DEG";
    TrigoUnit[TrigoUnit["RAD"] = 1] = "RAD"; // Radians
})(TrigoUnit = exports.TrigoUnit || (exports.TrigoUnit = {}));
/**
 * cos, sin, tan, cosh, sinh, tanh
 */
class Trigo extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Trigo;
        this._defaultCalculatedParam = prms.Y;
        this.resetDefaultCalculatedParam();
        this.properties.addObserver(this);
        this.operation = TrigoOperation.COS;
        this.unit = TrigoUnit.DEG;
    }
    /** paramètres castés au bon type */
    get prms() {
        return this._prms;
    }
    get operation() {
        return this.properties.getPropValue("trigoOperation");
    }
    set operation(o) {
        this.properties.setPropValue("trigoOperation", o);
    }
    get unit() {
        return this.properties.getPropValue("trigoUnit");
    }
    set unit(u) {
        this.properties.setPropValue("trigoUnit", u);
    }
    Equation(sVarCalc) {
        let v;
        switch (sVarCalc) {
            case "Y":
                let xv = this.prms.X.v;
                if (this.unit === TrigoUnit.DEG) {
                    xv = xv * Math.PI / 180;
                }
                switch (this.operation) {
                    case TrigoOperation.COS:
                        v = Math.cos(xv);
                        break;
                    case TrigoOperation.SIN:
                        v = Math.sin(xv);
                        break;
                    case TrigoOperation.TAN:
                        v = Math.tan(xv);
                        break;
                    case TrigoOperation.COSH:
                        v = Math.cosh(xv);
                        break;
                    case TrigoOperation.SINH:
                        v = Math.sinh(xv);
                        break;
                    case TrigoOperation.TANH:
                        v = Math.tanh(xv);
                        break;
                    default:
                        throw new Error("Trigo.Equation() : invalid operation " + this.operation);
                }
                break;
            case "X":
                const yv = this.prms.Y.v;
                switch (this.operation) {
                    case TrigoOperation.COS:
                        v = Math.acos(yv);
                        break;
                    case TrigoOperation.SIN:
                        v = Math.asin(yv);
                        break;
                    case TrigoOperation.TAN:
                        v = Math.atan(yv);
                        break;
                    case TrigoOperation.COSH:
                        v = Math.acosh(yv);
                        break;
                    case TrigoOperation.SINH:
                        v = Math.asinh(yv);
                        break;
                    case TrigoOperation.TANH:
                        v = Math.atanh(yv);
                        break;
                    default:
                        throw new Error("Trigo.Equation() : invalid operation " + this.operation);
                }
                if (this.unit === TrigoUnit.DEG) {
                    v = v / Math.PI * 180;
                }
                break;
            default:
                throw new Error("Trigo.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    // interface Observer
    update(sender, data) {
        if (data.action === "propertyChange") {
            switch (data.name) {
                case "trigoOperation":
                    // adjust definition domains
                    const un = this.unit;
                    switch (this.operation) {
                        case TrigoOperation.COS:
                        case TrigoOperation.SIN:
                            // [-1,1]
                            this.prms.Y.setDomain(new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, -1, 1));
                            break;
                        case TrigoOperation.TANH:
                            // ]-1,1[
                            // @TODO manage boundaries exclusion in Interval
                            this.prms.Y.setDomain(new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, -0.99999999, 0.99999999));
                            break;
                        case TrigoOperation.TAN:
                        case TrigoOperation.SINH:
                            // R
                            this.prms.Y.setDomain(param_domain_1.ParamDomainValue.ANY);
                            break;
                        case TrigoOperation.COSH:
                            // [1,+∞[
                            this.prms.Y.setDomain(new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 1, Infinity));
                            break;
                    }
                    break;
            }
        }
    }
    /** paramétrage de la calculabilité des paramètres */
    setParametersCalculability() {
        this.prms.Y.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.X.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.Trigo = Trigo;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../param/param-domain":87,"../util/result":155}],28:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrigoParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * Y = f(X)
 */
class TrigoParams extends params_equation_1.ParamsEquation {
    constructor(rY, rX) {
        super();
        this._Y = new param_definition_1.ParamDefinition(this, "Y", param_domain_1.ParamDomainValue.ANY, undefined, rY, param_definition_1.ParamFamily.ANY);
        this._X = new param_definition_1.ParamDefinition(this, "X", param_domain_1.ParamDomainValue.ANY, undefined, rX, param_definition_1.ParamFamily.ANY);
        this.addParamDefinition(this._Y);
        this.addParamDefinition(this._X);
    }
    get Y() {
        return this._Y;
    }
    get X() {
        return this._X;
    }
}
exports.TrigoParams = TrigoParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],29:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YAXB = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
/**
 * Y = A.X + B
 */
class YAXB extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.YAXB;
        this._defaultCalculatedParam = prms.Y;
        this.resetDefaultCalculatedParam();
    }
    /** paramètres castés au bon type */
    get prms() {
        return this._prms;
    }
    Equation(sVarCalc) {
        let v;
        switch (sVarCalc) {
            case "Y":
                v = this.prms.A.v * this.prms.X.v + this.prms.B.v;
                break;
            case "A":
                if (this.prms.X.v === 0) {
                    const m = new message_1.Message(message_1.MessageCode.ERROR_DIVISION_BY_ZERO);
                    m.extraVar.symbol = "X";
                    return new result_1.Result(m);
                }
                v = (this.prms.Y.v - this.prms.B.v) / this.prms.X.v;
                break;
            case "X":
                if (this.prms.A.v === 0) {
                    const m = new message_1.Message(message_1.MessageCode.ERROR_DIVISION_BY_ZERO);
                    m.extraVar.symbol = "A";
                    return new result_1.Result(m);
                }
                v = (this.prms.Y.v - this.prms.B.v) / this.prms.A.v;
                break;
            case "B":
                v = this.prms.Y.v - (this.prms.A.v * this.prms.X.v);
                break;
            default:
                throw new Error("YAXB.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    /** paramétrage de la calculabilité des paramètres */
    setParametersCalculability() {
        this.prms.Y.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.A.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.X.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.B.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.YAXB = YAXB;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/message":151,"../util/result":155}],30:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YAXBParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * Y = A.X + B
 */
class YAXBParams extends params_equation_1.ParamsEquation {
    constructor(rY, rA, rX, rB) {
        super();
        this._Y = new param_definition_1.ParamDefinition(this, "Y", param_domain_1.ParamDomainValue.ANY, undefined, rY, param_definition_1.ParamFamily.ANY);
        this._A = new param_definition_1.ParamDefinition(this, "A", param_domain_1.ParamDomainValue.ANY, undefined, rA, param_definition_1.ParamFamily.ANY);
        this._X = new param_definition_1.ParamDefinition(this, "X", param_domain_1.ParamDomainValue.ANY, undefined, rX, param_definition_1.ParamFamily.ANY);
        this._B = new param_definition_1.ParamDefinition(this, "B", param_domain_1.ParamDomainValue.ANY, undefined, rB, param_definition_1.ParamFamily.ANY);
        this.addParamDefinition(this._Y);
        this.addParamDefinition(this._A);
        this.addParamDefinition(this._X);
        this.addParamDefinition(this._B);
    }
    get Y() {
        return this._Y;
    }
    get A() {
        return this._A;
    }
    get X() {
        return this._X;
    }
    get B() {
        return this._B;
    }
}
exports.YAXBParams = YAXBParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],31:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YAXN = void 0;
const child_nub_1 = require("../child_nub");
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
/**
 * Y = A.X^N
 */
class YAXN extends child_nub_1.ChildNub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.YAXN;
        this._defaultCalculatedParam = prms.Y;
        this.resetDefaultCalculatedParam();
    }
    /** paramètres castés au bon type */
    get prms() {
        return this._prms;
    }
    Equation(sVarCalc) {
        let v;
        switch (sVarCalc) {
            case "Y":
                if (this.prms.X.v < 0 && !Number.isInteger(this.prms.N.v)) {
                    const m = new message_1.Message(message_1.MessageCode.ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER);
                    m.extraVar.X = this.prms.X.v;
                    m.extraVar.N = this.prms.N.v;
                    return new result_1.Result(m);
                }
                v = this.prms.A.v * Math.pow(this.prms.X.v, this.prms.N.v);
                break;
            default:
                throw new Error("YAXN.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    /** paramétrage de la calculabilité des paramètres */
    setParametersCalculability() {
        this.prms.Y.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.X.calculability = param_definition_1.ParamCalculability.DICHO;
    }
}
exports.YAXN = YAXN;

},{"../child_nub":4,"../compute-node":5,"../param/param-definition":86,"../util/message":151,"../util/result":155}],32:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YAXNParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * Y = A.X^N
 */
class YAXNParams extends params_equation_1.ParamsEquation {
    constructor(rA, rX, rN, rY) {
        super();
        this._A = new param_definition_1.ParamDefinition(this, "A", param_domain_1.ParamDomainValue.ANY, undefined, rA, param_definition_1.ParamFamily.ANY);
        this._X = new param_definition_1.ParamDefinition(this, "X", param_domain_1.ParamDomainValue.ANY, undefined, rX, param_definition_1.ParamFamily.ANY);
        this._N = new param_definition_1.ParamDefinition(this, "N", param_domain_1.ParamDomainValue.ANY, undefined, rN, param_definition_1.ParamFamily.ANY);
        this._Y = new param_definition_1.ParamDefinition(this, "Y", param_domain_1.ParamDomainValue.ANY, undefined, rY, undefined, false);
        this.addParamDefinition(this._A);
        this.addParamDefinition(this._X);
        this.addParamDefinition(this._N);
        this.addParamDefinition(this._Y);
    }
    get Y() {
        return this._Y;
    }
    get A() {
        return this._A;
    }
    get X() {
        return this._X;
    }
    get N() {
        return this._N;
    }
}
exports.YAXNParams = YAXNParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],33:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Nub = void 0;
const compute_node_1 = require("./compute-node");
const dichotomie_1 = require("./dichotomie");
const index_1 = require("./index");
const linked_value_1 = require("./linked-value");
const param_definition_1 = require("./param/param-definition");
const param_value_mode_1 = require("./param/param-value-mode");
const param_definition_iterator_1 = require("./param/param_definition_iterator");
const params_equation_array_iterator_1 = require("./param/params_equation_array_iterator");
const props_1 = require("./props");
const session_settings_1 = require("./session_settings");
const message_1 = require("./util/message");
const observer_1 = require("./util/observer");
const result_1 = require("./util/result");
const resultelement_1 = require("./util/resultelement");
/**
 * Classe abstraite de Noeud de calcul dans une session :
 * classe de base pour tous les calculs
 */
class Nub extends compute_node_1.ComputeNode {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        /** paramétrage de la dichotomie */
        this.dichoStartIntervalMaxSteps = 100;
        /** type of children elements, used by GUI for translation */
        this._childrenType = "";
        /** properties describing the Nub type */
        this._props = new props_1.Props();
        /** a rough indication of calculation progress, between 0 and 100 */
        this._progress = 0;
        /** allows notifying of progress every X milliseconds only */
        this.previousNotificationTimestamp = 0;
        this.deleteAllChildren();
        this._observable = new observer_1.Observable();
        this._defaultCalculatedParam = this.getFirstAnalyticalParameter();
        this.resetDefaultCalculatedParam();
    }
    /**
     * Find longest series, BUT: if any varying parameter is a calculation result,
     * its values can't be extended (could be, but not at the moment), so
     * the global size in this case will be the min of sizes of linked variated results
     * @param variated list of variated parameter/valuesList pairs to examinate
     */
    static findVariatedSize(variated) {
        if (variated.length > 0) {
            let minLinkedResultParam;
            let minLinkedResultsSize = Infinity;
            let longest = 0;
            for (let i = 0; i < variated.length; i++) {
                if (variated[i].param.valueMode === param_value_mode_1.ParamValueMode.LINK
                    && variated[i].param.referencedValue.isCalculated()) {
                    if (variated[i].values.valuesIterator.count() < minLinkedResultsSize) {
                        minLinkedResultParam = variated[i];
                        minLinkedResultsSize = minLinkedResultParam.values.valuesIterator.count();
                    }
                }
                if (variated[i].values.valuesIterator.count() > variated[longest].values.valuesIterator.count()) {
                    longest = i;
                }
            }
            return {
                size: variated[longest].values.valuesIterator.count(),
                longest,
                minLinkedResultParam
            };
        }
        else {
            return {
                size: 0,
                longest: undefined,
                minLinkedResultParam: undefined
            };
        }
    }
    static concatPrms(p1, p2) {
        const p3 = p1;
        for (const p of p2) {
            p3.push(p);
        }
        return p3;
    }
    get childrenType() {
        return this._childrenType;
    }
    get result() {
        return this._result;
    }
    /**
     * Local setter to set results of Equation() / Solve() / …  as current
     * ResultElement, instead of overwriting the whole Result object
     * (used by CalcSerie with varying parameters)
     */
    set currentResult(r) {
        if (!this._result) {
            this.initNewResultElement();
        }
        this._result.resultElement = r.resultElement;
    }
    /** Returns Props object (observable set of key-values) associated to this Nub */
    get properties() {
        // completes props with calcType if not already set
        this._props.setPropValue("calcType", this.calcType);
        return this._props;
    }
    set properties(props) {
        this.setProperties(props);
    }
    /**
     * return ParamsEquation of all children recursively
     */
    get childrenPrms() {
        const prms = [];
        if (this._children.length) { // if called within constructor, default class member value is not set yet
            for (const child of this._children) {
                prms.push(child.prms);
                if (child.getChildren()) {
                    if (child.getChildren().length) {
                        Nub.concatPrms(prms, child.childrenPrms);
                    }
                }
            }
        }
        return prms;
    }
    /**
     * Returns an array with the calculable parameters
     */
    get calculableParameters() {
        const calcPrms = [];
        for (const p of this.parameterIterator) {
            if ([param_definition_1.ParamCalculability.DICHO, param_definition_1.ParamCalculability.EQUATION].includes(p.calculability)) {
                calcPrms.push(p);
            }
        }
        return calcPrms;
    }
    /**
     * Returns an iterator over :
     *  - own parameters (this._prms)
     *  - children parameters (this._children[*]._prms)
     */
    get parameterIterator() {
        const prms = [];
        prms.push(this._prms);
        if (this._children) {
            Nub.concatPrms(prms, this.childrenPrms);
        }
        return new params_equation_array_iterator_1.ParamsEquationArrayIterator(prms);
    }
    get progress() {
        return this._progress;
    }
    /**
     * Updates the progress percentage and notifies observers,
     * at most once per 300ms
     */
    set progress(v) {
        this._progress = v;
        const currentTime = new Date().getTime();
        if ((currentTime - this.previousNotificationTimestamp) > 30
            || v === 100) {
            // console.log(">> notifying !");
            this.notifyProgressUpdated();
            this.previousNotificationTimestamp = currentTime;
        }
    }
    get calcType() {
        return this._calcType;
    }
    get calculatedParam() {
        return this._calculatedParam;
    }
    /**
     * Sets p as the parameter to be computed; sets it to CALC mode
     */
    set calculatedParam(p) {
        this._calculatedParam = p;
        this._calculatedParam.valueMode = param_value_mode_1.ParamValueMode.CALCUL;
    }
    /**
     * Returns a parameter descriptor compatible with Calc() methods,
     * ie. a symbol string for a Nub's main parameter, or an object
     * of the form { uid: , symbol: } for a Nub's sub-Nub parameter
     * (ex: Structure parameter in ParallelStructure)
     */
    get calculatedParamDescriptor() {
        if (this.uid === this._calculatedParam.nubUid) {
            return this._calculatedParam.symbol;
        }
        else {
            return {
                uid: this._calculatedParam.nubUid,
                symbol: this._calculatedParam.symbol
            };
        }
    }
    // move code out of setter to ease inheritance
    setProperties(props, resetProps = false) {
        // copy observers
        const observers = this._props.getObservers();
        // empty props
        if (resetProps) {
            this._props.reset();
        }
        // restore observers
        for (const obs of observers) {
            this._props.addObserver(obs);
        }
        // set new props values
        let error;
        for (const p of Object.keys(props.props)) {
            // try properties one by one so that if an error is thrown,
            // remaining properties are still copied
            let oldValue;
            try {
                // keep old value to restore it in case of an error, or else it breaks fixTargets()
                oldValue = this._props.getPropValue(p); // should always be undefined but who knows
                this._props.setPropValue(p, props.getPropValue(p));
            }
            catch (e) {
                error = e;
                // no one will notice ^^
                this._props.setPropValue(p, oldValue);
            }
        }
        // throw caught error if any
        if (error) {
            throw error;
        }
    }
    /**
     * Finds the previous calculated parameter and sets its mode to SINGLE
     */
    unsetCalculatedParam(except) {
        for (const p of this.parameterIterator) {
            if (p.valueMode === param_value_mode_1.ParamValueMode.CALCUL
                && p !== except) {
                p.setValueMode(param_value_mode_1.ParamValueMode.SINGLE, false);
            }
        }
    }
    /**
     * Tries to reset the calculated parameter, successively, to :
     *  - the default one if it is in SINGLE mode
     *  - the first SINGLE calculable parameter other than requirer
     *  - the first MINMAX/LISTE calculable parameter other than requirer
     *
     * If no default calculated parameter is defined, does nothing.
     */
    resetDefaultCalculatedParam(requirer) {
        if (this._defaultCalculatedParam) {
            // if default calculated param is not eligible to CALC mode
            if (requirer === this._defaultCalculatedParam
                || this._defaultCalculatedParam.valueMode === param_value_mode_1.ParamValueMode.LINK) {
                // first SINGLE calculable parameter if any
                const newCalculatedParam = this.findFirstCalculableParameter(requirer);
                if (newCalculatedParam) {
                    this.calculatedParam = newCalculatedParam;
                }
                else {
                    // @TODO throws when a new linkable nub is added, and all parameters are already linked !
                    throw Error("resetDefaultCalculatedParam : could not find any SINGLE/MINMAX/LISTE parameter");
                }
            }
            else {
                // default one
                this.calculatedParam = this._defaultCalculatedParam;
            }
        }
        else {
            // do nothing (ex: Section Paramétrée)
        }
    }
    /**
     * Returns the first visible calculable parameter other than otherThan,
     * that is set to SINGLE, MINMAX or LISTE mode (there might be none)
     */
    findFirstCalculableParameter(otherThan) {
        for (const p of this.parameterIterator) {
            if ([param_definition_1.ParamCalculability.EQUATION, param_definition_1.ParamCalculability.DICHO].includes(p.calculability)
                && p.visible
                && p !== otherThan
                && [param_value_mode_1.ParamValueMode.SINGLE, param_value_mode_1.ParamValueMode.MINMAX, param_value_mode_1.ParamValueMode.LISTE].includes(p.valueMode)) {
                return p;
            }
        }
        return undefined;
    }
    /**
     * Calculate and put in cache the symbol of first parameter calculable analytically
     */
    get firstAnalyticalPrmSymbol() {
        if (this._firstAnalyticalPrmSymbol === undefined) {
            this._firstAnalyticalPrmSymbol = this.getFirstAnalyticalParameter().symbol;
        }
        return this._firstAnalyticalPrmSymbol;
    }
    /**
     * Run Equation with first analytical parameter to compute
     * Returns the result in number form
     */
    EquationFirstAnalyticalParameter() {
        const res = this.Equation(this.firstAnalyticalPrmSymbol);
        if (!res.ok) {
            this._result = res;
            throw new Error(this.constructor.name + ".EquationFirstAnalyticalParameter(): fail");
        }
        return res.vCalc;
    }
    /**
     * Calcul d'une équation quelle que soit l'inconnue à calculer; déclenche le calcul en
     * chaîne des modules en amont si nécessaire
     * @param sVarCalc nom de la variable à calculer
     * @param rInit valeur initiale de la variable à calculer dans le cas de la dichotomie
     */
    Calc(sVarCalc, rInit) {
        let computedVar;
        // overload calculated param at execution time ?
        if (sVarCalc) {
            computedVar = this.getParameter(sVarCalc);
        }
        else {
            computedVar = this.calculatedParam;
        }
        if (rInit === undefined) {
            rInit = computedVar.initValue;
        }
        if (computedVar.isAnalytical()) {
            this.currentResult = this.Equation(sVarCalc);
            this._result.symbol = sVarCalc;
            return this._result;
        }
        const resSolve = this.Solve(sVarCalc, rInit);
        if (!resSolve.ok) {
            this.currentResult = resSolve;
            this._result.symbol = sVarCalc;
            return this._result;
        }
        const sAnalyticalPrm = this.getFirstAnalyticalParameter().symbol;
        computedVar.v = resSolve.vCalc;
        const res = this.Equation(sAnalyticalPrm);
        res.vCalc = resSolve.vCalc;
        this.currentResult = res;
        this._result.symbol = sVarCalc;
        this.notifyResultUpdated();
        return this._result;
    }
    /**
     * Calculates required Nubs so that all input data is available;
     * uses 50% of the progress
     * @returns true if everything went OK, false otherwise
     */
    triggerChainCalculation() {
        const requiredNubs1stLevel = this.getRequiredNubs();
        if (requiredNubs1stLevel.length > 0) {
            const progressStep = Nub.progressPercentageAccordedToChainCalculation / requiredNubs1stLevel.length;
            for (const rn of requiredNubs1stLevel) {
                const r = rn.CalcSerie();
                if (r.hasGlobalError() || r.hasOnlyErrors) {
                    // something has failed in chain
                    return {
                        ok: false,
                        message: new message_1.Message(message_1.MessageCode.ERROR_IN_CALC_CHAIN)
                    };
                }
                else if (this.resultHasMultipleValues
                    && (r.hasErrorMessages() // some steps failed
                        // or upstream Nub has already triggered a warning message; pass it on
                        || r.globalLog.contains(message_1.MessageCode.WARNING_ERROR_IN_CALC_CHAIN_STEPS))) {
                    // if a parameter varies, errors might have occurred for
                    // certain steps (but not all steps)
                    return {
                        ok: true,
                        message: new message_1.Message(message_1.MessageCode.WARNING_ERROR_IN_CALC_CHAIN_STEPS)
                    };
                }
                this.progress += progressStep;
            }
            // round progress to accorded percentage
            this.progress = Nub.progressPercentageAccordedToChainCalculation;
        }
        return {
            ok: true,
            message: undefined
        };
    }
    /**
     * Returns a list of parameters that are fixed, either because their valueMode
     * is SINGLE, or because their valueMode is LINK and the reference is
     * defined and fixed. Does not take calculated parameters into account.
     */
    findFixedParams() {
        const fixed = [];
        for (const p of this.parameterIterator) {
            if (p.visible
                && (p.valueMode === param_value_mode_1.ParamValueMode.SINGLE
                    || (p.valueMode === param_value_mode_1.ParamValueMode.LINK
                        && p.isReferenceDefined()
                        && !p.referencedValue.hasMultipleValues()))) {
                fixed.push(p);
            }
        }
        return fixed;
    }
    /**
     * Returns a list of parameters that are variating, either because their valueMode
     * is LISTE or MINMAX, or because their valueMode is LINK and the reference is
     * defined and variated. Does not take calculated parameters into account.
     */
    findVariatedParams() {
        const variated = [];
        for (const p of this.parameterIterator) {
            if (p.valueMode === param_value_mode_1.ParamValueMode.LISTE
                || p.valueMode === param_value_mode_1.ParamValueMode.MINMAX
                || (p.valueMode === param_value_mode_1.ParamValueMode.LINK
                    && p.isReferenceDefined()
                    && p.referencedValue.hasMultipleValues())) {
                variated.push({
                    param: p,
                    // extract variated values from variated Parameters
                    // (in LINK mode, proxies to target data)
                    values: p.paramValues
                });
            }
        }
        return variated;
    }
    /**
     * Effectue une série de calculs sur un paramètre; déclenche le calcul en chaîne
     * des modules en amont si nécessaire
     * @param rInit solution approximative du paramètre
     */
    CalcSerie(rInit) {
        // prepare calculation
        let extraLogMessage; // potential chain calculation warning to add to result at the end
        this.progress = 0;
        this.resetResult();
        const ccRes = this.triggerChainCalculation();
        if (ccRes.ok) {
            // might still have a warning log
            if (ccRes.message !== undefined) {
                extraLogMessage = ccRes.message;
            }
        }
        else {
            // something went wrong in the chain
            this._result = new result_1.Result(undefined, this);
            this._result.globalLog.add(ccRes.message);
            return this._result;
        }
        this.copySingleValuesToSandboxValues();
        // variated parameters caracteristics
        const variated = this.findVariatedParams();
        // find calculated parameter
        const computedSymbol = this.findCalculatedParameter();
        if (rInit === undefined && this.calculatedParam) {
            rInit = this.calculatedParam.v;
        }
        // reinit Result and children Results
        this.reinitResult();
        if (variated.length === 0) { // no parameter is varying
            // prepare a new slot to store results
            this.initNewResultElement();
            this.doCalc(computedSymbol, rInit); // résultat dans this.currentResult (resultElement le plus récent)
            this.progress = 100;
        }
        else { // at least one parameter is varying
            const fvsRes = Nub.findVariatedSize(variated);
            let size = fvsRes.size;
            const longest = fvsRes.longest;
            // if at least one linked variated result was found
            if (fvsRes.minLinkedResultParam !== undefined) {
                // if the size limited by linked variated results is shorter
                // than the size of the longest variating element, limit it
                if (fvsRes.minLinkedResultParam.values.valuesIterator.count() < size) {
                    size = fvsRes.minLinkedResultParam.values.valuesIterator.count();
                    // and add a warning
                    const m = new message_1.Message(message_1.MessageCode.WARNING_VARIATED_LENGTH_LIMITED_BY_LINKED_RESULT);
                    m.extraVar.symbol = fvsRes.minLinkedResultParam.param.symbol;
                    m.extraVar.size = size;
                    this._result.globalLog.add(m);
                }
            }
            // grant the remaining percentage of the progressbar to local calculation
            // (should be 50% if any chain calculation occurred, 100% otherwise)
            let progressStep;
            const remainingProgress = 100 - this.progress;
            progressStep = remainingProgress / size;
            // (re)init all iterators
            for (const v of variated) {
                // copy iterators, for linked params @see bug #222
                if (v === variated[longest]) {
                    v.iterator = v.values.initValuesIterator(false, undefined, true);
                }
                else {
                    v.iterator = v.values.initValuesIterator(false, size, true);
                }
            }
            // iterate over longest series (in fact any series would do)
            let l = 0; // extra counter if size is limited by linked variated results
            while (variated[longest].values.hasNext && l < size) {
                // get next value for all variating parameters
                for (const v of variated) {
                    const currentIteratorValue = v.iterator.nextValue();
                    v.param.v = currentIteratorValue.value;
                }
                // prepare a new slot to store results
                this.initNewResultElement();
                // calculate
                this.doCalc(computedSymbol, rInit); // résultat dans this.currentResult (resultElement le plus récent)
                if (this._result.resultElement.ok) {
                    rInit = this._result.resultElement.vCalc;
                }
                // update progress
                this.progress += progressStep;
                l++;
            }
            // round progress to 100%
            this.progress = 100;
            // abstract of iterations logs
            let errors = 0;
            let warnings = 0;
            for (const re of this._result.resultElements) {
                for (const lm of re.log.messages) {
                    if (lm.getSeverity() === message_1.MessageSeverity.ERROR) {
                        errors++;
                    }
                    else if (lm.getSeverity() === message_1.MessageSeverity.WARNING) {
                        warnings++;
                    }
                }
            }
            // String()ify numbers below, to avoid decimals formatting on screen (ex: "3.000 errors encoutered...")
            if (errors > 0) {
                if (errors > 1) {
                    this._result.globalLog.add(new message_1.Message(message_1.MessageCode.WARNING_ERRORS_ABSTRACT_PLUR, { nb: String(errors) }));
                }
                else {
                    this._result.globalLog.add(new message_1.Message(message_1.MessageCode.WARNING_ERRORS_ABSTRACT, { nb: String(errors) }) // should always be "1"
                    );
                }
            }
            if (warnings > 0) {
                this._result.globalLog.add(new message_1.Message(message_1.MessageCode.WARNING_WARNINGS_ABSTRACT, { nb: String(warnings) }));
            }
        }
        if (computedSymbol !== undefined) {
            const realSymbol = (typeof computedSymbol === "string") ? computedSymbol : computedSymbol.symbol;
            this._result.symbol = realSymbol;
        }
        this.notifyResultUpdated();
        if (extraLogMessage !== undefined) {
            this._result.globalLog.add(extraLogMessage);
        }
        return this._result;
    }
    addChild(child, after) {
        if (after !== undefined) {
            this._children.splice(after + 1, 0, child);
        }
        else {
            this._children.push(child);
        }
        // add reference to parent collection (this)
        child.parent = this;
        // postprocessing
        this.adjustChildParameters(child);
    }
    getChildren() {
        return this._children;
    }
    getParent() {
        return this.parent;
    }
    /**
     * Returns true if all parameters are valid; used to check validity of
     * parameters linked to Nub results
     */
    isComputable() {
        let valid = true;
        for (const p of this.prms) {
            valid = valid && p.isValid;
        }
        return valid;
    }
    /**
     * Liste des valeurs (paramètre, résultat, résultat complémentaire) liables à un paramètre
     * @param src paramètre auquel lier d'autres valeurs
     */
    getLinkableValues(src) {
        let res = [];
        const symbol = src.symbol;
        // If parameter comes from the same Nub, its parent or any of its children,
        // no linking is possible at all.
        // Different Structures in the same parent can get linked to each other.
        if (!this.isParentOrChildOf(src.nubUid)
            && ( // check grand-parent for PreBarrage special case @TODO find a generic way
            // to perform both tests (synthesise .nubUid and .originNub.uid)
            !(this._calcType === compute_node_1.CalculatorType.PreBarrage)
                || !this.isParentOrChildOf(src.originNub.uid))) {
            // 1. own parameters
            for (const p of this._prms) {
                // if symbol and Nub type are identical
                if ((p.symbol === symbol && this.calcType === src.nubCalcType && p.visible)
                    // or if this is a Section, and src direct parent is a Section too, and symbol is identical
                    || (p.symbol === symbol
                        && this instanceof index_1.acSection && src.getParentComputeNode(false) instanceof index_1.acSection
                        && p.visible // different types of sections hide certain parameters
                    )
                    // or if family is identical
                    || (p.family !== undefined && p.family === src.family && p.visible)
                    // or if one of the families is ANY and target parameter is visible
                    || ((src.family === param_definition_1.ParamFamily.ANY || p.family === param_definition_1.ParamFamily.ANY)
                        && p.visible)) {
                    // if variability doesn't cause any problem (a non-variable
                    // parameter cannot be linked to a variating one)
                    if (src.calculability !== param_definition_1.ParamCalculability.FIXED || !p.hasMultipleValues) {
                        // if it is safe to link p's value to src
                        if (p.isLinkableTo(src)) {
                            // if p is a CALC param of a Structure other than "Q"
                            // (structures always have Q as CALC param and cannot have another)
                            // or a CALC param of a Section, that is not sibling of the target
                            // (to prevent circular dependencies among ParallelStructures),
                            // expose its parent
                            if (((this instanceof index_1.Structure && p.symbol !== "Q" && !this.isSiblingOf(src.nubUid))
                                || this instanceof index_1.acSection)
                                && (p.valueMode === param_value_mode_1.ParamValueMode.CALCUL)) {
                                // trick to expose p as a result of the parent Nub
                                res.push(new linked_value_1.LinkedValue(this.parent, p, p.symbol));
                            }
                            else {
                                // do not suggest parameters that are already linked to another one
                                if (p.valueMode !== param_value_mode_1.ParamValueMode.LINK) {
                                    res.push(new linked_value_1.LinkedValue(this, p, p.symbol));
                                }
                            }
                        }
                    }
                }
            }
            // 2. extra results
            if (this._resultsFamilies) {
                // if I don't depend on your result, you may depend on mine !
                if (!this.dependsOnNubResult(src.parentNub)) {
                    const erk = Object.keys(this._resultsFamilies);
                    // browse extra results
                    for (const erSymbol of erk) {
                        const erFamily = this._resultsFamilies[erSymbol];
                        // if family is identical or ANY, and variability doesn't cause any problem
                        if ((( // both families cannot be undefined
                        erFamily === src.family
                            && erFamily !== undefined)
                            || erFamily === param_definition_1.ParamFamily.ANY
                            || src.family === param_definition_1.ParamFamily.ANY)
                            && (src.calculability !== param_definition_1.ParamCalculability.FIXED
                                || !this.resultHasMultipleValues)) {
                            res.push(new linked_value_1.LinkedValue(this, undefined, erSymbol));
                        }
                    }
                }
            }
        }
        // 3. children Nubs, except for PAB and MRC and PreBarrage
        if (!(this instanceof index_1.Pab || this instanceof index_1.MacrorugoCompound || this._calcType === compute_node_1.CalculatorType.PreBarrage)) {
            for (const cn of this.getChildren()) {
                res = res.concat(cn.getLinkableValues(src));
            }
        }
        return res;
    }
    /**
     * Returns true if the given Nub UID is either of :
     *  - the current Nub UID
     *  - the current Nub's parent Nub UID
     *  - the UID of any of the current Nub's children
     */
    isParentOrChildOf(uid) {
        if (this.uid === uid) {
            return true;
        }
        const parent = this.getParent();
        if (parent && parent.uid === uid) {
            return true;
        }
        for (const c of this.getChildren()) {
            if (c.uid === uid) {
                return true;
            }
        }
        return false;
    }
    /**
     * Returns true if the given Nub UID :
     *  - is the current Nub UID
     *  - is the UID of any of the current Nub's siblings (children of the same parent)
     */
    isSiblingOf(uid) {
        if (this.uid === uid) {
            return true;
        }
        const parent = this.getParent();
        if (parent) {
            for (const c of parent.getChildren()) {
                if (c.uid === uid) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }
    /**
     * Returns all Nubs whose results are required by the given one,
     * without following links (stops when it finds a Nub that has to
     * be calculated). Used to trigger chain calculation.
     */
    getRequiredNubs(visited = []) {
        let requiredNubs = [];
        // prevent loops
        if (!visited.includes(this.uid)) {
            visited.push(this.uid);
            // inspect all target Nubs
            for (const p of this.parameterIterator) {
                if (p.valueMode === param_value_mode_1.ParamValueMode.LINK && p.isReferenceDefined()) {
                    const nub = p.referencedValue.nub;
                    // a Nub is required if I depend on its result
                    if (this.dependsOnNubResult(nub, false)) {
                        requiredNubs.push(nub);
                    }
                }
            }
        }
        // 1. deduplicate
        requiredNubs = requiredNubs.filter((element, index, self) => self.findIndex((e) => e.uid === element.uid) === index);
        // 2. only keep requiredNubs that are not dependencies of others
        const realRequiredNubs = [];
        for (const rn of requiredNubs) {
            let keep = true;
            // test all combinations
            for (const rn2 of requiredNubs) {
                if (rn.uid !== rn2.uid) {
                    keep = (keep
                        && !rn2.dependsOnNubResult(rn, true));
                }
            }
            if (keep) {
                realRequiredNubs.push(rn);
            }
        }
        return realRequiredNubs;
    }
    /**
     * Returns all Nubs whose results are required by the given one,
     * following links. Used by Solveur.
     */
    getRequiredNubsDeep(visited = []) {
        let requiredNubs = this.getRequiredNubs(visited);
        for (const rn of requiredNubs) {
            requiredNubs = requiredNubs.concat(rn.getRequiredNubsDeep(visited));
        }
        requiredNubs = requiredNubs.filter((item, index) => requiredNubs.indexOf(item) === index // deduplicate
        );
        return requiredNubs;
    }
    /**
     * Returns all Nubs whose parameters or results are targetted
     * by the given one.
     * (used for dependencies checking at session saving time)
     */
    getTargettedNubs(visited = []) {
        const targettedNubs = [];
        // prevent loops
        if (!visited.includes(this.uid)) {
            visited.push(this.uid);
            // inspect all target Nubs
            for (const p of this.parameterIterator) {
                if (p.valueMode === param_value_mode_1.ParamValueMode.LINK && p.isReferenceDefined()) {
                    targettedNubs.push(p.referencedValue.nub);
                }
            }
        }
        return targettedNubs;
    }
    /**
     * Returns true if
     *  - this Nub
     *  - any of this Nub's children
     *  - this Nub's parent
     * depends on
     *  - the result of the given Nub
     *  - the result of any of the given Nub's children
     *  [ note: but NOT the result of the given Nub's parent, or the given Nub might
     *    be recalculated independently, which is not wanted ]
     *
     * (ie. "my family depends on the result of your family")
     *
     * If followLinksChain is false, will limit the exploration to the first target level only
     */
    dependsOnNubResult(nub, followLinksChain = true) {
        let thisFamily = [this];
        const tp = this.getParent();
        if (tp) {
            thisFamily = thisFamily.concat(tp);
        }
        thisFamily = thisFamily.concat(this.getChildren());
        let yourFamily = [];
        const you = nub;
        if (you) {
            yourFamily = [you].concat(you.getChildren());
        }
        let depends = false;
        outerloop: for (const t of thisFamily) {
            // look for a parameter linked to the result of any Nub of yourFamily
            for (const p of t.prms) {
                if (p.valueMode === param_value_mode_1.ParamValueMode.LINK) {
                    if (p.isLinkedToResultOfNubs(yourFamily, followLinksChain)) {
                        // dependence found !
                        depends = true;
                        break outerloop;
                    }
                }
            }
        }
        return depends;
    }
    /**
     * Returns true if this nub (parent and children included)
     * directly requires (1st level only)
     * the given parameter
     */
    dependsOnParameter(src) {
        let thisFamily = [this];
        const tp = this.getParent();
        if (tp) {
            thisFamily = thisFamily.concat(tp);
        }
        thisFamily = thisFamily.concat(this.getChildren());
        let depends = false;
        outerloop: for (const t of thisFamily) {
            // look for a parameter of thisFamily that is directly linked to src
            for (const p of t.prms) {
                if (p.valueMode === param_value_mode_1.ParamValueMode.LINK
                    && p.isReferenceDefined()
                    && p.referencedValue.isParameter()) {
                    if (p.referencedValue.nub.uid === src.nubUid
                        && p.referencedValue.symbol === src.symbol) {
                        // dependence found !
                        depends = true;
                        break outerloop;
                    }
                }
            }
        }
        return depends;
    }
    /**
     * Returns true if the computation of the current Nub (parent and children
     * included) directly requires (1st level only), anything (parameter or result)
     * from the given Nub UID "uid", its parent or any of its children
     * @param uid
     * @param symbol symbol of the target parameter whose value change triggered this method;
     *      if current Nub targets this symbol, it will be considered dependent
     * @param includeValuesLinks if true, even if this Nub targets only non-calculated non-modified
     *      parameters, it will be considered dependent @see jalhyd#98
     * @param includeOtherDependencies if true, then:
     *      - if this Nub is a Solveur, also returns true if ${uid} is either the X or the Ytarget's parent
     *        Nub of this Solveur
     *      - if this Nub is a Verificateur, also returns true if ${uid} is either a selected Custom
     *        Species or the Pass to check of this Verificateur
     */
    resultDependsOnNub(uid, visited = [], symbol, includeValuesLinks = false, includeOtherDependencies = false) {
        var _a, _b;
        if (uid !== this.uid && !visited.includes(this.uid)) {
            visited.push(this.uid);
            // does any of our parameters depend on the target Nub ?
            for (const p of this._prms) {
                if (p.valueMode === param_value_mode_1.ParamValueMode.LINK) {
                    if (p.dependsOnNubFamily(uid, symbol, includeValuesLinks)) {
                        return true;
                    }
                }
            }
            // does any of our parent's parameters depend on the target Nub ?
            const parent = this.getParent();
            if (parent) {
                if (parent.resultDependsOnNub(uid, visited, symbol, includeValuesLinks, includeOtherDependencies)) {
                    return true;
                }
            }
            // does any of our children' parameters depend on the target Nub ?
            for (const c of this.getChildren()) {
                if (c.resultDependsOnNub(uid, visited, symbol, includeValuesLinks, includeOtherDependencies)) {
                    return true;
                }
            }
            // other dependencies
            if (includeOtherDependencies) {
                // is this a Solveur
                if (this instanceof index_1.Solveur) {
                    return ((((_a = this.searchedParameter) === null || _a === void 0 ? void 0 : _a.nubUid) === uid)
                        || (((_b = this.nubToCalculate) === null || _b === void 0 ? void 0 : _b.uid) === uid));
                }
                else if (this instanceof index_1.Verificateur) {
                    return ((this.nubToVerify !== undefined && this.nubToVerify.uid === uid)
                        || (this.speciesList.includes(uid)));
                }
            }
        }
        return false;
    }
    /**
     * Returns true if the computation of the current Nub has multiple values
     * (whether it is already computed or not), by detecting if any parameter,
     * linked or not, is variated
     */
    resultHasMultipleValues() {
        let hmv = false;
        for (const p of this.parameterIterator) {
            if (p.valueMode === param_value_mode_1.ParamValueMode.MINMAX || p.valueMode === param_value_mode_1.ParamValueMode.LISTE) {
                hmv = true;
            }
            else if (p.valueMode === param_value_mode_1.ParamValueMode.LINK && p.isReferenceDefined()) {
                // indirect recursivity
                hmv = hmv || p.referencedValue.hasMultipleValues();
            }
        }
        return hmv;
    }
    /**
     * Sets the current result to undefined, as well as the results
     * of all depending Nubs; also invalidates all fake ParamValues
     * held by LinkedValues pointing to this result
     */
    resetResult(visited = []) {
        this._result = undefined;
        visited.push(this.uid);
        const dependingNubs = index_1.Session.getInstance().getDependingNubs(this.uid); // @TODO include Solveur / Verificateur links ?
        for (const dn of dependingNubs) {
            if (!visited.includes(dn.uid)) {
                dn.resetResult(visited);
            }
            dn.resetLinkedParamValues(this);
        }
    }
    /**
     * For all parameters pointing to the result of the given Nub,
     * invalidates fake ParamValues held by pointed LinkedValue
     */
    resetLinkedParamValues(nub) {
        for (const p of this.parameterIterator) {
            if (p.valueMode === param_value_mode_1.ParamValueMode.LINK && p.isReferenceDefined()) {
                if ((p.referencedValue.isResult() || p.referencedValue.isExtraResult())
                    && p.referencedValue.nub.uid === nub.uid) {
                    p.referencedValue.invalidateParamValues();
                }
            }
        }
    }
    /**
     * Duplicates the current Nub, but does not register it in the session
     */
    clone() {
        const serialised = this.serialise();
        const clone = index_1.Session.getInstance().unserialiseSingleNub(serialised, false);
        return clone.nub;
    }
    /**
     * Returns a JSON representation of the Nub's current state
     * @param extra extra key-value pairs, for ex. calculator title in GUI
     * @param nubUidsInSession UIDs of Nubs that will be saved in session along with this one;
     *                         useful to determine if linked parameters must be kept as links
     *                         or have their value copied (if target is not in UIDs list)
     */
    serialise(extra, nubUidsInSession) {
        return JSON.stringify(this.objectRepresentation(extra, nubUidsInSession));
    }
    /**
     * Returns an object representation of the Nub's current state
     * @param extra extra key-value pairs, for ex. calculator title in GUI
     * @param nubUidsInSession UIDs of Nubs that will be saved in session along with this one;
     *                         useful to determine if linked parameters must be kept as links
     *                         or have their value copied (if target is not in UIDs list)
     */
    objectRepresentation(extra, nubUidsInSession) {
        let ret = {
            uid: this.uid,
            props: index_1.Session.invertEnumKeysAndValuesInProperties(this.properties.props),
        };
        if (extra) {
            ret = Object.assign(Object.assign({}, ret), { meta: extra }); // merge properties
        }
        ret = Object.assign(Object.assign({}, ret), { children: [], parameters: [] }); // extend here to make "parameters" the down-most key
        // iterate over local parameters
        const localParametersIterator = new params_equation_array_iterator_1.ParamsEquationArrayIterator([this._prms]);
        for (const p of localParametersIterator) {
            if (p.visible) {
                ret.parameters.push(p.objectRepresentation(nubUidsInSession));
            }
        }
        // iterate over children Nubs
        for (const child of this._children) {
            ret.children.push(child.objectRepresentation(undefined, nubUidsInSession));
        }
        return ret;
    }
    /**
     * Fills the current Nub with parameter values, provided an object representation
     * @param obj object representation of a Nub content (parameters)
     * @returns the calculated parameter found, if any - used by child Nub to notify
     *          its parent of the calculated parameter to set
     */
    loadObjectRepresentation(obj) {
        // return value
        const ret = {
            p: undefined,
            hasErrors: false,
            changedUids: {}
        };
        // set parameter modes and values
        if (obj.parameters && Array.isArray(obj.parameters)) {
            // 1st pass: find calculated param
            // (if calculated param is not the default one, and default one is processed
            // before new one, prevents changing the former's mode from setting the 1st
            // param of the Nub to calculated, resetting the mode that had been loaded)
            for (const p of obj.parameters) {
                if (param_value_mode_1.ParamValueMode[p.mode] === param_value_mode_1.ParamValueMode.CALCUL) {
                    this.loadParam(p, ret);
                }
            }
            // define calculated param at Nub level
            if (ret.p) {
                this.calculatedParam = ret.p;
            }
            // set parameter modes and values - 2nd pass: set non-calculated params
            for (const p of obj.parameters) {
                if (param_value_mode_1.ParamValueMode[p.mode] !== param_value_mode_1.ParamValueMode.CALCUL) {
                    this.loadParam(p, ret);
                }
            }
        }
        // iterate over children if any
        if (obj.children && Array.isArray(obj.children)) {
            for (const s of obj.children) {
                // decode properties
                const props = index_1.Session.invertEnumKeysAndValuesInProperties(s.props, true);
                // create the Nub
                const subNub = index_1.Session.getInstance().createNub(new props_1.Props(props), this);
                // try to keep the original ID
                if (!index_1.Session.getInstance().uidAlreadyUsed(s.uid)) {
                    subNub.setUid(s.uid);
                }
                else {
                    ret.changedUids[s.uid] = subNub.uid;
                }
                const childRet = subNub.loadObjectRepresentation(s);
                // add Structure to parent
                this.addChild(subNub);
                // set calculated parameter for child ?
                if (childRet.p) {
                    this.calculatedParam = childRet.p;
                }
                // forward errors
                if (childRet.hasErrors) {
                    ret.hasErrors = true;
                }
            }
        }
        return ret;
    }
    /**
     * Load parameter state from JSON and amend "ret" status variable;
     * to be used by loadObjectRepresentation() only
     */
    loadParam(p, ret) {
        const param = this.getParameter(p.symbol);
        if (param) {
            // load parameter
            const pRet = param.loadObjectRepresentation(p, false);
            if (pRet.calculated) {
                ret.p = param;
            }
            // forward errors
            if (pRet.hasErrors) {
                ret.hasErrors = true;
            }
        }
        else {
            // tslint:disable-next-line:no-console
            console.error(`session file : cannot find parameter ${p.symbol} in target Nub`);
            ret.hasErrors = true;
        }
    }
    /**
     * Once session is loaded, run a second pass on all linked parameters to
     * reset their target if needed
     */
    fixLinks(obj) {
        // return value
        const ret = {
            hasErrors: false
        };
        if (obj.parameters && Array.isArray(obj.parameters)) {
            for (const p of obj.parameters) {
                const mode = param_value_mode_1.ParamValueMode[p.mode]; // get enum index for string value
                if (mode === param_value_mode_1.ParamValueMode.LINK) {
                    const param = this.getParameter(p.symbol);
                    // formulaire dont le Nub est la cible du lien
                    const destNub = index_1.Session.getInstance().findNubByUid(p.targetNub);
                    if (destNub) {
                        try {
                            param.defineReference(destNub, p.targetParam);
                        }
                        catch (err) {
                            // tslint:disable-next-line:no-console
                            console.error(`fixLinks: defineReference error`
                                + ` (${this.uid}.${param.symbol} => ${destNub.uid}.${p.targetParam})`);
                            // forward error
                            ret.hasErrors = true;
                        }
                    }
                    else {
                        // tslint:disable-next-line:no-console
                        console.error("fixLinks : cannot find target Nub");
                        // forward error
                        ret.hasErrors = true;
                    }
                }
            }
        }
        return ret;
    }
    /**
     * Replaces a child Nub
     * @param index index of child in _children array
     * @param child new child
     * @param resetCalcParam if true, resets default calculated parameter after replacing child
     * @param keepParametersState if true, mode and value of parameters will be kept between old and new section
     */
    replaceChild(index, child, resetCalcParam = false, keepParametersState = true) {
        if (index > -1 && index < this._children.length) {
            const hasOldChild = (this._children[index] !== undefined);
            const parametersState = {};
            // store old parameters state
            if (keepParametersState && hasOldChild) {
                for (const p of this._children[index].parameterIterator) {
                    // if p is also present and visible in new Nub
                    const cp = child.getParameter(p.symbol);
                    if ((cp === null || cp === void 0 ? void 0 : cp.visible) && p.visible) {
                        parametersState[p.symbol] = p.objectRepresentation([]);
                    }
                }
            }
            // replace child
            this._children[index] = child;
            // reapply parameters state
            if (keepParametersState && hasOldChild) {
                for (const p of this._children[index].parameterIterator) {
                    if (p.symbol in parametersState) {
                        p.loadObjectRepresentation(parametersState[p.symbol]);
                    }
                }
            }
        }
        else {
            throw new Error(`${this.constructor.name}.replaceChild invalid index ${index}`
                + ` (children length: ${this._children.length})`);
        }
        // postprocessing
        this.adjustChildParameters(child);
        // ensure one parameter is calculated
        if (resetCalcParam) {
            this.resetDefaultCalculatedParam();
        }
        // add reference to parent collection (this)
        child.parent = this;
    }
    /**
     * Finds oldChild in the list, and replaces it at the same index with newChild;
     * if the current calculated parameter belonged to the old child, resets a default one
     * @param oldChild child to get the index for
     * @param newChild child to set at this index
     */
    replaceChildInplace(oldChild, newChild) {
        const calcParamLost = (typeof this.calculatedParamDescriptor !== "string"
            && this.calculatedParamDescriptor.uid === oldChild.uid);
        const index = this.getIndexForChild(oldChild);
        if (index === -1) {
            throw new Error("old child not found");
        }
        this.replaceChild(index, newChild, calcParamLost);
    }
    /**
     * Returns the current index of the given child if any,
     * or else returns -1
     * @param child child or child UID to look for
     */
    getIndexForChild(child) {
        let index = -1;
        const uid = (child instanceof Nub) ? child.uid : child;
        for (let i = 0; i < this._children.length; i++) {
            if (this._children[i].uid === uid) {
                index = i;
            }
        }
        return index;
    }
    /**
     * @return true if given Nub id a child of current Nub
     */
    hasChild(child) {
        return this.hasChildUid(child.uid);
    }
    /**
     * @return true if given Nub uid id a child of current Nub
     */
    hasChildUid(childUid) {
        for (const s of this._children) {
            if (s.uid === childUid) {
                return true;
            }
        }
        return false;
    }
    /**
     * @return child having the given UID
     */
    getChild(uid) {
        for (const s of this._children) {
            if (s.uid === uid) {
                return s;
            }
        }
        return undefined;
    }
    /** Moves a child up (1 step) */
    moveChildUp(child) {
        let i = 0;
        for (const s of this._children) {
            if (s.uid === child.uid && i > 0) {
                const t = this._children[i - 1];
                this._children[i - 1] = this._children[i];
                this._children[i] = t;
                return;
            }
            i++;
        }
    }
    /** Moves a child down (1 step) */
    moveChildDown(child) {
        let i = 0;
        for (const s of this._children) {
            if (s.uid === child.uid && i < this._children.length - 1) {
                const t = this._children[i];
                this._children[i] = this._children[i + 1];
                this._children[i + 1] = t;
                return;
            }
            i++;
        }
    }
    /**
     * Removes a child
     * @param index
     */
    deleteChild(index) {
        if (index > -1) {
            this._children.splice(index, 1);
        }
        else {
            throw new Error(`${this.constructor.name}.deleteChild invalid index=${index}`);
        }
    }
    /** Removes all children */
    deleteAllChildren() {
        this._children = [];
    }
    /**
     * Returns the position or the current structure (starting at 0)
     * in the parent Nub
     */
    findPositionInParent() {
        if (this.parent) {
            return this.parent.getIndexForChild(this);
        }
        throw new Error(`Nub of class ${this.constructor.name} has no parent`);
    }
    /**
     * Adds a new empty ResultElement to the current Result object, so that
     * computation result is stored into it, via set currentResult(); does
     * the same for all children
     */
    initNewResultElement() {
        if (!this._result) {
            this._result = new result_1.Result(undefined, this);
        }
        // push empty result element for further affectation va set currentResult()
        this._result.addResultElement(new resultelement_1.ResultElement());
        // do the same for children
        for (const c of this._children) {
            c.initNewResultElement();
        }
    }
    /**
     * Sets this._result to a new empty Result, before starting a new CalcSerie();
     * does the same for all children
     */
    reinitResult() {
        this._result = new result_1.Result(undefined, this);
        for (const c of this._children) {
            c.reinitResult();
        }
    }
    // interface IObservable
    /**
     * ajoute un observateur à la liste
     */
    addObserver(o) {
        this._observable.addObserver(o);
    }
    /**
     * supprime un observateur de la liste
     */
    removeObserver(o) {
        this._observable.removeObserver(o);
    }
    /**
     * notifie un événement aux observateurs
     */
    notifyObservers(data, sender) {
        this._observable.notifyObservers(data, sender);
    }
    /**
     * Retrieves the ParamFamily associated to the given symbol, whether it is a
     * parameter or a result
     */
    getFamily(symbol) {
        // is it a parameter ?
        let p;
        try { // SectionNub.getParameter() (for ex.) throws exceptions
            p = this.getParameter(symbol);
        }
        catch (e) {
            // silent fail
        }
        if (p) {
            return p.family;
        }
        // is it a result ?
        if (symbol in this.resultsFamilies) {
            return this.resultsFamilies[symbol]; // might be undefined
        }
        // give up
        return undefined;
    }
    /**
     * Retrieves the unit (string) associated to the given symbol, whether it is a
     * parameter or a result
     */
    getUnit(symbol) {
        // is it a parameter ?
        let p;
        try { // SectionNub.getParameter() (for ex.) throws exceptions
            p = this.getParameter(symbol);
        }
        catch (e) {
            // silent fail
        }
        if (p) {
            return p.unit;
        }
        // is it a result ?
        if (symbol in this.resultsUnits) {
            return this.resultsUnits[symbol]; // might be undefined
        }
        // give up
        return undefined;
    }
    /**
     * Find the longest variating parameter, which will also be the Nub's result length
     */
    variatingLength() {
        let size = 0;
        for (const p of this.parameterIterator) {
            try {
                const iter = p.valuesIterator;
                size = Math.max(size, iter.count());
            }
            catch (e) {
                // silent fail (when p is in SINGLE mode and its value is undefined)
            }
        }
        return size;
    }
    /**
     * For all SINGLE, LINK and CALC parameters, copies singleValue to
     * sandbox value (.v) before calculation
     */
    copySingleValuesToSandboxValues() {
        for (const p of this.parameterIterator) {
            switch (p.valueMode) {
                case param_value_mode_1.ParamValueMode.SINGLE:
                case param_value_mode_1.ParamValueMode.CALCUL:
                    p.v = p.singleValue;
                    break;
                case param_value_mode_1.ParamValueMode.LINK:
                    if (p.referencedValue) {
                        p.v = p.referencedValue.getValue();
                    }
                    else {
                        throw Error("Nub.copySingleValuesToSandboxValues() : linked value not defined");
                    }
                    break;
                default:
                    // variable mode, let iterator do the job
                    p.v = undefined;
            }
        }
    }
    findCalculatedParameter() {
        let computedSymbol;
        if (this.calculatedParam === undefined) {
            throw new Error(`CalcSerie() : aucun paramètre à calculer`);
        }
        computedSymbol = this.calculatedParamDescriptor;
        return computedSymbol;
    }
    doCalc(computedSymbol, rInit) {
        return this.Calc(computedSymbol, rInit);
    }
    /**
     * Returns values of parameters and the result of the calculation for the calculated parameter
     * @param sVarCalc Symbol of the calculated param
     * @param result Result object of the calculation
     */
    getParamValuesAfterCalc(sVarCalc, result) {
        const cPrms = {};
        for (const p of this.parameterIterator) {
            if (p.symbol === sVarCalc) {
                cPrms[p.symbol] = result.vCalc;
            }
            else {
                cPrms[p.symbol] = p.v;
            }
        }
        return cPrms;
    }
    /**
     * To call Nub.calc(…) from indirect child
     */
    nubCalc(sVarCalc, rInit) {
        return Nub.prototype.Calc.call(this, sVarCalc, rInit);
    }
    /**
     * Used by GUI to update results display
     */
    notifyResultUpdated() {
        this.notifyObservers({
            action: "resultUpdated"
        }, this);
    }
    /**
     * Used by GUI to update progress bar
     */
    notifyProgressUpdated() {
        this.notifyObservers({
            action: "progressUpdated",
            value: this._progress
        }, this);
    }
    /**
     * Optional postprocessing after adding / replacing a child
     */
    // tslint:disable-next-line:no-empty
    adjustChildParameters(child) { }
    /**
     * Résoud l'équation par une méthode numérique
     * @param sVarCalc nom de la variable à calculer
     * @param rInit valeur initiale de la variable à calculer dans le cas de la dichotomie
     */
    Solve(sVarCalc, rInit) {
        const dicho = new dichotomie_1.Dichotomie(this, sVarCalc, this.DBG);
        dicho.startIntervalMaxSteps = this.dichoStartIntervalMaxSteps;
        const target = this.getFirstAnalyticalParameter();
        return dicho.Dichotomie(target.v, session_settings_1.SessionSettings.precision, rInit);
    }
    /**
     *  Check validity of all model parameters
     */
    check(withChildren = true) {
        let prmIt;
        if (withChildren) {
            prmIt = this.parameterIterator;
        }
        else {
            prmIt = new param_definition_iterator_1.ParamDefinitionIterator(this._prms);
        }
        for (const p of prmIt) {
            if (!p.isCalculated && p.visible) {
                try {
                    // will throw an error if no value is defined at all
                    p.paramValues.check();
                }
                catch (e) {
                    return false;
                }
            }
        }
        return true;
    }
}
exports.Nub = Nub;
Nub.progressPercentageAccordedToChainCalculation = 50;

},{"./compute-node":5,"./dichotomie":12,"./index":14,"./linked-value":17,"./param/param-definition":86,"./param/param-value-mode":89,"./param/param_definition_iterator":91,"./param/params_equation_array_iterator":93,"./props":103,"./session_settings":105,"./util/message":151,"./util/observer":154,"./util/result":155,"./util/resultelement":156}],34:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bief = void 0;
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const param_values_1 = require("../param/param-values");
const session_1 = require("../session");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const bief_params_1 = require("./bief_params");
const methode_resolution_1 = require("./methode-resolution");
const remous_1 = require("./remous");
const remous_params_1 = require("./remous_params");
const section_nub_1 = require("./section/section_nub");
class Bief extends section_nub_1.SectionNub {
    constructor(s, bp, dbg = false) {
        super(bp, dbg);
        this._calcType = compute_node_1.CalculatorType.Bief;
        this.setSection(s);
        this._props.addObserver(this);
        this.regime = bief_params_1.BiefRegime.Fluvial;
        this._defaultCalculatedParam = bp.Z1;
    }
    get prms() {
        return this._prms;
    }
    setSection(s) {
        super.setSection(s);
        this.setParametersCalculability(); // to override acSection's tuning
        if (this.section !== undefined) {
            // If from Section is not linkable here
            this.section.prms.If.undefineFamily();
            this.section.prms.If.visible = false;
        }
    }
    set regime(regime) {
        this.properties.setPropValue("regime", regime);
    }
    Calc(sVarCalc, rInit) {
        // reinit local CourbeRemous with same section and same parameters
        const rp = new remous_params_1.CourbeRemousParams(this.prms.Z1.v, this.prms.Z2.v, this.prms.ZF1.v, this.prms.ZF2.v, this.prms.Long.v, this.prms.Dx.v);
        const serialisedSection = this.section.serialise();
        const sectionCopy = session_1.Session.getInstance().unserialiseSingleNub(serialisedSection, false).nub;
        this.courbeRemous = new remous_1.CourbeRemous(sectionCopy, rp, methode_resolution_1.MethodeResolution.Trapezes);
        // set values for hidden parameters
        this.prms.setYamontYavalFromElevations();
        this.courbeRemous.prms.setYamontYavalFromElevations();
        this.courbeRemous.setIfFromElevations();
        // init .v for every parameter
        this.courbeRemous.section.copySingleValuesToSandboxValues();
        this.courbeRemous.copySingleValuesToSandboxValues();
        // if a section parameter is variating, copySingleValuesToSandboxValues() just set it
        // to undefined, because in general case the iterator is called after; here, re-set
        // the section copy parameters to the current section values
        for (const p of this.section.parameterIterator) {
            if (p.visible) { // do not affect hidden generated parameters (If)
                this.courbeRemous.section.getParameter(p.symbol).v = p.v;
            }
        }
        if (this.calculatedParam === this.prms.Z1
            && this.properties.getPropValue("regime") === bief_params_1.BiefRegime.Torrentiel) {
            throw new Error("Bief.Equation() : cannot calculate Z1 in Torrential regime");
        }
        if (this.calculatedParam === this.prms.Z2
            && this.properties.getPropValue("regime") === bief_params_1.BiefRegime.Fluvial) {
            throw new Error("Bief.Equation() : cannot calculate Z2 in Fluvial regime");
        }
        // regular calculation
        return super.Calc(sVarCalc, rInit);
    }
    Equation(sVarCalc) {
        let v;
        const xValues = new param_values_1.ParamValues();
        const l = this.prms.Long.v;
        const dx = this.prms.Dx.v;
        xValues.setValues(0, l, dx);
        // in Dichotomie mode, update sandbox value of calculated param
        if (!this.calculatedParam.isAnalytical()) {
            // we're calculating Ks or Q, in section
            this.courbeRemous.section.getParameter(this.calculatedParam.symbol).v = this.calculatedParam.v;
        }
        switch (sVarCalc) {
            case "Z1":
                this.courbeRemous.section.CalcSection("Yc");
                const ligneFlu = this.courbeRemous.calculFluvial(xValues);
                if (ligneFlu !== undefined
                    && ligneFlu.trY !== undefined
                    && ligneFlu.trY[0] !== undefined) {
                    v = ligneFlu.trY[0] + this.prms.ZF1.v;
                }
                else {
                    const res = new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_BIEF_Z1_CALC_FAILED), this);
                    res.log.insertLog(ligneFlu.log);
                    return res;
                }
                break;
            case "Z2":
                this.courbeRemous.section.CalcSection("Yc");
                const ligneTor = this.courbeRemous.calculTorrentiel(xValues);
                if (ligneTor !== undefined
                    && ligneTor.trY !== undefined
                    && ligneTor.trY[l] !== undefined) {
                    v = ligneTor.trY[l] + this.prms.ZF2.v;
                }
                else {
                    const res = new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_BIEF_Z2_CALC_FAILED), this);
                    res.log.insertLog(ligneTor.log);
                    return res;
                }
                break;
            default:
                throw new Error("Bief.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    // interface Observer
    update(sender, data) {
        if (data.action === "propertyChange") {
            switch (data.name) {
                case "regime":
                    if (data.value === bief_params_1.BiefRegime.Fluvial) {
                        this.prms.Z1.calculability = param_definition_1.ParamCalculability.EQUATION;
                        if (this.calculatedParam === undefined || this.calculatedParam === this.prms.Z2) {
                            this.prms.Z1.setCalculated();
                        }
                        this.prms.Z2.calculability = param_definition_1.ParamCalculability.FREE;
                    }
                    if (data.value === bief_params_1.BiefRegime.Torrentiel) {
                        this.prms.Z2.calculability = param_definition_1.ParamCalculability.EQUATION;
                        if (this.calculatedParam === undefined || this.calculatedParam === this.prms.Z1) {
                            this.prms.Z2.setCalculated();
                        }
                        this.prms.Z1.calculability = param_definition_1.ParamCalculability.FREE;
                    }
                    break;
            }
        }
    }
    setParametersCalculability() {
        this.prms.Long.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Dx.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Yamont.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Yaval.calculability = param_definition_1.ParamCalculability.FIXED;
        // do not set Z1 and Z2 here to avoid both being calculable at the same time
        this.prms.ZF1.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.ZF2.calculability = param_definition_1.ParamCalculability.FREE;
        // section params
        if (this.section) {
            this.section.prms.Ks.calculability = param_definition_1.ParamCalculability.DICHO;
            this.section.prms.Q.calculability = param_definition_1.ParamCalculability.DICHO;
            this.section.prms.If.calculability = param_definition_1.ParamCalculability.FREE;
            this.section.prms.YB.calculability = param_definition_1.ParamCalculability.FREE;
            this.section.prms.Y.calculability = param_definition_1.ParamCalculability.FREE;
            this.section.prms.LargeurBerge.calculability = param_definition_1.ParamCalculability.FREE;
            // parameters depending on section type
            const D = this.section.getParameter("D");
            if (D) {
                D.calculability = param_definition_1.ParamCalculability.FREE;
            }
            const k = this.section.getParameter("k");
            if (k) {
                k.calculability = param_definition_1.ParamCalculability.FREE;
            }
            const fruit = this.section.getParameter("Fruit");
            if (fruit) {
                fruit.calculability = param_definition_1.ParamCalculability.FREE;
            }
            const largeurFond = this.section.getParameter("LargeurFond");
            if (largeurFond) {
                largeurFond.calculability = param_definition_1.ParamCalculability.FREE;
            }
        }
    }
}
exports.Bief = Bief;

},{"../compute-node":5,"../param/param-definition":86,"../param/param-values":90,"../session":104,"../util/message":151,"../util/result":155,"./bief_params":35,"./methode-resolution":36,"./remous":40,"./remous_params":41,"./section/section_nub":49}],35:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BiefParams = exports.BiefRegime = void 0;
const remous_params_1 = require("./remous_params");
var BiefRegime;
(function (BiefRegime) {
    BiefRegime[BiefRegime["Fluvial"] = 0] = "Fluvial";
    BiefRegime[BiefRegime["Torrentiel"] = 1] = "Torrentiel";
})(BiefRegime = exports.BiefRegime || (exports.BiefRegime = {}));
/**
 * paramètres pour les cotes de bief; pour l'instant rien de spécifique
 * (le régime est une propriété et non un paramètre)
 */
class BiefParams extends remous_params_1.CourbeRemousParams {
}
exports.BiefParams = BiefParams;

},{"./remous_params":41}],36:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MethodeResolution = void 0;
var MethodeResolution;
(function (MethodeResolution) {
    MethodeResolution[MethodeResolution["Trapezes"] = 0] = "Trapezes";
    MethodeResolution[MethodeResolution["EulerExplicite"] = 1] = "EulerExplicite";
    MethodeResolution[MethodeResolution["RungeKutta4"] = 2] = "RungeKutta4";
})(MethodeResolution = exports.MethodeResolution || (exports.MethodeResolution = {}));

},{}],37:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pente = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
class Pente extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Pente;
        this._defaultCalculatedParam = prms.I;
        this.resetDefaultCalculatedParam();
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Equation(sVarCalc) {
        let v;
        switch (sVarCalc) {
            case "Z1":
                v = this.prms.Z2.v + (this.prms.I.v * this.prms.L.v);
                break;
            case "Z2":
                v = this.prms.Z1.v - (this.prms.I.v * this.prms.L.v);
                break;
            case "L":
                v = (this.prms.Z1.v - this.prms.Z2.v) / this.prms.I.v;
                break;
            case "I":
                v = (this.prms.Z1.v - this.prms.Z2.v) / this.prms.L.v;
                break;
            default:
                throw new Error("Pente.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.L.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.I.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.Pente = Pente;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/result":155}],38:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PenteParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class PenteParams extends params_equation_1.ParamsEquation {
    constructor(rZ1, rZ2, rL, rI) {
        super();
        this._Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, param_definition_1.ParamFamily.ELEVATIONS);
        this._Z2 = new param_definition_1.ParamDefinition(this, "Z2", param_domain_1.ParamDomainValue.ANY, "m", rZ2, param_definition_1.ParamFamily.ELEVATIONS);
        this._L = new param_definition_1.ParamDefinition(this, "L", param_domain_1.ParamDomainValue.POS, "m", rL, param_definition_1.ParamFamily.LENGTHS);
        this._I = new param_definition_1.ParamDefinition(this, "I", param_domain_1.ParamDomainValue.ANY, "m/m", rI, param_definition_1.ParamFamily.SLOPES);
        this.addParamDefinition(this._Z1);
        this.addParamDefinition(this._Z2);
        this.addParamDefinition(this._L);
        this.addParamDefinition(this._I);
    }
    get Z1() {
        return this._Z1;
    }
    get Z2() {
        return this._Z2;
    }
    get L() {
        return this._L;
    }
    get I() {
        return this._I;
    }
}
exports.PenteParams = PenteParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],39:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegimeUniforme = void 0;
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const section_nub_1 = require("./section/section_nub");
const section_parametree_params_1 = require("./section/section_parametree_params");
const section_circulaire_1 = require("./section/section_circulaire");
const base_1 = require("../base");
class RegimeUniforme extends section_nub_1.SectionNub {
    constructor(s, dbg = false) {
        super(new section_parametree_params_1.SectionParams(), dbg);
        this._calcType = compute_node_1.CalculatorType.RegimeUniforme;
        this.setSection(s);
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Equation(sVarCalc) {
        this.debug("RU: Equation(" + sVarCalc + ")");
        let r;
        if (typeof sVarCalc !== "string") {
            // dirty hack because calculated param descriptor for section params is an object { uid: , symbol: }
            sVarCalc = sVarCalc.symbol;
        }
        switch (sVarCalc) {
            case "Y":
                r = this.section.CalcSection("Yn");
                break;
            case "Q":
                r = this.Calc_Qn();
                break;
            default:
                throw new Error("RegimeUniforme.Equation() : invalid variable name " + sVarCalc);
        }
        return r;
    }
    /**
     * Calcul d'une équation quelle que soit l'inconnue à calculer; déclenche le calcul en
     * chaîne des modules en amont si nécessaire
     * @param sVarCalc nom de la variable à calculer
     * @param rInit valeur initiale de la variable à calculer dans le cas de la dichotomie
     */
    Calc(sVarCalc, rInit) {
        const r = super.Calc(sVarCalc, rInit);
        // Vitesse moyenne
        const V = this.section.CalcSection("V", this.section.prms.Y.v);
        r.resultElement.values.V = V.vCalc;
        // Est-ce que ça déborde ?
        if (this.section.prms.Y.v > this.section.prms.YB.v) {
            r.resultElement.log.add(new message_1.Message(message_1.MessageCode.WARNING_SECTION_OVERFLOW));
        }
        // Est-ce qu'on est en charge sur une section circulaire ? @see #214
        if (this.section instanceof section_circulaire_1.cSnCirc
            && this.section.prms.YB.v >= this.section.prms.D.v
            && base_1.isGreaterThan(this.section.prms.Y.v, this.section.prms.D.v, 1e-3)) {
            this.currentResult = new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_RU_CIRC_LEVEL_TOO_HIGH));
            return this.result;
        }
        return r;
    }
    setSection(s) {
        super.setSection(s);
        // had no analytical parameters before ?
        this._defaultCalculatedParam = this.getFirstAnalyticalParameter();
        this.resetDefaultCalculatedParam();
    }
    // tslint:disable-next-line:no-empty
    setParametersCalculability() { }
    adjustChildParameters() {
        this.section.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.section.prms.Y.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.section.prms.YB.calculability = param_definition_1.ParamCalculability.FREE;
    }
    setResultsUnits() {
        this._resultsUnits = {
            V: "m/s"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            V: param_definition_1.ParamFamily.SPEEDS
        };
    }
    /**
     * Calcul du débit en régime uniforme.
     * @return Débit en régime uniforme
     */
    Calc_Qn() {
        if (this.section.prms.If.v <= 0) {
            return new result_1.Result(0, this);
        }
        const rR = this.section.CalcSection("R", this.section.prms.Y.v);
        if (!rR) {
            return rR;
        }
        const rS = this.section.CalcSection("S", this.section.prms.Y.v);
        if (!rS) {
            return rS;
        }
        this.section.prms.Q.v = this.section.prms.Ks.v * Math.pow(rR.vCalc, 2 / 3) * rS.vCalc * Math.sqrt(this.section.prms.If.v);
        return new result_1.Result(this.section.prms.Q.v, this);
    }
}
exports.RegimeUniforme = RegimeUniforme;

},{"../base":3,"../compute-node":5,"../param/param-definition":86,"../util/message":151,"../util/result":155,"./section/section_circulaire":47,"./section/section_nub":49,"./section/section_parametree_params":51}],40:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CourbeRemous = void 0;
const base_1 = require("../base");
const compute_node_1 = require("../compute-node");
const dichotomie_1 = require("../dichotomie");
const mirror_iterator_1 = require("../param/mirror-iterator");
const param_definition_1 = require("../param/param-definition");
const param_values_1 = require("../param/param-values");
const session_settings_1 = require("../session_settings");
const log_1 = require("../util/log");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const resultelement_1 = require("../util/resultelement");
const methode_resolution_1 = require("./methode-resolution");
const section_nub_1 = require("./section/section_nub");
/**
 * Calcul d'une courbe de remous
 */
class CourbeRemous extends section_nub_1.SectionNub {
    constructor(s, crp, method = methode_resolution_1.MethodeResolution.Trapezes, dbg = false) {
        super(crp, dbg);
        this._debugDicho = false;
        this._calcType = compute_node_1.CalculatorType.CourbeRemous;
        this.setSection(s);
        this.methodeResolution = method;
        // no calculated parameter
        this._defaultCalculatedParam = undefined;
    }
    get methodeResolution() {
        return this.properties.getPropValue("methodeResolution");
    }
    set methodeResolution(m) {
        this.properties.setPropValue("methodeResolution", m);
    }
    setSection(s) {
        super.setSection(s);
        this.setParametersCalculability(); // to override acSection's tuning
        if (this.section !== undefined) {
            // If from Section is neither visible nor linkable here
            this.section.prms.If.undefineFamily();
            this.section.prms.If.visible = false;
        }
    }
    /**
     * This needs to be called before calculation if Z1, Z2, ZF1 or ZF2 was modified
     */
    setIfFromElevations() {
        if (this.section !== undefined) {
            this.section.prms.If.singleValue =
                (this.prms.ZF1.v - this.prms.ZF2.v) / this.prms.Long.v;
            this.section.prms.If.v = this.section.prms.If.singleValue;
        }
    }
    static get availableTargets() {
        return CourbeRemous._availableTargets;
    }
    /**
     * @param valACal nom de la variable à calculer
     */
    calculRemous(valACal) {
        this.triggerChainCalculation();
        this.copySingleValuesToSandboxValues();
        this.prms.setYamontYavalFromElevations();
        this.setIfFromElevations();
        const res = new result_1.Result(undefined, this);
        // vérifier qu'il y a de l'eau à l'amont / à l'aval
        if (this.prms.ZF1.singleValue >= this.prms.Z1.singleValue) {
            res.globalLog.add(new message_1.Message(message_1.MessageCode.WARNING_UPSTREAM_BOTTOM_HIGHER_THAN_WATER));
        }
        if (this.prms.ZF2.singleValue >= this.prms.Z2.singleValue) {
            res.globalLog.add(new message_1.Message(message_1.MessageCode.WARNING_DOWNSTREAM_BOTTOM_HIGHER_THAN_WATER));
        }
        // s'il n'y en a nulle part, on arrête tout
        if (this.prms.ZF1.singleValue >= this.prms.Z1.singleValue
            && this.prms.ZF2.singleValue >= this.prms.Z2.singleValue) {
            res.globalLog.add(new message_1.Message(message_1.MessageCode.ERROR_REMOUS_NO_WATER_LINE));
            return res;
        }
        // let Yc: number = this.Sn.CalcSection("Yc");
        const rYC = this.Sn.CalcSection("Yc");
        if (!rYC.ok) {
            res.addLog(rYC.log);
            return res;
        }
        // tslint:disable-next-line:variable-name
        const Yc = rYC.vCalc;
        const rB = this.Sn.CalcSection("B", this.Sn.prms.YB.v);
        if (!rB.ok) {
            res.addLog(rB.log);
            return res;
        }
        let m = new message_1.Message(message_1.MessageCode.INFO_REMOUS_H_CRITIQUE);
        m.extraVar.Yc = Yc;
        // this._log.add(m);
        res.addMessage(m);
        const rYN = this.Sn.CalcSection("Yn");
        // tslint:disable-next-line:variable-name
        let Yn;
        if (!rYN.ok) {
            const mc = rYN.log.messages[0].code;
            if (mc === message_1.MessageCode.ERROR_SECTION_PENTE_NEG_NULLE_HNORMALE_INF) {
                res.addMessage(new message_1.Message(message_1.MessageCode.WARNING_YN_SECTION_PENTE_NEG_NULLE_HNORMALE_INF));
            }
            else if (mc === message_1.MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE) {
                res.addMessage(new message_1.Message(message_1.MessageCode.WARNING_YN_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE));
            }
            Yn = 2 * Yc; // si le calcul de Yn échoue, on est en régime fluvial, d'où un Yn imposé > Yc
        }
        else {
            // tslint:disable-next-line:variable-name
            Yn = rYN.vCalc;
            m = new message_1.Message(message_1.MessageCode.INFO_REMOUS_H_NORMALE, { Yn });
            res.addMessage(m);
        }
        // this.debug("largeur berge " + this.Sn.CalcSection("B"));
        // const rB2: Result = this.Sn.CalcSection("B")
        // this.debug("largeur berge " + rB2.vCalc);
        // this.debug("hauteur critique " + Yc);
        // this.Sn.HautNormale = this.Sn.CalcSection("Yn");
        // this.debug("hauteur normale " + this.Sn.HautNormale);
        // this.debug("hauteur normale " + Yn);
        // Calcul des courbes de remous
        const xValues = new param_values_1.ParamValues();
        xValues.setValues(0, this.prms.Long.v, this.prms.Dx.v);
        // vérifier les tirants d'eau amont/aval par rapport à la hauteur critique
        if (this.Sn.HautCritique.vCalc > this.prms.Yaval.v
            && this.Sn.HautCritique.vCalc < this.prms.Yamont.v) {
            res.globalLog.add(new message_1.Message(message_1.MessageCode.ERROR_REMOUS_PAS_CALCUL));
            return res;
        }
        // on ne peut pas calculer la courbe fluviale s'il ny 'a pas d'eau à l'aval
        let rCourbeFlu;
        if (this.prms.ZF2.singleValue < this.prms.Z2.singleValue) {
            rCourbeFlu = this.calculFluvial(xValues);
            res.addLog(rCourbeFlu.log);
        }
        // on ne peut pas calculer la courbe torrentielle s'il ny 'a pas d'eau à l'amont
        let rCourbeTor;
        if (this.prms.ZF1.singleValue < this.prms.Z1.singleValue) {
            rCourbeTor = this.calculTorrentiel(xValues);
            res.addLog(rCourbeTor.log);
        }
        let crbFlu;
        if ((rCourbeFlu === null || rCourbeFlu === void 0 ? void 0 : rCourbeFlu.trY) !== undefined) {
            crbFlu = rCourbeFlu.trY;
        }
        else {
            crbFlu = {};
        }
        let crbTor;
        if ((rCourbeTor === null || rCourbeTor === void 0 ? void 0 : rCourbeTor.trY) !== undefined) {
            crbTor = rCourbeTor.trY;
        }
        else {
            crbTor = {};
        }
        // this.debug("HautCritique ", this.Sn.HautCritique);
        this.debug("flu");
        // this.logObject(crbFlu);
        this.debug(JSON.stringify(crbFlu));
        this.debug("tor");
        // this.logObject(crbTor);
        this.debug(JSON.stringify(crbTor));
        /* // tslint:disable-next-line:forin
        for (const xflu in crbFlu) {
            const yflu = crbFlu[xflu];
            // this.debug("imp x " + xflu + " flu " + this.Sn.CalcSection('Imp', yflu));
        }

        // tslint:disable-next-line:forin
        for (const xtor in crbTor) {
            const ytor = crbTor[xtor];
            // this.debug("imp x " + xtor + " tor " + this.Sn.CalcSection('Imp', ytor));
        } */
        // Détection du ressaut hydraulique
        const nFlu = this.size(crbFlu);
        const nTor = this.size(crbTor);
        if (nFlu !== 0 && nTor !== 0) {
            /* const firstYFlu = crbFlu[0];
            const lastYTor = this.last(crbTor);
            // this.debug("end flu " + firstYFlu);
            // this.debug("end tor " + lastYTor);
            // this.debug("nFlu " + nFlu);
            // this.debug("nTor " + nTor);
            // this.debug("Imp flu " + this.Sn.CalcSection('Imp', firstYFlu));
            // this.debug("Imp tor " + this.Sn.CalcSection('Imp', lastYTor)); */
            let crbComplete;
            let crbPartielle;
            let iSens;
            let sSens;
            if (nFlu > nTor || (nFlu === nTor && Yn < Yc)) {
                // La courbe fluviale va jusqu'au bout
                crbComplete = crbFlu; // courbe calculée sur tout le bief
                crbPartielle = crbTor; // courbe calculée sur une partie seulement du bief
                iSens = 1; // On cherche l'aval du ressaut
                sSens = "amont";
                this.debug("complete=flu, partielle=tor");
                // this.debug("complete(flu)");
                // this.debug(crbComplete);
                // this.debug("partielle(tor)");
                // this.debug(crbPartielle);
            }
            else {
                // La courbe torrentielle va jusqu'au bout
                crbComplete = crbTor;
                crbPartielle = crbFlu;
                iSens = -1; // On cherche l'amont du ressaut
                sSens = "aval";
                this.debug("complete=tor, partielle=flu");
                // this.debug("complete(tor)");
                // this.debug(crbComplete);
                // this.debug("partielle(flu)");
                // this.debug(crbPartielle);
            }
            // Parcours des sections de la ligne d'eau la plus courte
            const trX = Object.keys(crbPartielle);
            if (iSens === -1) { // tri dans l'ordre croissant { {
                trX.sort((a, b) => {
                    if (+a > +b) {
                        return 1;
                    }
                    if (+a < +b) {
                        return -1;
                    }
                    return 0;
                });
            }
            else { // tri dans l'ordre décroissant { {
                trX.sort((a, b) => {
                    if (+a > +b) {
                        return -1;
                    }
                    if (+a < +b) {
                        return 1;
                    }
                    return 0;
                });
            }
            const trXr = trX.slice(0); // copie
            trXr.reverse();
            // this.debug("trX");
            // this.debug(trX);
            let bRessaut = false;
            // tslint:disable-next-line:variable-name
            const Dx = xValues.step;
            // tslint:disable-next-line:prefer-for-of
            for (let irX = 0; irX < trX.length; irX++) {
                const rX = +trX[irX];
                // this.debug("irX=" + irX);
                // this.debug("rX=" + rX);
                // this.debug("partielle[" + rX + "]=" + crbPartielle[rX]);
                // Calcul de l'abscisse de la section dans l'autre régime
                const rYCO = this.Sn.CalcSection("Ycon", crbPartielle[rX]); // Y conjugué
                if (!rYCO.ok) {
                    res.addLog(rYCO.log);
                    return res;
                }
                // tslint:disable-next-line:variable-name
                const Ycon = rYCO.vCalc;
                // this.debug("rX=" + rX + " Ycon(Ypartiel=" + crbPartielle[rX] + ")=" + Ycon);
                const rLongRst = 5 * Math.abs(crbPartielle[rX] - Ycon); // Longueur du ressaut
                // this.debug("longueur ressaut=" + rLongRst);
                let xRst = rX + Math.round(iSens * rLongRst / Dx) * Dx; // Abscisse où comparer Yconj et Y
                // this.debug("xRst=" + xRst);
                // let rxRst = rX + iSens * rLongRst; // Abscisse réelle du ressaut
                // this.debug("xRst reel=" + (rX + iSens * rLongRst));
                xRst = base_1.round(xRst, this.section.prms.iPrec);
                // this.debug("xRst (arr)=" + xRst);
                const impYpartielle = this.Sn.CalcSection("Imp", crbPartielle[rX]);
                // this.debug("imp(Ypartiel[rX=" + rX + "]=" + crbPartielle[rX] + ")=" + impYpartielle);
                if (!impYpartielle.ok) {
                    res.addLog(impYpartielle.log);
                    return res;
                }
                if (crbComplete[xRst] !== undefined) {
                    // Hauteur décalée de la longueur du ressaut (il faut gérer la pente du fond)
                    // const Ydec: number = crbComplete[xRst] + rLongRst * this.section.prms.If.v * iSens;
                    // this.debug("Ydec=" + Ydec);
                    const impYcomplete = this.Sn.CalcSection("Imp", crbComplete[xRst]);
                    // this.debug("imp(Ycomplet[xRst=" + xRst + "]=" + crbComplete[xRst] + ")=" + impYcomplete);
                    if (!impYcomplete.ok) {
                        res.addLog(impYcomplete.log);
                        return res;
                    }
                    if (impYpartielle.vCalc > impYcomplete.vCalc) {
                        this.debug("Ressaut hydraulique détecté entre les abscisses " +
                            Math.min(rX, xRst) + " et " + Math.max(rX, xRst));
                        m = new message_1.Message(message_1.MessageCode.INFO_REMOUS_RESSAUT_HYDRO);
                        m.extraVar.xmin = Math.min(rX, xRst);
                        m.extraVar.xmax = Math.max(rX, xRst);
                        // this._log.add(m);
                        res.addMessage(m);
                        // this.debug("rX=" + rX + " xRst=" + xRst);
                        // Modification de la ligne d'eau complète
                        for (const pi of trXr) {
                            const rXCC = +pi;
                            // this.debug("rXCC=" + rXCC);
                            if ((rX !== xRst && iSens * (rXCC - rX) <= 0)
                                || (rX === xRst && iSens * (rXCC - rX) < 0)) {
                                delete crbComplete[rXCC];
                                this.debug("Modification de la ligne d'eau complète : suppression de la valeur à rXCC=" +
                                    rXCC + ", rX=" + rX + ", iSens*(rXCC-rX)=" + (iSens * (rXCC - rX)));
                            }
                        }
                        // Modification de la ligne d'eau partielle
                        for (const xcn of trX) {
                            const rXCN = +xcn;
                            // this.debug("rXCN=" + rXCN);
                            if ((rX !== xRst && iSens * (rXCN - xRst) >= 0)
                                || (rX === xRst && iSens * (rXCN - xRst) > 0)) {
                                this.debug("Modification de la ligne d'eau partielle : suppression de la valeur à rX=" + rXCN);
                                delete crbPartielle[rXCN];
                            }
                        }
                        bRessaut = true;
                        break;
                    }
                }
            }
            if (!bRessaut) {
                // Le ressaut est en dehors du canal
                m = new message_1.Message(message_1.MessageCode.INFO_REMOUS_RESSAUT_DEHORS);
                m.extraVar.sens = sSens;
                m.extraVar.x = +this.last(trX);
                //  this._log.add(m);
                res.addMessage(m);
                this.debug("Ressaut hydraulique détecté à l'" + sSens + " de l'abscisse " + this.last(trX));
                if (iSens === 1) {
                    crbTor = {};
                }
                else {
                    crbFlu = {};
                }
                crbPartielle = {}; // pour le log uniquement, à virer
            }
            this.debug("complete (" + (iSens === 1 ? "flu" : "tor") + ") modifiée");
            // this.logObject(crbComplete);
            this.debug(JSON.stringify(crbComplete));
            this.debug("partielle (" + (iSens === 1 ? "tor" : "flu") + ") modifiée");
            // this.logObject(crbPartielle);
            this.debug(JSON.stringify(crbPartielle));
        }
        // Définition des abscisses
        let trXabsc = [];
        if (nFlu !== 0) {
            trXabsc = Object.keys(crbFlu).map((k) => +k);
        }
        if (nTor !== 0) {
            const kTor = Object.keys(crbTor).map((k) => +k);
            trXabsc = trXabsc.concat(kTor);
        }
        // this.debug("trX=" + trX);
        trXabsc.sort((a, b) => {
            if (a > b) {
                return 1;
            }
            if (a < b) {
                return -1;
            }
            return 0;
        });
        // this.debug("trX tri=" + trX);
        trXabsc = trXabsc.filter((elem, index, array) => {
            if (index > 0) {
                return elem !== array[index - 1];
            }
            return true;
        });
        // this.debug("trX unique=" + trX);
        this.debug("abscisses ");
        this.logArray(trXabsc);
        // est-ce que ça déborde ?
        let xDebordeMin;
        let xDebordeMax;
        // compilation des résultats
        for (const x of trXabsc) {
            let ligneDeau;
            if (crbFlu[x] === undefined) {
                ligneDeau = crbTor[x];
            }
            else if (crbTor[x] === undefined) {
                ligneDeau = crbFlu[x];
            }
            else {
                ligneDeau = Math.max(crbFlu[x], crbTor[x]);
            }
            const re = new resultelement_1.ResultElement({ Y: ligneDeau + this.getCoteFond(x) });
            re.addExtraResult("X", x);
            if (crbFlu[x]) {
                re.addExtraResult("flu", crbFlu[x] + this.getCoteFond(x));
            }
            if (crbTor[x]) {
                re.addExtraResult("tor", crbTor[x] + this.getCoteFond(x));
            }
            // Détection du débordement
            if (xDebordeMin === undefined) {
                if (ligneDeau > this.section.prms.YB.v) {
                    xDebordeMin = x;
                }
            }
            else {
                if (xDebordeMax === undefined && ligneDeau <= this.section.prms.YB.v) {
                    xDebordeMax = x;
                }
            }
            // Calcul de la variable à calculer
            if (valACal) {
                if (ligneDeau !== undefined) {
                    const rVar = this.Sn.CalcSection(valACal, ligneDeau);
                    if (!rVar.ok) {
                        res.addLog(rVar.log);
                    }
                    else {
                        let val = rVar.vCalc;
                        if (["Hs", "Hsc", "Ycor", "Ycon"].includes(valACal)) {
                            val += this.getCoteFond(x);
                        }
                        re.addExtraResult(valACal, val);
                    }
                }
            }
            res.addResultElement(re);
        }
        if (xDebordeMin !== undefined || xDebordeMax !== undefined) {
            xDebordeMin = (xDebordeMin === undefined) ? trXabsc[0] : xDebordeMin;
            xDebordeMax = (xDebordeMax === undefined) ? trXabsc[trXabsc.length - 1] : xDebordeMax;
            const md = new message_1.Message(message_1.MessageCode.WARNING_SECTION_OVERFLOW_ABSC);
            md.extraVar.xa = xDebordeMin;
            md.extraVar.xb = xDebordeMax;
            res.globalLog.add(md);
        }
        return res;
    }
    /**
     * Calcule un seul point de la courbe, uniquement pour le solveur de la méthode Trapezes
     * @WARNING scorie toute pourrie
     */
    Calc(sVarCalc, rInit) {
        this.prms.setYamontYavalFromElevations();
        this.setIfFromElevations();
        if (sVarCalc === "Hs") {
            return this.Equation("Hs");
        }
        throw new Error("CourbeRemous.Calc() : parameter " + sVarCalc + " not allowed");
    }
    Equation(sVarCalc) {
        if (sVarCalc === "Hs") {
            const rHS = this.CalcHS();
            return new result_1.Result(rHS, this);
        }
        throw new Error("CourbeRemous.Equation() : parameter " + sVarCalc + " not allowed");
    }
    /**
     * Effectue une série de calculs sur un paramètre; déclenche le calcul en chaîne
     * des modules en amont si nécessaire
     * @param rInit solution approximative du paramètre
     * @param sDonnee éventuel symbole / paire symbole-uid du paramètre à calculer
     */
    CalcSerie(rInit) {
        const varCalc = this.properties.getPropValue("varCalc");
        const res = this.calculRemous(varCalc);
        return res;
    }
    CalcHS() {
        // Equation de l'intégration par la méthode des trapèzes
        this.Sn.Reset(); // pour forcer le calcul avec la nouvelle valeur de section.prms.Y
        return this.Sn.CalcSection("Hs").vCalc - this.Sn.CalcSection("J").vCalc / 2 * this.Dx;
    }
    get Sn() {
        return this.section;
    }
    get prms() {
        return this._prms;
    }
    /**
     * Retourne la cote de fond à l'abscisse x
     * @param x abscisse en m (0 = amont)
     */
    getCoteFond(x) {
        return (this.prms.ZF1.v - this.prms.ZF2.v) * (1 - x / this.prms.Long.v) + this.prms.ZF2.v;
    }
    /**
     * calcul de la ligne fluviale depuis l'aval (si possible)
     */
    calculFluvial(xValues) {
        let res = {
            log: new log_1.cLog(),
            trY: {}
        };
        if (!this.Sn.HautCritique.ok) {
            res.log.add(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE));
            return res;
        }
        if (this.Sn.HautCritique.vCalc > this.prms.Yaval.v) {
            this.debug("Condition limite aval (" + this.prms.Yaval.v +
                ") < Hauteur critique (" + this.Sn.HautCritique +
                ") : pas de calcul possible depuis l'aval");
            res.log.add(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL));
        }
        // Calcul depuis l'aval
        if (this.Sn.HautCritique.vCalc <= this.prms.Yaval.v) {
            this.debug(`Condition limite aval (${this.prms.Yaval.v}) >= ` +
                `Hauteur critique (${this.Sn.HautCritique}) : calcul de la partie fluviale à partir de l'aval`);
            this.Dx = this.prms.Dx.v;
            xValues.initValuesIterator(true, undefined, true);
            // reverse iterator does not work here if Dx is not a multiple of Long @see jalhyd#147
            // ex: Long = 10, Dx = 3
            // forward: [ 0, 3, 6, 9, 10 ]
            // reverse: [ 10, 7, 4, 1, 0 ] instead of [ 10, 9, 6, 3, 0 ]
            const mirror = new mirror_iterator_1.MirrorIterator(xValues);
            res = this.calcul(this.prms.Yaval.v, mirror);
            res.log.insert(new message_1.Message(message_1.MessageCode.INFO_REMOUS_CALCUL_FLUVIAL));
        }
        return res;
    }
    /**
     * calcul de la ligne torrentielle depuis l'amont (si possible)
     */
    calculTorrentiel(xValues) {
        let res = {
            log: new log_1.cLog(),
            trY: {}
        };
        if (!this.Sn.HautCritique.ok) {
            res.log.add(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE));
            return res;
        }
        if (this.Sn.HautCritique.vCalc < this.prms.Yamont.v) {
            this.debug("Condition limite amont (" + this.prms.Yamont.v +
                ") > Hauteur critique (" + this.Sn.HautCritique +
                ") : pas de calcul possible depuis l'amont");
            res.log.add(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT));
        }
        // Calcul depuis l'amont
        if (this.Sn.HautCritique.vCalc >= this.prms.Yamont.v) {
            this.debug("Condition limite amont (" + this.prms.Yamont.v +
                ") <= Hauteur critique (" + this.Sn.HautCritique +
                ") : calcul de la partie torrentielle à partir de l'amont");
            this.Dx = -this.prms.Dx.v;
            xValues.initValuesIterator(false, undefined, true);
            res = this.calcul(this.prms.Yamont.v, xValues);
            res.log.insert(new message_1.Message(message_1.MessageCode.INFO_REMOUS_CALCUL_TORRENTIEL));
        }
        return res;
    }
    setParametersCalculability() {
        this.prms.Long.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Dx.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Yamont.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Yaval.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.ZF1.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.ZF2.calculability = param_definition_1.ParamCalculability.FIXED;
        // section params
        if (this.section) {
            this.section.prms.Ks.calculability = param_definition_1.ParamCalculability.FIXED;
            this.section.prms.Q.calculability = param_definition_1.ParamCalculability.FIXED;
            this.section.prms.If.calculability = param_definition_1.ParamCalculability.FIXED;
            this.section.prms.YB.calculability = param_definition_1.ParamCalculability.FIXED;
            this.section.prms.Y.calculability = param_definition_1.ParamCalculability.FIXED;
            this.section.prms.LargeurBerge.calculability = param_definition_1.ParamCalculability.FIXED;
            // parameters depending on section type
            const D = this.section.getParameter("D");
            if (D) {
                D.calculability = param_definition_1.ParamCalculability.FIXED;
            }
            const k = this.section.getParameter("k");
            if (k) {
                k.calculability = param_definition_1.ParamCalculability.FIXED;
            }
            const fruit = this.section.getParameter("Fruit");
            if (fruit) {
                fruit.calculability = param_definition_1.ParamCalculability.FIXED;
            }
            const largeurFond = this.section.getParameter("LargeurFond");
            if (largeurFond) {
                largeurFond.calculability = param_definition_1.ParamCalculability.FIXED;
            }
        }
    }
    adjustChildParameters() {
        this.section.prms.Y.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    /**
     * Calcul de dy/dx (utilisé par Euler explicite et Runge-Kutta 4)
     * @param Y Tirant d'eau initial
     */
    Calc_dYdX(Y) {
        // L'appel à Calc('J') avec Y en paramètre réinitialise toutes les données dépendantes de la ligne d'eau
        // return - (this.section.prms.If.v - this.Sn.CalcSection('J', Y))
        //        / (1 - Math.pow(this.Sn.CalcSection('Fr', Y), 2));
        const rJ = this.Sn.CalcSection("J", Y);
        if (!rJ.ok) {
            return rJ;
        }
        const rFR = this.Sn.CalcSection("Fr", Y);
        if (!rFR.ok) {
            return rFR;
        }
        const v = -(this.section.prms.If.v - rJ.vCalc) / (1 - Math.pow(rFR.vCalc, 2));
        return new result_1.Result(v, this);
    }
    /**
     * Calcul du point suivant de la courbe de remous par la méthode Euler explicite.
     * @param Y Tirant d'eau initial
     * @return Tirant d'eau
     */
    Calc_Y_EulerExplicite(Y) {
        if (!this.Sn.HautCritique.ok) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE), this);
        }
        // L'appel à Calc('J') avec Y en paramètre réinitialise toutes les données dépendantes de la ligne d'eau
        const rDXDY = this.Calc_dYdX(Y);
        if (!rDXDY.ok) {
            return rDXDY;
        }
        // let Y2 = Y + this.Dx * this.Calc_dYdX(Y);
        const Y2 = Y + this.Dx * rDXDY.vCalc;
        return new result_1.Result(Y2, this);
    }
    /**
     * Calcul du point suivant de la courbe de remous par la méthode RK4.
     * @param Y Tirant d'eau initial
     * @return Tirant d'eau
     */
    Calc_Y_RK4(Y) {
        if (!this.Sn.HautCritique.ok) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE), this);
        }
        // L'appel à Calc('J') avec Y en paramètre réinitialise toutes les données dépendantes de la ligne d'eau
        const rDx = this.Dx;
        // let k1 = this.Calc_dYdX(Y);
        const rDXDY = this.Calc_dYdX(Y);
        if (!rDXDY.ok) {
            return rDXDY;
        }
        const k1 = rDXDY.vCalc;
        const hc = this.Sn.HautCritique.vCalc;
        if (base_1.XOR(rDx > 0, !(Y + rDx / 2 * k1 < hc))) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE), this);
        }
        // let k2 = this.Calc_dYdX(Y + Dx / 2 * k1);
        const rDXDY2 = this.Calc_dYdX(Y + rDx / 2 * k1);
        if (!rDXDY2.ok) {
            return rDXDY2;
        }
        const k2 = rDXDY2.vCalc;
        if (base_1.XOR(rDx > 0, !(Y + rDx / 2 * k2 < hc))) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE), this);
        }
        // let k3 = this.Calc_dYdX(Y + Dx / 2 * k2);
        const rDXDY3 = this.Calc_dYdX(Y + rDx / 2 * k2);
        if (!rDXDY3.ok) {
            return rDXDY3;
        }
        const k3 = rDXDY3.vCalc;
        if (base_1.XOR(rDx > 0, !(Y + rDx / 2 * k3 < hc))) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE), this);
        }
        // let k4 = this.Calc_dYdX(Y + Dx * k3);
        const rDXDY4 = this.Calc_dYdX(Y + rDx * k3);
        if (!rDXDY4.ok) {
            return rDXDY4;
        }
        const k4 = rDXDY4.vCalc;
        // tslint:disable-next-line:variable-name
        const Yout = Y + rDx / 6 * (k1 + 2 * (k2 + k3) + k4);
        // if ($this ->rDx > 0 xor !($Yout < $this ->oSect ->rHautCritique)) { return false; }
        if (base_1.XOR(rDx > 0, !(Yout < hc))) {
            const res = new result_1.Result(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE), this);
            return res;
        }
        return new result_1.Result(Yout, this);
    }
    /**
     * Calcul du point suivant de la courbe de remous par la méthode de l'intégration par trapèze
     * @param Y Tirant d'eau initial
     * @return Tirant d'eau
     */
    Calc_Y_Trapez(Y) {
        if (!this.Sn.HautCritique.ok) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE), this);
        }
        const dicho = new dichotomie_1.Dichotomie(this, "Y", this._debugDicho, this.CalcHS);
        // Calcul de H + J * \Delta x / 2
        // let Trapez_Fn = this.Sn.CalcSection('Hs', Y) + this.Sn.CalcSection('J', Y) / 2 * this.Dx
        const rHS = this.Sn.CalcSection("Hs", Y);
        if (!rHS.ok) {
            return rHS;
        }
        const rJ = this.Sn.CalcSection("J", Y);
        if (!rJ.ok) {
            return rJ;
        }
        let trapezFn = rHS.vCalc + rJ.vCalc / 2 * this.Dx;
        // H est la charge totale. On se place dans le référentiel ou Zf de la section à calculer = 0
        trapezFn -= this.Dx * this.section.prms.If.v;
        const r = dicho.Dichotomie(trapezFn, session_settings_1.SessionSettings.precision, Y);
        if (!r.ok) {
            return r;
        }
        // return new Result(Y2, this);
        return r;
    }
    /**
     * Calcul du point suivant d'une courbe de remous
     * @param Y Tirant d'eau initial
     * @return Tirant d'eau
     */
    Calc_Y(Y) {
        // let funcCalcY = 'Calc_Y_' + Resolution;
        // return this[funcCalcY](Y);
        let res;
        const methodeResolution = this.properties.getPropValue("methodeResolution");
        switch (methodeResolution) {
            case methode_resolution_1.MethodeResolution.Trapezes:
                res = this.Calc_Y_Trapez(Y);
                break;
            case methode_resolution_1.MethodeResolution.RungeKutta4:
                res = this.Calc_Y_RK4(Y);
                break;
            case methode_resolution_1.MethodeResolution.EulerExplicite:
                res = this.Calc_Y_EulerExplicite(Y);
                break;
            default:
                throw new Error("CourbeRemous.Calc_Y() : type de méthode de résolution " +
                    methode_resolution_1.MethodeResolution[methodeResolution] + " non pris en charge");
        }
        if (!res.ok || base_1.XOR(this.Dx > 0, !(res.vCalc < this.Sn.HautCritique.vCalc))) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE), this);
        }
        return res;
    }
    size(o) {
        return Object.keys(o).length;
    }
    last(o) {
        let res;
        // tslint:disable-next-line:forin
        for (const i in o) {
            res = o[i];
        }
        return res;
    }
    /**
     * Calcul d'une courbe de remous en fluvial ou torrentiel
     * @param YCL Condition limite amont (torrentiel) ou aval (fluvial)
     */
    calcul(YCL, varParam) {
        const res = {
            log: new log_1.cLog(),
            trY: {}
        };
        const abs0 = varParam.next().value;
        let lastY = YCL;
        res.trY[base_1.round(abs0, this.section.prms.iPrec)] = lastY;
        // Boucle de calcul de la courbe de remous
        while (varParam.hasNext) {
            const x = varParam.next().value;
            // this.debug("lastY " + lastY);
            const rY = this.Calc_Y(lastY);
            // this.debug("calcul : x " + x + " y " + rY.vCalc);
            // this.debug("trY ");
            // this.logObject(trY);
            // this.debug("end trY " + this.last(trY));
            // this.debug("Yn " + this.Sn.HautNormale);
            if (rY.ok) {
                // on vérifie qu'on ne traverse pas la hauteur normale (à la précision de calcul près)
                const prec = session_settings_1.SessionSettings.precision;
                const b1 = lastY - this.Sn.HautNormale > prec;
                const b2 = rY.vCalc - this.Sn.HautNormale > prec;
                if (base_1.XOR(b1, b2)) {
                    this.debug("La pente de la ligne d'eau est trop forte à l'abscisse "
                        + x + " m (Il faudrait réduire le pas de discrétisation)");
                    const m = new message_1.Message(message_1.MessageCode.ERROR_REMOUS_PENTE_FORTE);
                    m.extraVar.x = x;
                    // this._log.add(m);
                    res.log.add(m);
                }
                // ligne d'eau
                res.trY[base_1.round(x, this.section.prms.iPrec)] = rY.vCalc;
            }
            else {
                const m = new message_1.Message(message_1.MessageCode.WARNING_REMOUS_ARRET_CRITIQUE);
                m.extraVar.x = x;
                // this._log.add(m);
                res.log.add(m);
                this.debug("Arrêt du calcul : Hauteur critique atteinte à l'abscisse " + x + " m");
                break;
            }
            lastY = rY.vCalc;
        }
        return res;
    }
    logArray(a) {
        let s = "[";
        let first = true;
        for (const e of a) {
            if (!first) {
                s += ",";
            }
            s += String(e);
            first = false;
        }
        s += "]";
        this.debug(s);
    }
    logObject(o) {
        if (o === undefined) {
            this.debug("<undefined>");
        }
        else {
            const ks = Object.keys(o);
            ks.sort((a, b) => {
                if (+a > +b) {
                    return 1;
                }
                if (+a < +b) {
                    return -1;
                }
                return 0;
            });
            for (const k of ks) {
                this.debug("[" + (+k).toFixed(3) + "]=" + o[+k]);
            }
        }
    }
}
exports.CourbeRemous = CourbeRemous;
CourbeRemous._availableTargets = [
    "Hs", "Hsc", "B", "P", "S", "R", "V", "Fr", "Ycor", "Ycon", "J", "I-J", "Imp", "Tau0"
];

},{"../base":3,"../compute-node":5,"../dichotomie":12,"../param/mirror-iterator":85,"../param/param-definition":86,"../param/param-values":90,"../session_settings":105,"../util/log":149,"../util/message":151,"../util/result":155,"../util/resultelement":156,"./methode-resolution":36,"./section/section_nub":49}],41:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CourbeRemousParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * paramètres pour les courbes de remous
 */
class CourbeRemousParams extends params_equation_1.ParamsEquation {
    constructor(rZ1, rZ2, rZF1, rZF2, rLong, rDx) {
        super();
        this._Yamont = new param_definition_1.ParamDefinition(this, "Yamont", param_domain_1.ParamDomainValue.POS, "m", undefined, undefined, false);
        this._Yaval = new param_definition_1.ParamDefinition(this, "Yaval", param_domain_1.ParamDomainValue.POS, "m", undefined, undefined, false);
        this._Long = new param_definition_1.ParamDefinition(this, "Long", param_domain_1.ParamDomainValue.POS, "m", rLong, param_definition_1.ParamFamily.LENGTHS);
        this._Dx = new param_definition_1.ParamDefinition(this, "Dx", param_domain_1.ParamDomainValue.POS, "m", rDx);
        this._Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, param_definition_1.ParamFamily.ELEVATIONS);
        this._Z2 = new param_definition_1.ParamDefinition(this, "Z2", param_domain_1.ParamDomainValue.ANY, "m", rZ2, param_definition_1.ParamFamily.ELEVATIONS);
        this._ZF1 = new param_definition_1.ParamDefinition(this, "ZF1", param_domain_1.ParamDomainValue.ANY, "m", rZF1, param_definition_1.ParamFamily.ELEVATIONS);
        this._ZF2 = new param_definition_1.ParamDefinition(this, "ZF2", param_domain_1.ParamDomainValue.ANY, "m", rZF2, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this._Yamont);
        this.addParamDefinition(this._Yaval);
        this.addParamDefinition(this._Long);
        this.addParamDefinition(this._Dx);
        this.addParamDefinition(this._Z1);
        this.addParamDefinition(this._Z2);
        this.addParamDefinition(this._ZF1);
        this.addParamDefinition(this._ZF2);
    }
    get Yamont() {
        return this._Yamont;
    }
    get Yaval() {
        return this._Yaval;
    }
    get Long() {
        return this._Long;
    }
    get Dx() {
        return this._Dx;
    }
    get Z1() {
        return this._Z1;
    }
    get Z2() {
        return this._Z2;
    }
    get ZF1() {
        return this._ZF1;
    }
    get ZF2() {
        return this._ZF2;
    }
    /**
     * Reconstruit Yamont et Yaval à partir de Z1/ZF1 et Z2/ZF2,
     * durant le calcul
     */
    setYamontYavalFromElevations() {
        this._Yamont.singleValue = Math.max(1e-12, (this._Z1.v - this._ZF1.v));
        this._Yamont.v = this._Yamont.singleValue;
        this._Yaval.singleValue = Math.max(1e-12, (this._Z2.v - this._ZF2.v));
        this._Yaval.v = this._Yaval.singleValue;
    }
}
exports.CourbeRemousParams = CourbeRemousParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],42:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cHautConjuguee = void 0;
const message_1 = require("../../util/message");
const result_1 = require("../../util/result");
const newton_1 = require("./newton");
const section_type_params_1 = require("./section_type_params");
/**
 * Calcul de la hauteur conjuguée (Impulsion égale)
 */
// tslint:disable-next-line:class-name
class cHautConjuguee extends newton_1.acNewton {
    /**
     * Constructeur de la classe
     * @param oSn Section sur laquelle on fait le calcul
     */
    constructor(oSn, maxIter, dbg = false) {
        super(oSn.prms, maxIter, dbg);
        this.rQ2 = Math.pow(oSn.prms.Q.v, 2);
        this.Sn = oSn;
        this.rS = oSn.CalcSection("S");
        this.rSYg = oSn.CalcSection("SYg");
    }
    /**
     * Calcul de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    CalcFn(rX) {
        if (!this.rS.ok) {
            return this.rS;
        }
        if (!this.rSYg.ok) {
            return this.rSYg;
        }
        const rS2 = this.Sn.CalcSection("S", rX);
        if (!rS2.ok) {
            return rS2;
        }
        // Réinitialisation des paramètres hydrauliques de this.Sn avec l'appel this.Sn.CalcSection("S',rX)
        // if (this.rS > 0 && this.Sn.CalcSection("S", rX) > 0) {
        if (this.rS.vCalc > 0 && rS2.vCalc > 0) {
            // let Fn = this.rQ2 * (1 / this.rS - 1 / this.Sn.CalcSection("S"));
            const rS3 = this.Sn.CalcSection("S");
            if (!rS3.ok) {
                return rS3;
            }
            // tslint:disable-next-line:variable-name
            const Fn = this.rQ2 * (1 / this.rS.vCalc - 1 / rS3.vCalc);
            const rSYg = this.Sn.CalcSection("SYg");
            if (!rSYg.ok) {
                return rSYg;
            }
            // return Fn + ParamsSection.G * (this.rSYg - this.Sn.CalcSection("SYg"));
            const v = Fn + section_type_params_1.ParamsSection.G * (this.rSYg.vCalc - rSYg.vCalc);
            return new result_1.Result(v);
        }
        return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_PERIMETRE_NUL));
        // return -Infinity;
    }
    /**
     * Calcul analytique de la dérivée de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    CalcDer(rX) {
        if (!this.rS.ok) {
            return this.rS;
        }
        //                let S = this.Sn.CalcSection("S");
        const rS2 = this.Sn.CalcSection("S");
        if (!rS2.ok) {
            return rS2;
        }
        const S = rS2.vCalc;
        // L'initialisation a été faite lors de l'appel à CalcFn
        // if (this.rS > 0 && S > 0) {
        if (this.rS.vCalc > 0 && S > 0) {
            const rDS = this.Sn.CalcSection("dS");
            if (!rDS.ok) {
                return rDS;
            }
            // let Der = this.rQ2 * this.Sn.CalcSection("dS") * Math.pow(S, -2);
            // tslint:disable-next-line:variable-name
            const Der = this.rQ2 * rDS.vCalc * Math.pow(S, -2);
            const rDYG = this.Sn.CalcSection("dSYg", rX);
            if (!rDYG.ok) {
                return rDYG;
            }
            // return Der - ParamsSection.G * this.Sn.CalcSection("dSYg", rX);
            const v = Der - section_type_params_1.ParamsSection.G * rDYG.vCalc;
            return new result_1.Result(v);
        }
        return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_PERIMETRE_NUL));
        // return -Infinity;
    }
}
exports.cHautConjuguee = cHautConjuguee;

},{"../../util/message":151,"../../util/result":155,"./newton":46,"./section_type_params":59}],43:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cHautCorrespondante = void 0;
const message_1 = require("../../util/message");
const result_1 = require("../../util/result");
const newton_1 = require("./newton");
const section_type_params_1 = require("./section_type_params");
/**
 * Calcul de la hauteur correspondante (charge égale)
 */
// tslint:disable-next-line:class-name
class cHautCorrespondante extends newton_1.acNewton {
    /**
     * Constructeur de la classe
     * @param oSn Section sur laquelle on fait le calcul
     */
    // tslint:disable-next-line:variable-name
    constructor(Sn, maxIter, dbg = false) {
        super(Sn.prms, maxIter, dbg);
        this.Y = Sn.prms.Y.v;
        //                this.rS2 = Math.pow(Sn.CalcSection("S"), -2);
        this.Sn = Sn;
        // tslint:disable-next-line:no-unused-expression
        this.rS2; // pour initialiser la valeur @WTF (utilise le getter)
        this.rQ2G = Math.pow(Sn.prms.Q.v, 2) / (2 * section_type_params_1.ParamsSection.G);
    }
    get rS2() {
        if (this._rS2 === undefined) {
            const rS = this.Sn.CalcSection("S");
            if (rS.ok) {
                const v = Math.pow(rS.vCalc, -2);
                this._rS2 = new result_1.Result(v);
            }
            else {
                this._rS2 = rS;
            }
        }
        return this._rS2;
    }
    /**
     * Calcul de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    CalcFn(rX) {
        if (!this.rS2.ok) {
            return this.rS2;
        }
        const rS = this.Sn.CalcSection("S", rX);
        if (!rS.ok) {
            return rS;
        }
        // Calcul de la fonction
        // return this.Y - rX + (this.rS2 - Math.pow(this.Sn.CalcSection("S", rX), -2)) * this.rQ2G;
        const v = this.Y - rX + (this.rS2.vCalc - Math.pow(rS.vCalc, -2)) * this.rQ2G;
        return new result_1.Result(v);
    }
    /**
     * Calcul analytique de la dérivée de la fonction dont on protected function cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    CalcDer(rX) {
        // let S = this.Sn.CalcSection("S");
        const rS = this.Sn.CalcSection("S");
        if (!rS.ok) {
            return rS;
        }
        const S = rS.vCalc;
        // L'initialisation a été faite lors de l'appel à CalcFn
        // if (S !== 0)
        if (S === 0) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }
        const rB = this.Sn.CalcSection("B");
        if (!rB.ok) {
            return rB;
        }
        // return -1 + 2 * this.rQ2G * this.Sn.CalcSection("B") / Math.pow(S, 3);
        const v = -1 + 2 * this.rQ2G * rB.vCalc / Math.pow(S, 3);
        return new result_1.Result(v);
        // return Infinity;
    }
}
exports.cHautCorrespondante = cHautCorrespondante;

},{"../../util/message":151,"../../util/result":155,"./newton":46,"./section_type_params":59}],44:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cHautCritique = void 0;
const message_1 = require("../../util/message");
const result_1 = require("../../util/result");
const newton_1 = require("./newton");
const section_type_params_1 = require("./section_type_params");
/**
 * Calcul de la hauteur critique
 */
// tslint:disable-next-line:class-name
class cHautCritique extends newton_1.acNewton {
    /**
     * Constructeur de la classe
     * @param Sn Section sur laquelle on fait le calcul
     */
    // tslint:disable-next-line:variable-name
    constructor(Sn, maxIter, dbg = false) {
        super(Sn.prms, maxIter, dbg);
        this.Sn = Sn.clone();
    }
    /**
     * Calcul de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    CalcFn(rX) {
        const rS = this.Sn.CalcSection("S", rX);
        if (!rS.ok) {
            return rS;
        }
        // Calcul de la fonction
        // if (this.Sn.CalcSection("S", rX) !== 0)
        if (rS.vCalc === 0) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }
        /* return (Math.pow(this.Sn.prms.Q.v, 2)
                * this.Sn.CalcSection("B", rX) / Math.pow(this.Sn.CalcSection("S", rX), 3)
                / ParamsSection.G - 1); */
        const rB = this.Sn.CalcSection("B", rX);
        if (!rB.ok) {
            return rB;
        }
        const rS2 = this.Sn.CalcSection("S", rX);
        if (!rS2.ok) {
            return rS2;
        }
        const v = (Math.pow(this.Sn.prms.Q.v, 2) * rB.vCalc / Math.pow(rS2.vCalc, 3) / section_type_params_1.ParamsSection.G - 1);
        return new result_1.Result(v);
        // return Infinity;
    }
    /**
     * Calcul analytique de la dérivée de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    CalcDer(rX) {
        // let S = this.Sn.CalcSection("S");
        const rS = this.Sn.CalcSection("S");
        if (!rS.ok) {
            return rS;
        }
        const S = rS.vCalc;
        // if (S !== 0) {
        if (S === 0) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }
        // let B = this.Sn.CalcSection("B");
        const rB = this.Sn.CalcSection("B");
        if (!rB.ok) {
            return rB;
        }
        const B = rB.vCalc;
        const rDB = this.Sn.CalcSection("dB");
        if (!rDB.ok) {
            return rDB;
        }
        // L'initialisation à partir de rX a été faite lors de l'appel à CalcFn
        // let Der = (this.Sn.CalcSection("dB") * S - 3 * B * B);
        // tslint:disable-next-line:variable-name
        const Der = (rDB.vCalc * S - 3 * B * B);
        const v = Math.pow(this.Sn.prms.Q.v, 2) / section_type_params_1.ParamsSection.G * Der / Math.pow(S, 4);
        return new result_1.Result(v);
        // }
        // return Infinity;
    }
}
exports.cHautCritique = cHautCritique;

},{"../../util/message":151,"../../util/result":155,"./newton":46,"./section_type_params":59}],45:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cHautNormale = void 0;
const result_1 = require("../../util/result");
const newton_1 = require("./newton");
/**
 * Calcul de la hauteur normale
 */
// tslint:disable-next-line:class-name
class cHautNormale extends newton_1.acNewton {
    /**
     * Constructeur de la classe
     * @param oSn Section sur laquelle on fait le calcul
     */
    // tslint:disable-next-line:variable-name
    constructor(Sn, maxIter, dbg = false) {
        super(Sn.prms, maxIter, dbg);
        this.Sn = Sn;
        this.Q = Sn.prms.Q.v;
        this.Ks = Sn.prms.Ks.v;
        this.If = Sn.prms.If.v;
    }
    /**
     * Calcul de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    CalcFn(rX) {
        // Calcul de la fonction
        /* return (this.Q - this.Ks * Math.pow(this.Sn.CalcSection("R", rX), 2 / 3)
            * this.Sn.CalcSection("S", rX) * Math.sqrt(this.If)); */
        const rR = this.Sn.CalcSection("R", rX);
        if (!rR.ok) {
            return rR;
        }
        const rS = this.Sn.CalcSection("S", rX);
        if (!rS.ok) {
            return rS;
        }
        const v = (this.Q - this.Ks * Math.pow(rR.vCalc, 2 / 3) * rS.vCalc * Math.sqrt(this.If));
        return new result_1.Result(v);
    }
    /**
     * Calcul analytique de la dérivée de la fonction dont on cherche le zéro
     * @param rX Variable dont dépend la fonction
     */
    CalcDer(rX) {
        const rDR = this.Sn.CalcSection("dR");
        if (!rDR.ok) {
            return rDR;
        }
        const rR = this.Sn.CalcSection("R");
        if (!rR.ok) {
            return rR;
        }
        const rS = this.Sn.CalcSection("S");
        if (!rS.ok) {
            return rS;
        }
        // L'initialisation a été faite lors de l'appel à CalcFn
        // let Der = 2 / 3 * this.Sn.CalcSection("dR") * Math.pow(this.Sn.CalcSection("R"), -1 / 3)
        //                 * this.Sn.CalcSection("S");
        // tslint:disable-next-line:variable-name
        let Der = 2 / 3 * rDR.vCalc * Math.pow(rR.vCalc, -1 / 3) * rS.vCalc;
        const rR2 = this.Sn.CalcSection("R");
        if (!rR2.ok) {
            return rR2;
        }
        const rB = this.Sn.CalcSection("B");
        if (!rB.ok) {
            return rB;
        }
        // Der = Der + Math.pow(this.Sn.CalcSection("R"), 2 / 3) * this.Sn.CalcSection("B");
        Der = Der + Math.pow(rR2.vCalc, 2 / 3) * rB.vCalc;
        Der = Der * -this.Ks * Math.sqrt(this.If);
        return new result_1.Result(Der);
    }
}
exports.cHautNormale = cHautNormale;

},{"../../util/result":155,"./newton":46}],46:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.acNewton = void 0;
const base_1 = require("../../base");
const session_settings_1 = require("../../session_settings");
const message_1 = require("../../util/message");
const result_1 = require("../../util/result");
const resultelement_1 = require("../../util/resultelement");
// tslint:disable-next-line:class-name
class acNewton extends base_1.Debug {
    /**
     * Constructeur de la classe
     * @param prms Paramètres supplémentaires (Débit, précision...)
     */
    constructor(prms, maxIter, dbg = false) {
        super(dbg);
        this.rRelax = 1; /// Coefficient de relaxation
        this.rFnPrec = 0; /// Mémorisation du Fn précédent pour détecter le changement de signe
        this.iOscil = 0; /// Nombre de changement de signe de Delta
        this.rTol = session_settings_1.SessionSettings.precision;
        this.Dx = session_settings_1.SessionSettings.precision / 10;
        this.iCptMax = maxIter;
    }
    Newton(rX) {
        this.iCpt = 0;
        return this.doNewton(rX);
    }
    /**
     * Calcul de la dérivée f'(x) (peut être redéfini pour calcul analytique)
     * @param rX x
     * @return Calcul de la fonction
     */
    CalcDer(x) {
        const rFN1 = this.CalcFn(x - this.Dx);
        if (!rFN1.ok) {
            return rFN1;
        }
        const rFN2 = this.CalcFn(x + this.Dx);
        if (!rFN2.ok) {
            return rFN2;
        }
        // return (this.CalcFn(x + this.Dx) - this.CalcFn(x - this.Dx)) / (2 * this.Dx);
        const v = (rFN2.vCalc - rFN1.vCalc) / (2 * this.Dx);
        return new result_1.Result(v);
    }
    /**
     * Test d'égalité à une tolérance près
     * @param rFn x
     * @return True si égal, False sinon
     */
    FuzzyEqual(rFn) {
        return (Math.abs(rFn) < this.rTol);
    }
    /**
     * Fonction récursive de calcul de la suite du Newton
     * @param rX x
     * @return Solution du zéro de la fonction
     */
    doNewton(rX) {
        this.iCpt++;
        const rFN = this.CalcFn(rX);
        if (!rFN.ok) {
            return rFN;
        }
        const rFn = rFN.vCalc;
        // if (this.FuzzyEqual(rFn) || this.iCpt >= this.iCptMax)
        //         return new Result(rX);
        if (this.FuzzyEqual(rFn)) { // convergence
            return new result_1.Result(rX);
        }
        if (this.iCpt >= this.iCptMax) { // non convergence
            const mess = new message_1.Message(message_1.MessageCode.ERROR_NEWTON_NON_CONVERGENCE);
            const res = new resultelement_1.ResultElement(mess);
            res.addExtraResult("res", rX);
            return new result_1.Result(res);
        }
        const rDER = this.CalcDer(rX);
        if (!rDER.ok) {
            return rDER;
        }
        const rDer = Math.max(-1E10, Math.min(rDER.vCalc, 1E10)); // Bornage de la dérivée pour éviter Delta=0
        if (rDer !== 0) {
            if (base_1.XOR(rFn < 0, this.rFnPrec < 0)) {
                this.iOscil++;
                if (this.rRelax > 1) {
                    // Sur une forte relaxation, au changement de signe on réinitialise
                    this.rRelax = 1;
                }
                else if (this.iOscil > 2) {
                    // On est dans le cas d'une oscillation autour de la solution
                    // On réduit le coefficient de relaxation
                    this.rRelax = this.rRelax * 0.5;
                }
            }
            this.rFnPrec = rFn;
            // tslint:disable-next-line:variable-name
            const Delta = rFn / rDer;
            // 2^8 = 2E8 ?
            while (Math.abs(Delta * this.rRelax) < this.rTol && rFn > 10 * this.rTol && this.rRelax < 2E8) {
                // On augmente le coefficicient de relaxation s'il est trop petit
                this.rRelax = this.rRelax * 2;
            }
            let rRelax = this.rRelax;
            while (rX - Delta * rRelax <= 0 && rRelax > 1E-4) {
                // On diminue le coefficient de relaxation si on passe en négatif
                rRelax = rRelax * 0.5; // Mais on ne le mémorise pas pour les itérations suivantes
            }
            rX = rX - Delta * rRelax;
            // this.rDelta = Delta; ???
            if (rX < 0) {
                rX = this.rTol;
            } // Aucune valeur recherchée ne peut être négative ou nulle
            return this.doNewton(rX);
        }
        // Echec de la résolution
        // return undefined;
        const m = new message_1.Message(message_1.MessageCode.ERROR_NEWTON_DERIVEE_NULLE);
        return new result_1.Result(m);
    }
}
exports.acNewton = acNewton;

},{"../../base":3,"../../session_settings":105,"../../util/message":151,"../../util/result":155,"../../util/resultelement":156}],47:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cSnCirc = void 0;
const compute_node_1 = require("../../compute-node");
const param_definition_1 = require("../../param/param-definition");
const message_1 = require("../../util/message");
const result_1 = require("../../util/result");
const section_type_1 = require("./section_type");
/**
 * Calculs de la section circulaire
 */
// tslint:disable-next-line:class-name
class cSnCirc extends section_type_1.acSection {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this.nbDessinPoints = 50;
        this._nodeType = compute_node_1.SectionType.SectionCercle;
        // commenté car si D est la variable à déterminer, il peut valoir n'importe
        // quoi... if (prms.YB.v > D) { prms.YB.v = D; } // On place la berge au sommet du cercle
        // if (this.prms.D.isDefined() && this.prms.YB.isDefined())
        //         this.CalcGeo('B');
    }
    get prms() {
        return this._prms;
    }
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.D.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    Calc_Alpha() {
        this.debug("Calc_Alpha : bSnFermee " + this.bSnFermee);
        if (this.prms.Y.v <= 0) {
            return new result_1.Result(0);
        }
        if (this.isDebordement()) {
            if (this.bSnFermee) {
                return new result_1.Result(Math.PI);
            }
            return this.Calc_AlphaY(this.prms.YB.v, this.prms.D.v);
        }
        return this.Calc_AlphaY(this.prms.Y.v, this.prms.D.v);
    }
    /**
     * Calcul de dérivée de l'angle Alpha de la surface libre par rapport au fond.
     * @return dAlpha
     */
    Calc_dAlpha() {
        if (this.prms.Y.v <= 0 || this.isDebordement()) {
            return new result_1.Result(0);
        }
        const v = 2. / this.prms.D.v / Math.sqrt(1. - Math.pow(1. - 2. * this.prms.Y.v / this.prms.D.v, 2));
        return new result_1.Result(v);
    }
    /**
     * Calcul de la largeur au miroir.
     * @return B
     */
    Calc_B() {
        if (this.isDebordement()) {
            return this.Calc_LargeurBerge();
        }
        this.debug("circ.Calc_B() : PAS débordement");
        if (this.prms.D.hasCurrentValue && this.prms.Y.hasCurrentValue) {
            const rAlpha = this.calcFromY("Alpha");
            if (!rAlpha.ok) {
                return rAlpha;
            }
            const res = this.prms.D.v * Math.sin(rAlpha.vCalc);
            this.debug("circ.Calc_B() : res=" + res);
            return new result_1.Result(res);
        }
        const e = new message_1.Message(message_1.MessageCode.ERROR_PARAMDEF_VALUE_UNDEFINED);
        e.extraVar.diameterValue = this.prms.D.hasCurrentValue ? String(this.prms.D.v) : "undefined";
        e.extraVar.yValue = this.prms.Y.hasCurrentValue ? String(this.prms.Y.v) : "undefined";
        throw e;
    }
    /**
     * Calcul du périmètre mouillé.
     * @return B
     */
    Calc_P() {
        let v;
        if (!this.bSnFermee && this.isDebordement()) {
            // On n'ajoute pas le périmètre dans le cas d'une fente de Preissmann
            // return this.CalcGeo("P") + super.Calc_P_Debordement(this.valeurYDebordement());
            const rGeoP = this.CalcGeo("P");
            if (!rGeoP.ok) {
                return rGeoP;
            }
            const rPDeb = super.Calc_P_Debordement(this.valeurYDebordement());
            if (!rPDeb.ok) {
                return rPDeb;
            }
            v = rGeoP.vCalc + rPDeb.vCalc;
            return new result_1.Result(v);
        }
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        v = this.prms.D.v * rAlpha.vCalc;
        return new result_1.Result(v);
    }
    /**
     * Calcul de la surface mouillée.
     * @return S
     */
    Calc_S() {
        let v;
        if (!this.bSnFermee && this.isDebordement()) {
            // return this.CalcGeo("S") + super.Calc_S_Debordement(this.valeurYDebordement());
            const rGeoS = this.CalcGeo("S");
            if (!rGeoS.ok) {
                return rGeoS;
            }
            const rSDeb = super.Calc_S_Debordement(this.valeurYDebordement());
            if (!rSDeb.ok) {
                return rSDeb;
            }
            v = rGeoS.vCalc + rSDeb.vCalc;
            return new result_1.Result(v);
        }
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        // return Math.pow(this.prms.D.v, 2) / 4 * (alpha - Math.sin(alpha) * Math.cos(alpha));
        const alpha = rAlpha.vCalc;
        v = Math.pow(this.prms.D.v, 2) / 4 * (alpha - Math.sin(alpha) * Math.cos(alpha));
        return new result_1.Result(v);
    }
    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau.
     * @return dP
     */
    Calc_dP() {
        if (!this.bSnFermee && this.isDebordement()) {
            return super.Calc_dP_Debordement();
        }
        const rDAlpha = this.calcFromY("dAlpha");
        if (!rDAlpha.ok) {
            return rDAlpha;
        }
        const v = this.prms.D.v * rDAlpha.vCalc;
        return new result_1.Result(v);
    }
    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau.
     * @return dB
     */
    Calc_dB() {
        if (this.isDebordement()) {
            return super.Calc_dB_Debordement();
        }
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const rDAlpha = this.calcFromY("dAlpha");
        if (!rDAlpha.ok) {
            return rDAlpha;
        }
        const v = this.prms.D.v * rDAlpha.vCalc * Math.cos(rAlpha.vCalc);
        return new result_1.Result(v);
    }
    /**
     * Calcul de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    Calc_SYg() {
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const alpha = rAlpha.vCalc;
        // tslint:disable-next-line:variable-name
        let SYg = Math.sin(alpha) - Math.pow(Math.sin(alpha), 3) / 3 - alpha * Math.cos(alpha);
        SYg = Math.pow(this.prms.D.v, 3) / 8 * SYg;
        return new result_1.Result(SYg);
    }
    /**
     * Calcul de la dérivée de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    Calc_dSYg() {
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const rDAlpha = this.calcFromY("dAlpha");
        if (!rDAlpha.ok) {
            return rDAlpha;
        }
        const alpha = rAlpha.vCalc;
        const dAlpha = rDAlpha.vCalc;
        const cos = Math.cos(alpha);
        const sin = Math.sin(alpha);
        // tslint:disable-next-line:variable-name
        let SYg = dAlpha * cos;
        SYg += -dAlpha * cos * Math.pow(sin, 2);
        SYg += -dAlpha * cos + alpha * dAlpha * sin;
        SYg = 3 * Math.pow(this.prms.D.v, 3) / 8 * SYg;
        return new result_1.Result(SYg);
    }
    /**
     * valeur du débordement
     * Le tirant d'eau est soustrait soit à la côte de berge, soit au diamètre
     * au cas où le canal soit "enterré" (côte de berge > diamètre)
     */
    valeurYDebordement() {
        return this.prms.Y.v - Math.min(this.prms.YB.v, this.prms.D.v);
        // >= 0 par définition, et toujours vrai car utilisé après test isDebordement()
    }
    /**
     * teste le débordement.
     * @returns true en cas de débordement
     */
    isDebordement() {
        this.debug("circ.isDebordement() : Y " + this.prms.Y.toString());
        this.debug("circ.isDebordement() : D " + this.prms.D.toString());
        this.debug("circ.isDebordement() : YB " + this.prms.YB.toString());
        this.debug("circ.isDebordement() : res=" + (this.prms.Y.v > Math.min(this.prms.D.v, this.prms.YB.v)));
        return this.prms.Y.v > Math.min(this.prms.D.v, this.prms.YB.v);
    }
    /**
     * Calcul de l'angle Alpha de la surface libre par rapport au fond.
     * @return Alpha
     */
    Calc_AlphaY(Y, D) {
        const alpha = Math.acos(1. - Y / (D / 2.));
        return new result_1.Result(Math.min(alpha, Math.PI));
    }
    Calc_LargeurBerge() {
        const d = this.prms.D.v;
        const yb = Math.min(this.prms.YB.v, d);
        const rAY = this.Calc_AlphaY(yb, d);
        if (!rAY.ok) {
            return rAY;
        }
        // let res = d * Math.sin(this.Calc_AlphaY(yb, d));
        const res = d * Math.sin(rAY.vCalc);
        return new result_1.Result(res);
    }
}
exports.cSnCirc = cSnCirc;

},{"../../compute-node":5,"../../param/param-definition":86,"../../util/message":151,"../../util/result":155,"./section_type":58}],48:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamsSectionCirc = void 0;
const param_definition_1 = require("../../param/param-definition");
const param_domain_1 = require("../../param/param-domain");
const section_type_params_1 = require("./section_type_params");
class ParamsSectionCirc extends section_type_params_1.ParamsSection {
    constructor(rD, rY, rKs, rQ, rIf, rYB) {
        // set LargeurBerge to default value so that it is not undefined when changing section type
        super(rY, 2.5, rKs, rQ, rIf, rYB);
        this._D = new param_definition_1.ParamDefinition(this, "D", param_domain_1.ParamDomainValue.POS, "m", rD, param_definition_1.ParamFamily.DIAMETERS);
        this.addParamDefinition(this._D);
        // hide params
        this.LargeurBerge.visible = false;
        this.LargeurBerge.undefineFamily(); // or else something might get linked to it although it is undefined
    }
    /**
     * Diamètre du cercle
     */
    get D() {
        return this._D;
    }
}
exports.ParamsSectionCirc = ParamsSectionCirc;

},{"../../param/param-definition":86,"../../param/param-domain":87,"./section_type_params":59}],49:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SectionNub = void 0;
const index_1 = require("../../index");
const nub_1 = require("../../nub");
/**
 * A Nub that contains an acSection (ex: RegimeUniforme, SectionParametree, Remous)
 */
class SectionNub extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._sectionVars = {};
    }
    /** Section is always the first child */
    get section() {
        if (this._children && this._children.length > 0) {
            return this._children[0];
        }
        else {
            return undefined;
        }
    }
    /** Returns Props object (observable set of key-values) associated to this Nub */
    get properties() {
        // completes props with calcType if not already set
        this._props.setPropValue("calcType", this.calcType);
        return this._props;
    }
    // setter is not inherited from Nub if getter is redefined :/
    set properties(props) {
        super.setProperties(props);
    }
    getParameter(name) {
        if (typeof name !== "string") {
            // dirty hack because calculated param descriptor for section params is an object { uid: , symbol: }
            name = name.symbol;
        }
        let res = super.getParameter(name);
        if (res === undefined) {
            // not found - maybe a section var ?
            res = this._sectionVars[name];
            if (!res) {
                throw new Error(`in ${this.constructor.name}, SectionNub.getParameter() : nom de paramètre ${name} incorrect`);
            }
        }
        return res;
    }
    /**
     * Once session is loaded, run a second pass on all linked parameters to
     * reset their target if needed
     */
    fixLinks(obj) {
        // return value
        const ret = {
            hasErrors: false
        };
        // iterate over parameters
        super.fixLinks(obj);
        // iterate over children if any
        if (obj.children && Array.isArray(obj.children)) {
            for (const s of obj.children) {
                // get the Nub
                const subNub = index_1.Session.getInstance().findNubByUid(s.uid);
                const res = subNub.fixLinks(s);
                // forward errors
                if (res.hasErrors) {
                    ret.hasErrors = true;
                }
            }
        }
        return ret;
    }
    /**
     * Proxy to setSection() to ensure extra code is executed when
     * unserialising session (a SectionNub child can only be the
     * one and only Section)
     */
    addChild(child, after) {
        this.setSection(child);
    }
    /**
     * Sets / replaces the current section
     */
    setSection(s) {
        if (s) {
            // prevent index out of bounds at first time
            if (this._children.length === 0) {
                this._children[0] = undefined;
            }
            this.replaceChild(0, s);
            // Y from Section is not linkable here
            this.section.prms.Y.undefineFamily();
        }
    }
}
exports.SectionNub = SectionNub;

},{"../../index":14,"../../nub":33}],50:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SectionParametree = void 0;
const compute_node_1 = require("../../compute-node");
const param_definition_1 = require("../../param/param-definition");
const param_domain_1 = require("../../param/param-domain");
const message_1 = require("../../util/message");
const section_nub_1 = require("./section_nub");
const section_parametree_params_1 = require("./section_parametree_params");
/**
 * Nub sur les sections paramétrées
 */
class SectionParametree extends section_nub_1.SectionNub {
    constructor(s, dbg = false) {
        super(new section_parametree_params_1.SectionParams(), dbg);
        this._calcType = compute_node_1.CalculatorType.SectionParametree;
        this.setSection(s);
        this.initSectionVars();
        // no calculated parameter
        this._defaultCalculatedParam = undefined;
    }
    setSection(s) {
        super.setSection(s);
        this.setParametersCalculability(); // to override acSection's tuning
    }
    getFirstAnalyticalParameter() {
        const res = super.getFirstAnalyticalParameter();
        if (res) {
            return res;
        }
        // tslint:disable-next-line:forin
        for (const k in this._sectionVars) {
            const p = this._sectionVars[k];
            if (p.isAnalytical) {
                return p;
            }
        }
        return undefined;
    }
    Equation(sVarCalc) {
        let r;
        switch (sVarCalc) {
            case "Hs":
            case "Hsc":
            case "B":
            case "P":
            case "S":
            case "R":
            case "V":
            case "Fr":
            case "Yc":
            case "Yn":
            case "Ycor":
            case "Ycon":
            case "J":
            case "Imp":
            case "Tau0":
            case "I-J":
                r = this.section.CalcSection(sVarCalc, this.getParameter("Y").v);
                break;
            default:
                throw new Error(`SectionParam.Equation() : calcul sur ${sVarCalc} non implémenté`);
        }
        return r;
    }
    /**
     * Aucune variable à calculer plus que les autres, on stocke toutes les
     * valeurs des variables à calcul dans les résultats complémentaires
     */
    Calc() {
        // tirant d'eau original (doit être fourni à acSection.Calc() sous peine d'être modifié
        // par les appels successifs car c'est en même temps un paramètre et une variable temporaire)
        const Y = this.getParameter("Y").v;
        if (!this.result) {
            this.initNewResultElement();
        }
        // charge spécifique
        this.addExtraResultFromVar("Hs", Y);
        // charge critique
        this.addExtraResultFromVar("Hsc", Y);
        // largeur au miroir
        this.addExtraResultFromVar("B", Y);
        // périmètre hydraulique
        this.addExtraResultFromVar("P", Y);
        // surface hydraulique
        this.addExtraResultFromVar("S", Y);
        // rayon hydraulique
        this.addExtraResultFromVar("R", Y);
        // vitesse moyenne
        this.addExtraResultFromVar("V", Y);
        // nombre de Froude
        this.addExtraResultFromVar("Fr", Y);
        // tirant d'eau critique
        this.addExtraResultFromVar("Yc", Y);
        // tirant d'eau normal
        this.addExtraResultFromVar("Yn", Y);
        // tirant d'eau correspondant
        this.addExtraResultFromVar("Ycor", Y);
        // tirant d'eau conjugué
        this.addExtraResultFromVar("Ycon", Y);
        // perte de charge
        this.addExtraResultFromVar("J", Y);
        // Variation linéaire de l'énergie spécifique
        this.addExtraResultFromVar("I-J", Y);
        // impulsion hydraulique
        this.addExtraResultFromVar("Imp", Y);
        // contrainte de cisaillement / force tractrice
        this.addExtraResultFromVar("Tau0", Y);
        // Est-ce que ça déborde ?
        if (this.section.prms.Y.v > this.section.prms.YB.v) {
            this.result.resultElement.log.add(new message_1.Message(message_1.MessageCode.WARNING_SECTION_OVERFLOW));
        }
        return this.result;
    }
    // calculated param is always "Y"
    findCalculatedParameter() {
        return undefined;
    }
    // calculated param is always "Y"
    doCalc(computedSymbol, rInit) {
        return this.Calc();
    }
    // tslint:disable-next-line:no-empty
    setParametersCalculability() {
        if (this.section) {
            this.section.prms.Ks.calculability = param_definition_1.ParamCalculability.FREE;
            this.section.prms.Q.calculability = param_definition_1.ParamCalculability.FREE;
            this.section.prms.If.calculability = param_definition_1.ParamCalculability.FREE;
            this.section.prms.YB.calculability = param_definition_1.ParamCalculability.FREE;
            this.section.prms.Y.calculability = param_definition_1.ParamCalculability.FREE;
            this.section.prms.LargeurBerge.calculability = param_definition_1.ParamCalculability.FREE;
            // parameters depending on section type
            const D = this.section.getParameter("D");
            if (D) {
                D.calculability = param_definition_1.ParamCalculability.FREE;
            }
            const k = this.section.getParameter("k");
            if (k) {
                k.calculability = param_definition_1.ParamCalculability.FREE;
            }
            const fruit = this.section.getParameter("Fruit");
            if (fruit) {
                fruit.calculability = param_definition_1.ParamCalculability.FREE;
            }
            const largeurFond = this.section.getParameter("LargeurFond");
            if (largeurFond) {
                largeurFond.calculability = param_definition_1.ParamCalculability.FREE;
            }
        }
    }
    // tslint:disable-next-line:no-empty
    adjustChildParameters() { }
    exposeResults() {
        this._resultsFamilies = {
            B: param_definition_1.ParamFamily.WIDTHS,
            Yc: param_definition_1.ParamFamily.HEIGHTS,
            Yn: param_definition_1.ParamFamily.HEIGHTS,
            Ycor: param_definition_1.ParamFamily.HEIGHTS,
            Ycon: param_definition_1.ParamFamily.HEIGHTS
        };
    }
    initSectionVars() {
        this.initSectionVar("Hs");
        this.initSectionVar("Hsc");
        this.initSectionVar("B");
        this.initSectionVar("P");
        this.initSectionVar("S");
        this.initSectionVar("R");
        this.initSectionVar("V");
        this.initSectionVar("Fr");
        this.initSectionVar("Yc");
        this.initSectionVar("Yn");
        this.initSectionVar("Ycor");
        this.initSectionVar("Ycon");
        this.initSectionVar("J");
        this.initSectionVar("Imp");
        this.initSectionVar("Tau0");
        this.initSectionVar("I-J");
    }
    initSectionVar(symbol) {
        this._sectionVars[symbol] = this.createSectionVar(symbol);
    }
    createSectionVar(symbol) {
        let dom;
        let unit;
        switch (symbol) {
            case "Hs":
            case "Hsc":
            case "B":
            case "P":
            case "R":
            case "Yc":
            case "Yn":
            case "Ycor":
            case "Ycon":
            case "J":
            case "Imp":
                dom = new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.POS_NULL);
                unit = "m";
                break;
            case "Tau0": // force tractrice
                dom = new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.POS_NULL);
                unit = "Pa";
                break;
            case "V": // vitesse moyenne
                dom = new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.POS_NULL);
                unit = "m/s";
                break;
            case "S": // surface
                dom = new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.POS_NULL);
                unit = "m²";
                break;
            case "Fr": // Froude
                dom = new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.POS_NULL);
                break;
            case "I-J": // var. lin. énergie spécifique
                unit = "m/m";
                dom = new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.ANY);
                break;
            default:
                throw new Error(`SectionParametree.createSectionVar() : symbole ${symbol} non pris en charge`);
        }
        const pe = new section_parametree_params_1.SectionParams(this);
        const res = new param_definition_1.ParamDefinition(pe, symbol, dom, unit);
        res.calculability = param_definition_1.ParamCalculability.EQUATION;
        return res;
    }
    /**
     * Calculates varCalc from Y, and sets the result as an
     * extraResult of the given ResultElement
     */
    addExtraResultFromVar(varCalc, Y) {
        const r = this.section.CalcSection(varCalc, Y);
        if (r.ok) {
            this.result.resultElement.addExtraResult(varCalc, r.vCalc);
        }
        else {
            this.result.resultElement.log.addLog(r.log);
        }
    }
}
exports.SectionParametree = SectionParametree;

},{"../../compute-node":5,"../../param/param-definition":86,"../../param/param-domain":87,"../../util/message":151,"./section_nub":49,"./section_parametree_params":51}],51:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SectionParams = void 0;
const params_equation_1 = require("../../param/params-equation");
// dummy ParamsEquation for code consistency
class SectionParams extends params_equation_1.ParamsEquation {
}
exports.SectionParams = SectionParams;

},{"../../param/params-equation":92}],52:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cSnPuiss = void 0;
const compute_node_1 = require("../../compute-node");
const param_definition_1 = require("../../param/param-definition");
const result_1 = require("../../util/result");
const section_type_1 = require("./section_type");
/**
 * Calculs de la section parabolique ou "puissance"
 */
// tslint:disable-next-line:class-name
class cSnPuiss extends section_type_1.acSection {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this.nbDessinPoints = 50;
        this._nodeType = compute_node_1.SectionType.SectionPuissance;
    }
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.k.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    get prms() {
        return this._prms;
    }
    /**
     * Calcul de Lambda (mais on garde la routine Alpha commune avec la section circulaire)
     * @return Lambda
     */
    Calc_Alpha() {
        const v = this.prms.LargeurBerge.v / Math.pow(this.prms.YB.v, this.prms.k.v);
        return new result_1.Result(v);
    }
    /**
     * Calcul de la largeur au miroir.
     * @return B
     */
    Calc_B() {
        if (this.prms.Y.v >= this.prms.YB.v) {
            return new result_1.Result(this.prms.LargeurBerge.v);
        }
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const v = rAlpha.vCalc * Math.pow(this.prms.Y.v, this.prms.k.v);
        return new result_1.Result(v);
    }
    /**
     * Calcul du périmètre mouillé.
     * @return B
     */
    Calc_P() {
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const n = 100; /// Le nombre de partie pour le calcul de l'intégrale
        // tslint:disable-next-line:variable-name
        const Lambda2 = Math.pow(rAlpha.vCalc, 2);
        let P = 0; /// Le périmètre à calculer
        // tslint:disable-next-line:variable-name
        let Previous = 0;
        for (let i = 1; i <= n; i++) {
            // tslint:disable-next-line:variable-name
            const Current = Math.pow(this.prms.Y.v * i / n, this.prms.k.v) / 2;
            P += Math.sqrt(Math.pow(n, -2) + Lambda2 * Math.pow(Current - Previous, 2));
            Previous = Current;
        }
        P *= 2;
        return new result_1.Result(P);
    }
    /**
     * Calcul de la surface mouillée.
     * @return S
     */
    Calc_S() {
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const k = this.prms.k.v;
        const v = rAlpha.vCalc * Math.pow(this.prms.Y.v, k + 1) / (k + 1);
        return new result_1.Result(v);
    }
    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau.
     * @return dP
     */
    Calc_dP() {
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const k = this.prms.k.v;
        const v = 2 * Math.sqrt(1 + Math.pow(k * rAlpha.vCalc / 2, 2) * Math.pow(this.prms.Y.v, 2 * (k - 1)));
        return new result_1.Result(v);
    }
    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau.
     * @return dB
     */
    Calc_dB() {
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const k = this.prms.k.v;
        const v = rAlpha.vCalc * k * Math.pow(this.prms.Y.v, k - 1);
        return new result_1.Result(v);
    }
    /**
     * Calcul de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    Calc_SYg() {
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const k = this.prms.k.v;
        const v = rAlpha.vCalc * Math.pow(this.prms.Y.v, k + 2) / ((k + 1) * (k + 2));
        return new result_1.Result(v);
    }
    /**
     * Calcul de la dérivée distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    Calc_dSYg() {
        const rAlpha = this.calcFromY("Alpha");
        if (!rAlpha.ok) {
            return rAlpha;
        }
        const rDAlpha = this.calcFromY("dAlpha");
        if (!rDAlpha.ok) {
            return rDAlpha;
        }
        const k = this.prms.k.v;
        const Y = this.prms.Y.v;
        // tslint:disable-next-line:variable-name
        const SYg = rDAlpha.vCalc * Math.pow(Y, k + 2) + rAlpha.vCalc * Math.pow(Y, k + 1) * (k + 2);
        const v = SYg / ((k + 1) * (k + 2));
        return new result_1.Result(v);
    }
}
exports.cSnPuiss = cSnPuiss;

},{"../../compute-node":5,"../../param/param-definition":86,"../../util/result":155,"./section_type":58}],53:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamsSectionPuiss = void 0;
const param_definition_1 = require("../../param/param-definition");
const param_domain_1 = require("../../param/param-domain");
const section_type_params_1 = require("./section_type_params");
/**
 * Paramètres de la section parabolique ou "puissance"
 */
class ParamsSectionPuiss extends section_type_params_1.ParamsSection {
    constructor(rk, rY, rLargeurBerge, rKs, rQ, rIf, rYB) {
        super(rY, rLargeurBerge, rKs, rQ, rIf, rYB);
        this._k = new param_definition_1.ParamDefinition(this, "k", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 1), undefined, rk);
        this.addParamDefinition(this._k);
    }
    /**
     * Coefficient de forme compris entre 0 et 1
     */
    get k() {
        return this._k;
    }
}
exports.ParamsSectionPuiss = ParamsSectionPuiss;

},{"../../param/param-definition":86,"../../param/param-domain":87,"./section_type_params":59}],54:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cSnRectang = void 0;
const compute_node_1 = require("../../compute-node");
const result_1 = require("../../util/result");
const section_type_1 = require("./section_type");
/**
 * Calculs de la section rectangulaire
 */
// tslint:disable-next-line:class-name
class cSnRectang extends section_type_1.acSection {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._nodeType = compute_node_1.SectionType.SectionRectangle;
    }
    get prms() {
        return this._prms;
    }
    /**
     * Calcul du périmètre mouillé
     * @return Périmètre mouillé (m)
     */
    Calc_P() {
        // return this.prms.LargeurBerge.v + super.Calc_P_Debordement(this.prms.Y.v);
        const rPDeb = super.Calc_P_Debordement(this.prms.Y.v);
        if (!rPDeb.ok) {
            return rPDeb;
        }
        const v = this.prms.LargeurBerge.v + rPDeb.vCalc;
        return new result_1.Result(v);
    }
    Calc_dP() {
        return super.Calc_dP_Debordement();
    }
    Calc_B() {
        return super.Calc_B_Debordement();
    }
    Calc_dB() {
        return super.Calc_dB_Debordement();
    }
    Calc_S() {
        return super.Calc_S_Debordement(this.prms.Y.v);
    }
    /**
     * Calcul du tirant d'eau conjugué avec la formule analytique pour la section rectangulaire
     * @return tirant d'eau conjugué
     */
    CalcYco() {
        const rFR = this.calcFromY("Fr");
        if (!rFR.ok) {
            return rFR;
        }
        const v = this.prms.Y.v * (Math.sqrt(1 + 8 * Math.pow(rFR.vCalc, 2)) - 1) / 2;
        return new result_1.Result(v);
    }
}
exports.cSnRectang = cSnRectang;

},{"../../compute-node":5,"../../util/result":155,"./section_type":58}],55:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamsSectionRectang = void 0;
const section_type_params_1 = require("./section_type_params");
class ParamsSectionRectang extends section_type_params_1.ParamsSection {
    constructor(rY, rLargeurFond, rKs, rQ, rIf, rYB) {
        super(rY, rLargeurFond, // LargeurBerge=LargeurFond
        rKs, rQ, rIf, rYB);
    }
}
exports.ParamsSectionRectang = ParamsSectionRectang;

},{"./section_type_params":59}],56:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cSnTrapez = void 0;
const compute_node_1 = require("../../compute-node");
const param_definition_1 = require("../../param/param-definition");
const result_1 = require("../../util/result");
const section_type_1 = require("./section_type");
/**
 * Calculs de la section trapézoïdale
 */
// tslint:disable-next-line:class-name
class cSnTrapez extends section_type_1.acSection {
    get prms() {
        return this._prms;
    }
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._nodeType = compute_node_1.SectionType.SectionTrapeze;
    }
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.LargeurFond.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Fruit.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    Calc_B() {
        let v;
        if (this.isDebordement()) {
            v = this.prms.LargeurFond.v + 2 * this.prms.Fruit.v * this.prms.YB.v;
            return new result_1.Result(v);
        }
        v = this.prms.LargeurFond.v + 2 * this.prms.Fruit.v * this.prms.Y.v;
        return new result_1.Result(v);
    }
    /**
     * Calcul du périmètre mouillé
     * @return Périmètre mouillé (m)
     */
    Calc_P() {
        let v;
        if (this.isDebordement()) {
            // return this.CalcGeo("P") + super.Calc_P_Debordement(this.prms.Y.v - this.prms.YB.v);
            const rGeoP = this.CalcGeo("P");
            if (!rGeoP.ok) {
                return rGeoP;
            }
            const rPDeb = super.Calc_P_Debordement(this.prms.Y.v - this.prms.YB.v);
            if (!rPDeb.ok) {
                return rPDeb;
            }
            v = rGeoP.vCalc + rPDeb.vCalc;
            return new result_1.Result(v);
        }
        v = this.prms.LargeurFond.v + 2 * Math.sqrt(1 + Math.pow(this.prms.Fruit.v, 2)) * this.prms.Y.v;
        return new result_1.Result(v);
    }
    /**
     * Calcul de la surface mouillée
     * @return Surface mouillée (m2)
     */
    Calc_S() {
        let v;
        if (this.isDebordement()) {
            // return this.CalcGeo("S") + super.Calc_S_Debordement(this.prms.Y.v - this.prms.YB.v);
            const rGeoS = this.CalcGeo("S");
            if (!rGeoS.ok) {
                return rGeoS;
            }
            const rSDeb = super.Calc_S_Debordement(this.prms.Y.v - this.prms.YB.v);
            if (!rSDeb.ok) {
                return rSDeb;
            }
            v = rGeoS.vCalc + rSDeb.vCalc;
            return new result_1.Result(v);
        }
        v = this.prms.Y.v * (this.prms.LargeurFond.v + this.prms.Fruit.v * this.prms.Y.v);
        return new result_1.Result(v);
    }
    /**
     * Calcul de dérivée de la surface hydraulique par rapport au tirant d'eau.
     * @return dS
     */
    Calc_dS() {
        if (this.isDebordement()) {
            return super.Calc_dS();
        }
        const v = this.prms.LargeurFond.v + 2 * this.prms.Fruit.v * this.prms.Y.v;
        return new result_1.Result(v);
    }
    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau.
     * @return dP
     */
    Calc_dP() {
        if (this.isDebordement()) {
            return super.Calc_dP_Debordement();
        }
        const v = 2 * Math.sqrt(1 + this.prms.Fruit.v * this.prms.Fruit.v);
        return new result_1.Result(v);
    }
    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau.
     * @return dB
     */
    Calc_dB() {
        if (this.isDebordement()) {
            return super.Calc_dB_Debordement();
        }
        const v = 2 * this.prms.LargeurFond.v * this.prms.Fruit.v;
        return new result_1.Result(v);
    }
    /**
     * Calcul de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    Calc_SYg() {
        const v = (this.prms.LargeurFond.v / 2 + this.prms.Fruit.v * this.prms.Y.v / 3)
            * Math.pow(this.prms.Y.v, 2);
        return new result_1.Result(v);
    }
    /**
     * Calcul de la dérivée de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    Calc_dSYg() {
        // tslint:disable-next-line:variable-name
        let SYg = this.prms.Fruit.v / 3 * Math.pow(this.prms.Y.v, 2);
        SYg += (this.prms.LargeurFond.v / 2 + this.prms.Fruit.v * this.prms.Y.v / 3) * 2 * this.prms.Y.v;
        return new result_1.Result(SYg);
    }
    isDebordement() {
        return this.prms.Y.v > this.prms.YB.v;
    }
}
exports.cSnTrapez = cSnTrapez;

},{"../../compute-node":5,"../../param/param-definition":86,"../../util/result":155,"./section_type":58}],57:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamsSectionTrapez = void 0;
const param_definition_1 = require("../../param/param-definition");
const param_domain_1 = require("../../param/param-domain");
const section_type_params_1 = require("./section_type_params");
class ParamsSectionTrapez extends section_type_params_1.ParamsSection {
    constructor(rLargeurFond, rFruit, rY, rKs, rQ, rIf, rYB) {
        // set LargeurBerge to default value so that it is not undefined when changing section type
        super(rY, 2.5, rKs, rQ, rIf, rYB);
        this._LargeurFond = new param_definition_1.ParamDefinition(this, "LargeurFond", param_domain_1.ParamDomainValue.POS_NULL, "m", rLargeurFond, param_definition_1.ParamFamily.WIDTHS);
        this._Fruit = new param_definition_1.ParamDefinition(this, "Fruit", param_domain_1.ParamDomainValue.POS_NULL, "m/m", rFruit);
        this.addParamDefinition(this._LargeurFond);
        this.addParamDefinition(this._Fruit);
        // hide params
        this.LargeurBerge.visible = false;
        this.LargeurBerge.undefineFamily(); // or else something might get linked to it although it is undefined
    }
    /**
     * Largeur au fond
     */
    get LargeurFond() {
        return this._LargeurFond;
    }
    /**
     * Fruit des berges
     */
    get Fruit() {
        return this._Fruit;
    }
}
exports.ParamsSectionTrapez = ParamsSectionTrapez;

},{"../../param/param-definition":86,"../../param/param-domain":87,"./section_type_params":59}],58:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.acSection = void 0;
const compute_node_1 = require("../../compute-node");
const nub_1 = require("../../nub");
const param_definition_1 = require("../../param/param-definition");
const session_settings_1 = require("../../session_settings");
const message_1 = require("../../util/message");
const result_1 = require("../../util/result");
const hauteur_conjuguee_1 = require("./hauteur_conjuguee");
const hauteur_correspondante_1 = require("./hauteur_correspondante");
const hauteur_critique_1 = require("./hauteur_critique");
const hauteur_normale_1 = require("./hauteur_normale");
const section_type_params_1 = require("./section_type_params");
/**
 * Gestion commune pour les différents types de section.
 * Comprend les formules pour la section rectangulaire pour gérer les débordements
 */
// tslint:disable-next-line:class-name
class acSection extends nub_1.Nub {
    /**
     * Construction de la classe.
     * Calcul des hauteurs normale et critique
     */
    constructor(prms, dbg = false) {
        super(prms, dbg);
        /**
         * true si la section est fermée (on considère alors qu'il existe une fente de Preissmann)
         */
        this.bSnFermee = false;
        this.arCalcGeo = {}; /// Données ne dépendant pas de la cote de l'eau
        /**
         * Tableau contenant les données dépendantes du tirant d'eau this->rY.
         *
         * Les clés du tableau peuvent être :
         * - S : la surface hydraulique
         * - P : le périmètre hydraulique
         * - R : le rayon hydraulique
         * - B : la largeur au miroir
         * - J : la perte de charge
         * - Fr : le nombre de Froude
         * - dP : la dérivée de P par rapport Y
         * - dR : la dérivée de R par rapport Y
         * - dB : la dérivée de B par rapport Y
         */
        this.arCalc = {};
        // tslint:disable-next-line:variable-name
        this.Calc_old = {}; /// Mémorisation des données hydrauliques pour calcul intermédiaire
        this._calcType = compute_node_1.CalculatorType.Section;
        this._newtonDbg = dbg;
    }
    get prms() {
        return this._prms;
    }
    get HautCritique() {
        return this._hautCritique;
    }
    static get availableSectionTypes() {
        return acSection._availableSectionTypes;
    }
    get nodeType() {
        return this._nodeType;
    }
    /** Returns Props object (observable set of key-values) associated to this Nub */
    get properties() {
        // completes props with calcType and nodeType if not already set
        this._props.setPropValue("calcType", this.calcType);
        if (this._props.getPropValue("nodeType") === undefined) {
            this._props.setPropValue("nodeType", this.nodeType);
        }
        return this._props;
    }
    // setter is not inherited from Nub if getter is redefined :/
    set properties(props) {
        super.setProperties(props);
    }
    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Section ones
     */
    get calculatedParam() {
        if (this.parent) {
            return this.parent.calculatedParam;
        }
        return undefined;
    }
    /**
     * Forwards to parent, that has vsibility over
     * all the parameters, including the Section ones
     */
    set calculatedParam(p) {
        if (this.parent) {
            this.parent.calculatedParam = p;
        }
    }
    clone() {
        return Object.create(this);
    }
    /**
     * Efface toutes les données calculées pour forcer le recalcul
     * @param bGeo Réinitialise les données de géométrie aussi
     */
    Reset(bGeo = true) {
        this.debug("reset(" + bGeo + ")");
        this.arCalc = {};
        if (bGeo) {
            this.arCalcGeo = {};
            this.bSnFermee = false;
        }
    }
    debug(m) {
        super.debug(this.calcIndent() + m);
    }
    /**
     * The parent is the Nub that holds the parameter to calculate;
     * ask it to perform calculation and copy its result to local
     * result, to facilitate values reading by targetting modules
     * (used by triggerChainCalculation)
     */
    CalcSerie(rInit) {
        this.currentResult = this.parent.CalcSerie(rInit);
        return this.result;
    }
    /**
     * Calcul des données à la section
     * effectue un reset du cache des résultats
     * @param sDonnee Clé de la donnée à calculer (voir this->arCalc)
     * @param rY valeur de Y à utiliser
     * @return la donnée calculée
     */
    CalcSection(sDonnee, rY) {
        this.debug(`Calc(${sDonnee},${rY})`);
        this._indentCalc = -1;
        this.Reset(true);
        return this.calcFromY(sDonnee, rY);
    }
    /**
     * Proxy to parent SectionNub, to compute non-hydraulic variables
     * @param sVarCalc
     * @param rInit
     */
    Calc(sVarCalc, rInit) {
        return this.parent.Calc(sVarCalc, rInit);
    }
    Equation(sVarCalc) {
        throw new Error("acSection.Equation() : cannot be called");
    }
    setParametersCalculability() {
        this.prms.Ks.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Q.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.If.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.YB.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Y.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.LargeurBerge.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    /**
     * calcul des données à la section dépendant de Y
     */
    calcFromY(sDonnee, rY) {
        this._indentCalc++;
        this.debug("in calcFromY(" + sDonnee + ", rY=" + rY + ") old " + sDonnee + "=" + this.arCalc[sDonnee]);
        this.debug("this.Y=" + this.prms.Y.toString());
        if (rY !== undefined && (!this.prms.Y.isDefined || (this.prms.Y.isDefined && rY !== this.prms.Y.v))) {
            this.prms.Y.v = rY;
            // On efface toutes les données dépendantes de Y pour forcer le calcul
            this.Reset(false);
        }
        let res;
        if (this.arCalc[sDonnee] === undefined) {
            // La donnée a besoin d'être calculée
            switch (sDonnee) {
                case "I-J": // Variation linéaire de l'énergie spécifique (I-J) en m/m
                    const rJ = this.calcFromY("J");
                    if (rJ.ok) {
                        this.arCalc[sDonnee] = this.prms.If.v - rJ.vCalc;
                        res = new result_1.Result(this.arCalc[sDonnee]);
                    }
                    else {
                        res = rJ;
                    }
                    break;
                default:
                    const methode = "Calc_" + sDonnee;
                    /* !!!!!
                       si on réunit les 2 lignes suivantes
                       ( this.arCalc[sDonnee] = this[methode](); ),
                       il arrive que this.arCalc[sDonnee] soit affecté à
                       undefined alors que this[methode]() renvoie
                       une valeur bien définie
                       !!!!!
                     */
                    res = this[methode]();
                    if (res.ok) {
                        this.arCalc[sDonnee] = res.vCalc;
                    }
                    break;
            }
            this.debug("calcFromY(" + sDonnee + ") resultat -> " + this.arCalc[sDonnee]);
        }
        else {
            this.debug("calcFromY(" + sDonnee + ") cache= " + this.arCalc[sDonnee]);
            res = new result_1.Result(this.arCalc[sDonnee]);
        }
        this._indentCalc--;
        this.debug("this.Y=" + this.prms.Y.toString());
        // return this.arCalc[sDonnee];
        return res;
    }
    /**
     * Calcul des données uniquement dépendantes de la géométrie de la section
     * @param sDonnee Clé de la donnée à calculer (voir this->arCalcGeo)
     * @return la donnée calculée
     */
    CalcGeo(sDonnee) {
        this._indentCalc++;
        this.debug("in CalcGeo(" + sDonnee + ") old " + sDonnee + "=" + this.arCalcGeo[sDonnee]);
        this.debug("this.Y=" + this.prms.Y.toString());
        // Si la largeur aux berges n'a pas encore été calculée, on commence par ça
        if (sDonnee !== "B" && this.arCalcGeo.B === undefined) {
            this.CalcGeo("B");
        }
        let res;
        if (this.arCalcGeo[sDonnee] === undefined) {
            // La donnée a besoin d'être calculée
            this.Swap(true); // On mémorise les données hydrauliques en cours
            this.Reset(false);
            this.prms.Y.v = this.prms.YB.v;
            switch (sDonnee) {
                case "B": // Largeur aux berges
                    // this.arCalcGeo[sDonnee] = this.Calc_B();
                    res = this.Calc_B();
                    if (res.ok) {
                        this.arCalcGeo[sDonnee] = res.vCalc;
                        if (this.arCalcGeo[sDonnee] < this.prms.YB.v / 100) {
                            // Section fermée
                            this.bSnFermee = true;
                            // On propose une fente de Preissmann
                            // égale à 1/100 de la hauteur des berges
                            this.arCalcGeo[sDonnee] = this.prms.YB.v / 100;
                        }
                        this.prms.LargeurBerge.v = this.arCalcGeo[sDonnee];
                    }
                    break;
                default:
                    const methode = "Calc_" + sDonnee;
                    // this.arCalcGeo[sDonnee] = this[methode]();
                    res = this[methode]();
                    if (res.ok) {
                        this.arCalcGeo[sDonnee] = res.vCalc;
                    }
                    break;
            }
            this.Swap(false); // On restitue les données hydrauliques en cours
            this.debug("CalcGeo(" + sDonnee + ") resultat -> " + this.arCalcGeo[sDonnee]);
        }
        else {
            this.debug("CalcGeo(" + sDonnee + ") cache= " + this.arCalcGeo[sDonnee]);
            res = new result_1.Result(this.arCalcGeo[sDonnee]);
        }
        // this.debug('this.Y=' + this.prms.Y.toString());
        this._indentCalc--;
        // return this.arCalcGeo[sDonnee];
        return res;
    }
    Calc_dS() {
        return this.calcFromY("B"); // largeur au miroir
    }
    /**
     * Calcul de la surface hydraulique en cas de débordement
     * @param Y hauteur d'eau au delà de la berge
     */
    Calc_S_Debordement(Y) {
        this.debug("section->CalcS(rY=" + Y + ") LargeurBerge=" + this.CalcGeo("B"));
        // return Y * this.CalcGeo("B");
        const rB = this.CalcGeo("B");
        if (!rB.ok) {
            return rB;
        }
        return new result_1.Result(Y * rB.vCalc);
    }
    /**
     * Calcul du périmètre hydraulique en cas de débordement
     * @param Y hauteur d'eau au dela de la berge
     */
    Calc_P_Debordement(Y) {
        return new result_1.Result(2 * Y);
    }
    /**
     * Calcul de dérivée du périmètre hydraulique par rapport au tirant d'eau en cas de débordement
     * @return la dérivée du périmètre hydraulique par rapport au tirant d'eau en cas de débordement
     */
    Calc_dP_Debordement() {
        return new result_1.Result(2);
    }
    /**
     * Calcul de la largeur au miroir en cas de débordement
     * @return La largeur au miroir en cas de débordement
     */
    Calc_B_Debordement() {
        // return this.prms.LargeurBerge.v;
        return new result_1.Result(this.prms.LargeurBerge.v);
    }
    /**
     * Calcul de dérivée de la largeur au miroir par rapport au tirant d'eau en cas de débordement
     * @return la dérivée de la largeur au miroir par rapport au tirant d'eau en cas de débordement
     */
    Calc_dB_Debordement() {
        return new result_1.Result(0);
    }
    /**
     * Calcul de la distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    Calc_SYg() {
        // return Math.pow(this.prms.Y.v, 2) * this.CalcGeo("B") / 2;
        const rGeoB = this.CalcGeo("B");
        if (!rGeoB.ok) {
            return rGeoB;
        }
        // return Math.pow(this.prms.Y.v, 2) * this.CalcGeo("B") / 2;
        const v = Math.pow(this.prms.Y.v, 2) * rGeoB.vCalc / 2;
        return new result_1.Result(v);
    }
    /**
     * Calcul de la dérivée distance du centre de gravité de la section à la surface libre
     * multiplié par la surface hydraulique
     * @return S x Yg
     */
    Calc_dSYg() {
        const rGeoB = this.CalcGeo("B");
        if (!rGeoB.ok) {
            return rGeoB;
        }
        // return this.prms.Y.v * this.CalcGeo("B");
        const v = this.prms.Y.v * rGeoB.vCalc;
        return new result_1.Result(v);
    }
    /**
     * Calcul de l'angle Alpha entre la surface libre et le fond pour les sections circulaires.
     * @return Angle Alpha pour une section circulaire, 0 sinon.
     */
    Calc_Alpha() {
        return new result_1.Result(0);
    }
    /**
     * Calcul de la dérivée de l'angle Alpha entre la surface libre et le fond pour les sections circulaires.
     * @return Dérivée de l'angle Alpha pour une section circulaire, 0 sinon.
     */
    Calc_dAlpha() {
        return new result_1.Result(0);
    }
    /**
     * Mémorise les données hydrauliques en cours ou les restitue
     * @param bMem true pour mémorisation, false pour restitution
     */
    Swap(bMem) {
        if (bMem) {
            this.debug("save Y=" + this.prms.Y.toString());
            this.Y_old = this.prms.Y.v;
            this.Calc_old = this.arCalc;
        }
        else {
            this.debug("restore Y=" + this.Y_old);
            this.prms.Y.v = this.Y_old;
            this.arCalc = this.Calc_old;
            this.Calc_old = {};
        }
    }
    calcIndent() {
        let res = "";
        for (let i = 0; i < this._indentCalc; i++) {
            res += "  ";
        }
        return res;
    }
    /**
     * Calcul de la dérivée surface hydraulique en cas de débordement
     * @return La dérivée de la surface hydraulique en cas de débordement
     */
    Calc_dS_Debordement() {
        return this.CalcGeo("B");
    }
    /**
     * Calcul du rayon hydraulique.
     * @return Le rayon hydraulique
     */
    Calc_R() {
        const rP = this.calcFromY("P");
        if (!rP.ok) {
            return rP;
        }
        if (rP.vCalc === 0) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_PERIMETRE_NUL));
        }
        const rS = this.calcFromY("S");
        if (!rS.ok) {
            return rS;
        }
        return new result_1.Result(rS.vCalc / rP.vCalc);
    }
    /**
     * Calcul de dérivée du rayon hydraulique par rapport au tirant d'eau.
     * @return dR
     */
    Calc_dR() {
        const rP = this.calcFromY("P");
        if (!rP.ok) {
            return rP;
        }
        // if (P !== 0)
        if (rP.vCalc === 0) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_PERIMETRE_NUL));
        }
        const rB = this.calcFromY("B");
        if (!rB.ok) {
            return rB;
        }
        const rS = this.calcFromY("S");
        if (!rS.ok) {
            return rS;
        }
        const rDP = this.calcFromY("dP");
        if (!rDP.ok) {
            return rDP;
        }
        const v = ((rB.vCalc * rP.vCalc - rS.vCalc * rDP.vCalc) / Math.pow(rP.vCalc, 2));
        return new result_1.Result(v);
        // return 0;
    }
    /**
     * Calcul de la perte de charge par la formule de Manning-Strickler.
     * @return La perte de charge
     */
    Calc_J() {
        const rR = this.calcFromY("R");
        if (!rR.ok) {
            return rR;
        }
        if (rR.vCalc === 0) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_RAYON_NUL));
        }
        const rV = this.calcFromY("V");
        if (!rV.ok) {
            return rV;
        }
        const v = Math.pow(rV.vCalc / this.prms.Ks.v, 2) / Math.pow(rR.vCalc, 4 / 3);
        return new result_1.Result(v);
    }
    /**
     * Calcul du nombre de Froude.
     * @return Le nombre de Froude
     */
    Calc_Fr() {
        const rS = this.calcFromY("S");
        if (!rS.ok) {
            return rS;
        }
        if (rS.vCalc === 0) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }
        const rB = this.calcFromY("B");
        if (!rB.ok) {
            return rB;
        }
        const v = this.prms.Q.v / rS.vCalc * Math.sqrt(rB.vCalc / rS.vCalc / section_type_params_1.ParamsSection.G);
        return new result_1.Result(v);
    }
    /**
     * Calcul de dy/dx
     */
    Calc_dYdX(Y) {
        // L'appel à calcFromY("J') avec Y en paramètre réinitialise toutes les données
        // dépendantes de la ligne d'eau
        const rJ = this.calcFromY("J", Y);
        if (!rJ.ok) {
            return rJ;
        }
        const rFR = this.calcFromY("Fr", Y);
        if (!rFR.ok) {
            return rFR;
        }
        const v = -(this.prms.If.v - rJ.vCalc / (1 - Math.pow(rFR.vCalc, 2)));
        return new result_1.Result(v);
    }
    /**
     * Calcul de la vitesse moyenne.
     * @return Vitesse moyenne
     */
    Calc_V() {
        const rS = this.calcFromY("S");
        if (!rS.ok) {
            return rS;
        }
        if (rS.vCalc === 0) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_SECTION_SURFACE_NULLE));
        }
        const v = this.prms.Q.v / rS.vCalc;
        return new result_1.Result(v);
    }
    /**
     * Calcul de la charge spécifique.
     * @return Charge spécifique
     */
    Calc_Hs() {
        const rV = this.calcFromY("V");
        if (!rV.ok) {
            return rV;
        }
        const v = this.prms.Y.v + Math.pow(rV.vCalc, 2) / (2 * section_type_params_1.ParamsSection.G);
        return new result_1.Result(v);
    }
    /**
     * Calcul de la charge spécifique critique.
     * @return Charge spécifique critique
     */
    Calc_Hsc() {
        let res;
        this.Swap(true); // On mémorise les données hydrauliques en cours
        // On calcule la charge avec la hauteur critique
        const rYC = this.CalcGeo("Yc");
        if (!rYC.ok) {
            res = rYC;
        }
        else {
            res = this.calcFromY("Hs", rYC.vCalc);
        }
        // On restitue les données initiales
        this.Swap(false);
        // return Hsc;
        return res;
    }
    /**
     * Calcul du tirant d'eau critique.
     * @return tirant d'eau critique
     */
    Calc_Yc() {
        const maxIter = session_settings_1.SessionSettings.maxIterations;
        const hautCritique = new hauteur_critique_1.cHautCritique(this, maxIter, this.newtonDbg);
        this._hautCritique = hautCritique.Newton(this.prms.YB.v);
        if (!this._hautCritique.ok) {
            const m = new message_1.Message(message_1.MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCRITIQUE);
            this._hautCritique = new result_1.Result(m);
        }
        return this._hautCritique;
    }
    /**
     * Calcul du tirant d'eau normal.
     * @return tirant d'eau normal
     */
    Calc_Yn() {
        if (this.prms.If.v <= 0) {
            const m = new message_1.Message(message_1.MessageCode.ERROR_SECTION_PENTE_NEG_NULLE_HNORMALE_INF);
            // this.oLog.add(m);
            // return undefined;
            return new result_1.Result(m);
        }
        const rYC = this.CalcGeo("Yc");
        if (!rYC.ok) {
            return rYC;
        }
        const maxIter = session_settings_1.SessionSettings.maxIterations;
        const oHautNormale = new hauteur_normale_1.cHautNormale(this, maxIter, this._newtonDbg);
        // let res = oHautNormale.Newton(this.CalcGeo("Yc"));
        let rYN = oHautNormale.Newton(rYC.vCalc);
        // if (res === undefined || !oHautNormale.hasConverged()) {
        if (!rYN.ok) {
            const m = new message_1.Message(message_1.MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE);
            // this.oLog.add(m);
            rYN = new result_1.Result(m);
        }
        return rYN;
    }
    /**
     * Calcul du tirant d'eau correspondant.
     * @return tirant d'eau correpondant
     */
    Calc_Ycor() {
        const rGeoYC = this.CalcGeo("Yc");
        if (!rGeoYC.ok) {
            return rGeoYC;
        }
        let nInit;
        if (this.prms.Y.v < rGeoYC.vCalc) {
            nInit = rGeoYC.vCalc * 2;
        }
        else {
            nInit = rGeoYC.vCalc / 2;
        }
        const maxIter = session_settings_1.SessionSettings.maxIterations;
        const oHautCorrespondante = new hauteur_correspondante_1.cHautCorrespondante(this, maxIter, this._newtonDbg);
        let rYF = oHautCorrespondante.Newton(nInit);
        if (!rYF.ok) {
            const m = new message_1.Message(message_1.MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR);
            rYF = new result_1.Result(m);
        }
        return rYF;
    }
    /**
     * Calcul du tirant d'eau conjugué.
     * @return tirant d'eau conjugué
     */
    Calc_Ycon() {
        let res;
        this.Swap(true);
        const maxIter = session_settings_1.SessionSettings.maxIterations;
        const oHautConj = new hauteur_conjuguee_1.cHautConjuguee(this, maxIter, this._newtonDbg);
        const rFR = this.calcFromY("Fr");
        if (!rFR.ok) {
            res = rFR;
        }
        else {
            let Y0;
            // Choisir une valeur initiale du bon côté de la courbe
            Y0 = this.calcFromY("Ycor");
            if (!Y0.ok) {
                res = Y0;
            }
            else {
                // let Ycon = oHautConj.Newton(Y0);
                // tslint:disable-next-line:variable-name
                const Ycon = oHautConj.Newton(Y0.vCalc);
                // if (Ycon === undefined || !oHautConj.hasConverged()) {
                if (!Ycon.ok) {
                    const m = new message_1.Message(message_1.MessageCode.ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCONJUG);
                    // this.oLog.add(m);
                    res = new result_1.Result(m);
                }
                else {
                    res = Ycon;
                }
            }
        }
        this.Swap(false);
        return res;
    }
    /**
     * Calcul de la contrainte de cisaillement.
     * @return contrainte de cisaillement
     */
    Calc_Tau0() {
        const rR = this.calcFromY("R");
        if (!rR.ok) {
            return rR;
        }
        const rJ = this.calcFromY("J");
        if (!rJ.ok) {
            return rJ;
        }
        return new result_1.Result(1000 * section_type_params_1.ParamsSection.G * rR.vCalc * rJ.vCalc);
    }
    /**
     * Calcul de l'impulsion hydraulique.
     * @return Impulsion hydraulique
     */
    Calc_Imp() {
        const rV = this.calcFromY("V");
        if (!rV.ok) {
            return rV;
        }
        const rSYG = this.calcFromY("SYg");
        if (!rSYG.ok) {
            return rSYG;
        }
        const v = 1000 * (this.prms.Q.v * rV.vCalc + section_type_params_1.ParamsSection.G * rSYG.vCalc);
        return new result_1.Result(v);
    }
}
exports.acSection = acSection;
acSection._availableSectionTypes = [
    {
        id: "SectionCercle",
        value: compute_node_1.SectionType.SectionCercle
    },
    {
        id: "SectionRectangle",
        value: compute_node_1.SectionType.SectionRectangle
    },
    {
        id: "SectionTrapeze",
        value: compute_node_1.SectionType.SectionTrapeze
    },
    {
        id: "SectionPuissance",
        value: compute_node_1.SectionType.SectionPuissance
    }
];

},{"../../compute-node":5,"../../nub":33,"../../param/param-definition":86,"../../session_settings":105,"../../util/message":151,"../../util/result":155,"./hauteur_conjuguee":42,"./hauteur_correspondante":43,"./hauteur_critique":44,"./hauteur_normale":45,"./section_type_params":59}],59:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamsSection = void 0;
const param_definition_1 = require("../../param/param-definition");
const param_domain_1 = require("../../param/param-domain");
const params_equation_1 = require("../../param/params-equation");
const session_settings_1 = require("../../session_settings");
class ParamsSection extends params_equation_1.ParamsEquation {
    constructor(rY, rLargeurBerge, rKs, rQ, rIf, rYB) {
        super();
        this._Ks = new param_definition_1.ParamDefinition(this, "Ks", param_domain_1.ParamDomainValue.POS, "SI", rKs, param_definition_1.ParamFamily.STRICKLERS);
        this._Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.POS_NULL, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this._If = new param_definition_1.ParamDefinition(this, "If", param_domain_1.ParamDomainValue.ANY, "m/m", rIf, param_definition_1.ParamFamily.SLOPES);
        this._YB = new param_definition_1.ParamDefinition(this, "YB", param_domain_1.ParamDomainValue.POS, "m", rYB, param_definition_1.ParamFamily.HEIGHTS);
        this._Y = new param_definition_1.ParamDefinition(this, "Y", param_domain_1.ParamDomainValue.POS_NULL, "m", rY, param_definition_1.ParamFamily.HEIGHTS);
        this._LargeurBerge = new param_definition_1.ParamDefinition(this, "LargeurBerge", param_domain_1.ParamDomainValue.POS_NULL, "m", rLargeurBerge, param_definition_1.ParamFamily.WIDTHS);
        this._iPrec = Math.round(-Math.log(session_settings_1.SessionSettings.precision) / Math.log(10));
        this.addParamDefinition(this._Ks);
        this.addParamDefinition(this._Q);
        this.addParamDefinition(this._If);
        this.addParamDefinition(this._YB);
        this.addParamDefinition(this._Y);
        this.addParamDefinition(this._LargeurBerge);
        if (rY === undefined) { // avoid undefined on Swap()
            this._Y.v = 1E6;
        }
    }
    /**
     * Strickler
     */
    get Ks() {
        return this._Ks;
    }
    /**
     * Débit
     */
    get Q() {
        return this._Q;
    }
    /**
     * Pente du fond
     */
    get If() {
        return this._If;
    }
    /**
     * Précision en nombre de décimales
     */
    get iPrec() {
        return this._iPrec;
    }
    /**
     * Hauteur de berge
     */
    get YB() {
        return this._YB;
    }
    /**
     * Tirant d'eau
     */
    get Y() {
        return this._Y;
    }
    /**
     * Largeur au débordement
     */
    get LargeurBerge() {
        return this._LargeurBerge;
    }
}
exports.ParamsSection = ParamsSection;
ParamsSection.G = 9.81; /// Constante de gravité

},{"../../param/param-definition":86,"../../param/param-domain":87,"../../param/params-equation":92,"../../session_settings":105}],60:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CloisonAval = void 0;
const index_1 = require("../index");
const param_definition_1 = require("../param/param-definition");
const parallel_structure_1 = require("../structure/parallel_structure");
const structure_props_1 = require("../structure/structure_props");
const message_1 = require("../util/message");
class CloisonAval extends parallel_structure_1.ParallelStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = index_1.CalculatorType.CloisonAval;
    }
    /**
     * Return index structure of vanne levante
     */
    get indexVanneLevante() {
        for (let i = 0; i < this.structures.length; i++) {
            if (structure_props_1.loiAdmissiblesCloisonAval.VanneLevante.includes(this.structures[i].properties.getPropValue("loiDebit"))) {
                return i;
            }
        }
        return null;
    }
    get prms() {
        return this._prms;
    }
    /**
     * Returns admissible LoiDebit grouped by StructureType
     * Only one vanne levante is allowed on a CloisonAval
     */
    getLoisAdmissibles() {
        return structure_props_1.loiAdmissiblesCloisonAval;
    }
    /**
     * Calcul de la cote amont de la cloison
     * @param sVarCalc Nom du paramètre à calculer : seul Z1 est admis
     * @param rInit Valeur initiale
     */
    Calc(sVarCalc, rInit) {
        if (sVarCalc !== "Z1") {
            throw new Error("CloisonAval sVarCalc should be Z1");
        }
        if (!this.checkVanneLevante()) {
            this.currentResult.addMessage(new message_1.Message(message_1.MessageCode.ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE));
            return this.result;
        }
        let m;
        if (this.hasVanneLevante()) {
            const s = this.structures[this.indexVanneLevante];
            this.prms.Z1.v = this.prms.Z2.v + s.getParameter("DH").v;
            this.currentResult = this.CalcStructPrm(this.indexVanneLevante, "ZDV");
            if (this.result.ok) {
                s.prms.ZDV.v = this.result.vCalc;
            }
            // Check if calculated ZDV is between minZDV and maxZDV
            if (this.result.vCalc < s.prms.minZDV.v) {
                m = new message_1.Message(message_1.MessageCode.WARNING_VANLEV_ZDV_INF_MIN);
                s.prms.ZDV.v = s.prms.minZDV.v;
            }
            else if (this.result.vCalc > s.prms.maxZDV.v) {
                m = new message_1.Message(message_1.MessageCode.WARNING_VANLEV_ZDV_SUP_MAX);
                s.prms.ZDV.v = s.prms.maxZDV.v;
            }
        }
        if (!this.hasVanneLevante() || this.result.ok) {
            // Calculation of Z1 with the new ZDV in case of existing vanne levante
            this.currentResult = super.Calc("Z1", rInit);
            if (this.result.ok) {
                this.getParameter(sVarCalc).v = this.result.vCalc;
                // Recalcul du débit total pour récupérer les résultats des ouvrages dans les résultats complémentaires
                const resQtot = this.CalcQ();
                for (const extraResKey in resQtot.extraResults) {
                    if (resQtot.resultElement.values.hasOwnProperty(extraResKey)) {
                        this.result.resultElement.addExtraResult(extraResKey, resQtot.resultElement.values[extraResKey]);
                    }
                }
                if (this.hasVanneLevante()) {
                    this.result.resultElement.values.ZDV = this.structures[this.indexVanneLevante].prms.ZDV.v;
                    if (m !== undefined) {
                        this.result.resultElement.addMessage(m);
                    }
                }
            }
        }
        return this.result;
    }
    /**
     * return false if there is more than one vanne levante in the cloison
     */
    checkVanneLevante() {
        let n = 0;
        for (const st of this.structures) {
            if (structure_props_1.loiAdmissiblesCloisonAval.VanneLevante.includes(st.properties.getPropValue("loiDebit"))) {
                n += 1;
            }
        }
        return n < 2;
    }
    /**
     * Is there at least one Vanne Levante in the Cloison ?
     */
    hasVanneLevante() {
        return !(this.indexVanneLevante === null);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    exposeResults() {
        this._resultsFamilies = {
            ZDV: undefined
        };
    }
}
exports.CloisonAval = CloisonAval;

},{"../index":14,"../param/param-definition":86,"../structure/parallel_structure":111,"../structure/structure_props":126,"../util/message":151}],61:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CloisonsAvalParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const parallel_structure_params_1 = require("../structure/parallel_structure_params");
class CloisonsAvalParams extends parallel_structure_params_1.ParallelStructureParams {
    constructor(rQ, rZ1, rZ2, rZRAM) {
        super(rQ, rZ1, rZ2);
        this.ZRAM = new param_definition_1.ParamDefinition(this, "ZRAM", param_domain_1.ParamDomainValue.ANY, "m", rZRAM, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZRAM);
    }
}
exports.CloisonsAvalParams = CloisonsAvalParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../structure/parallel_structure_params":112}],62:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cloisons = void 0;
const compute_node_1 = require("../compute-node");
const index_1 = require("../index");
const param_definition_1 = require("../param/param-definition");
const parallel_structure_1 = require("../structure/parallel_structure");
const structure_kivi_params_1 = require("../structure/structure_kivi_params");
const structure_props_1 = require("../structure/structure_props");
const message_1 = require("../util/message");
class Cloisons extends parallel_structure_1.ParallelStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Cloisons;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    getLoisAdmissibles() {
        return structure_props_1.loiAdmissiblesCloisons;
    }
    /**
     * Is the Nub used in a PAB?
     */
    get isInPAB() {
        return (this.parent !== undefined && this.parent instanceof index_1.Pab);
    }
    Equation(sVarCalc) {
        this.updatePrms();
        return super.Equation(sVarCalc);
    }
    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer :
     *                 "Q", "Z1", "DH" ou "n.X" avec "n" l'index de l'ouvrage et "X" son paramètre
     * @param rInit Valeur initiale
     */
    Calc(sVarCalc, rInit) {
        let sVC = sVarCalc;
        if (sVarCalc === "DH") {
            sVC = "Z2";
            rInit = this.prms.Z1.v - this.prms.DH.v;
        }
        this.updatePrms();
        const r = super.Calc(sVC, rInit);
        if (!r.ok) { // Error during calculation
            return r;
        }
        // Transformation Z2 => DH
        if (sVarCalc === "DH") {
            r.vCalc = this.prms.Z1.v - r.vCalc;
        }
        // Ajout du calcul de la puissance dissipée
        const prms = this.getParamValuesAfterCalc(sVarCalc, r);
        const ro = 1000; // masse volumique de l'eau en kg/m3
        const g = 9.81; // accélération de la gravité terrestre en m/s2.
        r.resultElement.values.PV = ro * g * this.prms.Q.v * (prms.Z1 - prms.Z2) / (prms.LB * prms.BB * prms.PB);
        // Ajout de la cote de radier de bassin
        r.resultElement.values.ZRMB = this.prms.ZRMB.v;
        r.resultElement.values.ZRAM = this.prms.ZRAM.v;
        // Ajout de ZDV pour les seuils
        for (const s of this.structures) {
            if (s.prms.h1.visible) {
                s.result.resultElement.addExtraResult("ZDV", this.prms.Z1.v - s.prms.h1.v);
            }
            // calcul de la pelle
            if (s.properties.getPropValue("loiDebit") !== structure_props_1.LoiDebit.OrificeSubmerged) {
                const pelle = s.prms.ZDV.v - this.prms.ZRAM.v;
                s.result.resultElement.values.P = pelle;
                if (pelle < -1E-7) { // si c'est enfoncé d'un dixième de micron ça va
                    const m = new message_1.Message(message_1.MessageCode.WARNING_NEGATIVE_SILL);
                    s.result.resultElement.log.add(m);
                    // merge in parent for GUI display
                    this.result.resultElement.log.add(m);
                }
            }
        }
        return r;
    }
    adjustChildParameters(child) {
        const prms = child.prms;
        if (!this.isInPAB && prms.ZDV.visible) {
            // Dans le contexte hors PAB
            // Pour les seuils (i.e. Structures avec cote de radier de seuil)
            // on remplace ZDV par h1 la charge sur le seuil
            prms.h1.visible = true;
            prms.ZDV.visible = false;
        }
        if (child.prms instanceof structure_kivi_params_1.StructureKiviParams) {
            // hide ZRAM for KIVI, in Cloisons and PAB context only
            child.prms.ZRAM.visible = false;
        }
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.LB.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.BB.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.PB.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.DH.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.ZRMB.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.ZRAM.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.QA.calculability = param_definition_1.ParamCalculability.FREE;
    }
    setResultsUnits() {
        this._resultsUnits = {
            PV: "W/m³",
            ZRAM: "m",
            ZRMB: "m"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            PV: undefined,
            ZRAM: undefined,
            ZRMB: undefined,
            P: undefined
        };
    }
    updatePrms() {
        if (this.prms.DH.visible !== false) {
            // if NONE => PAB context. it doesn't need update of elevations
            if (this.calculatedParam !== this.prms.DH) {
                // Z2 is the variable to find if DH is the calculated param
                this.prms.Z2.v = this.prms.Z1.v - this.prms.DH.v;
            }
            this.prms.ZRMB.v = this.prms.Z1.v - this.prms.PB.v - this.prms.DH.v;
            this.prms.ZRAM.v = this.prms.ZRMB.v + this.prms.DH.v / 2;
            for (const structure of this.structures) {
                const prms = structure.prms;
                if (prms.h1.visible) {
                    // MAJ de ZDV des seuils à partir de la charge
                    prms.ZDV.v = this.prms.Z1.v - prms.h1.v;
                }
                if (structure.prms instanceof structure_kivi_params_1.StructureKiviParams) {
                    structure.prms.ZRAM.v = this.prms.Z1.v - this.prms.PB.v;
                }
            }
        }
    }
}
exports.Cloisons = Cloisons;

},{"../compute-node":5,"../index":14,"../param/param-definition":86,"../structure/parallel_structure":111,"../structure/structure_kivi_params":120,"../structure/structure_props":126,"../util/message":151}],63:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CloisonsParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const parallel_structure_params_1 = require("../structure/parallel_structure_params");
/**
 * Common parameters of hydraulic structure equations
 */
class CloisonsParams extends parallel_structure_params_1.ParallelStructureParams {
    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit total (m3/s)
     * @param rZ1 Cote de l'eau amont (m)
     * @param rLB Longueur des bassins (m)
     * @param rBB Largeur des bassins (m)
     * @param rPB Profondeur moyenne (m)
     * @param rDH Hauteur de chute (m)
     */
    constructor(rQ, rZ1, rLB, rBB, rPB, rDH) {
        super(rQ, rZ1, rZ1 - rDH);
        this.LB = new param_definition_1.ParamDefinition(this, "LB", param_domain_1.ParamDomainValue.POS, "m", rLB, param_definition_1.ParamFamily.LENGTHS);
        this.addParamDefinition(this.LB);
        this.BB = new param_definition_1.ParamDefinition(this, "BB", param_domain_1.ParamDomainValue.POS, "m", rBB, param_definition_1.ParamFamily.WIDTHS);
        this.addParamDefinition(this.BB);
        this.PB = new param_definition_1.ParamDefinition(this, "PB", param_domain_1.ParamDomainValue.POS, "m", rPB, param_definition_1.ParamFamily.HEIGHTS);
        this.addParamDefinition(this.PB);
        this.DH = new param_definition_1.ParamDefinition(this, "DH", param_domain_1.ParamDomainValue.POS, "m", rDH, param_definition_1.ParamFamily.BASINFALLS);
        this.addParamDefinition(this.DH);
        this.ZRMB = new param_definition_1.ParamDefinition(this, "ZRMB", param_domain_1.ParamDomainValue.ANY, "m", 0, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZRMB);
        this.ZRAM = new param_definition_1.ParamDefinition(this, "ZRAM", param_domain_1.ParamDomainValue.ANY, "m", 0, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZRAM);
        this.QA = new param_definition_1.ParamDefinition(this, "QA", param_domain_1.ParamDomainValue.ANY, "m³/s", 0);
        this.addParamDefinition(this.QA);
        this.Z2.visible = false;
        this.ZRAM.visible = false;
        this.ZRMB.visible = false;
        this.QA.visible = false;
    }
}
exports.CloisonsParams = CloisonsParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../structure/parallel_structure_params":112}],64:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pab = void 0;
const compute_node_1 = require("../compute-node");
const index_1 = require("../index");
const index_2 = require("../index");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const params_equation_array_iterator_1 = require("../param/params_equation_array_iterator");
const props_1 = require("../props");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const resultelement_1 = require("../util/resultelement");
const cloison_aval_1 = require("./cloison_aval");
const cloison_aval_params_1 = require("./cloison_aval_params");
const fish_pass_1 = require("../fish_pass");
class Pab extends fish_pass_1.FishPass {
    constructor(prms, downWall, dbg = false) {
        super(prms, dbg);
        this.downWall = downWall;
        this._calcType = compute_node_1.CalculatorType.Pab;
        this._childrenType = "Cloison";
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /**
     * enfants castés au bon type
     */
    get children() {
        return this._children;
    }
    get downWall() {
        return this._downWall;
    }
    set downWall(dw) {
        this._downWall = dw;
        if (dw) { // might be undefined
            dw.parent = this; // important
            // postprocessing
            this.adjustDownwallParameters(this.downWall);
        }
    }
    /**
     * Add Cloisons to the PAB from a cloison model
     * @param cloisonModel Cloison model parametrised as first upstream basin
     * @param n Number of walls (or falls) of the PAB (Number of basin = n - 1)
     */
    addCloisonsFromModel(cloisonModel, n) {
        // Fix some parameters of the upstream cloison (= Wall + basin)
        const DH = cloisonModel.prms.DH.currentValue;
        const ZRMB = this.prms.Z1.currentValue - cloisonModel.prms.PB.currentValue - DH;
        const ZRAM = ZRMB + DH / 2;
        // Generate an image of the object for multiplication
        // Use 2nd param as a trick to break links and copy values
        const serialisedCloisonModel = cloisonModel.serialise(undefined, [cloisonModel.uid]);
        for (let i = 0; i < n; i++) {
            const cl = index_1.Session.getInstance().unserialiseSingleNub(serialisedCloisonModel, false).nub;
            const p = cl.prms;
            // Copy calculated param to the new Cloison #130
            cl.calculatedParam.singleValue = cloisonModel.calculatedParam.V;
            p.ZRMB.singleValue = ZRMB - i * DH;
            p.ZRAM.singleValue = ZRAM - i * DH;
            // Set Structure ZDVs
            for (const st of cl.structures) {
                if (st.isZDVcalculable) {
                    st.prms.ZDV.singleValue = (this.prms.Z1.currentValue - st.prms.h1.currentValue) - i * DH;
                    if (st.getParameter("ZT") !== undefined) {
                        const stTT = st;
                        stTT.prms.ZT.singleValue = stTT.prms.ZT.currentValue - i * DH;
                    }
                }
            }
            if (i !== n - 1) {
                // Add wall + basin = cloison
                this.addChild(cl);
            }
            else {
                // The last wall is a CloisonAval (= Wall only, without basin)
                const zram = this.children[this.children.length - 1].prms.ZRMB.singleValue
                    - this.children[this.children.length - 1].prms.DH.singleValue / 2;
                const dw = new cloison_aval_1.CloisonAval(new cloison_aval_params_1.CloisonsAvalParams(0, 0, 0, zram));
                dw.structures = cl.structures;
                this.downWall = dw;
            }
        }
    }
    CalcSerie(rInit) {
        if (!this.downWall.checkVanneLevante()) {
            this._result = new result_1.Result(undefined, this);
            this._result.globalLog.insert(new message_1.Message(message_1.MessageCode.ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE));
            return this._result;
        }
        return super.CalcSerie(rInit);
    }
    /**
     * @param sVarCalc Calcul
     */
    Calc(sVarCalc, rInit) {
        const r = new result_1.Result(new resultelement_1.ResultElement());
        if (!this.downWall.checkVanneLevante()) {
            r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE));
        }
        if (sVarCalc === "Q") {
            if (this.prms.Z1.v < this.prms.Z2.v) {
                r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.ERROR_PAB_Z1_LOWER_THAN_Z2));
            }
            this.children[0].prms.Z1.v = this.prms.Z1.v;
            this.children[0].prms.Z2.v = -Infinity;
            if (this.children[0].CalcQ().vCalc < 1E-7) {
                r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.ERROR_PAB_Z1_LOWER_THAN_UPSTREAM_WALL));
            }
        }
        if (r.hasErrorMessages()) {
            this.currentResult = r;
            return this.result;
        }
        return super.Calc(sVarCalc, rInit);
    }
    /**
     * Calcul analytique
     * @warning Should be called by this.Calc only for parameter initialisations
     * @param sVarCalc Variable à calculer (Z1 uniquement)
     */
    Equation(sVarCalc) {
        const r = new result_1.Result(0, this);
        // Up to down course : discharge distribution and bottom elevation
        if (this.children.length > 0) {
            this.children[0].prms.Q.v = this.prms.Q.v;
        }
        let l = 0; // Length of the fishway and wall abscissae
        for (let i = 0; i < this.children.length; i++) {
            let wall;
            if (i !== this.children.length - 1) {
                wall = this.children[i + 1];
            }
            else {
                wall = this.downWall;
            }
            l += this.children[i].prms.LB.v;
            // Set discharge for the next wall from the current basin
            wall.prms.Q.v = this.children[i].prms.Q.v + this.children[i].prms.QA.v;
        }
        // Down to up course : water surface calculation
        let Z = this.prms.Z2.v;
        Z = this.calcCloisonZ1(this.downWall, Z);
        if (!this.downWall.result.ok) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_PAB_CALC_Z1_CLOISON_DW), this);
        }
        this.downWall.result.resultElement.values.Q = this.downWall.prms.Q.v;
        this.downWall.result.resultElement.values.x = l;
        this.debug("Downstream wall");
        this.dbgWall(this.downWall);
        for (let i = this.children.length - 1; i >= 0; i--) {
            // Initialisations for current cloison
            const cl = this.children[i];
            // Calculation of upstream water elevation
            cl.prms.PB.v = Z - cl.prms.ZRMB.v;
            Z = this.calcCloisonZ1(cl, Z);
            if (!cl.result.ok) {
                const m = new message_1.Message(message_1.MessageCode.ERROR_PAB_CALC_Z1_CLOISON);
                m.extraVar.n = String(cl.findPositionInParent() + 1);
                return new result_1.Result(m, this);
            }
            // Add extraresults: mean depth in pool and discharge
            cl.result.resultElement.values.YMOY = cl.prms.PB.v;
            cl.result.resultElement.values.Q = cl.prms.Q.v;
            cl.result.resultElement.values.QA = cl.prms.QA.v;
            l -= cl.prms.LB.v;
            cl.result.resultElement.values.x = l;
            this.debug("Bassin n°" + i);
            this.dbgWall(cl);
        }
        r.vCalc = Z;
        return r;
    }
    get calculableParameters() {
        const calcPrms = super.calculableParameters.filter((p) => p.nubCalcType === compute_node_1.CalculatorType.Pab);
        return calcPrms;
    }
    /**
     * Returns an iterator over :
     *  - own parameters (this._prms)
     *  - children parameters (this._children[*]._prms)
     *  Special treatment for PAB's downwall
     */
    get parameterIterator() {
        const prms = [];
        prms.push(this._prms);
        if (this._children) {
            nub_1.Nub.concatPrms(prms, this.childrenPrms);
        }
        if (this.downWall) {
            nub_1.Nub.concatPrms(prms, this.downWall.childrenPrms);
        }
        return new params_equation_array_iterator_1.ParamsEquationArrayIterator(prms);
    }
    /**
     * Returns an object representation of the Nub's current state
     * @param extra extra key-value pairs, for ex. calculator title in GUI
     */
    objectRepresentation(extra) {
        // regular serialization
        const ret = super.objectRepresentation(extra, []);
        // downwall
        ret.downWall = this.downWall.objectRepresentation(undefined, []);
        return ret;
    }
    /**
     * Fills the current Nub with parameter values, provided an object representation
     * @param obj object representation of a Nub content (parameters)
     * @returns the calculated parameter found, if any - used by child Nub to notify
     *          its parent of the calculated parameter to set
     */
    loadObjectRepresentation(obj) {
        // return value
        const ret = super.loadObjectRepresentation(obj);
        // load downwall if any
        if (obj.downWall) {
            // decode properties
            const props = index_1.Session.invertEnumKeysAndValuesInProperties(obj.downWall.props, true);
            // create the Nub
            const dw = index_1.Session.getInstance().createNub(new props_1.Props(props), this);
            // try to keep the original ID
            if (!index_1.Session.getInstance().uidAlreadyUsed(obj.downWall.uid)) {
                dw.setUid(obj.downWall.uid);
            }
            const childRet = dw.loadObjectRepresentation(obj.downWall);
            // add downWall to parent
            this.downWall = dw;
            // forward errors
            if (childRet.hasErrors) {
                ret.hasErrors = true;
            }
        }
        return ret;
    }
    /**
     * Adds a new empty resultElement to the current Result object, so that
     * computation result is stored into it, via set currentResult(); does
     * the same for all children and downWall
     */
    initNewResultElement() {
        super.initNewResultElement();
        this.downWall.initNewResultElement();
    }
    /**
     * Sets this._result to a new empty Result, before starting a new CalcSerie();
     * does the same for all children and downWall
     */
    reinitResult() {
        super.reinitResult();
        this.downWall.reinitResult();
    }
    /**
     * Only Q and Z1 are calculable (see #108)
     */
    findFirstCalculableParameter(otherThan) {
        for (const p of [this.prms.Q, this.prms.Z1]) {
            if (p !== otherThan
                && p.valueMode !== index_2.ParamValueMode.CALCUL // event LINK might be reset here
            ) {
                return p;
            }
        }
        throw new Error(`PAB : no calculable parameter found other than ${otherThan ? otherThan.symbol : "undefined"}`);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Q.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    /**
     * Remove Calculability of DH for not updating Z2 during calculation; adjust
     * visibility for PAB display and serialisation
     * @param child Cloison newly added to the PAB
     */
    adjustChildParameters(child) {
        child.prms.QA.visible = true;
        child.prms.ZRAM.visible = true;
        child.prms.ZRMB.visible = true;
        child.prms.Q.visible = false;
        child.prms.Z1.visible = false;
        child.prms.PB.visible = false;
        child.prms.DH.visible = false;
        for (const st of child.structures) {
            if (st.prms.h1.visible) {
                // Set parameter visibility for ZDV and h1 in PAB context
                st.prms.ZDV.visible = true;
                st.prms.h1.visible = false;
            }
        }
    }
    /**
     * Remove visibility of downwall hydraulic parameters, for serialisation
     * @param dw
     */
    adjustDownwallParameters(dw) {
        dw.prms.Q.visible = false;
        dw.prms.Z1.visible = false;
        dw.prms.Z2.visible = false;
        for (const st of dw.structures) {
            if (st.prms.h1.visible) {
                // Set parameter visibility for ZDV and h1 in PAB context
                st.prms.ZDV.visible = true;
                st.prms.h1.visible = false;
            }
        }
    }
    calcCloisonZ1(cl, Z) {
        // Initialisations for current cloison
        cl.prms.Z2.v = Z;
        // Calculation of upstream water elevation
        cl.Calc("Z1", Z + 0.1);
        // Fall on this wall
        cl.result.resultElement.values.DH = cl.result.vCalc - cl.prms.Z2.v;
        // Return Update elevation for next pool
        return cl.result.vCalc;
    }
    dbgWall(cl) {
        if (!this.DBG) {
            return;
        }
        let s = "";
        for (const p of cl.prms) {
            s += p.symbol + " = " + p.v + "; ";
        }
        this.debug(s);
        for (const c of cl.getChildren()) {
            this.debug("Ouvrage");
            s = "";
            for (const p of c.prms) {
                if (p.visible) {
                    s += "*";
                }
                s += p.symbol + " = " + p.v + "; ";
            }
            this.debug(s);
        }
    }
}
exports.Pab = Pab;

},{"../compute-node":5,"../fish_pass":13,"../index":14,"../nub":33,"../param/param-definition":86,"../param/params_equation_array_iterator":93,"../props":103,"../util/message":151,"../util/result":155,"../util/resultelement":156,"./cloison_aval":60,"./cloison_aval_params":61}],65:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabChute = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
class PabChute extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.PabChute;
        this._defaultCalculatedParam = prms.DH;
        this.resetDefaultCalculatedParam();
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Equation(sVarCalc) {
        let v;
        if (!["Z1", "Z2"].includes(sVarCalc) && this.prms.Z1.v <= this.prms.Z2.v) {
            const m = new message_1.Message(message_1.MessageCode.ERROR_ELEVATION_ZI_LOWER_THAN_Z2);
            m.extraVar.Z1 = this.prms.Z1.v;
            m.extraVar.Z2 = this.prms.Z2.v;
            return new result_1.Result(m);
        }
        switch (sVarCalc) {
            case "Z1":
                v = this.prms.Z2.v + this.prms.DH.v;
                break;
            case "Z2":
                v = this.prms.Z1.v - this.prms.DH.v;
                break;
            case "DH":
                v = this.prms.Z1.v - this.prms.Z2.v;
                break;
            default:
                throw new Error("PabChute.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.DH.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.PabChute = PabChute;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/message":151,"../util/result":155}],66:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabChuteParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class PabChuteParams extends params_equation_1.ParamsEquation {
    constructor(rZ1, rZ2, rDH) {
        super();
        this._Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, param_definition_1.ParamFamily.ELEVATIONS);
        this._Z2 = new param_definition_1.ParamDefinition(this, "Z2", param_domain_1.ParamDomainValue.ANY, "m", rZ2, param_definition_1.ParamFamily.ELEVATIONS);
        this._DH = new param_definition_1.ParamDefinition(this, "DH", param_domain_1.ParamDomainValue.POS_NULL, "m", rDH, param_definition_1.ParamFamily.TOTALFALLS);
        this.addParamDefinition(this._Z1);
        this.addParamDefinition(this._Z2);
        this.addParamDefinition(this._DH);
    }
    get Z1() {
        return this._Z1;
    }
    get Z2() {
        return this._Z2;
    }
    get DH() {
        return this._DH;
    }
}
exports.PabChuteParams = PabChuteParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],67:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabDimension = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
class PabDimension extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.PabDimensions;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Equation(sVarCalc) {
        let v;
        switch (sVarCalc) {
            case "V":
                v = this.prms.L.v * this.prms.W.v * this.prms.Y.v;
                break;
            case "L":
                v = this.prms.V.v / this.prms.W.v / this.prms.Y.v;
                break;
            case "W":
                v = this.prms.V.v / this.prms.L.v / this.prms.Y.v;
                break;
            case "Y":
                v = this.prms.V.v / this.prms.L.v / this.prms.W.v;
                break;
            default:
                throw new Error("PabDimension.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.L.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.W.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Y.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.V.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.PabDimension = PabDimension;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/result":155}],68:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabDimensionParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class PabDimensionParams extends params_equation_1.ParamsEquation {
    constructor(rL, rW, rY, rV) {
        super();
        this._L = new param_definition_1.ParamDefinition(this, "L", param_domain_1.ParamDomainValue.POS, "m", rL, param_definition_1.ParamFamily.LENGTHS);
        this._W = new param_definition_1.ParamDefinition(this, "W", param_domain_1.ParamDomainValue.POS, "m", rW, param_definition_1.ParamFamily.WIDTHS);
        this._Y = new param_definition_1.ParamDefinition(this, "Y", param_domain_1.ParamDomainValue.POS, "m", rY, param_definition_1.ParamFamily.HEIGHTS);
        this._V = new param_definition_1.ParamDefinition(this, "V", param_domain_1.ParamDomainValue.POS, "m³", rV, param_definition_1.ParamFamily.VOLUMES);
        this.addParamDefinition(this._L);
        this.addParamDefinition(this._W);
        this.addParamDefinition(this._Y);
        this.addParamDefinition(this._V);
    }
    get L() {
        return this._L;
    }
    get W() {
        return this._W;
    }
    get Y() {
        return this._Y;
    }
    get V() {
        return this._V;
    }
}
exports.PabDimensionParams = PabDimensionParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],69:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabNombre = void 0;
const base_1 = require("../base");
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
class PabNombre extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.PabNombre;
        this._defaultCalculatedParam = prms.N;
        this.resetDefaultCalculatedParam();
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Equation(sVarCalc) {
        let rounded = false;
        if (sVarCalc !== "N" && !Number.isInteger(this.prms.N.v)) {
            this.prms.N.v = Math.round(this.prms.N.v);
            rounded = true;
        }
        let v;
        let DHR;
        let NB;
        let DHB;
        let NH;
        let DHH;
        switch (sVarCalc) {
            case "DHT":
                v = this.prms.N.v * this.prms.DH.v;
                break;
            case "N":
                const divAndMod = base_1.floatDivAndMod(this.prms.DHT.v, this.prms.DH.v);
                v = divAndMod.q;
                DHR = divAndMod.r;
                // harmonisation ?
                if (!base_1.isEqual(DHR, 0)) {
                    // vers le bas
                    if (v > 0) {
                        NB = v;
                        DHB = this.prms.DHT.v / NB;
                    }
                    // vers le haut
                    NH = v + 1;
                    DHH = this.prms.DHT.v / NH;
                }
                break;
            case "DH":
                v = this.prms.DHT.v / this.prms.N.v;
                break;
            default:
                throw new Error("PabNombre.Equation() : invalid variable name " + sVarCalc);
        }
        const r = new result_1.Result(v, this);
        if (DHR !== undefined) {
            r.resultElement.values.DHR = DHR;
        }
        if (NB !== undefined) {
            r.resultElement.values.NB = NB;
        }
        if (DHB !== undefined) {
            r.resultElement.values.DHB = DHB;
        }
        if (NH !== undefined) {
            r.resultElement.values.NH = NH;
        }
        if (DHH !== undefined) {
            r.resultElement.values.DHH = DHH;
        }
        if (rounded) {
            const m = new message_1.Message(message_1.MessageCode.WARNING_VALUE_ROUNDED_TO_INTEGER);
            m.extraVar.symbol = "N";
            m.extraVar.rounded = this.prms.N.v;
            r.resultElement.log.add(m);
        }
        return r;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.DHT.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.N.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.DH.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
    setResultsUnits() {
        this._resultsUnits = {
            DHR: "m",
            DHB: "m",
            DHH: "m"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            DHR: undefined,
            NB: undefined,
            NH: undefined,
            DHB: undefined,
            DHH: undefined
        };
    }
}
exports.PabNombre = PabNombre;

},{"../base":3,"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/message":151,"../util/result":155}],70:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabNombreParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class PabNombreParams extends params_equation_1.ParamsEquation {
    constructor(rDHT, rN, rDH) {
        super();
        this._DHT = new param_definition_1.ParamDefinition(this, "DHT", param_domain_1.ParamDomainValue.POS, "m", rDHT, param_definition_1.ParamFamily.TOTALFALLS);
        this._N = new param_definition_1.ParamDefinition(this, "N", param_domain_1.ParamDomainValue.POS, undefined, rN);
        this._DH = new param_definition_1.ParamDefinition(this, "DH", param_domain_1.ParamDomainValue.POS, "m", rDH, param_definition_1.ParamFamily.BASINFALLS);
        this.addParamDefinition(this._DHT);
        this.addParamDefinition(this._N);
        this.addParamDefinition(this._DH);
    }
    get DHT() {
        return this._DHT;
    }
    get N() {
        return this._N;
    }
    get DH() {
        return this._DH;
    }
}
exports.PabNombreParams = PabNombreParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],71:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * Parameters of a fish ladder
 */
class PabParams extends params_equation_1.ParamsEquation {
    /**
     * Paramètres communs à toutes les équations de structure
     * @param rZ1 Cote de l'eau amont (m)
     * @param rZ2 Cote de l'eau aval (m)
     */
    constructor(rQ, rZ1, rZ2) {
        super();
        this.Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.POS_NULL, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this.addParamDefinition(this.Q);
        this.Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.Z1);
        this.Z2 = new param_definition_1.ParamDefinition(this, "Z2", param_domain_1.ParamDomainValue.ANY, "m", rZ2, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.Z2);
    }
}
exports.PabParams = PabParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],72:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabPuissance = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
class PabPuissance extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.PabPuissance;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Equation(sVarCalc) {
        let v;
        const ro = 1000; // masse volumique de l'eau en kg/m3
        const g = 9.81; // accélération de la gravité terrestre en m/s2.
        switch (sVarCalc) {
            case "PV":
                v = ro * g * this.prms.Q.v * this.prms.DH.v / this.prms.V.v;
                break;
            case "Q":
                v = this.prms.PV.v * this.prms.V.v / (ro * g * this.prms.DH.v);
                break;
            case "V":
                v = ro * g * this.prms.Q.v * this.prms.DH.v / this.prms.PV.v;
                break;
            case "DH":
                v = this.prms.PV.v * this.prms.V.v / (ro * g * this.prms.Q.v);
                break;
            default:
                throw new Error("PabPuissance.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.DH.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.V.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.PV.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.PabPuissance = PabPuissance;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/result":155}],73:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PabPuissanceParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class PabPuissanceParams extends params_equation_1.ParamsEquation {
    constructor(rDH, rQ, rV, rPV) {
        super();
        this._DH = new param_definition_1.ParamDefinition(this, "DH", param_domain_1.ParamDomainValue.POS, "m", rDH, param_definition_1.ParamFamily.BASINFALLS);
        this._Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.POS_NULL, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this._V = new param_definition_1.ParamDefinition(this, "V", param_domain_1.ParamDomainValue.POS, "m³", rV, param_definition_1.ParamFamily.VOLUMES);
        this._PV = new param_definition_1.ParamDefinition(this, "PV", param_domain_1.ParamDomainValue.POS, "W/m³", rPV);
        this.addParamDefinition(this._DH);
        this.addParamDefinition(this._Q);
        this.addParamDefinition(this._V);
        this.addParamDefinition(this._PV);
    }
    get DH() {
        return this._DH;
    }
    get Q() {
        return this._Q;
    }
    get V() {
        return this._V;
    }
    get PV() {
        return this._PV;
    }
}
exports.PabPuissanceParams = PabPuissanceParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],74:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Par = exports.ParType = void 0;
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const par_type_plane_1 = require("./par_type_plane");
const par_type_fatou_1 = require("./par_type_fatou");
const par_type_superactive_1 = require("./par_type_superactive");
const par_type_chevron_1 = require("./par_type_chevron");
const message_1 = require("../util/message");
const resultelement_1 = require("../util/resultelement");
const base_1 = require("../base");
const fish_pass_1 = require("../fish_pass");
/** Type de Passe à Ralentisseurs */
var ParType;
(function (ParType) {
    ParType[ParType["PLANE"] = 0] = "PLANE";
    ParType[ParType["FATOU"] = 1] = "FATOU";
    ParType[ParType["SUPERACTIVE"] = 2] = "SUPERACTIVE";
    ParType[ParType["CHEVRON"] = 3] = "CHEVRON";
})(ParType = exports.ParType || (exports.ParType = {}));
class Par extends fish_pass_1.FishPass {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Par;
        this._defaultCalculatedParam = prms.Q;
        this.resetDefaultCalculatedParam();
        this._props.addObserver(this);
        this.parType = ParType.PLANE;
    }
    get prms() {
        return this._prms;
    }
    get parType() {
        return this.properties.getPropValue("parType");
    }
    set parType(e) {
        this.properties.setPropValue("parType", e);
    }
    Calc(sVarCalc, rInit) {
        // possible error result
        const r = new result_1.Result(new resultelement_1.ResultElement());
        let hasError = false;
        // if P is given and L is not calculated, check P value
        if (this.calculatedParam !== this.prms.L && this.prms.P.v !== undefined) {
            const standardP = this.parCalc.CalcP();
            const minP = standardP * 0.9;
            const maxP = standardP * 1.05;
            if (base_1.isLowerThan(this.prms.P.v, minP) || base_1.isGreaterThan(this.prms.P.v, maxP)) {
                const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
                m.extraVar.stdP = standardP;
                r.resultElement.addMessage(m);
                hasError = true;
            }
        }
        // Check admissible values for S, L, N, a, M
        const status = this.parCalc.checkInput();
        if (status.fatal) {
            for (const m of status.messages) {
                r.resultElement.addMessage(m);
            }
            hasError = true;
        }
        // Check that input data is in the range given by abacuses
        switch (this.calculatedParam) {
            case this.prms.ha:
                // check qStar
                const qStar = this.parCalc.CalcQStar();
                if (base_1.isLowerThan(qStar, this.parCalc.minQstar) || base_1.isGreaterThan(qStar, this.parCalc.maxQstar)) {
                    const mqstar = new message_1.Message(message_1.MessageCode.ERROR_PAR_QSTAR_OUT_OF_RANGE);
                    mqstar.extraVar.val = this.prms.Q.V;
                    mqstar.extraVar.min = this.parCalc.CalcQFromQStar(this.parCalc.minQstar);
                    mqstar.extraVar.max = this.parCalc.CalcQFromQStar(this.parCalc.maxQstar);
                    r.resultElement.addMessage(mqstar);
                    hasError = true;
                }
                break;
            case this.prms.Q:
                const m = this.checkHaAbacus();
                if (m !== undefined) {
                    r.resultElement.addMessage(m);
                    hasError = true;
                }
        }
        // if any fatal error occurred
        if (hasError) {
            this.currentResult = r;
            return this.result;
        }
        super.Calc(sVarCalc, rInit);
        // add checkInput()'s non fatal warnings to current result
        for (const m of status.messages) {
            this.result.resultElement.addMessage(m);
        }
        if (!this.result.ok)
            return this.result;
        // extra results
        this.parCalc.addExtraResults(this.result);
        // pass type independent extra results
        if (this.prms.Z1.v !== undefined) {
            // cote du déversoir à l'amont
            this.result.resultElement.values.ZD1 = this.parCalc.ZD1;
            if (this.prms.Z2.v !== undefined) {
                const P = this.result.resultElement.values.P; // calculated by addExtraResults()
                const ZR1 = this.result.resultElement.values.ZR1; // calculated by addExtraResults()
                // nombre de ralentisseurs
                // tslint:disable-next-line:variable-name
                const Nb = this.parCalc.CalcNb(); // depends on Z1 and Z2
                this.result.resultElement.values.Nb = Nb;
                // cote de déversement à l'aval de la passe
                const ZD2 = this.parCalc.ZD1 - this.parCalc.CalcLs() * this.prms.S.v / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
                this.result.resultElement.values.ZD2 = ZD2; // DEBUG
                // cote de radier de l'aval de la passe
                this.result.resultElement.values.ZR2 = ZD2 - (this.parCalc.ZD1 - ZR1);
                // longueur de la passe en suivant la pente
                const LPI = this.parCalc.CalcLs();
                this.result.resultElement.values.LPI = LPI;
                // longueur de la passe en projection horizontale
                this.result.resultElement.values.LPH = this.parCalc.CalcLh();
            }
        }
        return this.result;
    }
    checkHaAbacus() {
        let m;
        if (base_1.isLowerThan(this.parCalc.ha, this.parCalc.minHa) || base_1.isGreaterThan(this.parCalc.ha, this.parCalc.maxHa)) {
            m = new message_1.Message(message_1.MessageCode.ERROR_PAR_HA_OUT_OF_RANGE);
            m.extraVar.val = this.parCalc.ha;
            m.extraVar.min = this.parCalc.minHa;
            m.extraVar.max = this.parCalc.maxHa;
        }
        return m;
    }
    Equation(sVarCalc) {
        let v;
        switch (sVarCalc) {
            case "Q":
                v = this.parCalc.CalcQFromHa();
                break;
            case "ha":
                v = this.parCalc.CalcHa();
                break;
            default:
                throw new Error("Par.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    getFirstAnalyticalParameter() {
        // use ha in dicho, when calculating L, to prevent negative sqrt() in doCalcQYFromHa()
        return this.prms.ha;
    }
    update(sender, data) {
        if (data.action === "propertyChange") {
            if (data.name === "parType") {
                switch (data.value) {
                    case ParType.PLANE:
                        this.parCalc = new par_type_plane_1.ParTypePlane(this.prms);
                        break;
                    case ParType.FATOU:
                        this.parCalc = new par_type_fatou_1.ParTypeFatou(this.prms);
                        break;
                    case ParType.SUPERACTIVE:
                        this.parCalc = new par_type_superactive_1.ParTypeSuperactive(this.prms);
                        break;
                    case ParType.CHEVRON:
                        this.parCalc = new par_type_chevron_1.ParTypeChevron(this.prms);
                        break;
                    default:
                        throw new Error("Par.update() : invalid Par Type name " + data.value);
                }
                this.updateParamsVisibility();
            }
        }
    }
    updateParamsVisibility() {
        this.prms.L.visible = ([ParType.PLANE, ParType.FATOU].includes(this.parType));
        this.prms.a.visible = ([ParType.SUPERACTIVE, ParType.CHEVRON].includes(this.parType));
        this.prms.N.visible = ([ParType.SUPERACTIVE, ParType.CHEVRON].includes(this.parType));
        this.prms.M.visible = (this.parType === ParType.CHEVRON);
    }
    setParametersCalculability() {
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.ha.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
    setResultsUnits() {
        this._resultsUnits = {
            h: "m",
            qStar: "m³/s",
            V: "m/s",
            ZD1: "m",
            ZR1: "m",
            ZD2: "m",
            ZR2: "m",
            ZM: "m",
            LPI: "m",
            LPH: "m",
            B: "m",
            C: "m",
            D: "m",
            H: "m",
            Hmin: "m",
            Hmax: "m"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            h: undefined,
            qStar: undefined,
            V: undefined,
            Nb: undefined,
            ZD1: undefined,
            ZR1: undefined,
            ZD2: undefined,
            ZR2: undefined,
            ZM: undefined,
            LPI: undefined,
            LPH: undefined,
            L: undefined
        };
    }
}
exports.Par = Par;

},{"../base":3,"../compute-node":5,"../fish_pass":13,"../param/param-definition":86,"../util/message":151,"../util/result":155,"../util/resultelement":156,"./par_type_chevron":79,"./par_type_fatou":80,"./par_type_plane":82,"./par_type_superactive":84}],75:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class ParParams extends params_equation_1.ParamsEquation {
    constructor(rQ, rZ1, rZ2, rha, rS, rP, rL, ra, rN, rM) {
        super();
        // paramètres hydrauliques
        this._Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.POS_NULL, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this.addParamDefinition(this.Q);
        this._Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.Z1);
        this._Z2 = new param_definition_1.ParamDefinition(this, "Z2", param_domain_1.ParamDomainValue.ANY, "m", rZ2, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.Z2);
        // géométrie de la passe
        this._ha = new param_definition_1.ParamDefinition(this, "ha", param_domain_1.ParamDomainValue.POS_NULL, "m", rha, param_definition_1.ParamFamily.HEIGHTS);
        this.addParamDefinition(this.ha);
        this._S = new param_definition_1.ParamDefinition(this, "S", param_domain_1.ParamDomainValue.POS_NULL, "m/m", rS, param_definition_1.ParamFamily.SLOPES);
        this.addParamDefinition(this.S);
        this._P = new param_definition_1.ParamDefinition(this, "P", param_domain_1.ParamDomainValue.POS_NULL, "m", rP, param_definition_1.ParamFamily.WIDTHS);
        this.addParamDefinition(this.P);
        // paramètres dépendants du type de passe
        this._L = new param_definition_1.ParamDefinition(this, "L", param_domain_1.ParamDomainValue.POS, "m", rL, param_definition_1.ParamFamily.WIDTHS, false);
        this.addParamDefinition(this.L);
        this._a = new param_definition_1.ParamDefinition(this, "a", param_domain_1.ParamDomainValue.POS, "m", ra, param_definition_1.ParamFamily.HEIGHTS, false);
        this.addParamDefinition(this.a);
        this._N = new param_definition_1.ParamDefinition(this, "N", param_domain_1.ParamDomainValue.POS_NULL, "", rN, undefined, false);
        this.addParamDefinition(this.N);
        this._M = new param_definition_1.ParamDefinition(this, "M", param_domain_1.ParamDomainValue.POS_NULL, "", rM, undefined, false);
        this.addParamDefinition(this.M);
    }
    get Q() {
        return this._Q;
    }
    get Z1() {
        return this._Z1;
    }
    get Z2() {
        return this._Z2;
    }
    get ha() {
        return this._ha;
    }
    get S() {
        return this._S;
    }
    get P() {
        return this._P;
    }
    get L() {
        return this._L;
    }
    get a() {
        return this._a;
    }
    get N() {
        return this._N;
    }
    get M() {
        return this._M;
    }
}
exports.ParParams = ParParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],76:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParSimulation = exports.ParFlowRegime = void 0;
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const message_1 = require("../util/message");
const resultelement_1 = require("../util/resultelement");
const par_1 = require("./par");
const base_1 = require("../base");
var ParFlowRegime;
(function (ParFlowRegime) {
    /** Free flow (unsubmerged) */
    ParFlowRegime[ParFlowRegime["FREE"] = 0] = "FREE";
    /** Submerged flow */
    ParFlowRegime[ParFlowRegime["SUBMERGED"] = 1] = "SUBMERGED";
})(ParFlowRegime = exports.ParFlowRegime || (exports.ParFlowRegime = {}));
class ParSimulation extends par_1.Par {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.ParSimulation;
        this.prms.ha.visible = false; // show ZD1 instead
    }
    get prms() {
        return this._prms;
    }
    get parType() {
        return this.properties.getPropValue("parType");
    }
    set parType(e) {
        this.properties.setPropValue("parType", e);
    }
    Calc(sVarCalc, rInit) {
        // possible error result
        const r = new result_1.Result(new resultelement_1.ResultElement());
        let hasError = false;
        // If P is given, check it against standard value
        if (this.prms.P.v !== undefined) {
            const standardP = this.parCalc.CalcP();
            const minP = standardP * 0.9;
            const maxP = standardP * 1.05;
            if (base_1.isLowerThan(this.prms.P.v, minP) || base_1.isGreaterThan(this.prms.P.v, maxP)) {
                const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT);
                m.extraVar.stdP = standardP;
                r.resultElement.addMessage(m);
                hasError = true;
            }
        } // else will be calculated and displayed in results
        // Check admissible values for S, L, N, a, M
        const status = this.parCalc.checkInput();
        if (status.fatal) {
            for (const m of status.messages) {
                r.resultElement.addMessage(m);
            }
            hasError = true;
        }
        // Check elevations: ZR1 / ZD1 and ZR2 / ZD2
        const czRes1 = this.checkZrZd(this.prms.ZR1, this.prms.ZD1);
        const mzrd1 = czRes1.message;
        if (mzrd1 !== undefined) {
            r.resultElement.addMessage(mzrd1);
            hasError = true;
        }
        const czRes2 = this.checkZrZd(this.prms.ZR2, this.prms.ZD2);
        const mzrd2 = czRes2.message;
        if (mzrd2 !== undefined) {
            r.resultElement.addMessage(mzrd2);
            hasError = true;
        }
        // Check Nb
        const nb = this.CalcNb(czRes1.expectedZD, czRes2.expectedZD); // reference value
        if (this.prms.Nb.v === undefined) {
            this.prms.Nb.v = nb;
            this.prms.Nb.singleValue = base_1.round(nb, 3);
        }
        else {
            if (nb !== this.prms.Nb.v) {
                const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_NB_INCONSISTENT);
                m.extraVar.stdNb = String(nb); // to prevent "14.000"
                r.resultElement.addMessage(m);
                hasError = true;
            }
        } // else will be calculated and displayed in results
        // Check that input data is in the range given by abacuses
        if (this.calculatedParam === this.prms.Z1) {
            // check qStar
            const qStar = this.parCalc.CalcQStar();
            if (base_1.isLowerThan(qStar, this.parCalc.minQstar) || base_1.isGreaterThan(qStar, this.parCalc.maxQstar)) {
                const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_QSTAR_OUT_OF_RANGE);
                m.extraVar.val = this.prms.Q.V;
                m.extraVar.min = this.parCalc.CalcQFromQStar(this.parCalc.minQstar);
                m.extraVar.max = this.parCalc.CalcQFromQStar(this.parCalc.maxQstar);
                r.resultElement.addMessage(m);
                hasError = true;
            }
        }
        else if (this.calculatedParam === this.prms.Q) {
            const m = this.checkHaAbacus();
            if (m !== undefined) {
                r.resultElement.addMessage(m);
                hasError = true;
            }
        }
        // if any fatal error occurred
        if (hasError) {
            this.currentResult = r;
            return this.result;
        }
        this.nubCalc(sVarCalc, rInit);
        // add checkInput()'s non fatal warnings to current result
        if (!status.fatal) {
            for (const m of status.messages) {
                this.result.resultElement.addMessage(m);
            }
        }
        // extra results
        this.parCalc.addExtraResults(this.result, false);
        this.result.resultElement.values.ha = this.parCalc.ha;
        this.result.resultElement.values.Nb = nb;
        // add expected values for elevations that were not given
        if (this.prms.ZR1.v === undefined) {
            this.result.resultElement.values.ZR1 = czRes1.expectedZR;
        }
        else {
            // remove ZR1 systematically calculated by ParType.addExtraResults()
            delete this.result.resultElement.values.ZR1;
        }
        if (this.prms.ZR2.v === undefined) {
            this.result.resultElement.values.ZR2 = czRes2.expectedZR;
        }
        if (this.prms.ZD1.v === undefined) {
            this.result.resultElement.values.ZD1 = czRes1.expectedZD;
        }
        if (this.prms.ZD2.v === undefined) {
            this.result.resultElement.values.ZD2 = czRes2.expectedZD;
        }
        // do we have a fall at downstream baffle level ?
        if (this.prms.Z2.v !== undefined) {
            const ZD2 = this.prms.ZD2.v || this.result.resultElement.values.ZD2;
            // water level at downstream baffle
            const ZDB = ZD2 + this.result.resultElement.values.h;
            if (base_1.isLowerThan(this.prms.Z2.v, ZDB)) {
                const m = new message_1.Message(message_1.MessageCode.WARNING_PAR_NOT_SUBMERGED);
                m.extraVar.DH = ZDB - this.prms.Z2.v;
                this.result.resultElement.addMessage(m);
            }
            // expose fall for Verificateur
            this.result.resultElement.values.DH = Math.max(0, ZDB - this.prms.Z2.v);
        }
        return this.result;
    }
    /**
     * Checks that at least one of ZR, ZD is given; if both are given, checks that
     * each one corresponds to the expected value calculated from the other one;
     * returned message id undefined if everything is OK
     * @param ZR ZR1 or ZR2
     * @param ZD ZD1 or ZD2
     */
    checkZrZd(ZR, ZD) {
        let message;
        let expectedZR;
        let expectedZD;
        if (ZR.v === undefined && ZD.v === undefined) {
            message = new message_1.Message(message_1.MessageCode.ERROR_AT_LEAST_ONE_OF_THOSE_MUST_BE_DEFINED);
            message.extraVar.variables = [ZR.symbol, ZD.symbol];
        }
        else {
            // calculate missing value
            let zrDef = true;
            let zdDef = true;
            if (ZR.v === undefined) {
                zrDef = false;
                expectedZR = this.parCalc.CalcZRFromZD(ZD.V);
                /* ZR.v = expectedZR;
                ZR.singleValue = round(expectedZR, 3); */
            }
            if (ZD.v === undefined) {
                zdDef = false;
                expectedZD = this.parCalc.CalcZDFromZR(ZR.V);
                /* ZD.v = expectedZD;
                ZD.singleValue = round(expectedZD, 3); */
            }
            // if both values were defined, check that they match
            if (zrDef && zdDef) {
                expectedZR = this.parCalc.CalcZRFromZD(ZD.V);
                expectedZD = this.parCalc.CalcZDFromZR(ZR.V);
                if (!base_1.isEqual(ZR.v, expectedZR, 1E-3)) {
                    message = new message_1.Message(message_1.MessageCode.ERROR_PAR_ZR_ZD_MISMATCH);
                    message.extraVar.var_ZR = ZR.symbol;
                    message.extraVar.var_ZD = ZD.symbol;
                    message.extraVar.expectedZR = expectedZR;
                    message.extraVar.expectedZD = expectedZD;
                }
            }
        }
        return {
            message,
            expectedZR,
            expectedZD
        };
    }
    CalcNb(expectedZD1, expectedZD2) {
        // if parameters ZD1 and ZD2 were not given, use expected values calculated by checkZrZd
        let realZD1 = this.prms.ZD1.v;
        if (realZD1 === undefined) {
            realZD1 = expectedZD1;
        }
        let realZD2 = this.prms.ZD2.v;
        if (realZD2 === undefined) {
            realZD2 = expectedZD2;
        }
        const rLs = (realZD1 - realZD2) * Math.sqrt(1 + this.prms.S.v * this.prms.S.v) / this.prms.S.v;
        const nb = Math.floor((rLs + 0.01) / this.parCalc.P);
        switch (this.parType) {
            case par_1.ParType.PLANE:
            case par_1.ParType.FATOU:
                return nb + 1;
            case par_1.ParType.SUPERACTIVE:
            case par_1.ParType.CHEVRON:
                return nb;
        }
    }
    Equation(sVarCalc) {
        let v;
        switch (sVarCalc) {
            case "Q":
                v = this.parCalc.CalcQFromHa();
                break;
            case "Z1":
                v = this.parCalc.ZD1 + this.parCalc.CalcHa();
                break;
            default:
                throw new Error("Par.Equation() : invalid variable name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    setParametersCalculability() {
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.S.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.P.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Nb.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.ZR1.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.ZD1.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.ZR2.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.ZD2.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.L.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.a.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.N.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.M.calculability = param_definition_1.ParamCalculability.FIXED;
    }
    exposeResults() {
        this._resultsFamilies = {
            h: undefined,
            ha: undefined,
            V: undefined
        };
    }
}
exports.ParSimulation = ParSimulation;

},{"../base":3,"../compute-node":5,"../param/param-definition":86,"../util/message":151,"../util/result":155,"../util/resultelement":156,"./par":74}],77:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParSimulationParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const par_params_1 = require("./par_params");
class ParSimulationParams extends par_params_1.ParParams {
    constructor(rQ, rZ1, rZ2, rS, rP, rNb, rZR1, rZD1, rZR2, rZD2, rL, ra, rN, rM) {
        super(rQ, rZ1, rZ2, undefined, rS, rP, rL, ra, rN, rM);
        // géométrie de la passe
        this._Nb = new param_definition_1.ParamDefinition(this, "Nb", param_domain_1.ParamDomainValue.POS, "m", rNb);
        this.addParamDefinition(this.Nb);
        this._ZR1 = new param_definition_1.ParamDefinition(this, "ZR1", param_domain_1.ParamDomainValue.ANY, "m", rZR1, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZR1);
        this._ZD1 = new param_definition_1.ParamDefinition(this, "ZD1", param_domain_1.ParamDomainValue.ANY, "m", rZD1, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZD1);
        this._ZR2 = new param_definition_1.ParamDefinition(this, "ZR2", param_domain_1.ParamDomainValue.ANY, "m", rZR2, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZR2);
        this._ZD2 = new param_definition_1.ParamDefinition(this, "ZD2", param_domain_1.ParamDomainValue.ANY, "m", rZD2, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZD2);
    }
    get Nb() {
        return this._Nb;
    }
    get ZR1() {
        return this._ZR1;
    }
    get ZR2() {
        return this._ZR2;
    }
    get ZD1() {
        return this._ZD1;
    }
    get ZD2() {
        return this._ZD2;
    }
}
exports.ParSimulationParams = ParSimulationParams;

},{"../param/param-definition":86,"../param/param-domain":87,"./par_params":75}],78:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParTypeAbstract = void 0;
const param_value_mode_1 = require("../param/param-value-mode");
const par_simulation_params_1 = require("./par_simulation_params");
class ParTypeAbstract {
    constructor(prms) {
        this.prms = prms;
    }
    CalcZM() {
        return this.CalcZR1() + this.maxHa;
    }
    ;
    CalcZR1() {
        return this.CalcZRFromZD(this.ZD1);
    }
    addExtraResults(res, includeZmAndZr1 = true) {
        // hauteur d'eau dans la passe
        res.resultElement.values.h = this.CalcH();
        // débit adimensionnel
        res.resultElement.values.qStar = this.CalcQStar();
        // vitesse débitante
        res.resultElement.values.V = this.CalcVDeb();
        // espacement entre les ralentisseurs
        res.resultElement.values.P = this.P;
        // if Z1 is set
        if (this.prms.Z1.v !== undefined && includeZmAndZr1) {
            // cote d'arase minimale des murs latéraux à l'amont
            res.resultElement.values.ZM = this.CalcZM();
            // cote de radier à l'amont de la passe
            res.resultElement.values.ZR1 = this.CalcZR1();
        }
    }
    CalcVDeb() {
        const aw = this.CalcAw();
        // .V uses calculated value if any
        return this.prms.Q.V / aw;
    }
    get ZD1() {
        // when in ParSimulation
        if (this.prms instanceof par_simulation_params_1.ParSimulationParams) {
            // if ZD1 is given and not calculated, use it
            if (this.prms.ZD1.V !== undefined) {
                return this.prms.ZD1.V;
            }
            else {
                // else get the standard value
                return this.CalcZDFromZR(this.prms.ZR1.V);
            }
        }
        else {
            return this.prms.Z1.v - this.prms.ha.V; // returns undefined if Z1 (optional in Calage) is not set
        }
    }
    get ha() {
        // when in ParSimulation, do not trigger a calc loop if Z1 and ZD1 are given
        if (this.prms instanceof par_simulation_params_1.ParSimulationParams
            && this.prms.Z1.valueMode !== param_value_mode_1.ParamValueMode.CALCUL) {
            return this.prms.Z1.V - this.ZD1;
        }
        else if (this.prms.ha.V !== undefined) {
            // when ha is a given parameter, use it
            return this.prms.ha.V;
        }
        else {
            // else calculate ha from abacuses
            return this.CalcHa();
        }
    }
    /** returns the given input P if any, or the standard P calculated by CalcP() */
    get P() {
        if (this.prms.P.v !== undefined) {
            return this.prms.P.v;
        }
        else {
            return this.CalcP();
        }
    }
    /** minimum value of ha according to abacuses */
    get minHa() {
        return this.CalcHa(this.minQstar);
    }
    /** maximum value of ha according to abacuses */
    get maxHa() {
        return this.CalcHa(this.maxQstar);
    }
    /** Calculate the raw length of the pass based on water elevations */
    CalcLw() {
        return (this.prms.Z1.V - this.prms.Z2.v) * (Math.sqrt(1 + this.prms.S.v * this.prms.S.v) / this.prms.S.v);
    }
    /** Calculate the net length of the pass based on number of baffles */
    CalcLs() {
        return Math.ceil((this.CalcLw() - 0.001) / this.P) * this.P;
    }
    /** Calculate horizontal projection of pass length */
    CalcLh() {
        return this.CalcLs() / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }
    // return y part of CalcQFromHa
    doCalcQYFromHa(x) {
        return (Math.sqrt(Math.pow(this.c1ha, 2) - 4 * this.c2ha * (this.c0ha - x)) - this.c1ha) / (2 * this.c2ha);
    }
    /**
     * get c0, c1 or c2 coefficient for h or ha, for the current pass type
     * @param hOrHa "h" or "ha" :)
     * @param ci "c0", "c1" or "c2"
     */
    getCoeff(hOrHa, ci) {
        const a = this.coef[hOrHa][ci][0];
        const b = this.coef[hOrHa][ci][1];
        const c = this.coef[hOrHa][ci][2];
        return a * Math.pow(this.prms.S.v, 2) + b * this.prms.S.v + c;
    }
    get c0h() {
        return this.getCoeff("h", "c0");
    }
    get c1h() {
        return this.getCoeff("h", "c1");
    }
    get c2h() {
        return this.getCoeff("h", "c2");
    }
    get c0ha() {
        return this.getCoeff("ha", "c0");
    }
    get c1ha() {
        return this.getCoeff("ha", "c1");
    }
    get c2ha() {
        return this.getCoeff("ha", "c2");
    }
}
exports.ParTypeAbstract = ParTypeAbstract;

},{"../param/param-value-mode":89,"./par_simulation_params":77}],79:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParTypeChevron = void 0;
const par_type_sc_1 = require("./par_type_sc");
const par_1 = require("./par");
const message_1 = require("../util/message");
class ParTypeChevron extends par_type_sc_1.ParTypeSC {
    constructor(prms) {
        super(prms);
        this.type = par_1.ParType.CHEVRON;
    }
    // tslint:disable-next-line:variable-name
    CalcZRFromZD(ZDv) {
        return ZDv + (4 * this.prms.a.v * this.prms.S.v - this.prms.a.v) / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }
    // tslint:disable-next-line:variable-name
    CalcZDFromZR(ZRv) {
        return ZRv - (4 * this.prms.a.v * this.prms.S.v - this.prms.a.v) / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }
    CalcP() {
        return 4 * this.prms.a.v;
    }
    get minQstar() {
        return 0.7;
    }
    get maxQstar() {
        return 8.1;
    }
    addExtraResults(res) {
        super.addExtraResults(res);
        res.resultElement.values.B = this.CalcB();
    }
    checkInput() {
        const status = super.checkInput();
        // M, round to 1
        if (this.prms.M.v % 1 > 0.001 && this.prms.M.v % 1 < 0.999) {
            const m = new message_1.Message(message_1.MessageCode.WARNING_PAR_M_ROUNDED_TO_1);
            this.prms.M.v = Math.round(this.prms.M.v);
            m.extraVar.val = this.prms.M.v;
            status.messages.push(m);
        }
        // M, max. 2*N
        if (this.prms.M.v > this.prms.N.v * 2) {
            status.fatal = true;
            const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_M_GREATER_THAN_2_N);
            m.extraVar.max = this.prms.N.v * 2;
            status.messages.push(m);
        }
        return status;
    }
    get coef() {
        return {
            "h": {
                "c0": [0, -4.97686, 1.30546],
                "c1": [0, 0.176261, 0.661656],
                "c2": [0, -0.0733832, -0.00839864]
            },
            "ha": {
                "c0": [0, 5.02138, 0.709434],
                "c1": [0, -2.47998, 1.25363],
                "c2": [0, 0.188324, -0.0427461]
            }
        };
    }
    CalcB() {
        return 6 * this.prms.a.v * this.prms.N.v + 0.5 * this.prms.a.v * this.prms.M.v;
    }
}
exports.ParTypeChevron = ParTypeChevron;

},{"../util/message":151,"./par":74,"./par_type_sc":83}],80:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParTypeFatou = void 0;
const par_type_pf_1 = require("./par_type_pf");
const par_1 = require("./par");
const message_1 = require("../util/message");
const base_1 = require("../base");
class ParTypeFatou extends par_type_pf_1.ParTypePF {
    constructor(prms) {
        super(prms);
        this.type = par_1.ParType.FATOU;
    }
    CalcAw() {
        return 0.6 * this.CalcH() * this.prms.L.v;
    }
    // tslint:disable-next-line:variable-name
    CalcZRFromZD(ZDv) {
        return ZDv
            - (Math.sqrt(2) * this.CalcA() * Math.sin(45 * Math.PI / 180 + Math.atan(this.prms.S.v)))
            + this.prms.L.V * 0.5 * this.prms.S.v;
    }
    // tslint:disable-next-line:variable-name
    CalcZDFromZR(ZRv) {
        return ZRv
            + (Math.sqrt(2) * this.CalcA() * Math.sin(45 * Math.PI / 180 + Math.atan(this.prms.S.v)))
            - this.prms.L.V * 0.5 * this.prms.S.v;
    }
    CalcZM() {
        const H = 1.333 * this.prms.L.v;
        return this.ZD1 + H * Math.cos(Math.atan(this.prms.S.v));
    }
    CalcP() {
        return 0.5 * this.prms.L.V;
    }
    addExtraResults(res) {
        super.addExtraResults(res);
        res.resultElement.values.B = 0.6 * this.prms.L.V;
        res.resultElement.values.a = this.CalcA();
        res.resultElement.values.H = 1.333 * this.prms.L.V;
    }
    checkInput() {
        const status = super.checkInput();
        // S
        if (base_1.isLowerThan(this.prms.S.v, 0.08) || base_1.isGreaterThan(this.prms.S.v, 0.22)) {
            status.fatal = true;
            const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_S);
            m.extraVar.min = 0.08;
            m.extraVar.max = 0.22;
            status.messages.push(m);
        }
        else if (base_1.isGreaterThan(this.prms.S.v, 0.2)) {
            const m = new message_1.Message(message_1.MessageCode.WARNING_PAR_S);
            m.extraVar.min = 0.08;
            m.extraVar.max = 0.2;
            status.messages.push(m);
        }
        return status;
    }
    get coef() {
        return {
            "h": {
                "c0": [-3.56494, 0.450262, 0.0407576],
                "c1": [42.4113, -24.4941, 8.84146],
                "c2": [-73.4829, 54.6733, -14.0622]
            },
            "ha": {
                "c0": [15.8096, -5.19282, 0.465827],
                "c1": [302.623, -106.203, 13.2957],
                "c2": [-783.592, 269.991, -25.2637]
            }
        };
    }
    CalcA() {
        return 0.2 * this.prms.L.V;
    }
}
exports.ParTypeFatou = ParTypeFatou;

},{"../base":3,"../util/message":151,"./par":74,"./par_type_pf":81}],81:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParTypePF = void 0;
const par_type_1 = require("./par_type");
const message_1 = require("../util/message");
const param_value_mode_1 = require("../param/param-value-mode");
/**
 * Intermediate class for common stuff between PLANE and FATOU pass types
 */
class ParTypePF extends par_type_1.ParTypeAbstract {
    /**
     * Math.sqrt(9.81)
     */
    get RG() {
        return 3.13209;
    }
    CalcH() {
        const qStar = this.CalcQStar();
        return this.prms.L.v * (this.c2h * Math.pow(qStar, 2) + this.c1h * qStar + this.c0h);
    }
    CalcHa(qStar) {
        if (qStar === undefined) {
            qStar = this.CalcQStar();
        }
        // @TODO L.V (grand V) pour la vérification de ha p/r aux abaques ? Fait foirer le calcul de L …
        return this.prms.L.v * (this.c2ha * Math.pow(qStar, 2) + this.c1ha * qStar + this.c0ha);
    }
    CalcQStar() {
        return this.prms.Q.V / (this.RG * Math.pow(this.prms.L.v, 2.5));
    }
    get minQstar() {
        return 0.0065;
    }
    get maxQstar() {
        return 0.35;
    }
    CalcQFromQStar(qStar) {
        return qStar * this.RG * Math.pow(this.prms.L.v, 2.5);
    }
    CalcLFromQStar(qStar) {
        return Math.pow(qStar * this.RG / this.prms.Q.v, 0.4);
    }
    CalcQFromHa() {
        const x = this.ha / this.prms.L.v;
        const y = this.doCalcQYFromHa(x);
        return this.CalcQFromQStar(y);
    }
    /** calculates the number of baffles Nb based on the baffles spacing P */
    CalcNb() {
        return this.CalcLs() / this.P + 1;
    }
    checkInput() {
        let fatal = false;
        const messages = [];
        // L; when calculated, see Par.Calc()
        if (this.prms.L.valueMode !== param_value_mode_1.ParamValueMode.CALCUL) {
            if (this.prms.L.v < 0.3 || this.prms.L.v > 1.3) {
                fatal = true;
                const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_L);
                m.extraVar.min = 0.3;
                m.extraVar.max = 1.3;
                messages.push(m);
            }
            else if (this.prms.L.v > 1) {
                const m = new message_1.Message(message_1.MessageCode.WARNING_PAR_L);
                m.extraVar.max = 1;
                messages.push(m);
            }
        }
        return { fatal, messages };
    }
}
exports.ParTypePF = ParTypePF;

},{"../param/param-value-mode":89,"../util/message":151,"./par_type":78}],82:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParTypePlane = void 0;
const par_type_pf_1 = require("./par_type_pf");
const par_1 = require("./par");
const message_1 = require("../util/message");
const base_1 = require("../base");
class ParTypePlane extends par_type_pf_1.ParTypePF {
    constructor(prms) {
        super(prms);
        this.type = par_1.ParType.PLANE;
    }
    CalcAw() {
        const h = this.CalcH();
        return this.prms.L.v * (0.583 * h - 0.146 * this.prms.L.v);
    }
    // tslint:disable-next-line:variable-name
    CalcZRFromZD(ZDv) {
        return ZDv - (0.236 * this.prms.L.v * Math.sin(Math.PI / 4 + Math.atan(this.prms.S.v)));
    }
    // tslint:disable-next-line:variable-name
    CalcZDFromZR(ZRv) {
        return ZRv + (0.236 * this.prms.L.v * Math.sin(Math.PI / 4 + Math.atan(this.prms.S.v)));
    }
    CalcZM() {
        // tslint:disable-next-line:variable-name
        const Hmin = this.CalcHmin();
        return this.CalcZR1() + Hmin * Math.sin(Math.PI / 4 + Math.atan(this.prms.S.v));
    }
    CalcP() {
        return (2 / 3) * this.prms.L.V;
    }
    addExtraResults(res) {
        super.addExtraResults(res);
        res.resultElement.values.B = 0.583 * this.prms.L.v;
        res.resultElement.values.C = 0.472 * this.prms.L.v;
        res.resultElement.values.D = 0.236 * this.prms.L.v;
        res.resultElement.values.Hmin = this.CalcHmin();
        res.resultElement.values.Hmax = 2.2 * this.prms.L.v;
    }
    checkInput() {
        const status = super.checkInput();
        // S
        if (base_1.isLowerThan(this.prms.S.v, 0.06) || base_1.isGreaterThan(this.prms.S.v, 0.22)) {
            status.fatal = true;
            const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_S);
            m.extraVar.min = 0.06;
            m.extraVar.max = 0.22;
            status.messages.push(m);
        }
        else if (base_1.isLowerThan(this.prms.S.v, 0.08) || base_1.isGreaterThan(this.prms.S.v, 0.2)) {
            const m = new message_1.Message(message_1.MessageCode.WARNING_PAR_S);
            m.extraVar.min = 0.08;
            m.extraVar.max = 0.2;
            status.messages.push(m);
        }
        return status;
    }
    get coef() {
        return {
            "h": {
                "c0": [16.7218, -6.09624, 0.834851],
                "c1": [-139.382, 47.2186, 0.0547598],
                "c2": [347.368, -130.698, 8.14521]
            },
            "ha": {
                "c0": [15.2115, -5.22606, 0.633654],
                "c1": [-184.043, 59.7073, -0.530737],
                "c2": [315.110, -115.164, 6.85371]
            }
        };
    }
    CalcHmin() {
        return 1.85 * this.prms.L.v;
    }
}
exports.ParTypePlane = ParTypePlane;

},{"../base":3,"../util/message":151,"./par":74,"./par_type_pf":81}],83:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParTypeSC = void 0;
const par_type_1 = require("./par_type");
const message_1 = require("../util/message");
const base_1 = require("../base");
/**
 * Intermediate class for common stuff between SUPERACTIVE and CHEVRON pass types
 */
class ParTypeSC extends par_type_1.ParTypeAbstract {
    /**
     * Math.sqrt(2 * 9.81)
     */
    get RG() {
        return 4.42945;
    }
    CalcH() {
        const qStar = this.CalcQStar();
        return this.prms.a.v * (this.c2h * Math.pow(qStar, 2) + this.c1h * qStar + this.c0h);
    }
    CalcHa(qStar) {
        if (qStar === undefined) {
            qStar = this.CalcQStar();
        }
        return this.prms.a.v * (this.c2ha * Math.pow(qStar, 2) + this.c1ha * qStar + this.c0ha);
    }
    CalcQStar() {
        return (this.prms.Q.V / this.CalcB()) / (this.RG * Math.pow(this.prms.a.v, 1.5));
    }
    CalcQFromQStar(qStar) {
        return qStar * this.RG * Math.pow(this.prms.a.v, 1.5) * this.CalcB();
    }
    CalcQFromHa() {
        const x = this.ha / this.prms.a.v;
        const y = this.doCalcQYFromHa(x);
        return this.CalcQFromQStar(y);
    }
    CalcAw() {
        return this.CalcH() * this.CalcB();
    }
    /** calculates the number of baffles Nb based on the baffles spacing P */
    CalcNb() {
        return this.CalcLs() / this.P;
    }
    checkInput() {
        let fatal = false;
        const messages = [];
        // S
        if (base_1.isLowerThan(this.prms.S.v, 0.1) || base_1.isGreaterThan(this.prms.S.v, 0.18)) {
            fatal = true;
            const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_S);
            m.extraVar.min = 0.1;
            m.extraVar.max = 0.18;
            messages.push(m);
        }
        else if (base_1.isGreaterThan(this.prms.S.v, 0.16)) {
            const m = new message_1.Message(message_1.MessageCode.WARNING_PAR_S);
            m.extraVar.min = 0.1;
            m.extraVar.max = 0.16;
            messages.push(m);
        }
        // N
        if (this.prms.N.v % 0.5 > 0.001 && this.prms.N.v % 0.5 < 0.499) {
            const m = new message_1.Message(message_1.MessageCode.WARNING_PAR_N_ROUNDED_TO_05);
            this.prms.N.v = Math.round(this.prms.N.v * 2) / 2;
            m.extraVar.val = this.prms.N.v;
            messages.push(m);
        }
        // a
        if (base_1.isLowerThan(this.prms.a.v, 0.05) || base_1.isGreaterThan(this.prms.a.v, 0.25)) {
            fatal = true;
            const m = new message_1.Message(message_1.MessageCode.ERROR_PAR_A);
            m.extraVar.min = 0.05;
            m.extraVar.max = 0.25;
            messages.push(m);
        }
        else if (base_1.isGreaterThan(this.prms.a.v, 0.2)) {
            const m = new message_1.Message(message_1.MessageCode.WARNING_PAR_A);
            m.extraVar.max = 0.2;
            messages.push(m);
        }
        return { fatal, messages };
    }
}
exports.ParTypeSC = ParTypeSC;

},{"../base":3,"../util/message":151,"./par_type":78}],84:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParTypeSuperactive = void 0;
const par_type_sc_1 = require("./par_type_sc");
const par_1 = require("./par");
class ParTypeSuperactive extends par_type_sc_1.ParTypeSC {
    constructor(prms) {
        super(prms);
        this.type = par_1.ParType.SUPERACTIVE;
    }
    // tslint:disable-next-line:variable-name
    CalcZRFromZD(ZDv) {
        return ZDv + (2.6 * this.prms.a.v * this.prms.S.v - this.prms.a.v) / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }
    // tslint:disable-next-line:variable-name
    CalcZDFromZR(ZRv) {
        return ZRv - (2.6 * this.prms.a.v * this.prms.S.v - this.prms.a.v) / Math.sqrt(1 + this.prms.S.v * this.prms.S.v);
    }
    CalcP() {
        return 2.6 * this.prms.a.v;
    }
    get minQstar() {
        return 0.4;
    }
    get maxQstar() {
        return 5.3;
    }
    addExtraResults(res) {
        super.addExtraResults(res);
        res.resultElement.values.B = this.CalcB();
        res.resultElement.values.L = this.CalcB() / this.prms.N.v;
    }
    get coef() {
        return {
            "h": {
                "c0": [0, -2.62712, 0.601348],
                "c1": [0, 1.15807, 1.07554],
                "c2": [0, -0.559218, 0.000504060]
            },
            "ha": {
                "c0": [0, -2.22434, 0.596682],
                "c1": [0, 0.514953, 1.25460],
                "c2": [0, -0.354624, -0.0153156]
            }
        };
    }
    CalcB() {
        return 6 * this.prms.a.v * this.prms.N.v;
    }
}
exports.ParTypeSuperactive = ParTypeSuperactive;

},{"./par":74,"./par_type_sc":83}],85:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MirrorIterator = void 0;
/**
 * Itérateur miroir sur les (ou la) valeurs prises par un ParamValues.
 * Différent d'un ParamValuesIterator avec reverse = true.
 *
 * ex: min = 0, max = 10, step = 3
 * ParamValuesIterator:         [ 0, 3, 6, 9, 10 ]
 * ParamValuesIterator reverse: [ 10, 7, 4, 1, 0 ]
 * MirrorIterator:              [ 10, 9, 6, 3, 0 ]
 */
class MirrorIterator {
    constructor(prm) {
        prm.check();
        this._param = prm;
        this.reset();
    }
    get currentValue() {
        return this._current;
    }
    /**
     * Returns the number of elements in the iterator without altering it,
     * by counting a copy of the iterator
     */
    count() {
        return this._param.valuesIterator.count();
    }
    /**
     * Reinits the value iterator
     */
    reset() {
        this._index = 0;
        this._current = undefined;
        // get original values list
        this._valuesList = [];
        const it = this._param.valuesIterator;
        while (it.hasNext) {
            this._valuesList.push(it.next().value);
        }
        // mirror it
        this._valuesList.reverse();
    }
    next() {
        if (this.hasNext) {
            this._current = this._valuesList[this._index];
            this._index++;
            return {
                done: false,
                value: this._current
            };
        }
        else { // end of iterator
            return {
                done: true,
                value: undefined
            };
        }
    }
    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    nextValue() {
        return this.next();
    }
    // interface IterableIterator
    [Symbol.iterator]() {
        return this;
    }
    /**
     * Returns true if iterator has at least one more value
     */
    get hasNext() {
        return this._index < this._valuesList.length;
    }
}
exports.MirrorIterator = MirrorIterator;

},{}],86:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamDefinition = exports.ExtensionStrategy = exports.ParamFamily = exports.ParamCalculability = void 0;
const index_1 = require("../index");
const linked_value_1 = require("../linked-value");
const nub_1 = require("../nub");
const section_type_1 = require("../open-channel/section/section_type");
const interval_1 = require("../util/interval");
const message_1 = require("../util/message");
const observer_1 = require("../util/observer");
const param_domain_1 = require("./param-domain");
const param_value_mode_1 = require("./param-value-mode");
const param_values_1 = require("./param-values");
/**
 * Calculabilité du paramètre
 */
var ParamCalculability;
(function (ParamCalculability) {
    /** parameter may have any value, may not vary, may not be calculated */
    ParamCalculability[ParamCalculability["FIXED"] = 0] = "FIXED";
    /** parameter may have any value, may vary, may not be calculated */
    ParamCalculability[ParamCalculability["FREE"] = 1] = "FREE";
    /** parameter may have any value, may vary, may be calculated analytically */
    ParamCalculability[ParamCalculability["EQUATION"] = 2] = "EQUATION";
    /** parameter may have any value, may vary, may be calculated using numerical method */
    ParamCalculability[ParamCalculability["DICHO"] = 3] = "DICHO";
})(ParamCalculability = exports.ParamCalculability || (exports.ParamCalculability = {}));
/**
 * Parameter family: defines linkability with other parameters/results
 */
var ParamFamily;
(function (ParamFamily) {
    ParamFamily[ParamFamily["ANY"] = 0] = "ANY";
    ParamFamily[ParamFamily["LENGTHS"] = 1] = "LENGTHS";
    ParamFamily[ParamFamily["WIDTHS"] = 2] = "WIDTHS";
    ParamFamily[ParamFamily["SLOPES"] = 3] = "SLOPES";
    ParamFamily[ParamFamily["HEIGHTS"] = 4] = "HEIGHTS";
    ParamFamily[ParamFamily["BASINFALLS"] = 5] = "BASINFALLS";
    ParamFamily[ParamFamily["TOTALFALLS"] = 6] = "TOTALFALLS";
    ParamFamily[ParamFamily["ELEVATIONS"] = 7] = "ELEVATIONS";
    ParamFamily[ParamFamily["VOLUMES"] = 8] = "VOLUMES";
    ParamFamily[ParamFamily["FLOWS"] = 9] = "FLOWS";
    ParamFamily[ParamFamily["DIAMETERS"] = 10] = "DIAMETERS";
    ParamFamily[ParamFamily["SPEEDS"] = 11] = "SPEEDS";
    ParamFamily[ParamFamily["STRICKLERS"] = 12] = "STRICKLERS";
})(ParamFamily = exports.ParamFamily || (exports.ParamFamily = {}));
/**
 * Strategy to apply when multiple parameters are variating, and
 * values series have to be extended for sizes to match
 */
var ExtensionStrategy;
(function (ExtensionStrategy) {
    ExtensionStrategy[ExtensionStrategy["REPEAT_LAST"] = 0] = "REPEAT_LAST";
    ExtensionStrategy[ExtensionStrategy["RECYCLE"] = 1] = "RECYCLE"; // repeat the whole series from the beginning
    // autres propositions :
    // PAD_LEFT     // pad with zeroes at the beginning ?
    // PAD_RIGHT    // pad with zeroes at the end ?
    // INTERPOLATE  // insert regular steps between first and last value
})(ExtensionStrategy = exports.ExtensionStrategy || (exports.ExtensionStrategy = {}));
/**
 * Paramètre avec symbole, famille, domaine de définition, calculabilité,
 * pointant éventuellement vers un autre paramètre / résultat
 */
class ParamDefinition {
    constructor(parent, symb, d, unit, val, family, visible = true) {
        /** le paramètre doit-il être exposé (par ex: affiché par l'interface graphique) ? */
        this.visible = true;
        this._parent = parent;
        this._symbol = symb;
        this._unit = unit;
        this._observable = new observer_1.Observable();
        this._paramValues = new param_values_1.ParamValues();
        // set single value and copy it to sandbox value and initValue
        this._paramValues.singleValue = val;
        this.v = val;
        this.initValue = val;
        this._calc = ParamCalculability.FREE;
        this._family = family;
        this.visible = visible;
        this.valueMode = param_value_mode_1.ParamValueMode.SINGLE;
        this.extensionStrategy = ExtensionStrategy.REPEAT_LAST;
        this.setDomain(d);
        this.checkValueAgainstDomain(val);
    }
    /**
     * set parent a-posteriori; used by nghyd when populating forms
     */
    set parent(parent) {
        this._parent = parent;
    }
    /**
     * identifiant unique de la forme
     *   (uid du JalhydObject (en général un Nub) + "_" + symbole du paramètre)
     * ex: 123_Q
     */
    get uid() {
        return this.nubUid + "_" + this._symbol;
    }
    get unit() {
        return this._unit;
    }
    setUnit(u) {
        this._unit = u;
    }
    get initValue() {
        return this._initValue;
    }
    /**
     * Make sure that this value is never undefined, null or NaN, so that
     * there is always an initial value for "DICHO" calculations, even
     * when param value was set to undefined before
     */
    set initValue(v) {
        if (v !== undefined && !isNaN(v) && v !== null) {
            this._initValue = v;
        }
        else {
            if (this._initValue === undefined || isNaN(this._initValue) || this._initValue === null) {
                this._initValue = Math.random();
            }
        }
    }
    /**
     * @see getParentComputeNode()
     */
    get parentComputeNode() {
        return this.getParentComputeNode();
    }
    /**
     * pointer to the ComputeNode (usually Nub) that uses the ParamsEquation that
     * uses this ParamDefinition; for Section params, if returnEnclosingNubForSections
     * is true, this will return the Nub enclosing the Section, otherwise the section itself
     */
    getParentComputeNode(returnEnclosingNubForSections = true) {
        let parentCN;
        if (this._parent) {
            // ComputeNode utilisant le ParamsEquation
            parentCN = this._parent.parent;
            // Section: go up to enclosing Nub if any (might not have any yet when unserializing sessions)
            if (parentCN instanceof section_type_1.acSection && returnEnclosingNubForSections && parentCN.parent) {
                parentCN = parentCN.parent;
            }
        }
        else {
            // fail silently (the story of my life)
            // throw new Error("ParamDefinition.parentComputeNode : parameter has no parent !");
        }
        return parentCN;
    }
    /**
     * Returns the parent compute node as Nub if it is a Nub; returns
     * undefined otherwise
     */
    get parentNub() {
        if (this.parentComputeNode && this.parentComputeNode instanceof nub_1.Nub) {
            return this.parentComputeNode;
        }
        else {
            return undefined;
        }
    }
    /**
     * Identifiant unique du Nub parent
     */
    get nubUid() {
        return this.parentComputeNode.uid;
    }
    /**
     * Type de module du Nub parent
     */
    get nubCalcType() {
        let parentCalcType;
        if (this.parentComputeNode instanceof nub_1.Nub) {
            parentCalcType = this.parentComputeNode.calcType;
        }
        else {
            throw new Error("ParamDefinition.nubCalcType : parameter has no parent !");
        }
        return parentCalcType;
    }
    get symbol() {
        return this._symbol;
    }
    setDomain(d) {
        if (d instanceof param_domain_1.ParamDomain) {
            this._domain = d;
        }
        else {
            this._domain = new param_domain_1.ParamDomain(d);
        }
    }
    get domain() {
        return this._domain;
    }
    get interval() {
        return this._domain.interval;
    }
    get extensionStrategy() {
        return this._extensionStrategy;
    }
    set extensionStrategy(strategy) {
        this._extensionStrategy = strategy;
        // synchronise with underlying local ParamValues (for iterator), except for links
        if ([param_value_mode_1.ParamValueMode.SINGLE, param_value_mode_1.ParamValueMode.MINMAX, param_value_mode_1.ParamValueMode.LISTE].includes(this.valueMode)) {
            if (this._paramValues) {
                this._paramValues.extensionStrategy = strategy;
            }
        }
    }
    get valueMode() {
        return this._valueMode;
    }
    /**
     * Easy setter that propagates the change to other parameters
     * (default behaviour); use setValueMode(..., false) to prevent
     * possible infinite loops
     */
    set valueMode(newMode) {
        this.setValueMode(newMode);
    }
    /**
     * Sets the value mode and asks the Nub to ensure there is only one parameter
     * in CALC mode
     *
     * If propagateToCalculatedParam is true, any param that goes from CALC mode
     * to any other mode will trigger a reset of the default calculated param at
     * Nub level
     *
     * Propagates modes other than CALC and LINK to the underlying
     * ParamValues (local instance)
     */
    setValueMode(newMode, propagateToCalculatedParam = true) {
        const oldMode = this.valueMode;
        // ignore idempotent calls
        if (oldMode === newMode) {
            return;
        }
        if (oldMode === param_value_mode_1.ParamValueMode.CALCUL) {
            if (propagateToCalculatedParam) {
                // Set default calculated parameter, only if previous CALC param was
                // manually set to something else than CALC
                if (this.parentComputeNode && this.parentComputeNode instanceof nub_1.Nub) {
                    this.parentComputeNode.resetDefaultCalculatedParam(this);
                }
            }
        }
        else if (newMode === param_value_mode_1.ParamValueMode.CALCUL) {
            // set old CALC param to SINGLE mode
            if (this.parentComputeNode && this.parentComputeNode instanceof nub_1.Nub) {
                this.parentComputeNode.unsetCalculatedParam(this);
            }
        }
        // set new mode
        this._valueMode = newMode;
        // synchronise with underlying local ParamValues (for iterator)
        if (newMode === param_value_mode_1.ParamValueMode.SINGLE
            || newMode === param_value_mode_1.ParamValueMode.MINMAX
            || newMode === param_value_mode_1.ParamValueMode.LISTE) {
            if (this._paramValues) {
                this._paramValues.valueMode = newMode;
            }
        }
    }
    /**
     * Returns true if this parameter is the calculatedParam of
     * its parent Nub
     */
    get isCalculated() {
        if (this.parentNub) {
            return (this.parentNub.calculatedParam === this); // should be the same object
        }
        return false;
    }
    /**
     * Sets this parameter as the one to be calculated
     */
    setCalculated() {
        if (this.parentNub) {
            this.parentNub.calculatedParam = this;
        }
    }
    /**
     * Current values set associated to this parameter; in LINK mode, points
     * to a remote set of values.
     */
    get paramValues() {
        if (this.valueMode === param_value_mode_1.ParamValueMode.LINK && this.isReferenceDefined()) {
            return this._referencedValue.getParamValues();
        }
        else {
            return this._paramValues;
        }
    }
    get referencedValue() {
        return this._referencedValue;
    }
    get family() {
        return this._family;
    }
    undefineFamily() {
        this._family = undefined;
    }
    get calculability() {
        if (this._calc === undefined) {
            const e = new message_1.Message(message_1.MessageCode.ERROR_PARAMDEF_CALC_UNDEFINED);
            e.extraVar.symbol = this.symbol;
            throw e;
        }
        return this._calc;
    }
    set calculability(c) {
        this._calc = c;
    }
    /**
     * Returns true if current value (not singleValue !) is defined
     */
    get hasCurrentValue() {
        return (this.v !== undefined);
    }
    /**
     * Returns true if held value (not currentValue !) is defined,
     * depending on the value mode
     */
    get isDefined() {
        let defined = false;
        switch (this.valueMode) {
            case param_value_mode_1.ParamValueMode.SINGLE:
                defined = (this.singleValue !== undefined);
                break;
            case param_value_mode_1.ParamValueMode.MINMAX:
                defined = (this.paramValues.min !== undefined
                    && this.paramValues.max !== undefined
                    && this.paramValues.step !== undefined);
                break;
            case param_value_mode_1.ParamValueMode.LISTE:
                defined = (this.valueList && this.valueList.length > 0 && this.valueList[0] !== undefined);
                break;
            case param_value_mode_1.ParamValueMode.CALCUL:
                if (this.parentNub && this.parentNub.result && this.parentNub.result.resultElements.length > 0) {
                    const res = this.parentNub.result;
                    defined = (res.vCalc !== undefined);
                }
                break;
            case param_value_mode_1.ParamValueMode.LINK:
                defined = this.referencedValue.isDefined();
        }
        return defined;
    }
    // -- values getters / setters; in LINK mode, reads / writes from / to the target values
    get singleValue() {
        this.checkValueMode([param_value_mode_1.ParamValueMode.SINGLE, param_value_mode_1.ParamValueMode.CALCUL, param_value_mode_1.ParamValueMode.LINK]);
        return this.paramValues.singleValue;
    }
    set singleValue(v) {
        this.checkValueMode([param_value_mode_1.ParamValueMode.SINGLE, param_value_mode_1.ParamValueMode.CALCUL, param_value_mode_1.ParamValueMode.LINK]);
        this.checkValueAgainstDomain(v);
        this.paramValues.singleValue = v;
        this.initValue = v;
        this.notifyValueModified(this);
    }
    get min() {
        this.checkValueMode([param_value_mode_1.ParamValueMode.MINMAX, param_value_mode_1.ParamValueMode.LINK]);
        return this.paramValues.min;
    }
    set min(v) {
        this.checkValueMode([param_value_mode_1.ParamValueMode.MINMAX]);
        this.paramValues.min = v;
    }
    get max() {
        this.checkValueMode([param_value_mode_1.ParamValueMode.MINMAX, param_value_mode_1.ParamValueMode.LINK]);
        return this.paramValues.max;
    }
    set max(v) {
        this.checkValueMode([param_value_mode_1.ParamValueMode.MINMAX]);
        this.paramValues.max = v;
    }
    get step() {
        this.checkValueMode([param_value_mode_1.ParamValueMode.MINMAX, param_value_mode_1.ParamValueMode.LINK]);
        return this.paramValues.step;
    }
    set step(v) {
        this.checkValueMode([param_value_mode_1.ParamValueMode.MINMAX]);
        this.paramValues.step = v;
    }
    /**
     * Generates a reference step value, given the current (local) values for min / max
     */
    get stepRefValue() {
        this.checkValueMode([param_value_mode_1.ParamValueMode.MINMAX]);
        return new interval_1.Interval(1e-9, this._paramValues.max - this._paramValues.min);
    }
    get valueList() {
        this.checkValueMode([param_value_mode_1.ParamValueMode.LISTE, param_value_mode_1.ParamValueMode.LINK]);
        return this.paramValues.valueList;
    }
    set valueList(l) {
        this.checkValueMode([param_value_mode_1.ParamValueMode.LISTE]);
        this.paramValues.valueList = l;
    }
    count() {
        return this.paramValues.valuesIterator.count();
    }
    /**
     * Copies values of p into the current parameter, whatever the valueMode is; if
     * p has linked values, the target values will be copied and the links won't be kept
     * @param p a reference ParamDefinition or ParamValues to copy values from
     */
    copyValuesFrom(p) {
        this.valueMode = p.valueMode;
        switch (p.valueMode) {
            case param_value_mode_1.ParamValueMode.MINMAX:
                this.min = p.min;
                this.max = p.max;
                this.step = p.step;
                break;
            case param_value_mode_1.ParamValueMode.LISTE:
                this.valueList = p.valueList;
                break;
            case param_value_mode_1.ParamValueMode.LINK:
                if (p instanceof ParamDefinition) {
                    this.copyValuesFrom(p.referencedValue.getParamValues());
                }
                break;
            case param_value_mode_1.ParamValueMode.CALCUL:
            case param_value_mode_1.ParamValueMode.SINGLE:
            default:
                this.singleValue = p.singleValue;
        }
    }
    // for INumberiterator interface
    get currentValue() {
        // magically follows links
        return this.paramValues.currentValue;
    }
    /**
     * Get single value
     */
    getValue() {
        return this.paramValues.singleValue;
    }
    /**
     * Returns values as a number list, for LISTE and MINMAX modes;
     * in MINMAX mode, infers the list from min/max/step values
     * @param extendTo if given, will extend the values list to this number of values
     */
    getInferredValuesList(extendTo) {
        this.checkValueMode([param_value_mode_1.ParamValueMode.LISTE, param_value_mode_1.ParamValueMode.MINMAX, param_value_mode_1.ParamValueMode.LINK]);
        return this.paramValues.getInferredValuesList(false, extendTo, true);
    }
    /**
     * Magic method to define current values into Paramvalues set; defines the mode
     * accordingly by detecting values nature
     */
    setValues(o, max, step) {
        this.paramValues.setValues(o, max, step);
        if (typeof (o) === "number") {
            if (max === undefined) {
                this.valueMode = param_value_mode_1.ParamValueMode.SINGLE;
            }
            else {
                this.valueMode = param_value_mode_1.ParamValueMode.MINMAX;
            }
        }
        else if (Array.isArray(o)) {
            this.valueMode = param_value_mode_1.ParamValueMode.LISTE;
        }
        else {
            throw new Error(`ParamValues.setValues() : invalid call`);
        }
    }
    /**
     * Sets the current value of the parameter's own values set, then
     * notifies all observers
     */
    setValue(val, sender) {
        this.valueMode = param_value_mode_1.ParamValueMode.SINGLE;
        this.checkValueAgainstDomain(val);
        this.paramValues.singleValue = val;
    }
    /**
     * Same as setValue() but does not force SINGLE value mode; used for
     * updating initial value of a DICHO calculated parameter
     */
    setInitValue(val, sender) {
        this.checkValueAgainstDomain(val);
        this.paramValues.singleValue = val;
    }
    /**
     * Validates the given numeric value against the definition domain; throws
     * an error if value is outside the domain
     */
    checkValueAgainstDomain(v) {
        const sDomain = param_domain_1.ParamDomainValue[this._domain.domain];
        switch (this._domain.domain) {
            case param_domain_1.ParamDomainValue.ANY:
                break;
            case param_domain_1.ParamDomainValue.POS:
                if (v <= 0) {
                    const f = new message_1.Message(message_1.MessageCode.ERROR_PARAMDEF_VALUE_POS);
                    f.extraVar.symbol = this.symbol;
                    f.extraVar.value = v;
                    throw f;
                }
                break;
            case param_domain_1.ParamDomainValue.POS_NULL:
                if (v < 0) {
                    const f = new message_1.Message(message_1.MessageCode.ERROR_PARAMDEF_VALUE_POSNULL);
                    f.extraVar.symbol = this.symbol;
                    f.extraVar.value = v;
                    throw f;
                }
                break;
            case param_domain_1.ParamDomainValue.NOT_NULL:
                if (v === 0) {
                    const f = new message_1.Message(message_1.MessageCode.ERROR_PARAMDEF_VALUE_NULL);
                    f.extraVar.symbol = this.symbol;
                    throw f;
                }
                break;
            case param_domain_1.ParamDomainValue.INTERVAL:
                const min = this._domain.minValue;
                const max = this._domain.maxValue;
                if (v < min || v > max) {
                    const f = new message_1.Message(message_1.MessageCode.ERROR_PARAMDEF_VALUE_INTERVAL);
                    f.extraVar.symbol = this.symbol;
                    f.extraVar.value = v;
                    f.extraVar.minValue = min;
                    f.extraVar.maxValue = max;
                    throw f;
                }
                break;
            case param_domain_1.ParamDomainValue.INTEGER:
                if (!Number.isInteger(v)) {
                    const f = new message_1.Message(message_1.MessageCode.ERROR_PARAMDEF_VALUE_INTEGER);
                    f.extraVar.symbol = this.symbol;
                    f.extraVar.value = v;
                    throw f;
                }
                break;
            default:
                const e = new message_1.Message(message_1.MessageCode.ERROR_PARAMDOMAIN_INVALID);
                e.extraVar.symbol = this.symbol;
                e.extraVar.domain = sDomain;
                throw e;
        }
    }
    checkMin(min) {
        return this.isMinMaxDomainValid(min) && (min < this.max);
    }
    checkMax(max) {
        return this.isMinMaxDomainValid(max) && (this.min < max);
    }
    checkMinMaxStep(step) {
        return this.isMinMaxValid && this.stepRefValue.intervalHasValue(step);
    }
    /**
     * Return true if single value is valid regarding the domain constraints
     */
    get isValueValid() {
        try {
            const v = this.paramValues.singleValue;
            this.checkValueAgainstDomain(v);
            return true;
        }
        catch (e) {
            return false;
        }
    }
    get isMinMaxValid() {
        return this.checkMinMax(this.min, this.max);
    }
    /**
     * Return true if current value is valid regarding the range constraints : min / max / step
     */
    get isRangeValid() {
        switch (this._valueMode) {
            case param_value_mode_1.ParamValueMode.LISTE:
                return this.isListValid;
            case param_value_mode_1.ParamValueMode.MINMAX:
                return this.checkMinMaxStep(this.step);
        }
        throw new Error(`ParamDefinition.isRangeValid() : valeur ${param_value_mode_1.ParamValueMode[this._valueMode]}`
            + `de ParamValueMode non prise en compte`);
    }
    /**
     * Root method to determine if a field value is valid, regarding the model constraints.
     *
     * Does not take care of input validation (ie: valid number, not empty...) because an
     * invalid input should never be set as a value; input validation is up to the GUI.
     *
     * In LINK mode :
     *  - if target is a parameter, checks validity of the target value against the local model constraints
     *  - if target is a result :
     *    - is result is already computed, checks validity of the target result against the local model constraints
     *    - if it is not, checks the "computability" of the target Nub (ie. validity of the Nub) to allow
     *      triggering chain computation
     */
    get isValid() {
        switch (this._valueMode) {
            case param_value_mode_1.ParamValueMode.SINGLE:
                return this.isValueValid;
            case param_value_mode_1.ParamValueMode.MINMAX:
            case param_value_mode_1.ParamValueMode.LISTE:
                return this.isRangeValid;
            case param_value_mode_1.ParamValueMode.CALCUL:
                return true;
            case param_value_mode_1.ParamValueMode.LINK:
                if (!this.isReferenceDefined()) {
                    return false;
                }
                // valuesIterator covers both target param and target result cases
                const iterator = this.valuesIterator;
                if (iterator) {
                    try {
                        for (const v of iterator) {
                            this.checkValueAgainstDomain(v);
                        }
                        return true;
                    }
                    catch (e) {
                        return false;
                    }
                }
                else { // undefined iterator means target results are not computed yet
                    // check target Nub computability
                    return this.referencedValue.nub.isComputable();
                }
        }
        throw new Error(`ParamDefinition.isValid() : valeur de ParamValueMode '${param_value_mode_1.ParamValueMode[this._valueMode]}' inconnue`);
    }
    /**
     * Sets the current parameter to LINK mode, pointing to the given
     * symbol (might be a Parameter (computed or not) or an ExtraResult)
     * of the given Nub
     *
     * Prefer @see defineReferenceFromLinkedValue() whenever possible
     *
     * @param nub
     * @param symbol
     */
    defineReference(nub, symbol) {
        // prevent loops - beware that all Nubs must be registered in the
        // current session or this test will always fail
        if (!this.isAcceptableReference(nub, symbol)) {
            throw new Error(`defineReference() : link target ${nub.uid}.${symbol} is not an available linkable value`);
        }
        // clear current reference
        this.undefineReference();
        // find what the symbol points to
        if (nub) {
            // 1. extra result ?
            // - start here to avoid extra results being presented as
            // parameters by the iterator internally used by nub.getParameter()
            // - extra results with no family are linkable only if linking parameter family is ANY
            if (Object.keys(nub.resultsFamilies).includes(symbol)
                && (nub.resultsFamilies[symbol] !== undefined
                    || this._family === ParamFamily.ANY)) {
                this._referencedValue = new linked_value_1.LinkedValue(nub, undefined, symbol);
            }
            else {
                // 2. is it a parameter (possibly in CALC mode) ?
                const p = nub.getParameter(symbol);
                if (p) {
                    this._referencedValue = new linked_value_1.LinkedValue(nub, p, symbol);
                }
            }
        }
        if (this._referencedValue) {
            // set value mode
            this.valueMode = param_value_mode_1.ParamValueMode.LINK;
        }
        else {
            throw new Error(`defineReference - could not find target for ${nub.uid}.${symbol}`);
        }
    }
    /**
     * Asks the Session for available linkabke values for the current parameter,
     * and returns true if given { nub / symbol } pair is part of them
     */
    isAcceptableReference(nub, symbol) {
        const linkableValues = index_1.Session.getInstance().getLinkableValues(this);
        for (const lv of linkableValues) {
            if (lv.nub.uid === nub.uid && lv.symbol === symbol) {
                return true;
            }
        }
        return false;
    }
    defineReferenceFromLinkedValue(target) {
        this._referencedValue = target;
        this.valueMode = param_value_mode_1.ParamValueMode.LINK;
    }
    undefineReference() {
        this._referencedValue = undefined;
    }
    isReferenceDefined() {
        return (this._referencedValue !== undefined);
    }
    /**
     * Returns an object representation of the Parameter's current state
     * @param nubUidsInSession UIDs of Nubs that will be saved in session along with this one;
     *        useful to determine if linked parameters must be kept as links or have their value
     *        copied (if target is not in UIDs list); if undefined, wil consider all Nubs as
     *        available (ie. will not break links)
     */
    objectRepresentation(nubUidsInSession) {
        // parameter representation
        const paramRep = {
            symbol: this.symbol,
            mode: param_value_mode_1.ParamValueMode[this._valueMode]
        };
        // adjust parameter representation depending on value mode
        switch (this._valueMode) {
            case param_value_mode_1.ParamValueMode.SINGLE:
                paramRep.value = this.singleValue;
                break;
            case param_value_mode_1.ParamValueMode.CALCUL:
                // save initial value if param is calculated using dichotomy
                if (this.calculability === ParamCalculability.DICHO) {
                    paramRep.value = this.singleValue;
                }
                break;
            case param_value_mode_1.ParamValueMode.MINMAX:
                paramRep.min = this.min;
                paramRep.max = this.max;
                paramRep.step = this.step;
                paramRep.extensionStrategy = this.extensionStrategy;
                break;
            case param_value_mode_1.ParamValueMode.LISTE:
                paramRep.values = this.valueList;
                paramRep.extensionStrategy = this.extensionStrategy;
                break;
            case param_value_mode_1.ParamValueMode.LINK:
                if (nubUidsInSession === undefined || nubUidsInSession.includes(this._referencedValue.nub.uid)) {
                    // target Nub is available in session, link won't get broken
                    paramRep.targetNub = this._referencedValue.nub.uid;
                    paramRep.targetParam = this._referencedValue.symbol;
                }
                else {
                    // target Nub will be lost, copy value(s) to keep consistency
                    const targetPV = this._referencedValue.getParamValues(true);
                    paramRep.mode = param_value_mode_1.ParamValueMode[targetPV.valueMode];
                    switch (targetPV.valueMode) {
                        case param_value_mode_1.ParamValueMode.SINGLE:
                            paramRep.value = targetPV.singleValue;
                            break;
                        case param_value_mode_1.ParamValueMode.MINMAX:
                            paramRep.min = targetPV.min;
                            paramRep.max = targetPV.max;
                            paramRep.step = targetPV.step;
                            paramRep.extensionStrategy = targetPV.extensionStrategy;
                            break;
                        case param_value_mode_1.ParamValueMode.LISTE:
                            paramRep.values = targetPV.valueList;
                            paramRep.extensionStrategy = targetPV.extensionStrategy;
                            break;
                        // should never be in LINK or CALC mode (see LinkedValue methods)
                    }
                }
                break;
        }
        return paramRep;
    }
    /**
     * Fills the current Parameter, provided an object representation
     * @param obj object representation of a Parameter state
     * @returns object {
     *      calculated: boolean true if loaded parameter is in CALC mode
     *      hasErrors: boolean true if errors were encountered during loading
     * }
     */
    loadObjectRepresentation(obj, setCalcMode = true) {
        const ret = {
            calculated: false,
            hasErrors: false
        };
        // set mode
        const mode = param_value_mode_1.ParamValueMode[obj.mode]; // get enum index for string value
        // when loading parent Nub, setting mode to CALC would prevent ensuring
        // consistency when setting calculatedParam afterwards
        if (mode !== param_value_mode_1.ParamValueMode.CALCUL || setCalcMode) {
            try {
                this.valueMode = mode;
            }
            catch (err) {
                // silent fail : impossible to determine if this is an error, because
                // at this time, it is possible that no candidate for calculatedParam can
                // be found, since Nub children are not loaded yet (see nghyd#263)
                /* ret.hasErrors = true;
                console.error("loadObjectRepresentation: set valueMode error"); */
            }
        }
        // set value(s)
        switch (mode) {
            case param_value_mode_1.ParamValueMode.SINGLE:
                this.singleValue = obj.value;
                break;
            case param_value_mode_1.ParamValueMode.MINMAX:
                this.min = obj.min;
                this.max = obj.max;
                this.step = obj.step;
                if (obj.extensionStrategy !== undefined) {
                    this.extensionStrategy = obj.extensionStrategy;
                }
                break;
            case param_value_mode_1.ParamValueMode.LISTE:
                this.valueList = obj.values;
                if (obj.extensionStrategy !== undefined) {
                    this.extensionStrategy = obj.extensionStrategy;
                }
                break;
            case param_value_mode_1.ParamValueMode.CALCUL:
                // although calculated param is set at Nub level (see below),
                // it is detected as "the only parameter in CALC mode"
                if (obj.value !== undefined) { // dichotomy params have an initial value, others don't
                    this.singleValue = obj.value;
                }
                ret.calculated = true;
                break;
            case param_value_mode_1.ParamValueMode.LINK:
                // formulaire dont le Nub est la cible du lien
                const destNub = index_1.Session.getInstance().findNubByUid(obj.targetNub);
                if (destNub) {
                    try {
                        this.defineReference(destNub, obj.targetParam);
                    }
                    catch (err) {
                        // silent fail : impossible to determine if this is an error, because
                        // fixLinks() might solve it later
                    }
                } // si la cible du lien n'existe pas, Session.fixLinks() est censé s'en occuper
                break;
            default:
                throw new Error(`session file : invalid value mode '${obj.valueMode}' in param object ${obj.symbol}`);
        }
        return ret;
    }
    /**
     * Returns true if both the Nub UID and the symbol are identical
     */
    equals(p) {
        return (this.nubUid === p.nubUid
            && this.symbol === p.symbol);
    }
    /**
     * Returns true if this parameter's value can safely be linked to the
     * given parameter "src" (ie. without leading to circular dependencies)
     */
    isLinkableTo(src) {
        // did we loop back to the candidate parameter ?
        if (this.equals(src)) {
            // prevent infinite recursion
            return false;
        }
        if (this._valueMode === param_value_mode_1.ParamValueMode.CALCUL) {
            // if my Nub doesn't depend on your result,
            // and my Nub doesn't already have a parameter linked to you,
            // you may depend on its result (me) !
            if (this._parent && this._parent.parent) {
                const myNub = this.parentComputeNode;
                const ok = (!myNub.dependsOnNubResult(src.parentNub)
                    && !myNub.dependsOnParameter(src));
                return ok;
            }
        }
        if (this._valueMode === param_value_mode_1.ParamValueMode.LINK && this.isReferenceDefined()) {
            // does the link point to an extra result ?
            if (this.referencedValue.isExtraResult()) {
                // if the target Nub doesn't depend on your result, you may depend on its extra result (me) !
                return (!this.referencedValue.nub.dependsOnNubResult(src.parentNub));
            }
            else { // the link points to a parameter, computed or not
                // recursively follow links, unless it loops back to the original parameter
                return this.referencedValue.element.isLinkableTo(src);
            }
        }
        // SINGLE / MINMAX / LISTE parameters can always be linked without danger
        return true;
    }
    /**
     * Returns true if this parameter is ultimately (after followink links chain) linked
     * to a result or extra result of any of the given nubs.
     * If followLinksChain is false, will limit theexploration to the first target level only
     */
    isLinkedToResultOfNubs(nubs, followLinksChain = true) {
        let linked = false;
        if (this._valueMode === param_value_mode_1.ParamValueMode.LINK && this.isReferenceDefined()) {
            const targetNubUid = this.referencedValue.nub.uid;
            // is target a result ?
            if (this.referencedValue.isResult() || this.referencedValue.isExtraResult()) {
                for (const y of nubs) {
                    if (y.uid === targetNubUid) {
                        // dependence found !
                        linked = true;
                    }
                    else if (this.referencedValue.nub.dependsOnNubResult(y)) {
                        // indirect result dependence found !
                        linked = true;
                    }
                }
            }
            else { // target is a parameter
                // recursion ?
                if (followLinksChain) {
                    linked = this.referencedValue.element.isLinkedToResultOfNubs(nubs);
                }
            }
        }
        return linked;
    }
    /**
     * Returns true if the current parameter is directly linked to the
     * Nub having UID "uid", its parent or any of its children
     * @param uid
     * @param symbol symbol of the target parameter whose value change triggered this method;
     *      if current Parameter targets this symbol, Nub will be considered dependent
     * @param includeValuesLinks if true, even if this Parameter targets a non-calculated non-modified
     *      parameter, Nub will be considered dependent @see jalhyd#98
     */
    dependsOnNubFamily(uid, symbol, includeValuesLinks = false) {
        let linked = false;
        if (this._valueMode === param_value_mode_1.ParamValueMode.LINK && this.isReferenceDefined()) {
            const ref = this._referencedValue;
            // direct, parent or children reference ?
            if ((ref.nub.uid === uid)
                || (ref.nub.getParent() && ref.nub.getParent().uid === uid)
                || (ref.nub.getChildren().map((c) => c.uid).includes(uid))) {
                linked = ((symbol !== undefined && symbol === ref.symbol)
                    || ref.isCalculated()
                    || includeValuesLinks);
            } // else no recursion, checking level 1 only
        }
        return linked;
    }
    // interface IterableValues
    /**
     * Transparent proxy to own values iterator or targetted values iterator (in LINK mode)
     */
    get valuesIterator() {
        if (this.valueMode === param_value_mode_1.ParamValueMode.LINK && this.isReferenceDefined()) {
            try {
                return this._referencedValue.getParamValues().valuesIterator;
            }
            catch (e) {
                // values are not computed yet
                return undefined;
            }
        }
        else {
            return this.paramValues.valuesIterator;
        }
    }
    getExtendedValuesIterator(size) {
        return this.paramValues.getValuesIterator(false, size, true);
    }
    /**
     * Returns true if there are more than 1 value associated to this parameter;
     * might be its own values (MINMAX / LISTE mode), or targetted values (LINK mode)
     */
    get hasMultipleValues() {
        if (this._valueMode === param_value_mode_1.ParamValueMode.LINK && this.isReferenceDefined()) {
            return this._referencedValue.hasMultipleValues();
        }
        else {
            if (this._valueMode === param_value_mode_1.ParamValueMode.CALCUL) {
                return this.parentNub.resultHasMultipleValues();
            }
            else {
                return this.paramValues.hasMultipleValues;
            }
        }
    }
    initValuesIterator(reverse = false) {
        return this.paramValues.getValuesIterator(reverse, undefined, true);
    }
    get hasNext() {
        return this.paramValues.hasNext;
    }
    next() {
        return this.paramValues.next();
    }
    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    nextValue() {
        return this.next();
    }
    [Symbol.iterator]() {
        return this.paramValues;
    }
    // interface IObservable
    /**
     * ajoute un observateur à la liste
     */
    addObserver(o) {
        this._observable.addObserver(o);
    }
    /**
     * supprime un observateur de la liste
     */
    removeObserver(o) {
        this._observable.removeObserver(o);
    }
    /**
     * notifie un événement aux observateurs
     */
    notifyObservers(data, sender) {
        this._observable.notifyObservers(data, sender);
    }
    /**
     * variable calculable par l'équation ?
     */
    isAnalytical() {
        return this.calculability === ParamCalculability.EQUATION;
    }
    /**
     * Get the highest nub in the child nub hierarchy
     */
    get originNub() {
        let originNub = this.parentNub;
        while (true) {
            if (originNub.parent === undefined) {
                return originNub;
            }
            originNub = originNub.parent;
        }
    }
    /**
     * Renvoie la valeur actuelle du paramètre (valeur courante ou résultat si calculé)
     */
    get V() {
        // paramètre en calcul ou résultat complémentaire
        if (this.parentNub.result !== undefined) {
            if (this.parentNub.result.resultElement.ok) {
                if (this.parentNub.result.resultElement.values[this.symbol] !== undefined) {
                    return this.parentNub.result.resultElement.values[this.symbol];
                }
            }
        }
        // sinon, valeur courante
        return this.currentValue;
    }
    clone() {
        const res = new ParamDefinition(this._parent, this._symbol, this.domain.clone(), this._unit, this.singleValue, this._family, this.visible);
        res._calc = this._calc;
        return res;
    }
    /**
     * notification envoyée après la modification de la valeur du paramètre
     */
    notifyValueModified(sender) {
        this.notifyObservers({ action: "paramdefinitionAfterValue" }, sender);
    }
    /**
     * vérifie si un min/max est valide par rapport au domaine de définition
     */
    isMinMaxDomainValid(v) {
        if (v === undefined) {
            return false;
        }
        if (this._valueMode === param_value_mode_1.ParamValueMode.MINMAX) {
            try {
                this.checkValueAgainstDomain(v);
            }
            catch (e) {
                return false;
            }
        }
        return true;
    }
    checkMinMax(min, max) {
        return this.isMinMaxDomainValid(min) && this.isMinMaxDomainValid(max) && (min < max);
    }
    get isListValid() {
        if (this.paramValues.valueList === undefined) {
            return false;
        }
        for (const v of this.paramValues.valueList) {
            try {
                this.checkValueAgainstDomain(v);
            }
            catch (e) {
                return false;
            }
        }
        return true;
    }
    /**
     * Throws an error if this.paramValues._valueMode is not the expected value mode.
     * In LINK mode, the tested valueMode is the mode of the ultimately targetted
     * set of values (may never be LINK)
     */
    checkValueMode(expected) {
        if (!expected.includes(this.valueMode)) {
            throw new Error(`ParamDefinition : mode de valeurs ${param_value_mode_1.ParamValueMode[this.valueMode]} incorrect pour le paramètre ${this.symbol}`);
        }
    }
}
exports.ParamDefinition = ParamDefinition;

},{"../index":14,"../linked-value":17,"../nub":33,"../open-channel/section/section_type":58,"../util/interval":148,"../util/message":151,"../util/observer":154,"./param-domain":87,"./param-value-mode":89,"./param-values":90}],87:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamDomain = exports.ParamDomainValue = void 0;
const interval_1 = require("../util/interval");
const message_1 = require("../util/message");
/**
 * domaine de définition du paramètre
 */
var ParamDomainValue;
(function (ParamDomainValue) {
    /** >0, =0, <0 (-inf -> +inf) */
    ParamDomainValue[ParamDomainValue["ANY"] = 0] = "ANY";
    /** >=0 */
    ParamDomainValue[ParamDomainValue["POS_NULL"] = 1] = "POS_NULL";
    /** > 0 */
    ParamDomainValue[ParamDomainValue["POS"] = 2] = "POS";
    /** <>0 */
    ParamDomainValue[ParamDomainValue["NOT_NULL"] = 3] = "NOT_NULL";
    /** intervalle */
    ParamDomainValue[ParamDomainValue["INTERVAL"] = 4] = "INTERVAL";
    /** nombre entier relatif */
    ParamDomainValue[ParamDomainValue["INTEGER"] = 5] = "INTEGER";
})(ParamDomainValue = exports.ParamDomainValue || (exports.ParamDomainValue = {}));
class ParamDomain {
    constructor(d, min, max) {
        this.checkValue(d, min, max);
        this._domain = d;
        switch (this._domain) {
            case ParamDomainValue.INTERVAL:
                this._minValue = min;
                this._maxValue = max;
                break;
            default:
                const b = ParamDomain.getDefaultBounds(this._domain);
                this._minValue = b.min;
                this._maxValue = b.max;
                break;
        }
    }
    static getDefaultBounds(d) {
        switch (d) {
            case ParamDomainValue.INTERVAL:
                const e = new message_1.Message(message_1.MessageCode.ERROR_PARAMDOMAIN_INVALID);
                throw e;
            case ParamDomainValue.ANY:
            case ParamDomainValue.NOT_NULL:
            case ParamDomainValue.INTEGER:
                return { min: -Infinity, max: Infinity };
            case ParamDomainValue.POS:
                return { min: 1e-9, max: Infinity };
            case ParamDomainValue.POS_NULL:
                return { min: 0, max: Infinity };
            // default:
            //     throw "valeur de ParamDomainValue" + ParamDomainValue[d] + " non prise en charge";
        }
    }
    get domain() {
        return this._domain;
    }
    get minValue() {
        return this._minValue;
    }
    get maxValue() {
        return this._maxValue;
    }
    get interval() {
        switch (this._domain) {
            case ParamDomainValue.INTERVAL:
                return new interval_1.Interval(this._minValue, this._maxValue);
            default:
                const b = ParamDomain.getDefaultBounds(this._domain);
                return new interval_1.Interval(b.min, b.max);
        }
    }
    clone() {
        switch (this._domain) {
            case ParamDomainValue.INTERVAL:
                return new ParamDomain(this._domain, this._minValue, this._maxValue);
            default:
                return new ParamDomain(this._domain);
        }
    }
    /**
     * Return a value bounded by the definition domain
     * @param v the value to be bounded
     */
    getBoundedValue(v) {
        return Math.min(Math.max(v, this.minValue), this.maxValue);
    }
    checkValue(val, min, max) {
        switch (val) {
            case ParamDomainValue.INTERVAL:
                if (min === undefined || max === undefined || min > max) {
                    const e = new message_1.Message(message_1.MessageCode.ERROR_PARAMDOMAIN_INTERVAL_BOUNDS);
                    e.extraVar.minValue = min;
                    e.extraVar.maxValue = max;
                    throw e;
                }
                break;
            default:
                // en dehors du cas INTERVAL, on ne doit pas fournir de valeur
                if (min !== undefined || max !== undefined) {
                    const e = new message_1.Message(message_1.MessageCode.ERROR_PARAMDOMAIN_INTERVAL_BOUNDS);
                    e.extraVar.minValue = min;
                    e.extraVar.maxValue = max;
                    throw e;
                }
                break;
        }
    }
}
exports.ParamDomain = ParamDomain;

},{"../util/interval":148,"../util/message":151}],88:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamValueIterator = void 0;
const base_1 = require("../base");
const param_definition_1 = require("./param-definition");
const param_value_mode_1 = require("./param-value-mode");
/**
 * itérateur sur les (ou la) valeurs prises par un ParamValues
 */
class ParamValueIterator {
    constructor(prm, reverse = false, extendTo, addLastStep = false) {
        prm.check();
        this._param = prm;
        this.reset(reverse, extendTo, addLastStep);
    }
    get currentValue() {
        return this._current;
    }
    /**
     * Returns the number of elements in the iterator without altering it,
     * by counting a copy of the iterator
     */
    count() {
        const itCopy = new ParamValueIterator(this._param, this._reverse, this._extendTo, this._addLastStep);
        let length = 0;
        for (const i of itCopy) {
            length++;
        }
        return length;
    }
    /**
     * Reinits the value iterator
     * @param reverse if true, will iterate starting at the end
     * @param extendTo if defined, will extend values list until this boundary
     * @param addLastStep if true, if (max - min) is not a multiple of step, one more
     *                    iteration will be done return last value (max)
     */
    reset(reverse, extendTo, addLastStep = false) {
        this._reverse = reverse;
        this._extendTo = extendTo;
        this._addLastStep = addLastStep;
        this._xindex = 0;
        this._minMaxLastValueReached = false;
        switch (this._param.valueMode) {
            case param_value_mode_1.ParamValueMode.SINGLE:
                this._index = 0;
                break;
            case param_value_mode_1.ParamValueMode.MINMAX:
                if (reverse) {
                    this._index = this._param.max;
                }
                else {
                    this._index = this._param.min;
                }
                break;
            case param_value_mode_1.ParamValueMode.LISTE:
                this._index = 0;
                break;
            default:
                // tslint:disable-next-line:max-line-length
                throw new Error(`ParamValueIterator : mode ${param_value_mode_1.ParamValueMode[this._param.valueMode]} incorrect`);
        }
    }
    /**
     * Same as next(), but sets _currentValue on attached ParamValues; to be
     * used with dedicated iterators, for params having multiple links on them
     * @see jalhyd#222
     */
    nextValue() {
        const res = this.next();
        if (!res.done) {
            this._param.setCurrentValueFromIterator(res.value);
        }
        return res;
    }
    next() {
        if (this.hasNext) {
            switch (this._param.valueMode) {
                case param_value_mode_1.ParamValueMode.SINGLE:
                    if (this.hasNext) {
                        this._current = this._param.singleValue;
                        this._index++;
                        this._xindex++;
                        return {
                            done: false,
                            value: this._current
                        };
                    }
                    else {
                        return {
                            done: true,
                            value: undefined
                        };
                    }
                case param_value_mode_1.ParamValueMode.LISTE:
                    if (this.hasNextWithoutExtension) { // default case
                        this._current = this._param.valueList[this._index++]; // what about _reverse ?
                        this._xindex++; // count values for possible extension
                        return {
                            done: false,
                            value: this._current
                        };
                    }
                    else { // no more real values
                        if (this._extendTo && this.hasNext) {
                            // extend
                            this._index++;
                            switch (this._param.extensionStrategy) {
                                case param_definition_1.ExtensionStrategy.REPEAT_LAST:
                                    // repeat last real value (do not change this._current)
                                    return {
                                        done: false,
                                        value: this._current
                                    };
                                case param_definition_1.ExtensionStrategy.RECYCLE:
                                    // loop over real values
                                    if (this._xindex === undefined
                                        || this._xindex === this._param.valueList.length) {
                                        this._xindex = 0;
                                    }
                                    this._current = this._param.valueList[this._xindex++];
                                    return {
                                        done: false,
                                        value: this._current
                                    };
                                default:
                                    throw new Error(`ParamValueIterator.next() : no extension strategy (LISTE)`);
                            }
                        }
                        else {
                            return {
                                done: true,
                                value: undefined
                            };
                        }
                    }
                case param_value_mode_1.ParamValueMode.MINMAX:
                    // protection against infinite loops
                    if (this._param.step === 0) {
                        this._index = this._param.max;
                        return {
                            done: true,
                            value: this._current
                        };
                    }
                    // s'il reste des valeurs normales (sans extension, mais en comptant le lastStep)
                    if (this.hasNextWithoutExtension) {
                        // si on a atteint la dernière valeur sans compter le dernier pas
                        if (this._addLastStep && this.minMaxLastAlignedValueReached()) {
                            // on ajoute la dernière valeur
                            this._current = this._reverse ? this._param.min : this._param.max;
                        }
                        else {
                            // on ajoute / retranche un pas
                            this._current = this._index;
                            if (this._reverse) {
                                this._index -= this._param.step;
                            }
                            else {
                                this._index += this._param.step;
                            }
                        }
                        this._xindex++; // count values for possible extension
                        return {
                            done: false,
                            value: this._current
                        };
                    }
                    else { // no more real values
                        if (this._extendTo && this.hasNext) {
                            this._xindex++; // count values for possible extension
                            switch (this._param.extensionStrategy) {
                                case param_definition_1.ExtensionStrategy.REPEAT_LAST:
                                    // repeat last real value (do not change this._current)
                                    return {
                                        done: false,
                                        value: this._current
                                    };
                                case param_definition_1.ExtensionStrategy.RECYCLE:
                                    // loop over real values using a local iterator that does not extend
                                    if (this._locExIt === undefined || !this._locExIt.hasNext) {
                                        // create / rewind iterator
                                        this._locExIt = this._param.getValuesIterator(this._reverse, undefined, this._addLastStep);
                                    }
                                    this._current = this._locExIt.next().value;
                                    return {
                                        done: false,
                                        value: this._current
                                    };
                                default:
                                    throw new Error(`ParamValueIterator.next() : no extension strategy (MINMAX)`);
                            }
                        }
                        else { // end of iterator
                            return {
                                done: true,
                                value: undefined
                            };
                        }
                    }
                default:
                    throw new Error(`ParamValueIterator.next() : internal error`);
            }
        }
        else { // end of iterator
            return {
                done: true,
                value: undefined
            };
        }
    }
    // interface IterableIterator
    [Symbol.iterator]() {
        return this;
    }
    get hasNext() {
        return this.hasNextValue();
    }
    get hasNextWithoutExtension() {
        return this.hasNextValue(true);
    }
    /**
     * Returns true if iterator has at least one more value
     * @param ignoreExtension if true, will consider only real values (those that were
     *      set up), including the possible extraStep, and ignore extended values
     */
    hasNextValue(ignoreExtension = false) {
        switch (this._param.valueMode) {
            case param_value_mode_1.ParamValueMode.SINGLE:
                return this._index === 0;
            case param_value_mode_1.ParamValueMode.LISTE:
                if (this._extendTo && !ignoreExtension) {
                    return this._index < this._extendTo;
                }
                else {
                    return this._index < this._param.valueList.length;
                }
            case param_value_mode_1.ParamValueMode.MINMAX:
                // si extension, compter le nombre de valeurs
                if (this._extendTo && !ignoreExtension) {
                    return this._xindex < this._extendTo;
                }
                else {
                    // sinon, s'il y a un dernier pas à faire
                    return !this.minMaxLastValueReached();
                }
            default:
                throw new Error(`ParamValueIterator.hasNext() : internal error`);
        }
    }
    /**
     * Returns true if last value that is a multiple of step was reached;
     * it might still miss the lastStep if required
     */
    minMaxLastAlignedValueReached() {
        return this._reverse ?
            this._index < this._param.min - this._param.step * 1E-7 :
            this._index > this._param.max + this._param.step * 1E-7;
    }
    /**
     * Returns true if last value was reached in MINMAX mode
     */
    minMaxLastValueReached() {
        if (!this._minMaxLastValueReached) {
            // update flag
            if (this._addLastStep) {
                this._minMaxLastValueReached = this._reverse ?
                    base_1.isEqual(this._current, this._param.min, this._param.step * 1E-7) :
                    base_1.isEqual(this._current, this._param.max, this._param.step * 1E-7);
            }
            else {
                this._minMaxLastValueReached = this.minMaxLastAlignedValueReached();
            }
        }
        // return stored flag in any case
        return this._minMaxLastValueReached;
    }
}
exports.ParamValueIterator = ParamValueIterator;

},{"../base":3,"./param-definition":86,"./param-value-mode":89}],89:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamValueMode = void 0;
/**
 * mode de génération des valeurs d'un paramètre
 */
var ParamValueMode;
(function (ParamValueMode) {
    /**
     * valeur unique
     */
    ParamValueMode[ParamValueMode["SINGLE"] = 0] = "SINGLE";
    /**
     * min, max, pas
     */
    ParamValueMode[ParamValueMode["MINMAX"] = 1] = "MINMAX";
    /**
     * liste de valeurs discrètes
     */
    ParamValueMode[ParamValueMode["LISTE"] = 2] = "LISTE";
    /**
     * la valeur du paramètre est non définie et à calculer
     */
    ParamValueMode[ParamValueMode["CALCUL"] = 3] = "CALCUL";
    /**
     * la valeur du paramètre est liée à celle d'un paramètre, d'un résultat, ...
     */
    ParamValueMode[ParamValueMode["LINK"] = 4] = "LINK";
})(ParamValueMode = exports.ParamValueMode || (exports.ParamValueMode = {}));

},{}],90:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamValues = void 0;
const param_value_iterator_1 = require("./param-value-iterator");
const param_value_mode_1 = require("./param-value-mode");
/**
 * Represents the value(s) taken by a Parameter, along with and depending on
 * the value mode; linked values are not managed here, only the LINK value
 * mode is defined here
 */
class ParamValues {
    get singleValue() {
        return this._singleValue;
    }
    set singleValue(v) {
        this._singleValue = v;
        this._currentValue = v;
    }
    get currentValue() {
        return this._currentValue;
    }
    count() {
        return this._iterator.count();
    }
    setValues(o, max, step) {
        if (typeof (o) === "number") {
            if (max === undefined) {
                this.valueMode = param_value_mode_1.ParamValueMode.SINGLE;
                this.singleValue = o;
            }
            else {
                this.valueMode = param_value_mode_1.ParamValueMode.MINMAX;
                this.min = o;
                this.max = max;
                this.step = step;
            }
        }
        else if (Array.isArray(o)) {
            this.valueMode = param_value_mode_1.ParamValueMode.LISTE;
            this.valueList = o;
        }
        else {
            throw new Error(`ParamValues.setValues() :  appel invalide`);
        }
    }
    check() {
        switch (this.valueMode) {
            case param_value_mode_1.ParamValueMode.SINGLE:
                if (this.singleValue === undefined) {
                    throw new Error(`ParamValues : valeur fixe non définie`);
                }
                break;
            case param_value_mode_1.ParamValueMode.MINMAX:
                if (this.min === undefined) {
                    throw new Error(`ParamValues : valeur min non définie`);
                }
                if (this.max === undefined) {
                    throw new Error(`ParamValues : valeur max non définie`);
                }
                if (this.step === undefined) {
                    throw new Error(`ParamValues : valeur du pas non définie`);
                }
                if (this.min > this.max) {
                    throw new Error(`ParamValues : min > max`);
                }
                break;
            case param_value_mode_1.ParamValueMode.LISTE:
                if (this.valueList === undefined) {
                    throw new Error(`ParamValues : liste de valeurs non définie`);
                }
                break;
            case param_value_mode_1.ParamValueMode.LINK:
            case param_value_mode_1.ParamValueMode.CALCUL:
            default:
                break;
        }
    }
    /**
     * Returns values as a number list by running through the iterator,
     * taking in account reverse, extendTo and addLastStep if defined
     */
    getInferredValuesList(reverse = false, extendTo, addLastStep = false) {
        if ([param_value_mode_1.ParamValueMode.MINMAX, param_value_mode_1.ParamValueMode.LISTE].includes(this.valueMode)) {
            if ((this.valueMode === param_value_mode_1.ParamValueMode.LISTE)
                || (this.valueMode === param_value_mode_1.ParamValueMode.MINMAX
                    // protection against infinite loops
                    && this.step !== undefined && this.step > 0
                    && this.min !== undefined && this.min !== null
                    && this.max !== undefined && this.max !== null)) {
                const it = this.initValuesIterator(reverse, extendTo, addLastStep);
                const values = [];
                for (const v of it) {
                    values.push(v);
                }
                return values;
            }
            else {
                return [];
            }
        }
        throw new Error("ParamValues.getInferredValuesList() : incorrect value mode" + param_value_mode_1.ParamValueMode[this.valueMode]);
    }
    // -- iterator-related methods
    /**
     * Returns a ParamValueIterator over the current values
     * @param reverse if true, will iterate starting at the end
     * @param extendTo if defined, will extend values list until this boundary
     * @param addLastStep if true, if (max - min) is not a multiple of step, one more
     *                    iteration will be done return last value (max)
     */
    getValuesIterator(reverse = false, extendTo, addLastStep = false) {
        return new param_value_iterator_1.ParamValueIterator(this, reverse, extendTo, addLastStep);
    }
    // interface IterableValues
    get valuesIterator() {
        return this.getValuesIterator(false, undefined, true);
    }
    get hasMultipleValues() {
        try {
            // will throw an error if no value is defined at all
            this.check();
        }
        catch (e) {
            return false;
        }
        const it = this.getValuesIterator();
        if (it) {
            let n = 0;
            for (const v of it) {
                n++;
                if (n > 1) {
                    break;
                }
            }
            return n > 1;
        }
        else {
            return false;
        }
    }
    /**
     * Checks that values are in LIST or MINMAX mode, then retrieves an iterator
     * @param reverse if true, will iterate starting at the end
     * @param extendTo if defined, will extend values list until this boundary
     * @param addLastStep if true, if (max - min) is not a multiple of step, one more
     *                    iteration will be done return last value (max)
     */
    initValuesIterator(reverse = false, extendTo, addLastStep = false) {
        switch (this.valueMode) {
            case param_value_mode_1.ParamValueMode.LISTE:
            case param_value_mode_1.ParamValueMode.MINMAX:
                this._iterator = this.getValuesIterator(reverse, extendTo, addLastStep);
                break;
            default:
                throw new Error(`ParamValues : mode de valeurs ${param_value_mode_1.ParamValueMode[this.valueMode]} incorrect`);
        }
        return this._iterator;
    }
    /**
     * @return true si il reste des valeurs à parcourir par l'itérateur courant
     */
    get hasNext() {
        return this._iterator.hasNext;
    }
    /**
     * fixe la valeur courante à la prochaine valeur à parcourir par l'itérateur courant
     * @return prochaine valeur à parcourir par l'itérateur courant
     */
    next() {
        let res;
        res = this._iterator.next();
        if (!res.done) {
            this._currentValue = res.value;
        }
        return res;
    }
    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    nextValue() {
        return this.next();
    }
    /**
     * Trick method to explicitely set _currentValue when using a dedicated iterator, for
     * params having multiple links on them @see jalhyd#222 ; usually calling .next() on
     * ParamDefinition sets _currentValue, but not when a dedicated iterator is used; this
     * method should only be called by ParamValueIterator.nextValue()
     */
    setCurrentValueFromIterator(v) {
        this._currentValue = v;
    }
    [Symbol.iterator]() {
        return this;
    }
}
exports.ParamValues = ParamValues;

},{"./param-value-iterator":88,"./param-value-mode":89}],91:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamDefinitionIterator = void 0;
const map_iterator_1 = require("../util/map_iterator");
/**
 * itérateur sur les paramètres d'une seule instance de ParamsEquation
 */
class ParamDefinitionIterator {
    constructor(_params) {
        this._mapIterator = new map_iterator_1.MapIterator(_params.map);
    }
    next() {
        return this._mapIterator.next();
    }
    [Symbol.iterator]() {
        return this;
    }
}
exports.ParamDefinitionIterator = ParamDefinitionIterator;

},{"../util/map_iterator":150}],92:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamsEquation = void 0;
const param_definition_iterator_1 = require("./param_definition_iterator");
/**
 * liste des paramètres d'une équation
 */
class ParamsEquation {
    constructor(parent) {
        this._paramMap = {};
        this.parent = parent;
    }
    get nubUid() {
        if (!this.parent) {
            throw new Error("ParamsEquation.nubUid : equation has no parent Nub !");
        }
        return this.parent.uid;
    }
    hasParameter(name) {
        for (const p of this) {
            if (p.symbol === name) {
                return true;
            }
        }
        return false;
    }
    get map() {
        return this._paramMap;
    }
    /**
     * Retrieves a parameter from its symbol
     */
    get(name) {
        for (const s in this._paramMap) {
            if (s === name) {
                return this._paramMap[s];
            }
        }
        return undefined;
    }
    checkParametersCalculability() {
        const res = [];
        for (const p of this) {
            if (p.calculability === undefined) {
                res.push(p.symbol);
            }
        }
        if (res.length > 0) {
            throw new Error("Calculability of parameter(s) " + res.toString() + " has not been defined");
        }
    }
    [Symbol.iterator]() {
        return this.iterator;
    }
    get iterator() {
        return new param_definition_iterator_1.ParamDefinitionIterator(this);
    }
    addParamDefinition(p, force = false) {
        if (force || !this.hasParameter(p.symbol)) {
            this._paramMap[p.symbol] = p;
        }
    }
    addParamDefinitions(ps) {
        for (const p of ps) {
            // Force update of the map index
            this.addParamDefinition(p, true);
        }
    }
}
exports.ParamsEquation = ParamsEquation;

},{"./param_definition_iterator":91}],93:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParamsEquationArrayIterator = void 0;
const map_iterator_1 = require("../util/map_iterator");
/**
 * itérateur sur les paramètres d'un tableau de de ParamsEquation
 */
class ParamsEquationArrayIterator {
    constructor(p) {
        this._index = 0;
        this._paramsEqs = p;
    }
    get done() {
        return {
            done: true,
            value: undefined
        };
    }
    next() {
        if (this._currentMapIterator === undefined) {
            this.nextIterator();
        }
        let res = this.done;
        if (this._currentMapIterator) {
            res = this._currentMapIterator.next();
            if (res.done) {
                this.nextIterator();
                if (this._currentMapIterator) {
                    res = this._currentMapIterator.next();
                }
            }
        }
        return res;
    }
    [Symbol.iterator]() {
        return this;
    }
    nextIterator() {
        if (this._index < this._paramsEqs.length) {
            this._currentMapIterator = new map_iterator_1.MapIterator(this._paramsEqs[this._index++].map);
        }
        else {
            this._currentMapIterator = undefined;
        }
    }
}
exports.ParamsEquationArrayIterator = ParamsEquationArrayIterator;

},{"../util/map_iterator":150}],94:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConduiteDistrib = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
/**
 * classe de calcul sur la conduite distributrice
 */
class ConduiteDistrib extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.ConduiteDistributrice;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /**
     * calcul direct des différents paramètres
     * @param sVarCalc nom du paramètre
     * @return valeur calculée
     */
    Equation(sVarCalc) {
        let v;
        const K = 0.3164 * Math.pow(4, 1.75) / (5.5 * 9.81 * Math.pow(3.1415, 1.75)); // Constante de la formule
        switch (sVarCalc) {
            case "J":
                v = K * Math.pow(this.prms.Nu.v, 0.25) * Math.pow(this.prms.Q.v, 1.75)
                    * this.prms.Lg.v / Math.pow(this.prms.D.v, 4.75);
                break;
            case "D":
                v = Math.pow(this.prms.J.v / (K * Math.pow(this.prms.Nu.v, 0.25)
                    * Math.pow(this.prms.Q.v, 1.75) * this.prms.Lg.v), 1 / 4.75);
                break;
            case "Q":
                v = Math.pow(this.prms.J.v / (K * Math.pow(this.prms.Nu.v, 0.25)
                    * this.prms.Lg.v / Math.pow(this.prms.D.v, 4.75)), 1 / 1.75);
                break;
            case "Lg":
                v = this.prms.J.v / (K * Math.pow(this.prms.Nu.v, 0.25)
                    * Math.pow(this.prms.Q.v, 1.75) / Math.pow(this.prms.D.v, 4.75));
                break;
            case "Nu":
                v = Math.pow(this.prms.J.v / (K * Math.pow(this.prms.Q.v, 1.75)
                    * this.prms.Lg.v / Math.pow(this.prms.D.v, 4.75)), 1 / 0.25);
                break;
            default:
                throw new Error("ConduiteDistrib.Equation() : invalid parameter name " + sVarCalc);
        }
        return new result_1.Result(v, this);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.J.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.D.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Lg.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Nu.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.ConduiteDistrib = ConduiteDistrib;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/result":155}],95:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConduiteDistribParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * paramètres pour la conduite distributrice
 */
class ConduiteDistribParams extends params_equation_1.ParamsEquation {
    constructor(rQ, rD, rJ, rLg, rNu) {
        super();
        this.Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.POS, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this.D = new param_definition_1.ParamDefinition(this, "D", param_domain_1.ParamDomainValue.POS, "m", rD, param_definition_1.ParamFamily.DIAMETERS);
        this.J = new param_definition_1.ParamDefinition(this, "J", param_domain_1.ParamDomainValue.POS, "m", rJ);
        this.Lg = new param_definition_1.ParamDefinition(this, "Lg", param_domain_1.ParamDomainValue.POS, "m", rLg, param_definition_1.ParamFamily.LENGTHS);
        this.Nu = new param_definition_1.ParamDefinition(this, "Nu", param_domain_1.ParamDomainValue.POS, "Pa·s", rNu);
        this.addParamDefinition(this.Q);
        this.addParamDefinition(this.D);
        this.addParamDefinition(this.J);
        this.addParamDefinition(this.Lg);
        this.addParamDefinition(this.Nu);
    }
}
exports.ConduiteDistribParams = ConduiteDistribParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],96:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LechaptCalmon = void 0;
const compute_node_1 = require("../compute-node");
const lc_material_1 = require("../lc-material");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
/**
 * Calcul des pertes de charge dans un tube à partir des tables de Lechapt et Calmon
 */
class LechaptCalmon extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.LechaptCalmon;
        this._props.addObserver(this);
        this.material = lc_material_1.LCMaterial.PVCPolyethylene;
    }
    static get materials() {
        return LechaptCalmon._materials;
    }
    get material() {
        return this.properties.getPropValue("material");
    }
    set material(m) {
        this.properties.setPropValue("material", m);
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Calc(sVarCalc, rInit) {
        const r = super.Calc(sVarCalc, rInit);
        const V2 = Math.pow(r.values.V, 2);
        r.values.Kl = 19.62 * r.values.Jl / V2;
        r.values.fD = this.prms.J.V / this.prms.Lg.V / V2 * 19.62 * this.prms.D.V;
        return r;
    }
    Equation(sVarCalc) {
        if (sVarCalc !== "J") {
            throw new Error("LechaptCalmon.Equation() : invalid variable name " + sVarCalc);
        }
        const r = new result_1.Result(0, this);
        r.values.V = this.prms.Q.v / (Math.PI * Math.pow(this.prms.D.v / 2, 2));
        if (r.values.V < 0.4 || r.values.V > 2) {
            r.resultElement.log.add(new message_1.Message(message_1.MessageCode.WARNING_LECHAPT_CALMON_SPEED_OUTSIDE_04_2));
        }
        r.values.Jl = this.prms.L.v * Math.pow(this.prms.Q.v, this.prms.M.v)
            / Math.pow(this.prms.D.v, this.prms.N.v) * (this.prms.Lg.v / 1000);
        r.vCalc = r.values.Jl + this.prms.Ks.v / 19.62 * Math.pow(r.values.V, 2);
        return r;
    }
    // interface Observer
    update(sender, data) {
        if (data.action === "propertyChange" && data.name === "material") {
            this.applyMaterialPreset();
        }
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.Q.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.D.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.J.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Lg.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Ks.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.L.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.M.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.N.calculability = param_definition_1.ParamCalculability.FIXED;
    }
    exposeResults() {
        this._resultsFamilies = {
            Kl: undefined,
            fD: undefined,
            V: undefined,
            Jl: undefined
        };
    }
    /**
     * Modifies values of L, M, N depending on the given material,
     * according to this._materials presets
     */
    applyMaterialPreset() {
        const m = this.properties.getPropValue("material");
        if (m > 0) {
            const values = LechaptCalmon._materials[m - 1]; // "0" means no preset
            this.prms.L.singleValue = values.L;
            this.prms.M.singleValue = values.M;
            this.prms.N.singleValue = values.N;
        }
    }
}
exports.LechaptCalmon = LechaptCalmon;
LechaptCalmon._materials = [
    {
        L: 1.863,
        M: 2,
        N: 5.33,
        title: "Unlined cast iron - Coarse concrete (corrosive water)"
    },
    {
        L: 1.601,
        M: 1.975,
        N: 5.25,
        title: "Cast steel or uncoated - Coarse concrete (somewhat corrosive water)"
    },
    {
        L: 1.40,
        M: 1.96,
        N: 5.19,
        title: "Cast steel or cement coating"
    },
    {
        L: 1.16,
        M: 1.93,
        N: 5.11,
        title: "Cast iron or steel coating bitumen - Centrifuged concrete"
    },
    {
        L: 1.1,
        M: 1.89,
        N: 5.01,
        title: "Rolled steel - Smooth concrete"
    },
    {
        L: 1.049,
        M: 1.86,
        N: 4.93,
        title: "Cast iron or steel coating centrifuged"
    },
    {
        L: 1.01,
        M: 1.84,
        N: 4.88,
        title: "PVC - Polyethylene"
    },
    {
        L: 0.916,
        M: 1.78,
        N: 4.78,
        title: "Hydraulically smooth pipe - 0.05 ≤ D ≤ 0.2"
    },
    {
        L: 0.971,
        M: 1.81,
        N: 4.81,
        title: "Hydraulically smooth pipe - 0.25 ≤ D ≤ 1"
    }
];

},{"../compute-node":5,"../lc-material":16,"../nub":33,"../param/param-definition":86,"../util/message":151,"../util/result":155}],97:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LechaptCalmonParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * paramètres pour le calcul Lechapt et Calmon
 */
class LechaptCalmonParams extends params_equation_1.ParamsEquation {
    constructor(rQ, rD, rJ, rLg, rL, rM, rN, rKs) {
        super();
        this._Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.POS, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this._D = new param_definition_1.ParamDefinition(this, "D", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 20), "m", rD, param_definition_1.ParamFamily.DIAMETERS);
        this._J = new param_definition_1.ParamDefinition(this, "J", param_domain_1.ParamDomainValue.POS_NULL, "m", rJ);
        this._Lg = new param_definition_1.ParamDefinition(this, "Lg", param_domain_1.ParamDomainValue.POS, "m", rLg, param_definition_1.ParamFamily.LENGTHS);
        this._L = new param_definition_1.ParamDefinition(this, "L", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0.8, 2), undefined, rL, undefined, false);
        this._M = new param_definition_1.ParamDefinition(this, "M", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 1.5, 2.5), undefined, rM, undefined, false);
        this._N = new param_definition_1.ParamDefinition(this, "N", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 4.5, 5.5), undefined, rN, undefined, false);
        this._Ks = new param_definition_1.ParamDefinition(this, "Ks", param_domain_1.ParamDomainValue.POS_NULL, undefined, rKs);
        this.addParamDefinition(this._Q);
        this.addParamDefinition(this._D);
        this.addParamDefinition(this._J);
        this.addParamDefinition(this._Lg);
        this.addParamDefinition(this._L);
        this.addParamDefinition(this._M);
        this.addParamDefinition(this._N);
        this.addParamDefinition(this._Ks);
    }
    get Q() {
        return this._Q;
    }
    get D() {
        return this._D;
    }
    get J() {
        return this._J;
    }
    get Lg() {
        return this._Lg;
    }
    get L() {
        return this._L;
    }
    get M() {
        return this._M;
    }
    get N() {
        return this._N;
    }
    get Ks() {
        return this._Ks;
    }
}
exports.LechaptCalmonParams = LechaptCalmonParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],98:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PbBassin = void 0;
const nub_1 = require("../nub");
const result_1 = require("../util/result");
const param_definition_1 = require("../param/param-definition");
const compute_node_1 = require("../compute-node");
const message_1 = require("../util/message");
class PbBassin extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.PbBassin;
        this.cloisonsAmont = [];
        this.cloisonsAval = [];
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Calc(sVarCalc, rInit) {
        // if Calc() is called outside of CalcSerie(), _result might not be initialized
        if (!this.result) {
            this.initNewResultElement();
        }
        const r = this.result;
        r.resultElement.values.Z = this.CalcZ().moy;
        r.resultElement.values.Q = this.CalcQ();
        r.resultElement.values.YMOY = r.resultElement.values.Z - this.prms.ZF.v;
        // Puissance dissipée
        let energy = 0; // sum of Energy of upstream walls
        for (const w of this.cloisonsAmont) {
            energy += w.prms.Q.v * (w.prms.Z1.V - w.prms.Z2.v);
        }
        const ro = 1000; // masse volumique de l'eau en kg/m3
        const g = 9.81; // accélération de la gravité terrestre en m/s2.
        r.resultElement.values.PV = ro * g * energy / r.resultElement.values.YMOY / this.prms.S.v;
        return r;
    }
    Equation(sVarCalc) {
        switch (sVarCalc) {
            case "Q":
            case "Z":
                const r = new result_1.Result();
                r.values.Q = this.CalcQ();
                r.values.Z = this.CalcZ().moy;
                return r;
            default:
                throw new Error("PbBassin.Equation() : invalid variable name " + sVarCalc);
        }
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.S.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.ZF.calculability = param_definition_1.ParamCalculability.FREE;
    }
    CalcQ() {
        this.Q = 0;
        for (const c of this.cloisonsAmont) {
            this.Q += Math.max(0, c.prms.Q.v);
        }
        return this.Q;
    }
    CalcZ() {
        const zStat = this.parent.CalcZ1Cloisons(this.cloisonsAval);
        this.Z = zStat.moy;
        return zStat;
    }
    getMinZDV() {
        return this.parent.getMinZDV(this.cloisonsAval);
    }
    /** Returns order of current basin in parent PreBarrage, starting at 1 */
    get order() {
        return this.parent.findBasinPosition(this.uid) + 1;
    }
    /**
     * Returns a translatable message for basin description, containing order number
     */
    get description() {
        return new message_1.Message(message_1.MessageCode.INFO_PB_BASSIN_DESCRIPTION, {
            order: String(this.order)
        });
    }
}
exports.PbBassin = PbBassin;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../util/message":151,"../util/result":155}],99:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PbBassinParams = void 0;
const params_equation_1 = require("../param/params-equation");
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
class PbBassinParams extends params_equation_1.ParamsEquation {
    constructor(rS, rZF) {
        super();
        this.S = new param_definition_1.ParamDefinition(this, "S", param_domain_1.ParamDomainValue.POS, "m²", rS);
        this.addParamDefinition(this.S);
        this.ZF = new param_definition_1.ParamDefinition(this, "ZF", param_domain_1.ParamDomainValue.ANY, "m", rZF, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZF);
    }
}
exports.PbBassinParams = PbBassinParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],100:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PbCloison = void 0;
const parallel_structure_1 = require("../structure/parallel_structure");
const parallel_structure_params_1 = require("../structure/parallel_structure_params");
const compute_node_1 = require("../compute-node");
const structure_props_1 = require("../structure/structure_props");
const message_1 = require("../util/message");
class PbCloison extends parallel_structure_1.ParallelStructure {
    constructor(bassinAmont, bassinAval, dbg = false) {
        super(new parallel_structure_params_1.ParallelStructureParams(0, 0, 0), dbg);
        // prevent multiple CALC params in session file
        this.prms.Q.visible = false;
        this.prms.Z1.visible = false;
        this.prms.Z2.visible = false;
        this.properties.setPropValue("upstreamBasin", bassinAmont === undefined ? "" : bassinAmont.uid);
        this.properties.setPropValue("downstreamBasin", bassinAval === undefined ? "" : bassinAval.uid);
        this._calcType = compute_node_1.CalculatorType.PbCloison;
    }
    /** Bassin à l'amont de la cloison ou undefined pour condition limite amont */
    get bassinAmont() {
        let basin;
        const basinUID = this._props.getPropValue("upstreamBasin");
        if (basinUID !== undefined && basinUID !== "") {
            basin = this.parent.findChild(basinUID);
            if (basin === undefined) {
                // silent fail
            }
        }
        return basin;
    }
    /** sets the upstream basin by setting property "upstreamBasin" to the UID of the given PbBassin */
    set bassinAmont(b) {
        let uid = ""; // empty value
        if (b !== undefined) {
            uid = b.uid;
        }
        this.properties.setPropValue("upstreamBasin", uid);
        this.parent.updatePointers();
    }
    /** Bassin à l'aval de la cloison ou undefined pour condition limite aval */
    get bassinAval() {
        let basin;
        const basinUID = this._props.getPropValue("downstreamBasin");
        if (basinUID !== undefined && basinUID !== "") {
            basin = this.parent.findChild(basinUID);
            if (basin === undefined) {
                // silent fail
            }
        }
        return basin;
    }
    /** sets the downstream basin by setting property "downstreamBasin" to the UID of the given PbBassin */
    set bassinAval(b) {
        let uid = ""; // empty value
        if (b !== undefined) {
            uid = b.uid;
        }
        this.properties.setPropValue("downstreamBasin", uid);
        this.parent.updatePointers();
    }
    get Z1() {
        if (this.bassinAmont !== undefined) {
            return this.bassinAmont.Z;
        }
        else {
            return this.parent.prms.Z1.v;
        }
    }
    get Z2() {
        if (this.bassinAval !== undefined) {
            return this.bassinAval.Z;
        }
        else {
            return this.parent.prms.Z2.v;
        }
    }
    Calc(sVarCalc, rInit) {
        this.updateZ1Z2();
        const r = super.Calc(sVarCalc, rInit);
        switch (sVarCalc) {
            case "Z1":
                // Upstream water elevation should be at least equal minZDV
                r.vCalc = Math.max(this.getMinZDV(), r.vCalc);
                break;
            case "Q":
                r.vCalc = Math.max(0, r.vCalc);
        }
        return r;
    }
    finalCalc() {
        this.calculatedParam = this.prms.Q;
        this.Calc("Q");
        // add Z1, Z2 & DH extra results for GUI convenience
        this.result.resultElement.values.Z1 = this.prms.Z1.v;
        this.result.resultElement.values.Z2 = this.prms.Z2.v;
        this.result.resultElement.values.DH = this.prms.Z1.v - this.prms.Z2.v;
    }
    updateZ1Z2() {
        this.prms.Z1.v = this.Z1;
        this.prms.Z2.v = this.Z2;
    }
    /**
     * Give the minimum ZDV on all structures. Undefined if only orifice without ZDV.
     */
    getMinZDV() {
        let minZDV;
        for (const s of this.structures) {
            if (s.prms.ZDV.visible) {
                if (minZDV === undefined) {
                    minZDV = s.prms.ZDV.v;
                }
                else {
                    minZDV = Math.min(minZDV, s.prms.ZDV.v);
                }
            }
        }
        return minZDV;
    }
    getLoisAdmissibles() {
        return structure_props_1.loiAdmissiblesOuvrages; // @TODO loiAdmissiblesCloisons plutôt, mais penser à y intégrer Cunge80
    }
    /**
     * Returns a translatable message for wall description, containing order numbers of
     * upstream and downstream basin, or "upstream" / "downstream" mention if wall is
     * directly connected to the river
     */
    get description() {
        return new message_1.Message(message_1.MessageCode.INFO_PB_CLOISON_DESCRIPTION, {
            ub: this.bassinAmont === undefined ? "MSG_INFO_LIB_AMONT" : "B" + String(this.bassinAmont.order),
            db: this.bassinAval === undefined ? "MSG_INFO_LIB_AVAL" : "B" + String(this.bassinAval.order),
        });
    }
}
exports.PbCloison = PbCloison;

},{"../compute-node":5,"../structure/parallel_structure":111,"../structure/parallel_structure_params":112,"../structure/structure_props":126,"../util/message":151}],101:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PreBarrage = void 0;
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const compute_node_1 = require("../compute-node");
const pb_bassin_1 = require("./pb_bassin");
const pb_cloison_1 = require("./pb_cloison");
const session_settings_1 = require("../session_settings");
const message_1 = require("../util/message");
const resultelement_1 = require("../util/resultelement");
class PreBarrage extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.PreBarrage;
        this.cloisonsAmont = [];
        this._bassins = [];
        this.maxIterations = session_settings_1.SessionSettings.maxIterations;
        this._childrenType = "Cloison";
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /**
     * enfants castés au bon type
     */
    get children() {
        return this._children;
    }
    get bassins() {
        return this._bassins;
    }
    /**
     * Removes a child, along with all features (basins or walls) that are
     * related only to it, and pointers to it
     */
    deleteChild(index) {
        const item = this._children[index];
        if (item instanceof pb_bassin_1.PbBassin) {
            // reconnect walls to river upstream / downstream
            for (const w of item.cloisonsAmont) {
                w.bassinAval = undefined;
            }
            for (const w of item.cloisonsAval) {
                w.bassinAmont = undefined;
            }
        }
        else if (item instanceof pb_cloison_1.PbCloison) {
            if (item.bassinAmont !== undefined) {
                item.bassinAmont.cloisonsAval = item.bassinAmont.cloisonsAval.filter((v) => v !== item);
            }
            if (item.bassinAval !== undefined) {
                item.bassinAval.cloisonsAmont = item.bassinAval.cloisonsAmont.filter((v) => v !== item);
            }
        }
        // remove item
        super.deleteChild(index);
        this.updatePointers();
    }
    /**
     * Returns true if at least one path is connecting river upstream to river downstream,
     * using at least one basin (direct connection without basin doesn't count)
     */
    hasUpDownConnection(startWall, nbBasins = 0, visited = []) {
        let ok = false;
        // starting at river upstream ?
        if (startWall === undefined) {
            if (this.cloisonsAmont.length === 0) {
                return false;
            }
            // browse graph downwards
            for (const ca of this.cloisonsAmont) {
                ok = ok || this.hasUpDownConnection(ca, 0, visited);
            }
        }
        else {
            // starting from a wall
            if (startWall.bassinAval === undefined) {
                return (nbBasins > 0);
            }
            else {
                // browse graph downwards
                for (const ca of startWall.bassinAval.cloisonsAval) {
                    // prevent loops @TODO detect loops instead, and throw an error ?
                    if (!visited.includes(ca)) {
                        visited.push(ca);
                        ok = ok || this.hasUpDownConnection(ca, nbBasins + 1, visited);
                    }
                }
            }
        }
        return ok;
    }
    /**
     * Returns true if at least one basin is lacking upstream or downstream connection (or both)
     */
    hasBasinNotConnected() {
        for (const b of this._bassins) {
            if (b.cloisonsAmont.length === 0 || b.cloisonsAval.length === 0) {
                return true;
            }
        }
        return false;
    }
    /**
     * Returns the child (basin or wall) having the given UID, along with its position
     * among the _children array (*not* the _bassins array !)
     */
    findChildAndPosition(uid) {
        const res = { child: undefined, position: undefined };
        let pos = 0;
        for (const c of this.children) {
            if (c.uid === uid) {
                res.child = c;
                res.position = pos;
            }
            pos++;
        }
        return res;
    }
    /**
     * Returns the child (basin or wall) having the given UID
     */
    findChild(uid) {
        const { child } = this.findChildAndPosition(uid);
        return child;
    }
    /**
     * Returns the position of the child (basin or wall) having the given UID,
     * among the _children array (*not* the _bassins array !)
     */
    findChildPosition(uid) {
        const { position } = this.findChildAndPosition(uid);
        return position;
    }
    /**
     * Returns the position of the basin having the given UID,
     * among the _bassins array (*not* the _children array !)
     */
    findBasinPosition(uid) {
        let i = 0;
        for (const b of this._bassins) {
            if (b.uid === uid) {
                break;
            }
            i++;
        }
        return i;
    }
    /**
     * Moves the basin having the given UID, so that it becomes
     * basin #newBasinNumber, by reordering _children array and
     * rebuilding _bassins array afterwards
     */
    moveBasin(uid, newBasinNumber) {
        if (newBasinNumber >= this._bassins.length) {
            throw new Error(`PreBarrage.moveBasin: cannot make it #${newBasinNumber}, there are only ${this._bassins.length} basins`);
        }
        const tmp = [];
        // find position of basin currently occupying #newBasinNumber, in _children array
        const newPosition = this.findChildPosition(this._bassins[newBasinNumber].uid);
        // find basin to move and its current position
        const { child, position } = this.findChildAndPosition(uid);
        if (position > newPosition) {
            // move up:
            // insert slice [0 - newPosition[
            for (let i = 0; i < newPosition; i++) {
                tmp.push(this._children[i]);
            }
            // insert basin
            tmp.push(child);
            // insert slice [newPosition - position[
            for (let i = newPosition; i < position; i++) {
                tmp.push(this._children[i]);
            }
            // insert slice ]position, end]
            for (let i = position + 1; i < this._children.length; i++) {
                tmp.push(this._children[i]);
            }
            this._children = tmp;
        }
        else if (position < newPosition) {
            // move down:
            // @TODO should we also move down all walls connected to the moving basin, so that
            // they are still declared after it and not before ?
            // insert slice [0 - position[
            for (let i = 0; i < position; i++) {
                tmp.push(this._children[i]);
            }
            // insert slice ]position, newPosition[
            for (let i = position + 1; i < newPosition + 1; i++) {
                tmp.push(this._children[i]);
            }
            // insert basin
            tmp.push(child);
            // insert slice [newPosition - end]
            for (let i = newPosition + 1; i < this._children.length; i++) {
                tmp.push(this._children[i]);
            }
            this._children = tmp;
        } // else already at the right position, do nothing
        this.updatePointers();
    }
    /**
     * Detect duplicate walls between same basins pair, and merges
     * them into one wall containing all the previous walls' structures.
     */
    detectAndMergeDuplicateWalls() {
        // browse all upstream basins
        for (const b of this._bassins) {
            // for each upstream basin, group walls by downstream basin
            this.mergeDuplicateWalls(b);
        }
        // walls between upstream and another basin
        for (const b of this.cloisonsAmont.map((ca) => ca.bassinAval)) {
            if (b !== undefined) {
                this.mergeDuplicateWalls(b, false);
            }
        }
        // walls between river upstream and downstream
        const udWalls = [];
        for (const w of this._children) {
            if (w instanceof pb_cloison_1.PbCloison) {
                if (w.bassinAmont === undefined && w.bassinAval === undefined) {
                    udWalls.push(w);
                }
            }
        }
        this.mergeStructuresInFirstWall(udWalls);
    }
    /**
     * Finds all walls connected to the given basin's upstream (is upstream is true) or
     * downstream. Among those walls, finds those who have the same basin at their
     * opposite connection and merges them into one wall.
     * @param b
     * @param upstream
     */
    mergeDuplicateWalls(b, upstream = true) {
        // group walls attached to b by the other basin they are attached to
        const wallsByOtherBasin = {};
        const linkedWalls = upstream ? b.cloisonsAval : b.cloisonsAmont;
        for (const w of linkedWalls) {
            let dbId = "-"; // river upstream / downstream
            const otherBasin = upstream ? w.bassinAval : w.bassinAmont;
            if (otherBasin !== undefined) {
                dbId = otherBasin.uid;
            }
            if (!Object.keys(wallsByOtherBasin).includes(dbId)) {
                wallsByOtherBasin[dbId] = [];
            }
            wallsByOtherBasin[dbId].push(w);
        }
        // if multiple walls attached to b share the same other basin, merge them
        for (const dbUid of Object.keys(wallsByOtherBasin)) {
            const dbWalls = wallsByOtherBasin[dbUid];
            this.mergeStructuresInFirstWall(dbWalls);
        }
    }
    /**
     * Given a list of walls, moves all structures belonging to
     * structures #2+ into structure #1, and removes structures
     * #2+ from the current PreBarrage
     * @param walls
     */
    mergeStructuresInFirstWall(walls) {
        if (walls.length > 1) {
            // move all structures of walls > 1 to 1st wall
            for (let i = 1; i < walls.length; i++) {
                const dbw = walls[i];
                for (const s of dbw.structures) {
                    walls[0].addChild(s);
                }
                // delete "merged" wall
                this.deleteChild(dbw.findPositionInParent());
            }
        }
    }
    /**
     * Effectue une série de calculs sur un paramètre; déclenche le calcul en chaîne
     * des modules en amont si nécessaire
     * @param rInit solution approximative du paramètre
     */
    CalcSerie(rInit) {
        this.detectAndMergeDuplicateWalls();
        this._precision = Math.max(5E-4, session_settings_1.SessionSettings.precision);
        return super.CalcSerie(rInit);
    }
    Calc(sVarCalc, rInit) {
        // check elevations before calculating
        const cgResult = this.checkGeometry();
        if (cgResult.resultElement.hasErrorMessages()) {
            this._result.resultElement = cgResult.resultElement;
            return this.result;
        }
        const res = super.Calc(sVarCalc, rInit);
        // calculate basins so that they have a proper .result
        for (const b of this._bassins) {
            b.Calc();
        }
        // calculate Q on all walls so that their result shows Q and not Z1
        for (const c of this._children) {
            if (c instanceof pb_cloison_1.PbCloison) {
                c.finalCalc();
            }
        }
        // copy warnings issued by checkGeometry, if any
        for (const m of cgResult.resultElement.log.messages) {
            if (m.getSeverity() !== message_1.MessageSeverity.ERROR) {
                this._result.resultElement.log.add(m);
            }
        }
        return res;
    }
    /**
     * Calcul de Z1 pour une valeur fixe des paramètres
     * @param sVarCalc ignoré: Z1 uniquement
     */
    Equation(sVarCalc) {
        // Initialisation des cotes sur les bassins et la CL amont
        if (this.isMeshed()) {
            for (const b of this.bassins) {
                const zB = b.getMinZDV();
                if (zB !== undefined) {
                    b.Z = Math.max(zB, b.prms.ZF.v) + 1;
                }
                else {
                    b.Z = b.prms.ZF.v + 1;
                }
            }
            const Z1 = this.getMinZDV(this.cloisonsAmont);
            if (Z1 !== undefined) {
                this.prms.Z1.v = Math.max(Z1, this.bassins[0].Z) + 1;
            }
            else {
                this.prms.Z1.v = this.bassins[0].Z + 1;
            }
        }
        else {
            // On a le même débit dans tous les bassins et Cloisons
            for (const child of this.children) {
                if (child instanceof pb_bassin_1.PbBassin) {
                    child.Q = this.prms.Q.v;
                }
                else {
                    child.prms.Q.v = this.prms.Q.v;
                }
            }
        }
        let iter = this.maxIterations;
        let bConverged;
        let z1stat;
        // const tZ1: IPbBassinZstat[] = [];
        // const tQ: number[][] = [];
        while (iter-- > 0) {
            this._relax = Math.pow(iter / this.maxIterations, 3) * 0.25 + 0.05;
            this.debug(`***** Iteration n°${iter} relax=${this._relax} *****`);
            bConverged = true;
            if (this.isMeshed()) {
                // Balayage amont-aval: distribution des débits
                this.debug("*** Calcul des Q amont --> aval ***");
                // Répartition cloisons amont du PB
                // tQ.push(this.CalcQ(this.cloisonsAmont, this.prms.Q.v));
                this.CalcQ(this.cloisonsAmont, this.prms.Q.v);
                // Répartition pour chaque bassin sens amont-aval
                // TODO s'assurer du sens amont-aval des bassins dans this.children
                for (const b of this.bassins) {
                    this.debug("Bassin n°" + this.getIndexForChild(b));
                    this.CalcQ(b.cloisonsAval, b.CalcQ());
                }
            }
            // Balayage aval-amont: Calcul des cotes amont des cloisons
            this.debug("*** Calcul des Z aval --> amont ***");
            for (let i = this.bassins.length - 1; i >= 0; i--) {
                this.debug("Bassin " + i);
                const zStat = this.bassins[i].CalcZ();
                bConverged = bConverged && (zStat.max - zStat.min) < this._precision;
            }
            this.debug("Cloison amont");
            z1stat = this.CalcZ1Cloisons(this.cloisonsAmont);
            this.prms.Z1.v = z1stat.moy;
            // tZ1.push(z1stat);
            bConverged = bConverged && (z1stat.max - z1stat.min) < this._precision;
            if (bConverged) {
                break;
            }
        }
        // console.debug(tQ);
        // console.debug(tZ1);
        const r = new result_1.Result(this.prms.Z1.v);
        if (!bConverged) {
            r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_PREBARRAGE_NON_CONVERGENCE, {
                precision: (z1stat.max - z1stat.min)
            }));
        }
        return r;
    }
    /**
     * Checks pass geometry before calculation; adds relevant log messages
     */
    checkGeometry() {
        // A. error throwing cases: cannot calculate
        // each basin must have at least one upstream wall and one downstream wall
        for (const b of this._bassins) {
            if (b.cloisonsAmont.length === 0 || b.cloisonsAval.length === 0) {
                throw new Error(`PreBarrage.checkGeometry(): basin ${b.findPositionInParent()} (starting at 0) must have at least one upstream wall (has ${b.cloisonsAmont.length}) and one downstream wall (has ${b.cloisonsAval.length})`);
            }
        }
        // PreBarrage must have an upstream wall and a downstream wall
        if (this.cloisonsAmont.length === 0 /* || this.cloisonsAval.length === 0 */) {
            throw new Error(`PreBarrage.checkGeometry(): must have at least one upstream wall (has ${this.cloisonsAmont.length})` /* + ` and one downstream wall (has ${b.cloisonsAval.length})` */);
        }
        // PreBarrage must have at least one path from upstream to downstream
        if (!this.hasUpDownConnection()) {
            throw new Error("PreBarrage.checkGeometry(): must have at least one path from upstream to downstream");
        }
        // B. messages generating cases: calculation goes on
        const res = new result_1.Result(new resultelement_1.ResultElement(), this);
        // downstream water elevation > upstream water elevation ?
        if (this.calculatedParam === this.prms.Q && this.prms.Z2.v > this.prms.Z1.v) {
            res.resultElement.log.add(new message_1.Message(message_1.MessageCode.ERROR_PREBARRAGE_Z2_SUP_Z1));
        }
        // for each basin: is apron elevation > upstream water elevation ?
        for (const b of this._bassins) {
            if (b.prms.ZF.v > this.prms.Z1.v) {
                const m = new message_1.Message(message_1.MessageCode.WARNING_PREBARRAGE_BASSIN_ZF_SUP_Z1);
                m.extraVar.n = String(b.findPositionInParent() + 1);
                res.resultElement.log.add(m);
            }
        }
        // for each device of each wall: is ZDV < ZF of upstream basin ?
        for (const b of this.bassins) {
            for (const c of b.cloisonsAval) {
                for (const s of c.structures) {
                    if (s.prms.ZDV.v < b.prms.ZF.v) {
                        const m = new message_1.Message(message_1.MessageCode.ERROR_PREBARRAGE_STRUCTURE_ZDV_INF_ZF);
                        m.extraVar.ns = String(s.findPositionInParent() + 1);
                        const desc = c.description;
                        m.extraVar.cub = desc.extraVar.ub;
                        m.extraVar.cdb = desc.extraVar.db;
                        res.resultElement.log.add(m);
                    }
                }
            }
        }
        return res;
    }
    setParametersCalculability() {
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Q.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    adjustChildParameters(child) {
        this.updatePointers();
    }
    /** Clears basins list then builds it again fom children list */
    updatePointers() {
        this._bassins = [];
        this.cloisonsAmont = [];
        for (const c of this.children) {
            if (c.calcType === compute_node_1.CalculatorType.PbBassin) {
                this._bassins.push(c);
            }
            else if (c instanceof pb_cloison_1.PbCloison) {
                if (c.bassinAmont === undefined) {
                    this.cloisonsAmont.push(c);
                }
                if (c.bassinAmont !== undefined) {
                    if (!c.bassinAmont.cloisonsAval.includes(c)) {
                        c.bassinAmont.cloisonsAval.push(c);
                    }
                }
                if (c.bassinAval !== undefined) {
                    if (!c.bassinAval.cloisonsAmont.includes(c)) {
                        c.bassinAval.cloisonsAmont.push(c);
                    }
                }
            }
        }
    }
    isMeshed() {
        if (this.cloisonsAmont.length > 1) {
            return true;
        }
        for (const bassin of this.bassins) {
            if (bassin.cloisonsAval.length > 1) {
                return true;
            }
        }
        return false;
    }
    CalcQ(cloisons, QT) {
        // Calculation of repartition regarding actual water elevations
        let QT2 = 0;
        for (const c of cloisons) {
            c.prms.Q.initValue = Math.max(0, c.prms.Q.v);
            c.Calc("Q");
            // Relax! On ne prend pas toute la modification proposée !
            if (c.prms.Q.v > 0) {
                c.prms.Q.v = (1 - this._relax) * c.prms.Q.initValue + this._relax * c.prms.Q.v;
                QT2 += c.prms.Q.v;
            }
            if (this.DBG) {
                this.debug(`CalcQ: Q=${c.prms.Q.v} Z1=${c.prms.Z1.v} Z2=${c.prms.Z2.v}`);
            }
        }
        if (this.DBG) {
            this.debug(`CalcQ: QT=${QT} QT2=${QT2}`);
        }
        // Adjustement of each Q in order to get the good sum
        const adjustCoef = QT / QT2;
        // const tQ: number[] = [];
        for (const c of cloisons) {
            if (c.prms.Q.v >= 0) {
                if (QT2 !== 0) {
                    c.prms.Q.v = c.prms.Q.v * adjustCoef;
                }
                else {
                    c.prms.Q.v = QT / cloisons.length;
                }
            }
            // tQ.push(c.prms.Q.v)
            this.debug("CalcQ: Qc=" + c.prms.Q.v);
        }
        // return tQ;
    }
    CalcZ1Cloisons(cloisons) {
        const zStat = { moy: 0, min: Infinity, max: -Infinity };
        let n = 0;
        for (const c of cloisons) {
            let Z1;
            if (c.prms.Q.v >= 1E-6) {
                const r = c.Calc("Z1");
                if (r.ok) {
                    Z1 = r.vCalc;
                }
                else {
                    Z1 = c.prms.Z2.v;
                }
                zStat.moy += Z1;
                n++;
                this.debug(`CalcZ1Cloisons: n°${n} Z1=${Z1} Q=${c.prms.Q.v} Z2=${c.prms.Z2.v}`);
            }
            else {
                // Nul flow in submerged flow: Z1 = Z2
                c.updateZ1Z2();
                if (c.prms.Z2.v > c.getMinZDV()) {
                    Z1 = c.prms.Z2.v;
                    zStat.moy += Z1;
                    n++;
                }
                else {
                    c.prms.Q.v = 0;
                }
            }
            if (Z1 !== undefined) {
                zStat.min = Math.min(zStat.min, Z1);
                zStat.max = Math.max(zStat.max, Z1);
            }
        }
        if (n > 0) {
            zStat.moy = zStat.moy / n;
        }
        else {
            // Nul flow on all cloisons which are all in free flow => Z1 = ZminZDV
            zStat.moy = this.getMinZDV(cloisons);
            zStat.min = zStat.moy;
            zStat.max = zStat.moy;
        }
        this.debug(`CalcZ1Cloisons: Z= ${zStat.moy} [${(zStat.max - zStat.min)}]`);
        return zStat;
    }
    getMinZDV(cloisons) {
        let minZDV;
        for (const c of cloisons) {
            const minZDVCloison = c.getMinZDV();
            if (minZDVCloison !== undefined) {
                if (minZDV === undefined) {
                    minZDV = minZDVCloison;
                }
                else {
                    minZDV = Math.min(minZDV, minZDVCloison);
                }
            }
        }
        return minZDV;
    }
    /**
     * Fills the current Nub with parameter values, provided an object representation
     * @param obj object representation of a Nub content (parameters)
     * @returns the calculated parameter found, if any - used by child Nub to notify
     *          its parent of the calculated parameter to set
     */
    loadObjectRepresentation(obj) {
        // return value
        const ret = super.loadObjectRepresentation(obj);
        // propagate changed UIDs to walls properties
        for (const c of this._children) {
            if (c instanceof pb_cloison_1.PbCloison) {
                for (const k of Object.keys(ret.changedUids)) {
                    // find basins having the changed UID
                    if (c.properties.props.upstreamBasin === k) {
                        c.bassinAmont = this.findChild(ret.changedUids[k]);
                    }
                    if (c.properties.props.downstreamBasin === k) {
                        c.bassinAval = this.findChild(ret.changedUids[k]);
                    }
                }
            }
        }
        return ret;
    }
}
exports.PreBarrage = PreBarrage;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../session_settings":105,"../util/message":151,"../util/result":155,"../util/resultelement":156,"./pb_bassin":98,"./pb_cloison":100}],102:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PreBarrageParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
class PreBarrageParams extends params_equation_1.ParamsEquation {
    constructor(rQ, rZ1, rZ2) {
        super();
        this.Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.POS_NULL, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this.addParamDefinition(this.Q);
        this.Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.Z1);
        this.Z2 = new param_definition_1.ParamDefinition(this, "Z2", param_domain_1.ParamDomainValue.ANY, "m", rZ2, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.Z2);
    }
}
exports.PreBarrageParams = PreBarrageParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],103:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Props = void 0;
const observer_1 = require("./util/observer");
/**
 * gestion d'un ensemble de propriétés (clé/valeur) qui prévient quand
 * l'une d'entre-elles change
 */
class Props {
    constructor(_props = {}) {
        this._props = _props;
        this._observable = new observer_1.Observable();
    }
    getPropValue(key) {
        return this._props[key];
    }
    setPropValue(key, val, sender) {
        const oldValue = this._props[key];
        const changed = oldValue !== val;
        if (changed) {
            this._props[key] = val;
            this.notifyPropChange(key, val, sender);
        }
        return changed;
    }
    /**
     * fixe la valeur de toutes les propriétés
     * @param props nouvelles valeurs
     * @param sender objet modificateur
     */
    setProps(props, sender) {
        let p;
        if (props instanceof Props) {
            p = props._props;
        }
        else {
            p = props;
        }
        const changed = this.compareObjects(this._props, p);
        if (changed) {
            this._props = {};
            for (const k in p) {
                if (p.hasOwnProperty(k)) {
                    this._props[k] = p[k];
                }
            }
            this.notifyPropsChange(sender);
        }
    }
    get props() {
        return this._props;
    }
    getObservers() {
        return this._observable.getObservers();
    }
    /**
     * Remove all properties, does not notify of any change
     */
    reset() {
        this._props = {};
    }
    /**
     * Clones properties into a new Props object
     */
    clone() {
        const res = new Props();
        // copy values
        for (const k in this._props) {
            if (this._props.hasOwnProperty(k)) {
                res._props[k] = this._props[k];
            }
        }
        return res;
    }
    toString() {
        let res = "[";
        for (const k in this._props) {
            if (this._props.hasOwnProperty(k)) {
                if (res !== "[") {
                    res += ", ";
                }
                res += `${k}:${this._props[k]}`;
            }
        }
        res += "]";
        return res;
    }
    // interface IObservable
    /**
     * ajoute un observateur à la liste
     */
    addObserver(o) {
        this._observable.addObserver(o);
    }
    /**
     * supprime un observateur de la liste
     */
    removeObserver(o) {
        this._observable.removeObserver(o);
    }
    /**
     * notifie un événement aux observateurs
     */
    notifyObservers(data, sender) {
        this._observable.notifyObservers(data, sender);
    }
    /**
     * notification de changement de la valeur de toutes les propriétés
     * @param sender objet ayant changé les valeurs
     */
    notifyPropsChange(sender) {
        this.notifyObservers({
            action: "propertiesChange",
        }, sender);
    }
    /**
     * notification de changement de la valeur d'une propriété
     * @param prop nom de la propriété modifiée
     * @param val nouvelle valeur
     * @param sender objet ayant changé la valeur
     */
    notifyPropChange(prop, val, sender) {
        this.notifyObservers({
            action: "propertyChange",
            name: prop,
            value: val
        }, sender);
    }
    /**
     * compare 2 objets (clés et valeurs)
     * @return true s'il existe une différence
     */
    compareObjects(o1, o2) {
        const oldKeys = Object.keys(o1).sort();
        const newKeys = Object.keys(o2).sort();
        // nombre de clés
        const changed = oldKeys.length !== newKeys.length;
        if (changed) {
            return true;
        }
        // nom des clés
        for (const i in oldKeys) {
            if (oldKeys[i] !== newKeys[i]) {
                return true;
            }
        }
        // valeurs
        for (const i in o1) {
            if (o1[i] !== o2[i]) {
                return true;
            }
        }
        return false;
    }
}
exports.Props = Props;

},{"./util/observer":154}],104:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Session = void 0;
const base_1 = require("./base");
const compute_node_1 = require("./compute-node");
const config_1 = require("./config");
const lc_material_1 = require("./lc-material");
const nub_1 = require("./nub");
const props_1 = require("./props");
const session_settings_1 = require("./session_settings");
// Calculettes
const grille_1 = require("./devalaison/grille");
const grille_params_1 = require("./devalaison/grille_params");
const jet_1 = require("./devalaison/jet");
const jet_params_1 = require("./devalaison/jet_params");
const concentration_blocs_1 = require("./macrorugo/concentration_blocs");
const concentration_blocs_params_1 = require("./macrorugo/concentration_blocs_params");
const macrorugo_1 = require("./macrorugo/macrorugo");
const macrorugo_compound_1 = require("./macrorugo/macrorugo_compound");
const macrorugo_compound_params_1 = require("./macrorugo/macrorugo_compound_params");
const macrorugo_params_1 = require("./macrorugo/macrorugo_params");
const mrc_inclination_1 = require("./macrorugo/mrc-inclination");
const spp_1 = require("./math/spp");
const spp_params_1 = require("./math/spp_params");
const trigo_1 = require("./math/trigo");
const trigo_params_1 = require("./math/trigo_params");
const yaxb_1 = require("./math/yaxb");
const yaxb_params_1 = require("./math/yaxb_params");
const yaxn_1 = require("./math/yaxn");
const yaxn_params_1 = require("./math/yaxn_params");
const bief_1 = require("./open-channel/bief");
const bief_params_1 = require("./open-channel/bief_params");
const methode_resolution_1 = require("./open-channel/methode-resolution");
const pente_1 = require("./open-channel/pente");
const pente_params_1 = require("./open-channel/pente_params");
const regime_uniforme_1 = require("./open-channel/regime_uniforme");
const remous_1 = require("./open-channel/remous");
const remous_params_1 = require("./open-channel/remous_params");
const section_circulaire_1 = require("./open-channel/section/section_circulaire");
const section_circulaire_params_1 = require("./open-channel/section/section_circulaire_params");
const section_parametree_1 = require("./open-channel/section/section_parametree");
const section_puissance_1 = require("./open-channel/section/section_puissance");
const section_puissance_params_1 = require("./open-channel/section/section_puissance_params");
const section_rectang_1 = require("./open-channel/section/section_rectang");
const section_rectang_params_1 = require("./open-channel/section/section_rectang_params");
const section_trapez_1 = require("./open-channel/section/section_trapez");
const section_trapez_params_1 = require("./open-channel/section/section_trapez_params");
const cloison_aval_1 = require("./pab/cloison_aval");
const cloison_aval_params_1 = require("./pab/cloison_aval_params");
const cloisons_1 = require("./pab/cloisons");
const cloisons_params_1 = require("./pab/cloisons_params");
const pab_1 = require("./pab/pab");
const pab_chute_1 = require("./pab/pab_chute");
const pab_chute_params_1 = require("./pab/pab_chute_params");
const pab_dimension_1 = require("./pab/pab_dimension");
const pab_dimensions_params_1 = require("./pab/pab_dimensions_params");
const pab_nombre_1 = require("./pab/pab_nombre");
const pab_nombre_params_1 = require("./pab/pab_nombre_params");
const pab_params_1 = require("./pab/pab_params");
const pab_puissance_1 = require("./pab/pab_puissance");
const pab_puissance_params_1 = require("./pab/pab_puissance_params");
const cond_distri_1 = require("./pipe_flow/cond_distri");
const cond_distri_params_1 = require("./pipe_flow/cond_distri_params");
const lechaptcalmon_1 = require("./pipe_flow/lechaptcalmon");
const lechaptcalmon_params_1 = require("./pipe_flow/lechaptcalmon_params");
const solveur_1 = require("./solveur/solveur");
const solveur_params_1 = require("./solveur/solveur_params");
const dever_1 = require("./structure/dever");
const dever_params_1 = require("./structure/dever_params");
const factory_structure_1 = require("./structure/factory_structure");
const parallel_structure_1 = require("./structure/parallel_structure");
const parallel_structure_params_1 = require("./structure/parallel_structure_params");
const structure_props_1 = require("./structure/structure_props");
const par_1 = require("./par/par");
const par_params_1 = require("./par/par_params");
const par_simulation_1 = require("./par/par_simulation");
const par_simulation_params_1 = require("./par/par_simulation_params");
const fish_species_1 = require("./verification/fish_species");
const espece_1 = require("./verification/espece");
const espece_params_1 = require("./verification/espece_params");
const verificateur_1 = require("./verification/verificateur");
const diving_jet_support_1 = require("./verification/diving-jet-support");
const pre_barrage_1 = require("./prebarrage/pre_barrage");
const pre_barrage_params_1 = require("./prebarrage/pre_barrage_params");
const pb_cloison_1 = require("./prebarrage/pb_cloison");
const pb_bassin_1 = require("./prebarrage/pb_bassin");
const pb_bassin_params_1 = require("./prebarrage/pb_bassin_params");
class Session {
    constructor() {
        /** free documentation text, in Markdown format, to save into session file (optional) */
        this.documentation = "";
        /** Nubs de la session */
        this._nubs = [];
    }
    static getInstance() {
        if (Session._instance === undefined) {
            Session._instance = new Session();
        }
        return Session._instance;
    }
    /**
     * Returns a copy of given map, inverting enum keys and values
     */
    static invertEnumKeysAndValuesInProperties(stringProps, forceNumbers = false) {
        const res = JSON.parse(JSON.stringify(stringProps)); // clone
        for (const k in res) {
            if (!forceNumbers || !base_1.isNumeric(res[k])) {
                if (Object.keys(Session.enumFromProperty).includes(k)) {
                    const enumClass = Session.enumFromProperty[k];
                    res[k] = enumClass[res[k]];
                }
            }
        }
        return res;
    }
    /**
     * crée un Nub et l'ajoute à la session
     * @param props propriétés du Nub (computeType, nodeType...)
     */
    createSessionNub(p, dbg = false) {
        const res = this.createNub(p, undefined, dbg);
        this._nubs.push(res);
        return res;
    }
    /**
     * Adds an existing Nub to the session
     */
    registerNub(n) {
        if (this.uidAlreadyUsed(n.uid)) {
            n.setUid(nub_1.Nub.nextUID);
        }
        this._nubs.push(n);
    }
    /**
     * Adds many existing Nubs to the session
     */
    registerNubs(nubs) {
        for (const n of nubs) {
            this.registerNub(n);
        }
    }
    /**
     * Removes all Nubs from the Session
     */
    clear() {
        this._nubs = [];
    }
    /**
     * Returns number of Nubs in the session
     */
    getNumberOfNubs() {
        return this._nubs.length;
    }
    /** Accessor for Nubs list */
    getAllNubs() {
        return this._nubs;
    }
    /**
     * Removes a Nub from the session; does not consider Structure nubs inside Calculator nubs
     * @param sn the Nub to remove from the session
     */
    deleteNub(sn) {
        let i = 0;
        for (const n of this._nubs) {
            if (n.uid === sn.uid) {
                this._nubs.splice(i, 1);
                return;
            }
            i++;
        }
        throw new Error(`Session.deleteNub() : le Nub (uid ${sn.uid}) à supprimer n'a pas été trouvé`);
    }
    /**
     * Returns a JSON representation of (a part of) the current session
     * @param options an object having Nub uids as keys, with extra data object as values;
     *      if empty or undefined, all Nubs are serialised
     * @param settings app preferences to store in the session file (decimals, precision…)
     */
    serialise(options, settings) {
        const sess = [];
        // session-wide settings
        let sessionSettings = {
            precision: session_settings_1.SessionSettings.precision,
            maxIterations: session_settings_1.SessionSettings.maxIterations
        };
        if (settings) {
            sessionSettings = Object.assign(Object.assign({}, sessionSettings), settings);
        }
        // nubs in session
        let ids;
        let idsWithChildren;
        if (options) {
            ids = Object.keys(options);
            idsWithChildren = [...ids];
            // add ids of children
            for (const n of this._nubs) {
                if (ids.includes(n.uid)) {
                    for (const c of n.getChildren()) {
                        idsWithChildren.push(c.uid);
                    }
                }
            }
        }
        for (const n of this._nubs) {
            if (ids === undefined || ids.length === 0) {
                sess.push(n.objectRepresentation(undefined, ids));
            }
            else if (ids.includes(n.uid)) {
                sess.push(n.objectRepresentation(options[n.uid], idsWithChildren));
            }
        }
        return JSON.stringify({
            header: {
                source: "jalhyd",
                format_version: config_1.config.serialisation.fileFormatVersion,
                created: (new Date()).toISOString()
            },
            settings: sessionSettings,
            documentation: this.documentation,
            session: sess
        });
    }
    /**
     * Loads (a part of) a session from a JSON representation
     * @param serialised JSON data
     * @param uids unserialise only the Nubs havin the given UIDs
     */
    unserialise(serialised, uids) {
        // return value
        const ret = {
            nubs: [],
            hasErrors: false,
            settings: {}
        };
        // unserialise to object
        const data = JSON.parse(serialised);
        // settings
        if (data.settings) {
            ret.settings = data.settings;
            if (data.settings.precision !== undefined) {
                session_settings_1.SessionSettings.precision = data.settings.precision;
            }
            if (data.settings.maxIterations !== undefined) {
                session_settings_1.SessionSettings.maxIterations = data.settings.maxIterations;
            }
        }
        // nubs
        if (data.session && Array.isArray(data.session)) {
            data.session.forEach((e) => {
                if (!uids || uids.length === 0 || uids.includes(e.uid)) {
                    const nubPointer = this.createNubFromObjectRepresentation(e);
                    ret.nubs.push(nubPointer);
                    // forward errors
                    if (nubPointer.hasErrors) {
                        ret.hasErrors = true;
                    }
                }
            });
        }
        // concatenate doc
        if (data.documentation !== undefined && data.documentation !== "") {
            this.documentation += `\n\n` + data.documentation;
        }
        // second pass for links
        const flRes = this.fixLinks(serialised, uids);
        // forward errors
        if (flRes.hasErrors) {
            ret.hasErrors = true;
        }
        // second pass for Solveurs
        const fsRes = this.fixSolveurs(serialised, uids);
        // forward errors
        if (fsRes.hasErrors) {
            ret.hasErrors = true;
        }
        return ret;
    }
    /**
     * Creates a Nub from a JSON representation and adds it to the current session; returns
     * a pointer to the Nub and its JSON metadata
     * @param serialised JSON representation of a single Nub
     * @param register if false, new Nub will just be returned and won't be registered into the session
     */
    unserialiseSingleNub(serialised, register = true) {
        return this.createNubFromObjectRepresentation(JSON.parse(serialised), register);
    }
    /**
     * Returns the Nub identified by uid if any
     */
    findNubByUid(uid) {
        let foundNub;
        outerLoop: for (const n of this._nubs) {
            if (n.uid === uid) {
                foundNub = n;
            }
            for (const s of n.getChildren()) {
                if (s.uid === uid) {
                    foundNub = s;
                    break outerLoop;
                }
            }
        }
        return foundNub;
    }
    /**
     * Crée un Nub à partir d'une description (Props)
     * @param params propriétés à partir desquelles on détermine la classe du Nub à créer
     *      - calcType: type de Nub
     *      - nodeType: pour un Nub contenant une section
     *      - loiDebit: pour un Nub de type Structure (calcType doit être CalculatorType.Structure)
     *    Si d'autres propriétés sont fournies, elle écraseront les éventuelles propriétés par défaut
     *    définies dans le constructeur du Nub créé
     * @param dbg activer débogage
     */
    createNub(params, parentNub, dbg = false) {
        const calcType = params.getPropValue("calcType");
        let nub;
        let prms;
        switch (calcType) {
            case compute_node_1.CalculatorType.ConduiteDistributrice:
                prms = new cond_distri_params_1.ConduiteDistribParams(3, // débit Q
                1.2, // diamètre D
                0.6, // perte de charge J
                100, // Longueur de la conduite Lg
                1e-6);
                nub = new cond_distri_1.ConduiteDistrib(prms, dbg);
                break;
            case compute_node_1.CalculatorType.LechaptCalmon:
                prms = new lechaptcalmon_params_1.LechaptCalmonParams(3, // débit
                1.2, // diamètre
                0.6, /// perte de charge
                100, // longueur du toyo
                1.863, // paramètre L du matériau
                2, // paramètre M du matériau
                5.33, // paramètre N du matériau
                0 // Ks Perte de charge singulière
                );
                nub = new lechaptcalmon_1.LechaptCalmon(prms, dbg);
                break;
            case compute_node_1.CalculatorType.SectionParametree:
                nub = new section_parametree_1.SectionParametree(undefined, dbg);
                break;
            case compute_node_1.CalculatorType.RegimeUniforme:
                nub = new regime_uniforme_1.RegimeUniforme(undefined, dbg);
                break;
            case compute_node_1.CalculatorType.CourbeRemous:
                prms = new remous_params_1.CourbeRemousParams(100.25, // Z1 = cote de l'eau amont
                100.4, // Z2 = cote de l'eau aval
                100.1, // ZF1 = cote de fond amont
                100, // ZF2 = cote de fond aval
                100, // Long = Longueur du bief
                5);
                nub = new remous_1.CourbeRemous(undefined, prms, methode_resolution_1.MethodeResolution.EulerExplicite, dbg);
                break;
            case compute_node_1.CalculatorType.PabDimensions:
                prms = new pab_dimensions_params_1.PabDimensionParams(2, // Longueur L
                1, // Largeur W
                0.5, // Tirant d'eau Y
                2 // Volume V
                );
                nub = new pab_dimension_1.PabDimension(prms, dbg);
                break;
            case compute_node_1.CalculatorType.PabPuissance:
                prms = new pab_puissance_params_1.PabPuissanceParams(0.3, // Chute entre bassins DH (m)
                0.1, // Débit Q (m3/s)
                0.5, // Volume V (m3)
                588.6 // Puissance dissipée PV (W/m3)
                );
                nub = new pab_puissance_1.PabPuissance(prms, dbg);
                break;
            case compute_node_1.CalculatorType.Structure:
                const loiDebit = params.getPropValue("loiDebit");
                nub = factory_structure_1.CreateStructure(loiDebit, parentNub, dbg);
                break;
            case compute_node_1.CalculatorType.ParallelStructure:
                prms = new parallel_structure_params_1.ParallelStructureParams(0.5, // Q
                102, // Z1
                101.5 // Z2
                );
                nub = new parallel_structure_1.ParallelStructure(prms, dbg);
                break;
            case compute_node_1.CalculatorType.Dever:
                const deverPrms = new dever_params_1.DeverParams(0.5, // Q
                102, // Z1
                10, // BR : largeur du cours d'eau
                99 // ZR : cote du lit du cours d'eau
                );
                nub = new dever_1.Dever(deverPrms, dbg);
                break;
            case compute_node_1.CalculatorType.Cloisons:
                nub = new cloisons_1.Cloisons(new cloisons_params_1.CloisonsParams(1.5, // Débit total (m3/s)
                102, // Cote de l'eau amont (m)
                10, // Longueur des bassins (m)
                1, // Largeur des bassins (m)
                1, // Profondeur moyenne (m)
                0.5 // Hauteur de chute (m)
                ), dbg);
                break;
            case compute_node_1.CalculatorType.MacroRugo:
                nub = new macrorugo_1.MacroRugo(new macrorugo_params_1.MacrorugoParams(12.5, // ZF1
                6, // L
                1, // B
                0.05, // If
                1.57, // Q
                0.6, // h
                0.01, // Ks
                0.13, // C
                0.4, // D
                0.4, // k
                1 // Cd0
                ), dbg);
                break;
            case compute_node_1.CalculatorType.PabChute:
                nub = new pab_chute_1.PabChute(new pab_chute_params_1.PabChuteParams(2, // Z1
                0.5, // Z2
                1.5 // DH
                ), dbg);
                break;
            case compute_node_1.CalculatorType.PabNombre:
                nub = new pab_nombre_1.PabNombre(new pab_nombre_params_1.PabNombreParams(6, // DHT
                10, // N
                0.6 // DH
                ), dbg);
                break;
            case compute_node_1.CalculatorType.Section:
                const nodeType = params.getPropValue("nodeType");
                nub = this.createSection(nodeType, dbg);
                break;
            case compute_node_1.CalculatorType.Pab:
                nub = new pab_1.Pab(new pab_params_1.PabParams(1.5, // Q
                102, // Z1
                99 // Z2
                ), undefined, dbg);
                break;
            case compute_node_1.CalculatorType.CloisonAval: {
                prms = new cloison_aval_params_1.CloisonsAvalParams(0.5, // Q
                102, // Z1
                101.5, // Z2
                0 // ZRAM
                );
                nub = new cloison_aval_1.CloisonAval(prms, dbg);
                break;
            }
            case compute_node_1.CalculatorType.MacroRugoCompound:
                nub = new macrorugo_compound_1.MacrorugoCompound(new macrorugo_compound_params_1.MacrorugoCompoundParams(13.1, // Z1
                12.5, // ZRL
                12.5, // ZRR
                4, // B
                3, // DH
                0.05, // If
                0.01, // Ks
                0.13, // C
                0.4, // D
                0.4, // k
                1 // Cd0
                ), dbg);
                break;
            case compute_node_1.CalculatorType.Jet:
                nub = new jet_1.Jet(new jet_params_1.JetParams(5, // V0
                0.03, // S
                30, // ZJ
                29.2, // ZW
                28.5, // ZF
                3 // D
                ), dbg);
                break;
            case compute_node_1.CalculatorType.Grille:
                nub = new grille_1.Grille(new grille_params_1.GrilleParams(10, // QMax
                100, // CRad
                101.5, // CEau
                101.5, // CSomGrille
                2, // B
                72, // Beta
                90, // Alpha
                20, // b
                20, // p
                20, // e
                2, // a
                1.5, // c
                0.5, // O
                0.5, // Ob
                0.1, // OEntH
                4 // cIncl
                ), dbg);
                break;
            case compute_node_1.CalculatorType.Pente:
                nub = new pente_1.Pente(new pente_params_1.PenteParams(101, // Z1
                99.5, // Z2
                10, // L
                0.15 // I
                ), dbg);
                break;
            case compute_node_1.CalculatorType.Bief:
                nub = new bief_1.Bief(undefined, new bief_params_1.BiefParams(100.25, // Z1 = cote de l'eau amont
                100.4, // Z2 = cote de l'eau aval
                100.1, // ZF1 = cote de fond amont
                100, // ZF2 = cote de fond aval
                100, // Long = Longueur du bief
                5), dbg);
                break;
            case compute_node_1.CalculatorType.Solveur:
                nub = new solveur_1.Solveur(new solveur_params_1.SolveurParams(undefined));
                break;
            case compute_node_1.CalculatorType.YAXB:
                nub = new yaxb_1.YAXB(new yaxb_params_1.YAXBParams(10, // Y
                2, // A
                3, // X
                4 // B
                ), dbg);
                break;
            case compute_node_1.CalculatorType.Trigo:
                nub = new trigo_1.Trigo(new trigo_params_1.TrigoParams(0.985, // Y
                10 // X
                ), dbg);
                break;
            case compute_node_1.CalculatorType.SPP:
                nub = new spp_1.SPP(new spp_params_1.SPPParams(1 // Y
                ), dbg);
                break;
            case compute_node_1.CalculatorType.YAXN:
                nub = new yaxn_1.YAXN(new yaxn_params_1.YAXNParams(1, // A
                1, // X
                1 // B
                ), dbg);
                break;
            case compute_node_1.CalculatorType.ConcentrationBlocs:
                nub = new concentration_blocs_1.ConcentrationBlocs(new concentration_blocs_params_1.ConcentrationBlocsParams(0.128, // Concentration de blocs
                5, // Nombre de motifs
                4.9, // Largeur de la passe
                0.35 // Diamètre des plots
                ), dbg);
                break;
            case compute_node_1.CalculatorType.Par:
                nub = new par_1.Par(new par_params_1.ParParams(0.25, // Q
                10, // Z1
                9, // Z2
                0.64, // ha
                0.2, // S
                0.4, // P
                0.6, // L
                0.1, // a
                1, // N
                1 // M
                ), dbg);
                break;
            case compute_node_1.CalculatorType.ParSimulation:
                nub = new par_simulation_1.ParSimulation(new par_simulation_params_1.ParSimulationParams(0.25, // Q
                10, // Z1
                9, // Z2
                0.2, // S
                0.4, // P
                undefined, // Nb
                9.242, // ZR1
                undefined, // ZD1
                8.222, // ZR2
                undefined, // ZD2
                0.6, // L
                0.1, // a
                1, // N
                1 // M
                ), dbg);
                break;
            case compute_node_1.CalculatorType.PreBarrage:
                nub = new pre_barrage_1.PreBarrage(new pre_barrage_params_1.PreBarrageParams(1, // Q
                101, // Z1
                100 // Z2
                ), dbg);
                break;
            case compute_node_1.CalculatorType.Espece:
                nub = new espece_1.Espece(
                // default params are those for SPECIES_1 (Salmons and trouts)
                new espece_params_1.EspeceParams(0.35, // DHMaxS
                0.35, // DHMaxP
                0.3, // BMin
                1, // PMinS
                1, // PMinP
                2.5, // LMinS
                2.5, // LMinP
                0.3, // HMin
                0.4, // YMin
                2.5, // VeMax
                0.2, // YMinSB
                0.3, // YMinPB
                150, // PVMaxPrec
                200 // PVMaxLim
                ));
                break;
            case compute_node_1.CalculatorType.Verificateur:
                nub = new verificateur_1.Verificateur();
                break;
            case compute_node_1.CalculatorType.PbBassin:
                nub = new pb_bassin_1.PbBassin(new pb_bassin_params_1.PbBassinParams(10, // S
                100 // ZF
                ), dbg);
                break;
            case compute_node_1.CalculatorType.PbCloison:
                nub = new pb_cloison_1.PbCloison(undefined, undefined);
                break;
            default:
                throw new Error(`Session.createNub() : type de module '${compute_node_1.CalculatorType[calcType]}' non pris en charge`);
        }
        // propagate properties
        try {
            nub.properties = params;
        }
        catch (e) {
            // loading Solveur properties when unserialising a session might fail because target
            // Nub / param do not exist yet; silent fail in this case, and Solveur.fixTargets()
            // might fix it later
            if (!(nub instanceof solveur_1.Solveur)) {
                throw e;
            }
        }
        return nub;
    }
    /**
     * Returns true if given uid is already used by a Nub in this session,
     * or a Structure nub inside one of them
     */
    uidAlreadyUsed(uid, nubs = this._nubs) {
        let alreadyUsed = false;
        for (const n of nubs) {
            if (n.uid === uid) {
                alreadyUsed = true;
                break;
            }
            if (!alreadyUsed) {
                alreadyUsed = this.uidAlreadyUsed(uid, n.getChildren());
            }
        }
        return alreadyUsed;
    }
    /**
     * Returns all Nubs depending on the given one (parameter or result),
     * without following links (1st level only)
     * @param uid UID of the Nub that underwent a change
     * @param symbol symbol of the parameter whose value change triggered this method; if specified,
     *      Nubs targetting this symbol will be considered dependent
     * @param includeValuesLinks if true, even Nubs targetting non-calculated non-modified parameters
     *      will be considered dependent @see jalhyd#98
     * @param includeOtherDependencies if true, will be considered dependent
     *      - Solveur Nubs having given Nub either as X or as Ytarget's parent Nub
     *      - Verificateur Nubs having given Nub either as selected Custom Species or Pass to check
     */
    getDependingNubs(uid, symbol, includeValuesLinks = false, includeOtherDependencies = false) {
        const dependingNubs = [];
        for (const n of this._nubs) {
            if (n.uid !== uid
                && n.resultDependsOnNub(uid, [], symbol, includeValuesLinks, includeOtherDependencies)) {
                dependingNubs.push(n);
            }
        }
        return dependingNubs;
    }
    /**
     * Returns all Nubs depending on the result of at least one other Nub.
     * Used by Solveur to find available "target" nubs to calculate.
     */
    getDownstreamNubs() {
        const downstreamNubs = [];
        for (const n of this._nubs) {
            if (n.getRequiredNubs().length > 0) {
                downstreamNubs.push(n);
            }
        }
        return downstreamNubs;
    }
    /**
     * Returns all Nubs that do not depend on the result of any other Nub
     * (includes single Nubs).
     */
    getUpstreamNubs() {
        const upstreamNubs = [];
        for (const n of this._nubs) {
            if (n.getRequiredNubs().length === 0) {
                upstreamNubs.push(n);
            }
        }
        return upstreamNubs;
    }
    /**
     * Returns all upstream Nubs that have at least one declared extra result.
     * Used by Solveur to find available "target" nubs to calculate.
     */
    getUpstreamNubsHavingExtraResults() {
        const unher = [];
        for (const n of this.getUpstreamNubs()) {
            if (n.resultsFamilies && Object.keys(n.resultsFamilies).length > 0) {
                unher.push(n);
            }
        }
        return unher;
    }
    /**
     * Returns a list of nub/symbol couples, that can be linked to the given
     * parameter, among all current nubs
     * @param p
     */
    getLinkableValues(p) {
        let res = [];
        for (const n of this._nubs) {
            const linkableValues = n.getLinkableValues(p);
            res = res.concat(linkableValues);
        }
        /* console.log("LINKABLE VALUES", res.map((lv) => {
            return `${lv.nub.uid}(${lv.nub.constructor.name})/${lv.symbol}`;
        })); */
        return res;
    }
    /**
     * Crée un Nub de type Section
     * @param nt SectionType
     * @param dbg activer débogage
     */
    createSection(nt, dbg = false) {
        switch (nt) {
            case compute_node_1.SectionType.SectionTrapeze: {
                const prms = new section_trapez_params_1.ParamsSectionTrapez(2.5, // largeur de fond
                0.56, // fruit
                0.8, // tirant d'eau
                40, //  Ks=Strickler
                1.2, //  Q=Débit
                0.001, //  If=pente du fond
                1);
                return new section_trapez_1.cSnTrapez(prms, dbg);
            }
            case compute_node_1.SectionType.SectionRectangle: {
                const prms = new section_rectang_params_1.ParamsSectionRectang(0.8, // tirant d'eau
                2.5, // largeur de fond
                40, //  Ks=Strickler
                1.2, // Q=Débit
                0.001, // If=pente du fond
                1 // YB=hauteur de berge
                );
                return new section_rectang_1.cSnRectang(prms, dbg);
            }
            case compute_node_1.SectionType.SectionCercle: {
                const prms = new section_circulaire_params_1.ParamsSectionCirc(2, // diamètre
                0.8, // tirant d'eau
                40, //  Ks=Strickler
                1.2, //  Q=Débit
                0.001, //  If=pente du fond
                1);
                return new section_circulaire_1.cSnCirc(prms, dbg);
            }
            case compute_node_1.SectionType.SectionPuissance: {
                const prms = new section_puissance_params_1.ParamsSectionPuiss(0.5, // coefficient
                0.8, // tirant d'eau
                4, // largeur de berge
                40, //  Ks=Strickler
                1.2, //  Q=Débit
                0.001, //  If=pente du fond
                1);
                return new section_puissance_1.cSnPuiss(prms, dbg);
            }
            default:
                throw new Error(`type de section ${compute_node_1.SectionType[nt]} non pris en charge`);
        }
    }
    /**
     * Creates a Nub from an object representation and adds it to the current session; returns
     * a pointer to the Nub and its JSON metadata
     * @param obj object representation of a single Nub
     * @param register if false, new Nub will just be returned and won't be registered into the session
     */
    createNubFromObjectRepresentation(obj, register = true) {
        // return value;
        const nubPointer = {
            nub: undefined,
            meta: undefined,
            hasErrors: false
        };
        // decode properties
        const props = Session.invertEnumKeysAndValuesInProperties(obj.props, true);
        // create the Nub
        let newNub;
        if (register) {
            newNub = this.createSessionNub(new props_1.Props(props));
        }
        else {
            newNub = this.createNub(new props_1.Props(props));
        }
        // try to keep the original ID
        if (!this.uidAlreadyUsed(obj.uid)) {
            newNub.setUid(obj.uid);
        }
        const res = newNub.loadObjectRepresentation(obj);
        nubPointer.nub = newNub;
        // forward errors
        if (res.hasErrors) {
            nubPointer.hasErrors = true;
        }
        // add metadata (used by GUI, for ex.)
        if (obj.meta) {
            nubPointer.meta = obj.meta;
        }
        return nubPointer;
    }
    /**
     * Asks all loaded Nubs to relink any parameter that has a wrong target
     */
    fixLinks(serialised, uids) {
        // return value
        const res = {
            hasErrors: false
        };
        const data = JSON.parse(serialised);
        if (data.session && Array.isArray(data.session)) {
            // find each corresponding Nub in the session
            data.session.forEach((e) => {
                if (!uids || uids.length === 0 || uids.includes(e.uid)) {
                    const nub = this.findNubByUid(e.uid);
                    // find linked parameters
                    const ret = nub.fixLinks(e);
                    // forwardErrors
                    if (ret.hasErrors) {
                        res.hasErrors = true;
                    }
                }
            });
        }
        return res;
    }
    /**
     * Asks every loaded Solveur to reconnect to its nubToCalculate / searchedParameter
     */
    fixSolveurs(serialised, uids) {
        // return value
        const res = {
            hasErrors: false
        };
        const data = JSON.parse(serialised);
        if (data.session && Array.isArray(data.session)) {
            // find each corresponding Nub in the session
            data.session.forEach((e) => {
                if (!uids || uids.length === 0 || uids.includes(e.uid)) {
                    const nub = this.findNubByUid(e.uid);
                    if (nub instanceof solveur_1.Solveur) {
                        // find targetted nubToCalculate / searchedParam
                        const ret = nub.fixTargets(e);
                        // forwardErrors
                        if (ret.hasErrors) {
                            res.hasErrors = true;
                        }
                    }
                }
            });
        }
        return res;
    }
}
exports.Session = Session;
/** correspondance entre les noms des propriétés et les enum associés */
Session.enumFromProperty = {
    loiDebit: structure_props_1.LoiDebit,
    methodeResolution: methode_resolution_1.MethodeResolution,
    material: lc_material_1.LCMaterial,
    gridProfile: grille_1.GrilleProfile,
    gridType: grille_1.GrilleType,
    regime: bief_params_1.BiefRegime,
    trigoOperation: trigo_1.TrigoOperation,
    trigoUnit: trigo_1.TrigoUnit,
    sppOperation: spp_1.SPPOperation,
    nodeType: compute_node_1.SectionType,
    calcType: compute_node_1.CalculatorType,
    structureType: structure_props_1.StructureType,
    inclinedApron: mrc_inclination_1.MRCInclination,
    parType: par_1.ParType,
    species: fish_species_1.FishSpecies,
    divingJetSupported: diving_jet_support_1.DivingJetSupport
};

},{"./base":3,"./compute-node":5,"./config":6,"./devalaison/grille":8,"./devalaison/grille_params":9,"./devalaison/jet":10,"./devalaison/jet_params":11,"./lc-material":16,"./macrorugo/concentration_blocs":18,"./macrorugo/concentration_blocs_params":19,"./macrorugo/macrorugo":20,"./macrorugo/macrorugo_compound":21,"./macrorugo/macrorugo_compound_params":22,"./macrorugo/macrorugo_params":23,"./macrorugo/mrc-inclination":24,"./math/spp":25,"./math/spp_params":26,"./math/trigo":27,"./math/trigo_params":28,"./math/yaxb":29,"./math/yaxb_params":30,"./math/yaxn":31,"./math/yaxn_params":32,"./nub":33,"./open-channel/bief":34,"./open-channel/bief_params":35,"./open-channel/methode-resolution":36,"./open-channel/pente":37,"./open-channel/pente_params":38,"./open-channel/regime_uniforme":39,"./open-channel/remous":40,"./open-channel/remous_params":41,"./open-channel/section/section_circulaire":47,"./open-channel/section/section_circulaire_params":48,"./open-channel/section/section_parametree":50,"./open-channel/section/section_puissance":52,"./open-channel/section/section_puissance_params":53,"./open-channel/section/section_rectang":54,"./open-channel/section/section_rectang_params":55,"./open-channel/section/section_trapez":56,"./open-channel/section/section_trapez_params":57,"./pab/cloison_aval":60,"./pab/cloison_aval_params":61,"./pab/cloisons":62,"./pab/cloisons_params":63,"./pab/pab":64,"./pab/pab_chute":65,"./pab/pab_chute_params":66,"./pab/pab_dimension":67,"./pab/pab_dimensions_params":68,"./pab/pab_nombre":69,"./pab/pab_nombre_params":70,"./pab/pab_params":71,"./pab/pab_puissance":72,"./pab/pab_puissance_params":73,"./par/par":74,"./par/par_params":75,"./par/par_simulation":76,"./par/par_simulation_params":77,"./pipe_flow/cond_distri":94,"./pipe_flow/cond_distri_params":95,"./pipe_flow/lechaptcalmon":96,"./pipe_flow/lechaptcalmon_params":97,"./prebarrage/pb_bassin":98,"./prebarrage/pb_bassin_params":99,"./prebarrage/pb_cloison":100,"./prebarrage/pre_barrage":101,"./prebarrage/pre_barrage_params":102,"./props":103,"./session_settings":105,"./solveur/solveur":106,"./solveur/solveur_params":107,"./structure/dever":108,"./structure/dever_params":109,"./structure/factory_structure":110,"./structure/parallel_structure":111,"./structure/parallel_structure_params":112,"./structure/structure_props":126,"./verification/diving-jet-support":159,"./verification/espece":160,"./verification/espece_params":161,"./verification/fish_species":162,"./verification/verificateur":163}],105:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SessionSettings = void 0;
class SessionSettings {
}
exports.SessionSettings = SessionSettings;
/** session-wide calculation precision, used by all Nubs */
SessionSettings.precision = 1e-7;
/** session-wide maximum number of iterations, used by all Nubs in Newton() and Dichotomie() */
SessionSettings.maxIterations = 100;

},{}],106:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Solveur = void 0;
const compute_node_1 = require("../compute-node");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const param_value_mode_1 = require("../param/param-value-mode");
const session_1 = require("../session");
const result_1 = require("../util/result");
const resultelement_1 = require("../util/resultelement");
const message_1 = require("../util/message");
class Solveur extends nub_1.Nub {
    /**
     * Finds all parameters whose value can be searched for by dichotomy, for
     * a given value of calculated param of given Nub
     * @param nub Nub calculated by Solveur
     * @param addSelf if true, will also return parameters of the current Nub
     */
    static getDependingNubsSearchableParams(nub, addSelf = false) {
        const searchableParams = [];
        if (nub !== undefined) {
            const upstreamNubs = nub.getRequiredNubsDeep();
            if (addSelf) {
                upstreamNubs.push(nub);
            }
            for (const un of upstreamNubs) {
                for (const p of un.parameterIterator) {
                    // only visible, non-varying, non-calculated parameters can be looked for
                    if (p.visible && p.valueMode === param_value_mode_1.ParamValueMode.SINGLE) {
                        searchableParams.push(p);
                    }
                }
            }
        }
        return searchableParams;
    }
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Solveur;
        this._childrenType = "Espece";
        this._props.addObserver(this);
        this.prms.addObserver(this);
        // UID of Session Nub to calculate, the result of the which must be this.prms.Ytarget.singleValue
        this.nubToCalculate = undefined;
        // symbol of extra result targetted on the Nub to calculate; if specified, then this.prms.Ytarget.singleValue
        // will be the expected value not for nubToCalculate.vCalc, but for nubToCalculate.values[this.targettedResult]
        this.targettedResult = undefined;
        // UID of Session Nub / symbol of source parameter to vary (the "searched" parameter)
        this.searchedParameter = undefined;
        // calculated param should always be pseudo-parameter "X" (the one whose value we're looking for)
        this._defaultCalculatedParam = prms.X;
        this.resetDefaultCalculatedParam();
    }
    get prms() {
        return this._prms;
    }
    /** finds the Nub to calculate by its UID */
    get nubToCalculate() {
        let nub;
        const nubUID = this._props.getPropValue("nubToCalculate");
        if (nubUID !== undefined && nubUID !== "") {
            nub = session_1.Session.getInstance().findNubByUid(nubUID);
            if (nub === undefined) {
                // silent fail
                // throw new Error(`Solveur.get nubToCalculate(): cannot find Nub ${nubUID} in session`);
            }
        }
        return nub;
    }
    /** defines the Nub to calculate by setting property "nubToCalculate" to the UID of the given Nub */
    set nubToCalculate(n) {
        let uid = ""; // empty value
        if (n !== undefined) {
            uid = n.uid;
        }
        this.properties.setPropValue("nubToCalculate", uid);
    }
    get targettedResult() {
        return this._props.getPropValue("targettedResult");
    }
    set targettedResult(symbol) {
        this._props.setPropValue("targettedResult", symbol);
    }
    /** finds the source parameter whose value we're looking for, by its Nub UID / symbol */
    get searchedParameter() {
        let p;
        const nubUIDAndSymbol = this._props.getPropValue("searchedParameter");
        if (nubUIDAndSymbol !== undefined && nubUIDAndSymbol !== "") {
            const slashPos = nubUIDAndSymbol.indexOf("/");
            const nubUID = nubUIDAndSymbol.substring(0, slashPos);
            const paramSymbol = nubUIDAndSymbol.substring(slashPos + 1);
            // console.log("(i) searched param Nub UID / symbol :", nubUID, paramSymbol);
            if (nubUID) {
                const nub = session_1.Session.getInstance().findNubByUid(nubUID);
                if (nub !== undefined) {
                    if (paramSymbol) {
                        p = nub.getParameter(paramSymbol);
                        if (p === undefined) {
                            throw new Error(`Solveur.get searchedParameter(): cannot find Parameter ${paramSymbol} in Nub`);
                        }
                    }
                }
                else {
                    // silent fail
                    // throw new Error(`Solveur.get searchedParameter(): cannot find Nub ${nubUID} in session`);
                }
            }
        }
        return p;
    }
    /**
     * defines the searched parameter by setting property "searchedParameter" to the UID of the
     * parameter's Nub / the parameter's symbol
     */
    set searchedParameter(p) {
        let sp = ""; // empty value
        if (p !== undefined) {
            sp = p.nubUid + "/" + p.symbol;
        }
        this.properties.setPropValue("searchedParameter", sp);
    }
    /**
     * Looks for potential errors before calling Nub.Calc()
     */
    Calc(sVarCalc, rInit) {
        const r = new result_1.Result(new resultelement_1.ResultElement());
        if (this.nubToCalculate.resultHasMultipleValues()) {
            r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.ERROR_SOLVEUR_NO_VARIATED_PARAMS_ALLOWED));
            this.currentResult = r;
            return this.result;
        }
        return super.Calc(sVarCalc, rInit);
    }
    CalcSerie(rInit) {
        // affect initial value of X for Dichotomie search
        this.prms.X.singleValue = this.prms.Xinit.currentValue;
        const res = super.CalcSerie(rInit);
        res.updateSourceNub(this);
        res.removeExtraResults();
        // clodo trick: realculate nubToCalculate() dissociates results @see jalhyd#157
        this.nubToCalculate.CalcSerie();
        return res;
    }
    /**
     * Calculates the target "downstream" Nub, that will trigger calculation of
     * all linked "upstream" Nubs, including the one that contains the variating
     * parameter X
     * @param sVarCalc should always be pseudo-parameter "Y"
     */
    Equation(sVarCalc) {
        if (sVarCalc !== "Y") {
            throw new Error(`Solveur: calculated param should always be Y (found ${sVarCalc})`);
        }
        // set the Y value we have to obtain
        this.prms.Y.v = this.prms.Ytarget.v;
        // set the current value of X, determined by Dichotomie, on the upstream Nub
        this.searchedParameter.singleValue = this.prms.X.v;
        // calculate Nubs chain
        const res = this.nubToCalculate.CalcSerie();
        // if targetting an extraresult, replace vCalc with targetted extraresult
        if (this.targettedResult) { // might be undefined, or "" (when unserializing)
            res.vCalc = res.values[this.targettedResult];
        }
        return res;
    }
    getFirstAnalyticalParameter() {
        // always use pseudo-parameter Y for Dichotomie iterations;
        // always update target value from input parameter
        this.prms.Y.v = this.prms.Ytarget.v;
        return this.prms.Y;
    }
    // interface Observer
    update(sender, data) {
        if (data.action === "propertyChange") {
            if (data.name === "nubToCalculate" || data.name === "searchedParameter" || data.name === "targettedResult") {
                const n = this.nubToCalculate;
                const t = this.targettedResult;
                const p = this.searchedParameter;
                if (n !== undefined && p !== undefined) {
                    if (n !== p.parentNub && !n.dependsOnNubResult(p.parentNub)) {
                        throw new Error("Solveur.update(): Nub to calculate is not linked to result of searchedParameter parent Nub");
                    }
                }
                if (data.name === "nubToCalculate") {
                    if (n !== undefined) {
                        if (n.resultHasMultipleValues()) {
                            throw new Error("Solveur.update(): Nub to calculate must not have multiple values");
                        }
                        // update Ytarget
                        if (this.prms.Ytarget.singleValue === undefined) {
                            this.prms.Ytarget.singleValue = n.calculatedParam.singleValue;
                        }
                        // reset targetted result
                        this.targettedResult = undefined;
                    }
                }
                if (data.name === "searchedParameter") {
                    if (p !== undefined) {
                        if (p.valueMode !== param_value_mode_1.ParamValueMode.SINGLE) {
                            throw new Error("Solveur.update(): searched parameter X must be in SINGLE mode");
                        }
                        // update pseudo-parameter X
                        this.prms.setX(p);
                    }
                }
                if (data.name === "targettedResult") {
                    if (t !== undefined && t !== "") {
                        if (n.resultsFamilies
                            && !Object.keys(n.resultsFamilies).includes(t)) {
                            throw new Error("Solveur.update(): targetted result T is not a declared extra result of Nub to calculate");
                        }
                    }
                    else {
                        // empty value − is nubToCalc single ? If so, invalidate searchedParam
                        if (Solveur.getDependingNubsSearchableParams(n).length === 0) {
                            this.searchedParameter = undefined;
                        }
                    }
                }
            }
        }
        if (data.action === "XUpdated") {
            this._defaultCalculatedParam = this.prms.X;
            this.resetDefaultCalculatedParam();
        }
    }
    /**
     * Once session is loaded, run a second pass on targetted
     * objects if they couldn't be set before
     */
    fixTargets(obj) {
        // return value
        const ret = {
            hasErrors: false
        };
        try {
            // do not use setters, to allow setting directly the string UIDs
            this.properties.setPropValue("nubToCalculate", obj.props.nubToCalculate);
            this.properties.setPropValue("searchedParameter", obj.props.searchedParameter);
        }
        catch (e) {
            ret.hasErrors = true;
        }
        return ret;
    }
    setParametersCalculability() {
        if (this.prms.X !== undefined) {
            this.prms.X.calculability = param_definition_1.ParamCalculability.DICHO;
        }
        this.prms.Xinit.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Ytarget.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.Y.calculability = param_definition_1.ParamCalculability.FREE;
    }
    Solve(sVarCalc, rInit) {
        const res = super.Solve(sVarCalc, rInit);
        // if Y has log about a failure in calc chain, copy it to
        // a resultElement so that Nub.Calc() does not complain
        if (res.resultElements.length === 0) {
            const re = new resultelement_1.ResultElement();
            re.log.addLog(res.globalLog);
            res.addResultElement(re);
        }
        return res;
    }
}
exports.Solveur = Solveur;

},{"../compute-node":5,"../nub":33,"../param/param-definition":86,"../param/param-value-mode":89,"../session":104,"../util/message":151,"../util/result":155,"../util/resultelement":156}],107:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SolveurParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
const observer_1 = require("../util/observer");
/**
 * Y = f(X)
 * On cherche la valeur de X pour laquelle Y vaut Ytarget
 */
class SolveurParams extends params_equation_1.ParamsEquation {
    /**
     * Nub to calculate, targetted result and searched parameter must be set through
     * Solveur properties, or .nubToCalculate(), .targettedResult() and .searchedParameter() setters
     * @param rYtarget value we want to obtain for Y
     * @param rXinit initial value f searched parameter; will be overwritten as soon
     *               as Solveur's property "searchedParam" is updated
     */
    constructor(rYtarget, rXinit) {
        super();
        this._observable = new observer_1.Observable();
        this._Xinit = new param_definition_1.ParamDefinition(this, "Xinit", param_domain_1.ParamDomainValue.ANY, undefined, rXinit);
        this._Ytarget = new param_definition_1.ParamDefinition(this, "Ytarget", param_domain_1.ParamDomainValue.ANY, undefined, rYtarget);
        this._Y = new param_definition_1.ParamDefinition(this, "Y", param_domain_1.ParamDomainValue.ANY, undefined, undefined, undefined, false);
        this.addParamDefinition(this._Xinit);
        this.addParamDefinition(this._Ytarget);
        this.addParamDefinition(this._Y);
    }
    get X() {
        return this._X;
    }
    setX(X) {
        // copy of real X, to be used by Dichotomie() that will look for its value
        this._X = new param_definition_1.ParamDefinition(this, "X", X.domain, X.unit, X.singleValue, undefined, false);
        // Update Xinit
        if (this._Xinit !== undefined) {
            this._Xinit.singleValue = X.singleValue;
            this._Xinit.setDomain(X.domain);
            this._Xinit.setUnit(X.unit);
        }
        // replace pointer for parameters iterator
        this.addParamDefinition(this._X, true);
        // notify Solveur so that it updates calculatedParam pointer
        this.notifyObservers({
            action: "XUpdated"
        }, this);
    }
    get Y() {
        return this._Y;
    }
    get Xinit() {
        return this._Xinit;
    }
    get Ytarget() {
        return this._Ytarget;
    }
    // interface IObservable
    /**
     * ajoute un observateur à la liste
     */
    addObserver(o) {
        this._observable.addObserver(o);
    }
    /**
     * supprime un observateur de la liste
     */
    removeObserver(o) {
        this._observable.removeObserver(o);
    }
    /**
     * notifie un événement aux observateurs
     */
    notifyObservers(data, sender) {
        this._observable.notifyObservers(data, sender);
    }
}
exports.SolveurParams = SolveurParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92,"../util/observer":154}],108:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dever = void 0;
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const session_settings_1 = require("../session_settings");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const parallel_structure_1 = require("./parallel_structure");
const structure_props_1 = require("./structure_props");
class Dever extends parallel_structure_1.ParallelStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Dever;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    getLoisAdmissibles() {
        return structure_props_1.loiAdmissiblesDever;
    }
    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer :
     *                 "Q", "Z1", "Z2"
     *                 ou { uid: "abcdef", symbol: "X" } avec "abcdef" l'index de l'ouvrage et "X" son paramètre
     * @param rInit Valeur initiale
     */
    Calc(sVarCalc, rInit) {
        if (["Q", "Z1"].includes(sVarCalc)) {
            this.bQcorrected = true;
        }
        else {
            this.bQcorrected = false;
        }
        this.bZcorrected = true;
        this.prms.Q.v = (this.prms.Q.V !== undefined && !isNaN(this.prms.Q.V)) ? this.prms.Q.V : 0;
        const r = super.Calc(sVarCalc, rInit);
        if (!r.ok) {
            return r;
        }
        const QT = this.prms.Q.V;
        // Vérifier que les cotes de radier des ouvrages ne sont pas sous la cote de fond du lit
        for (const s of this.structures) {
            if (s.prms.ZDV.v < this.prms.ZR.v) {
                const m = new message_1.Message(message_1.MessageCode.WARNING_DEVER_ZDV_INF_ZR);
                m.extraVar.number = String(s.findPositionInParent() + 1); // String to avoid decimals
                r.resultElement.addMessage(m);
            }
        }
        if (this.prms.Z1.v > this.prms.ZR.v) {
            // Vitesse dans le cours d'eau amont
            r.resultElement.values.V = QT / (this.prms.BR.v * (this.prms.Z1.v - this.prms.ZR.v));
            // Energie cinétique dans le cours d'eau amont
            r.resultElement.values.Ec = r.resultElement.values.V * r.resultElement.values.V / (2 * 9.81);
            // Calcul de Cv
            this.bZcorrected = false;
            const Q0 = super.CalcQ().vCalc;
            r.resultElement.values.Cv = (Q0 > 0) ? QT / Q0 : 1;
        }
        else {
            r.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_DEVER_ZR_SUP_Z1));
        }
        return r;
    }
    /**
     * Calcul de la somme des débits de chaque structure
     * @param iExcept Index de la structure à ne pas additionner (optionnel)
     */
    CalcQ(iExcept) {
        if (this.bQcorrected) {
            let r;
            let i = session_settings_1.SessionSettings.maxIterations;
            do {
                if (r === null || r === void 0 ? void 0 : r.ok) {
                    this.prms.Q.v = r.vCalc;
                }
                r = super.CalcQ(iExcept);
                i--;
            } while (i && Math.abs(r.vCalc - this.prms.Q.v) > session_settings_1.SessionSettings.precision);
            if (i === 0) {
                r = new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_DICHO_CONVERGE, { lastApproximation: r.vCalc }));
            }
            return r;
        }
        else {
            return super.CalcQ(iExcept);
        }
    }
    /**
     * Mise à jour de Z1, h1 pour tous les ouvrages
     */
    updateStructuresH1H2() {
        if (this.bZcorrected) {
            for (const structure of this.structures) {
                const rEc = (this.prms.Z1.v - this.prms.ZR.v > 0) ?
                    0.5 * this.prms.Q.v / ((this.prms.Z1.v - this.prms.ZR.v) * this.prms.BR.v * 9.81) : 0;
                structure.prms.Z1.v = this.prms.Z1.v + rEc;
                structure.prms.Z2.v = this.prms.Z2.v;
                structure.prms.update_h1h2();
            }
        }
        else {
            super.updateStructuresH1H2();
        }
    }
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.BR.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.ZR.calculability = param_definition_1.ParamCalculability.FREE;
        // On supprime Z2 de l'interface car régime dénoyé uniquement
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.FIXED;
        this.prms.Z2.visible = false;
    }
    setResultsUnits() {
        this._resultsUnits = {
            Ec: "m",
            V: "m/s"
        };
    }
    exposeResults() {
        this._resultsFamilies = {
            V: undefined,
            Ec: undefined,
            Cv: undefined
        };
    }
}
exports.Dever = Dever;

},{"../compute-node":5,"../param/param-definition":86,"../session_settings":105,"../util/message":151,"../util/result":155,"./parallel_structure":111,"./structure_props":126}],109:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeverParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const parallel_structure_params_1 = require("./parallel_structure_params");
/**
 * Common parameters of hydraulic structure equations
 */
class DeverParams extends parallel_structure_params_1.ParallelStructureParams {
    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit total (m3/s)
     * @param rZ1 Cote de l'eau amont (m)
     * @param rBR Largeur du cours d'eau amont (m)
     * @param rZR Cote du lit du cours d'eau amont (m)
     */
    constructor(rQ, rZ1, rBR, rZR) {
        super(rQ, rZ1, -Infinity);
        this.BR = new param_definition_1.ParamDefinition(this, "BR", param_domain_1.ParamDomainValue.ANY, "m", rBR, param_definition_1.ParamFamily.WIDTHS);
        this.addParamDefinition(this.BR);
        this.ZR = new param_definition_1.ParamDefinition(this, "ZR", param_domain_1.ParamDomainValue.ANY, "m", rZR, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZR);
        // hide fields
        this.Z2.visible = false;
        this.Z2.singleValue = -Infinity;
    }
}
exports.DeverParams = DeverParams;

},{"../param/param-definition":86,"../param/param-domain":87,"./parallel_structure_params":112}],110:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateStructure = void 0;
// Classes générales sur les structures
const rectangular_structure_params_1 = require("./rectangular_structure_params");
const structure_props_1 = require("./structure_props");
// Equations de débit
const structure_gate_cem88d_1 = require("./structure_gate_cem88d");
const structure_gate_cem88v_1 = require("./structure_gate_cem88v");
const structure_gate_cunge80_1 = require("./structure_gate_cunge80");
const structure_kivi_1 = require("./structure_kivi");
const structure_kivi_params_1 = require("./structure_kivi_params");
const structure_orifice_free_1 = require("./structure_orifice_free");
const structure_orifice_free_params_1 = require("./structure_orifice_free_params");
const structure_orifice_submerged_1 = require("./structure_orifice_submerged");
const structure_orifice_submerged_params_1 = require("./structure_orifice_submerged_params");
const structure_rectangular_orifice_free_1 = require("./structure_rectangular_orifice_free");
const structure_rectangular_orifice_submerged_1 = require("./structure_rectangular_orifice_submerged");
const structure_triangular_trunc_weir_1 = require("./structure_triangular_trunc_weir");
const structure_triangular_trunc_weir_params_1 = require("./structure_triangular_trunc_weir_params");
const structure_triangular_weir_1 = require("./structure_triangular_weir");
const structure_triangular_weir_broad_1 = require("./structure_triangular_weir_broad");
const structure_triangular_weir_params_1 = require("./structure_triangular_weir_params");
const structure_vanlev_larinier_1 = require("./structure_vanlev_larinier");
const structure_vanlev_params_1 = require("./structure_vanlev_params");
const structure_vanlev_villemonte_1 = require("./structure_vanlev_villemonte");
const structure_weir_cem88d_1 = require("./structure_weir_cem88d");
const structure_weir_cem88v_1 = require("./structure_weir_cem88v");
const structure_weir_cunge80_1 = require("./structure_weir_cunge80");
const structure_weir_free_1 = require("./structure_weir_free");
const structure_weir_submerged_1 = require("./structure_weir_submerged");
const structure_weir_submerged_larinier_1 = require("./structure_weir_submerged_larinier");
const structure_weir_villemonte_1 = require("./structure_weir_villemonte");
function CreateStructure(loiDebit, parentNub, dbg = false) {
    let ret;
    const oCd = { SeuilR: 0.4, VanneR: 0.6, VanneRS: 0.8, SeuilT: 1.36 };
    const rectStructPrms = new rectangular_structure_params_1.RectangularStructureParams(0, // Q
    100, // ZDV
    102, // Z1
    101.5, // Z2
    2, // L
    oCd.SeuilR, // Cd de seuil par défaut
    0.5 // W (pour les seuils réglé automatiquement à infinity par w.visible = false)
    );
    const vanLevPrms = new structure_vanlev_params_1.StructureVanLevParams(0, 100, 102, 101.5, 0.4, 0.4, 0.5, 99.5, 100.5);
    // Cd pour une vanne rectangulaire
    rectStructPrms.CdGR.singleValue = oCd.VanneR;
    rectStructPrms.CdGRS.singleValue = oCd.VanneRS;
    // Instanciation of the equation
    switch (loiDebit) {
        case structure_props_1.LoiDebit.WeirCem88d:
            ret = new structure_weir_cem88d_1.StructureWeirCem88d(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.WeirCem88v:
            ret = new structure_weir_cem88v_1.StructureWeirCem88v(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.GateCem88d:
            ret = new structure_gate_cem88d_1.StructureGateCem88d(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.GateCem88v:
            ret = new structure_gate_cem88v_1.StructureGateCem88v(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.GateCunge80:
            ret = new structure_gate_cunge80_1.StructureGateCunge80(rectStructPrms, dbg);
            rectStructPrms.CdCunge.singleValue = 1;
            break;
        case structure_props_1.LoiDebit.WeirCunge80:
            ret = new structure_weir_cunge80_1.StructureWeirCunge80(rectStructPrms, dbg);
            rectStructPrms.CdCunge.singleValue = 1;
            break;
        case structure_props_1.LoiDebit.RectangularOrificeFree:
            ret = new structure_rectangular_orifice_free_1.StructureRectangularOrificeFree(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.RectangularOrificeSubmerged:
            ret = new structure_rectangular_orifice_submerged_1.StructureRectangularOrificeSubmerged(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.WeirFree:
            ret = new structure_weir_free_1.StructureWeirFree(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.WeirSubmergedLarinier:
            rectStructPrms.L.singleValue = 0.2;
            rectStructPrms.CdWSL.singleValue = 0.75;
            rectStructPrms.ZDV.singleValue = 101;
            rectStructPrms.h1.singleValue = 1;
            ret = new structure_weir_submerged_larinier_1.StructureWeirSubmergedLarinier(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.WeirSubmerged:
            rectStructPrms.CdWS.singleValue = 0.9;
            ret = new structure_weir_submerged_1.StructureWeirSubmerged(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.KIVI:
            const structKiviPrm = new structure_kivi_params_1.StructureKiviParams(8.516, // Q
            101.5, // ZDV
            103, // Z1
            102, // Z2
            2, // L
            oCd.SeuilR, // alpha
            0.001, // béta
            100); // ZRAM : cote Radier Amont
            structKiviPrm.h1.singleValue = 1.5;
            ret = new structure_kivi_1.StructureKivi(structKiviPrm, dbg);
            break;
        case structure_props_1.LoiDebit.TriangularWeirFree:
        case structure_props_1.LoiDebit.TriangularWeirBroad:
            const structTriangPrms = new structure_triangular_weir_params_1.TriangularStructureParams(0, // Q
            100, // ZDV
            102, // Z1
            100, // Z2
            45, // Alpha2
            oCd.SeuilT // Cd pour un seuil triangulaire
            // W = Infinity par défaut pour un seuil
            );
            if (loiDebit === structure_props_1.LoiDebit.TriangularWeirFree) {
                ret = new structure_triangular_weir_1.StructureTriangularWeir(structTriangPrms, dbg);
            }
            else {
                ret = new structure_triangular_weir_broad_1.StructureTriangularWeirBroad(structTriangPrms, dbg);
            }
            break;
        case structure_props_1.LoiDebit.TriangularTruncWeirFree:
            const structTriTruncPrms = new structure_triangular_trunc_weir_params_1.TriangularTruncStructureParams(0, // Q
            100.1, // ZDV
            102, // Z1
            100.1, // Z2
            0.9, // BT
            101, // ZT
            oCd.SeuilT // Cd pour un seuil triangulaire
            // W = Infinity par défaut pour un seuil
            );
            ret = new structure_triangular_trunc_weir_1.StructureTriangularTruncWeirFree(structTriTruncPrms, dbg);
            break;
        case structure_props_1.LoiDebit.OrificeSubmerged:
            ret = new structure_orifice_submerged_1.StructureOrificeSubmerged(new structure_orifice_submerged_params_1.StructureOrificeSubmergedParams(0, // Q
            102, // Z1
            101.5, // Z2
            0.7, // Cd
            0.1 // S
            ), dbg);
            break;
        case structure_props_1.LoiDebit.OrificeFree:
            ret = new structure_orifice_free_1.StructureOrificeFree(new structure_orifice_free_params_1.StructureOrificeFreeParams(0, // Q
            102, // Z1
            101.5, // Z2
            0.7, // Cd
            0.1, // S
            101 // Zco
            ), dbg);
            break;
        case structure_props_1.LoiDebit.WeirVillemonte:
            ret = new structure_weir_villemonte_1.StructureWeirVillemonte(rectStructPrms, dbg);
            break;
        case structure_props_1.LoiDebit.VanLevVillemonte:
            ret = new structure_vanlev_villemonte_1.StructureVanLevVillemonte(vanLevPrms, dbg);
            break;
        case structure_props_1.LoiDebit.VanLevLarinier:
            vanLevPrms.CdWSL.singleValue = 0.75;
            ret = new structure_vanlev_larinier_1.StructureVanLevLarinier(vanLevPrms, dbg);
            break;
        default:
            throw new Error(`type de LoiDebit ${structure_props_1.LoiDebit[loiDebit]} non pris en charge`);
    }
    // set reference to parent
    if (parentNub) {
        ret.parent = parentNub;
        // Set Structure Type
        ret.properties.setPropValue("structureType", structure_props_1.StructureProperties.findCompatibleStructure(loiDebit, parentNub));
    }
    return ret;
}
exports.CreateStructure = CreateStructure;

},{"./rectangular_structure_params":114,"./structure_gate_cem88d":116,"./structure_gate_cem88v":117,"./structure_gate_cunge80":118,"./structure_kivi":119,"./structure_kivi_params":120,"./structure_orifice_free":121,"./structure_orifice_free_params":122,"./structure_orifice_submerged":123,"./structure_orifice_submerged_params":124,"./structure_props":126,"./structure_rectangular_orifice_free":127,"./structure_rectangular_orifice_submerged":128,"./structure_triangular_trunc_weir":129,"./structure_triangular_trunc_weir_params":130,"./structure_triangular_weir":131,"./structure_triangular_weir_broad":132,"./structure_triangular_weir_params":134,"./structure_vanlev_larinier":135,"./structure_vanlev_params":136,"./structure_vanlev_villemonte":137,"./structure_weir_cem88d":138,"./structure_weir_cem88v":139,"./structure_weir_cunge80":140,"./structure_weir_free":141,"./structure_weir_submerged":142,"./structure_weir_submerged_larinier":143,"./structure_weir_villemonte":144}],111:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParallelStructure = void 0;
const compute_node_1 = require("../compute-node");
const index_1 = require("../index");
const nub_1 = require("../nub");
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
const message_1 = require("../util/message");
/**
 * Calcul de une ou plusieurs structures hydrauliques en parallèles
 * reliées par les cotes amont et aval et dont le débit est égal à la
 * somme des débits de chaque structure.
 */
class ParallelStructure extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.ParallelStructure;
        this._childrenType = "Ouvrage";
    }
    /** children casting */
    get structures() {
        return this._children;
    }
    set structures(structures) {
        this._children = structures;
        this._children.forEach((s) => {
            s.parent = this;
        });
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /** Returns admissible LoiDebit grouped by StructureType */
    getLoisAdmissibles() {
        return structure_props_1.loiAdmissiblesOuvrages;
    }
    /** Returns a flat array of all admissible LoiDebit */
    getLoisAdmissiblesArray() {
        let loisAdm = [];
        const lois = this.getLoisAdmissibles();
        // tslint:disable-next-line:forin
        for (const k in lois) {
            loisAdm = loisAdm.concat(lois[k]);
        }
        return loisAdm;
    }
    /** Returns the 1st deep item from LoisAdmissibles */
    getDefaultLoiDebit() {
        const lois = this.getLoisAdmissibles();
        return lois[Object.keys(lois)[0]][0];
    }
    /**
     * Effectue une série de calculs sur un paramètre; déclenche le calcul en chaîne
     * des modules en amont si nécessaire
     * Surcharge pour les tests préalables liés à la structure du nub
     * @param rInit solution approximative du paramètre
     */
    CalcSerie(rInit) {
        if (this.structures.length === 0) {
            this._result = new result_1.Result(undefined, this);
            this._result.globalLog.insert(new message_1.Message(message_1.MessageCode.ERROR_STRUCTURE_AU_MOINS_UNE));
            return this._result;
        }
        return super.CalcSerie(rInit);
    }
    /**
     * Calcul du débit des structures en parallèle (sans détail pour chaque structure)
     * @param sVarCalc Variable à calculer (Q uniquement)
     */
    Equation(sVarCalc) {
        structure_1.Structure.CheckEquation(sVarCalc);
        return this.CalcQ();
    }
    /**
     * Calcul de la somme des débits de chaque structure
     * @param iExcept Index de la structure à ne pas additionner (optionnel)
     */
    CalcQ(iExcept) {
        if (iExcept !== undefined) {
            if (iExcept < 0 || iExcept >= this._children.length) {
                throw new Error("ParallelStructure.CalcQ iExcept not in [0;" + (this._children.length - 1) + "]");
            }
        }
        this.updateStructuresH1H2();
        const calcRes = new result_1.Result(0, this);
        let qTot = 0;
        for (let i = 0; i < this._children.length; i++) {
            if (i !== iExcept) {
                const res = this._children[i].Calc("Q");
                qTot += res.vCalc;
                // merge logs
                calcRes.resultElement.log.addLog(res.log);
            }
        }
        // Assigne le débit total dans le résultat
        calcRes.resultElement.vCalc = qTot;
        return calcRes;
    }
    /**
     * Calcul du débit total, de la cote amont ou aval ou d'un paramètre d'une structure
     * @param sVarCalc Nom du paramètre à calculer :
     *                 "Q", "Z1", "Z2"
     *                 ou { uid: "abcdef", symbol: "X" } avec "abcdef" l'index de l'ouvrage et "X" son paramètre
     * @param rInit Valeur initiale
     */
    Calc(sVarCalc, rInit) {
        // if Calc() is called outside of CalcSerie(), _result might not be initialized
        if (!this.result) {
            this.initNewResultElement();
        }
        switch (sVarCalc) {
            case "Z1":
            case "Z2":
            case "Q":
                this.currentResult = super.Calc(sVarCalc, rInit);
                if (this.result.ok) {
                    this.getParameter(sVarCalc).v = this.result.resultElement.vCalc;
                }
                break;
            default:
                if (typeof sVarCalc === "string") {
                    throw new Error("ParallelStructures.Calc() : unknow parameter to calculate " + sVarCalc);
                }
                // Pour les caractéristiques des ouvrages
                const structureIndex = this.getIndexForChild(sVarCalc.uid);
                const r = this.CalcStructPrm(structureIndex, sVarCalc.symbol, rInit);
                // whether r is .ok() or not, just copy vCalc and logs to avoid
                // this._result being polluted by structure.flagsNull
                this.result.symbol = r.symbol;
                this.result.vCalc = r.vCalc;
                // merge logs
                this.result.log.addLog(r.log);
                this.result.globalLog.addLog(r.globalLog);
        }
        return this.result;
    }
    /**
     * Once session is loaded, run a second pass on all linked parameters to
     * reset their target if needed
     */
    fixLinks(obj) {
        // return value
        const ret = {
            hasErrors: false
        };
        // iterate over parameters
        super.fixLinks(obj);
        // iterate over children if any
        if (obj.children && Array.isArray(obj.children)) {
            for (const s of obj.children) {
                // get the Nub
                const subNub = index_1.Session.getInstance().findNubByUid(s.uid);
                const res = subNub.fixLinks(s);
                // forward errors
                if (res.hasErrors) {
                    ret.hasErrors = true;
                }
            }
        }
        return ret;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    /**
     * Mise à jour de Z1, Z2, h1 et h2 pour tous les ouvrages
     */
    updateStructuresH1H2() {
        for (const structure of this.structures) {
            structure.prms.Z1.v = this.prms.Z1.v;
            structure.prms.Z2.v = this.prms.Z2.v;
            structure.prms.update_h1h2();
        }
    }
    /**
     * Calcul du paramètre d'un des ouvrages en parallèle
     * @param sVC Index de l'ouvrage et paramètre à calculer
     * @param rInit Valeur initiale
     */
    CalcStructPrm(index, symbol, rInit) {
        // Le débit restant sur la structure en calcul est :
        this.structures[index].prms.Q.v = this.prms.Q.v - this.CalcQ(index).vCalc;
        // Calcul du paramètre de la structure en calcul
        const r = this.structures[index].Calc(symbol, rInit);
        this.structures[index].result.values.Q = this.structures[index].prms.Q.v;
        return r;
    }
}
exports.ParallelStructure = ParallelStructure;

},{"../compute-node":5,"../index":14,"../nub":33,"../param/param-definition":86,"../util/message":151,"../util/result":155,"./structure":115,"./structure_props":126}],112:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParallelStructureParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * Common parameters of hydraulic structure equations
 */
class ParallelStructureParams extends params_equation_1.ParamsEquation {
    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit total (m3/s)
     * @param rZ1 Cote de l'eau amont (m)
     * @param rZ2 Cote de l'eau aval (m)
     */
    constructor(rQ, rZ1, rZ2) {
        super();
        this.Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.ANY, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS);
        this.addParamDefinition(this.Q);
        this.Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.Z1);
        this.Z2 = new param_definition_1.ParamDefinition(this, "Z2", param_domain_1.ParamDomainValue.ANY, "m", rZ2, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.Z2);
    }
}
exports.ParallelStructureParams = ParallelStructureParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],113:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RectangularStructure = void 0;
const param_definition_1 = require("../param/param-definition");
const structure_1 = require("./structure");
/**
 * Classe mère pour toutes les structures ayant une base rectangulaire (vannes, seuils)
 */
class RectangularStructure extends structure_1.Structure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this.prms.CdGR.visible = false;
        this.prms.CdGRS.visible = false;
        this.prms.CdWR.visible = false;
        this.prms.CdWS.visible = false;
        this.prms.CdWSL.visible = false;
        this.prms.CdCunge.visible = false;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.L.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.CdGR.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.CdGRS.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.CdWR.calculability = param_definition_1.ParamCalculability.DICHO;
    }
}
exports.RectangularStructure = RectangularStructure;

},{"../param/param-definition":86,"./structure":115}],114:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RectangularStructureParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const structure_params_1 = require("./structure_params");
/**
 * Parameters for rectangular structures (common for all rectangular structure equations)
 */
class RectangularStructureParams extends structure_params_1.StructureParams {
    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZDV  Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rL    Largeur de la vanne ou du déversoir (m)
     * @param rCd   Coefficient de débit (-)
     * @param rW    Ouverture de la vanne (m) (Valeur par défaut +infinity pour les déversoirs)
     */
    constructor(rQ, rZDV, rZ1, rZ2, rL, rCd, rW = Infinity) {
        super(rQ, rZDV, rZ1, rZ2, rW);
        this.L = new param_definition_1.ParamDefinition(this, "L", param_domain_1.ParamDomainValue.POS, "m", rL, param_definition_1.ParamFamily.WIDTHS);
        this.addParamDefinition(this.L);
        const domainCd = new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 10);
        this.CdGR = new param_definition_1.ParamDefinition(this, "CdGR", domainCd, undefined, rCd);
        this.addParamDefinition(this.CdGR);
        this.CdGRS = new param_definition_1.ParamDefinition(this, "CdGRS", domainCd, undefined, rCd);
        this.addParamDefinition(this.CdGRS);
        this.CdWR = new param_definition_1.ParamDefinition(this, "CdWR", domainCd, undefined, rCd);
        this.addParamDefinition(this.CdWR);
        this.CdWSL = new param_definition_1.ParamDefinition(this, "CdWSL", domainCd, undefined, rCd);
        this.addParamDefinition(this.CdWSL);
        this.CdWS = new param_definition_1.ParamDefinition(this, "CdWS", domainCd, undefined, rCd);
        this.addParamDefinition(this.CdWS);
        this.CdCunge = new param_definition_1.ParamDefinition(this, "CdCunge", domainCd, undefined, rCd);
        this.addParamDefinition(this.CdCunge);
    }
}
exports.RectangularStructureParams = RectangularStructureParams;

},{"../param/param-definition":86,"../param/param-domain":87,"./structure_params":125}],115:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Structure = exports.StructureJetType = exports.StructureFlowRegime = exports.StructureFlowMode = void 0;
const child_nub_1 = require("../child_nub");
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const structure_props_1 = require("./structure_props");
/**
 * Flow mode: weir or orifice flow
 */
var StructureFlowMode;
(function (StructureFlowMode) {
    /** Weir flow */
    StructureFlowMode[StructureFlowMode["WEIR"] = 0] = "WEIR";
    /** Orifice flow */
    StructureFlowMode[StructureFlowMode["ORIFICE"] = 1] = "ORIFICE";
    /** Zéro flow */
    StructureFlowMode[StructureFlowMode["NULL"] = 2] = "NULL";
})(StructureFlowMode = exports.StructureFlowMode || (exports.StructureFlowMode = {}));
/**
 * Flow regime: free flow, partially submerged or submerged
 */
var StructureFlowRegime;
(function (StructureFlowRegime) {
    /** Free flow (unsubmerged) */
    StructureFlowRegime[StructureFlowRegime["FREE"] = 0] = "FREE";
    /** Partially submerged flow */
    StructureFlowRegime[StructureFlowRegime["PARTIAL"] = 1] = "PARTIAL";
    /** Submerged flow */
    StructureFlowRegime[StructureFlowRegime["SUBMERGED"] = 2] = "SUBMERGED";
    /** Zéro flow */
    StructureFlowRegime[StructureFlowRegime["NULL"] = 3] = "NULL";
})(StructureFlowRegime = exports.StructureFlowRegime || (exports.StructureFlowRegime = {}));
/** Type de jet : Sans objet (orifice), plongeant, de surface */
var StructureJetType;
(function (StructureJetType) {
    /** Plongeant */
    StructureJetType[StructureJetType["PLONGEANT"] = 0] = "PLONGEANT";
    /** De surface */
    StructureJetType[StructureJetType["SURFACE"] = 1] = "SURFACE";
    /** Sans objet (orifice) */
    StructureJetType[StructureJetType["SO"] = 2] = "SO";
})(StructureJetType = exports.StructureJetType || (exports.StructureJetType = {}));
/**
 * classe de calcul sur la conduite distributrice
 */
class Structure extends child_nub_1.ChildNub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Structure;
        this._isZDVcalculable = true;
        // Q is always the only calculated variable; setting another parameter
        // of a Structure to CALC mode makes it the calculated variable of the
        // *parent* ParallelStructures
        this.calculatedParam = this.prms.Q;
    }
    /**
     * Test générique si VarCalc="Q" pour l'utilisation de Equation
     */
    static CheckEquation(sVarCalc) {
        if (sVarCalc !== "Q") {
            throw new Error("Structure.Equation() : invalid parameter name " + sVarCalc);
        }
    }
    /** Returns Props object (observable set of key-values) associated to this Nub */
    get properties() {
        // completes props with calcType, structureType and loiDebit if not already set
        this._props.setPropValue("calcType", this.calcType);
        if (this._props.getPropValue("loiDebit") === undefined) {
            this._props.setPropValue("loiDebit", this._loiDebit);
        }
        if (this._props.getPropValue("structureType") === undefined) {
            this._props.setPropValue("structureType", structure_props_1.StructureProperties.findCompatibleStructure(this._props.getPropValue("loiDebit"), this.parent));
        }
        return this._props;
    }
    // setter is not inherited from Nub if getter is redefined :/
    set properties(props) {
        super.setProperties(props);
    }
    get isZDVcalculable() {
        return this._isZDVcalculable;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    get W() {
        if (this.prms.W.visible) {
            return this.prms.W.v;
        }
        else {
            return Infinity;
        }
    }
    get loiDebit() {
        return this._loiDebit;
    }
    /**
     * Returns the nth visible parameter (used in nghyd/PabTable)
     */
    getNthVisibleParam(n) {
        let i = 0;
        for (const p of this.parameterIterator) {
            if (p.visible) {
                if (n === i) {
                    return p;
                }
                i++;
            }
        }
        return undefined;
    }
    /**
     * Calcul d'une équation quelle que soit l'inconnue à calculer.
     * Gestion du débit nul et de l'inversion de débit
     * @param sVarCalc nom de la variable à calculer
     * @param rInit valeur initiale de la variable à calculer dans le cas de la dichotomie
     */
    Calc(sVarCalc, rInit) {
        // Gestion de l'exception de calcul de W sur les seuils
        if (rInit === undefined) {
            rInit = this.getParameter(sVarCalc).v;
        }
        if (sVarCalc === "W" && rInit === Infinity) {
            throw new Error("Structure:Calc : Calcul de W impossible sur un seuil");
        }
        // Gestion de l'erreur de calcul de ZDV quand il n'est pas calculable
        if (sVarCalc === "ZDV" && !this.isZDVcalculable) {
            return new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_STRUCTURE_ZDV_PAS_CALCULABLE));
        }
        this.prms.update_h1h2();
        // Gestion du débit nul
        const flagsNull = {
            ENUM_StructureFlowMode: StructureFlowMode.NULL,
            ENUM_StructureFlowRegime: StructureFlowRegime.NULL,
            ENUM_StructureJetType: StructureJetType.SO
        };
        if (sVarCalc === "Q") {
            if (this.prms.h1.v <= 1E-20 || Math.abs(this.prms.h1.v - this.prms.h2.v) < 1E-20 || this.W <= 1E-20) {
                this.currentResult = new result_1.Result(0, this, flagsNull);
                return this._result;
            }
        }
        else if (this.prms.Q.v === 0) {
            // Débit nul <=> tirant d'eau amont = tirant d'eau aval ou tout autre paramètre nul
            switch (sVarCalc) {
                case "Z1":
                    this.currentResult = new result_1.Result(this.prms.Z2.v, this, flagsNull);
                    return this._result;
                case "Z2":
                    this.currentResult = new result_1.Result(this.prms.Z1.v, this, flagsNull);
                    return this._result;
                default:
                    // Est-ce toujours vrai ? Nécessitera peut-être d'étendre la méthode
                    this.currentResult = new result_1.Result(0, this, flagsNull);
                    return this._result;
            }
        }
        else if (this.W === 0 && sVarCalc === "Z1") {
            // Si la vanne est fermée la cote amont est infinie
            this.currentResult = new result_1.Result(Infinity, this, flagsNull);
            return this._result;
        }
        // Gestion du cas d'écoulement impossible Z1 > Z2 et Q <= 0
        if (!(sVarCalc === "Q" || sVarCalc === "Z1" || sVarCalc === "Z2")) {
            if ((this.prms.Z1.v >= this.prms.Z2.v && this.prms.Q.v <= 0) ||
                (this.prms.Z1.v <= this.prms.Z2.v && this.prms.Q.v >= 0)) {
                // On ferme l'ouvrage et on renvoie un code d'erreur
                let rPrm;
                switch (sVarCalc) {
                    case "ZDV":
                        rPrm = Math.max(this.prms.Z1.v, this.prms.Z2.v);
                        break;
                    default:
                        rPrm = 0;
                }
                let res;
                if (this.prms.Z1.v === this.prms.Z2.v && this.prms.Q.v !== 0) {
                    res = new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_STRUCTURE_Z_EGAUX_Q_NON_NUL), this);
                }
                else {
                    res = new result_1.Result(new message_1.Message(message_1.MessageCode.ERROR_STRUCTURE_Q_TROP_ELEVE), this, flagsNull);
                }
                res.vCalc = rPrm;
                this.currentResult = res;
                // "Les cotes et le débit ne sont pas cohérents => fermeture de l'ouvrage
                return res;
            }
        }
        // Gestion de l'inversion de débit : on inverse l'amont et l'aval pour le calcul
        if (sVarCalc !== "Q" && this.prms.Q.v < 0) {
            [this.prms.Z1.v, this.prms.Z2.v] = [this.prms.Z2.v, this.prms.Z1.v]; // Swap ES6 fashion
            const res = super.Calc(sVarCalc, rInit);
            [this.prms.Z1.v, this.prms.Z2.v] = [this.prms.Z2.v, this.prms.Z1.v]; // Swap ES6 fashion
            this.currentResult = res;
            return res;
        }
        // Calcul normal hors débit nul
        return super.Calc(sVarCalc, rInit);
    }
    /**
     * Equation preprocessing
     * @return true if inverted discharge
     */
    Equation(sVarCalc) {
        Structure.CheckEquation(sVarCalc);
        let res;
        let bInverted = false;
        if (this.prms.Z1.v < this.prms.Z2.v) {
            [this.prms.Z1.v, this.prms.Z2.v] = [this.prms.Z2.v, this.prms.Z1.v]; // Swap ES6 fashion
            bInverted = true;
        }
        this.prms.update_h1h2();
        res = this.CalcQ();
        if (bInverted) {
            if (sVarCalc === "Q") {
                res.vCalc = -res.vCalc;
            }
            [this.prms.Z1.v, this.prms.Z2.v] = [this.prms.Z2.v, this.prms.Z1.v]; // Swap ES6 fashion
        }
        return res;
    }
    getResultData() {
        return {
            ENUM_StructureFlowMode: this.getFlowMode(),
            ENUM_StructureFlowRegime: this.getFlowRegime(),
            ENUM_StructureJetType: this.getJetType()
        };
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        this.prms.Q.calculability = param_definition_1.ParamCalculability.EQUATION;
        this.prms.ZDV.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Z1.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Z2.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.h1.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.h2.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.W.calculability = param_definition_1.ParamCalculability.FIXED;
    }
    /**
     * Give the flow mode : weir or orifice flow
     */
    getFlowMode() {
        if (this.prms.h1.v > this.W) {
            this.debug("Structure.getFlowMode(h1=" + this.prms.h1.v + ",W=" + this.W + ")=ORIFICE");
            return StructureFlowMode.ORIFICE;
        }
        else {
            this.debug("Structure.getFlowMode(h1=" + this.prms.h1.v + ",W=" + this.W + ")=WEIR");
            return StructureFlowMode.WEIR;
        }
    }
    /**
     * Give the flow regime for a rectangular section : free, partially submerged or submerged flow
     */
    getFlowRegime() {
        // Weir have only two flow regimes: free and submerged flow
        // Orifice have three flow regimes: free, partially submerged and (totally) submerged
        if (this.prms.h2.v <= 2 / 3 * this.prms.h1.v) {
            // free flow for both weirs and orifices
            this.debug("Structure.getFlowRegime(h1="
                + this.prms.h1.v + ",h2=" + this.prms.h2.v
                + ",W=" + this.W + ")=FREE");
            return StructureFlowRegime.FREE;
        }
        else if (this.prms.h1.v > this.W && this.prms.h2.v < (2 * this.prms.h1.v + this.W) / 3) {
            // Partially submerged only for orifices
            this.debug("Structure.getFlowRegime(h1="
                + this.prms.h1.v + ",h2=" + this.prms.h2.v
                + ",W=" + this.W + ")=PARTIAL");
            return StructureFlowRegime.PARTIAL;
        }
        else {
            // (Totally) submerged for both weirs and orifices
            this.debug("Structure.getFlowRegime(h1=" + this.prms.h1.v
                + ",h2=" + this.prms.h2.v + ",W=" + this.W + ")=SUBMERGED");
            return StructureFlowRegime.SUBMERGED;
        }
    }
    /**
     * Give the Jet Type for weir flow
     * Cf. Baudoin J.M., Burgun V., Chanseau M., Larinier M., Ovidio M., Sremski W., Steinbach P. et Voegtle B., 2014.
     * Evaluer le franchissement des obstacles par les poissons. Principes et méthodes. Onema. 200 pages
     */
    getJetType() {
        if (this.getFlowMode() === StructureFlowMode.WEIR) {
            if (Math.abs(this.prms.h1.v - this.prms.h2.v) < 0.5 * this.prms.h1.v) {
                return StructureJetType.SURFACE;
            }
            else {
                return StructureJetType.PLONGEANT;
            }
        }
        else {
            return StructureJetType.SO;
        }
    }
    exposeResults() {
        this._resultsFamilies = {
            Q: param_definition_1.ParamFamily.FLOWS
        };
    }
    getFirstAnalyticalParameter() {
        return this.prms.Q;
    }
}
exports.Structure = Structure;
/** Constante utile : Racine de 2g */
Structure.R2G = Math.sqrt(2 * 9.81);

},{"../child_nub":4,"../compute-node":5,"../param/param-definition":86,"../util/message":151,"../util/result":155,"./structure_props":126}],116:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureGateCem88d = void 0;
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const rectangular_structure_1 = require("./rectangular_structure");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
/**
 * Equation CEM88D : déversoir / orifice (pelle importante) Cemagref 1988
 */
class StructureGateCem88d extends rectangular_structure_1.RectangularStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.GateCem88d;
        this._isZDVcalculable = false;
        this.prms.W.visible = true;
        this.prms.CdWR.visible = true;
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) CEM88D
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const data = this.getResultData();
        let v;
        const cd = this.prms.CdWR.v * this.prms.L.v * structure_1.Structure.R2G;
        const b1 = Math.sqrt(this.prms.h1.v);
        const b2 = Math.sqrt(this.prms.h1.v - this.prms.h2.v);
        const cd1 = cd * 2.5981; // cd * 3*sqrt(3)/2
        this.debug("StructureWeirCem88d.Equation b1=" + b1 + " b2=" + b2 + " cd1=" + cd1);
        switch (data.ENUM_StructureFlowMode) {
            case structure_1.StructureFlowMode.WEIR:
                switch (data.ENUM_StructureFlowRegime) {
                    case structure_1.StructureFlowRegime.FREE:
                        v = cd * this.prms.h1.v * b1;
                        break;
                    case structure_1.StructureFlowRegime.SUBMERGED:
                        v = cd1 * this.prms.h2.v * b2;
                        this.debug("StructureWeirCem88d.Equation WEIR SUBMERGED Q=" + v);
                        break;
                }
                break;
            case structure_1.StructureFlowMode.ORIFICE:
                const b3 = Math.pow(this.prms.h1.v - this.W, 1.5);
                switch (data.ENUM_StructureFlowRegime) {
                    case structure_1.StructureFlowRegime.FREE:
                        v = cd * (this.prms.h1.v * b1 - b3);
                        break;
                    case structure_1.StructureFlowRegime.PARTIAL:
                        v = cd1 * b2 * this.prms.h2.v - cd * b3;
                        break;
                    case structure_1.StructureFlowRegime.SUBMERGED:
                        v = cd1 * b2 * this.W;
                        this.debug("StructureWeirCem88d.Equation ORIFICE SUBMERGED Q=" + v);
                        break;
                }
        }
        this.debug("StructureWeirCem88d.Equation(h1=" + this.prms.h1.v + ",h2="
            + this.prms.h2.v + ",W=" + this.W + ") => Q=" + v);
        return new result_1.Result(v, this, data);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = param_definition_1.ParamCalculability.DICHO;
    }
}
exports.StructureGateCem88d = StructureGateCem88d;

},{"../param/param-definition":86,"../util/result":155,"./rectangular_structure":113,"./structure":115,"./structure_props":126}],117:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureGateCem88v = void 0;
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const rectangular_structure_1 = require("./rectangular_structure");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
/**
 * Equation CEM88V : déversoir / vanne de fond (pelle faible) Cemagref 1988
 */
class StructureGateCem88v extends rectangular_structure_1.RectangularStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.GateCem88v;
        this.prms.W.visible = true;
        this.prms.CdGR.visible = true;
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) CEM88V
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const data = this.getResultData();
        let v;
        const mu0 = 2 / 3 * this.prms.CdGR.v;
        let KF;
        if (data.ENUM_StructureFlowRegime !== structure_1.StructureFlowRegime.FREE) {
            KF = this.getKF(Math.sqrt(1 - this.prms.h2.v / this.prms.h1.v), this.getAlfa(this.prms.h2.v));
        }
        switch (data.ENUM_StructureFlowMode) {
            case structure_1.StructureFlowMode.WEIR:
                const muf = mu0 - 0.08;
                switch (data.ENUM_StructureFlowRegime) {
                    case structure_1.StructureFlowRegime.FREE:
                        v = muf * this.prms.L.v * structure_1.Structure.R2G * Math.pow(this.prms.h1.v, 1.5);
                        break;
                    case structure_1.StructureFlowRegime.SUBMERGED:
                        // console.log("KF="+KF+" muf="+muf);
                        v = KF * muf * this.prms.L.v * structure_1.Structure.R2G * Math.pow(this.prms.h1.v, 1.5);
                        break;
                }
                break;
            case structure_1.StructureFlowMode.ORIFICE:
                const mu = mu0 - 0.08 / (this.prms.h1.v / this.W);
                const mu1 = mu0 - 0.08 / (this.prms.h1.v / this.W - 1);
                if (data.ENUM_StructureFlowRegime === structure_1.StructureFlowRegime.FREE) {
                    v = this.prms.L.v * structure_1.Structure.R2G
                        * (mu * Math.pow(this.prms.h1.v, 1.5)
                            - mu1 * Math.pow(this.prms.h1.v - this.W, 1.5));
                }
                else {
                    if (data.ENUM_StructureFlowRegime === structure_1.StructureFlowRegime.PARTIAL) {
                        v = this.prms.L.v * structure_1.Structure.R2G * (KF * mu * Math.pow(this.prms.h1.v, 1.5)
                            - mu1 * Math.pow(this.prms.h1.v - this.W, 1.5));
                    }
                    else {
                        const KF1 = this.getKF(Math.sqrt(1 - (this.prms.h2.v - this.W) / (this.prms.h1.v - this.W)), this.getAlfa(this.prms.h2.v - this.W));
                        v = this.prms.L.v * structure_1.Structure.R2G * (KF * mu * Math.pow(this.prms.h1.v, 1.5)
                            - KF1 * mu1 * Math.pow(this.prms.h1.v - this.W, 1.5));
                    }
                }
        }
        this.debug("StructureWeirCem88v.Equation(h1=" + this.prms.h1.v
            + ",h2=" + this.prms.h2.v + ",W=" + this.W + ") => Q=" + v);
        return new result_1.Result(v, this, data);
    }
    /**
     * Give the flow regime for Equation CEM88V : free, partially submerged or submerged flow
     */
    getFlowRegime() {
        const mode = this.getFlowMode();
        // Weir have only two flow regimes: free and submerged flow
        let alfa;
        switch (mode) {
            case structure_1.StructureFlowMode.WEIR:
                alfa = 0.75;
                break;
            case structure_1.StructureFlowMode.ORIFICE:
                alfa = this.getAlfa(this.prms.h2.v);
                break;
        }
        if (this.prms.h2.v <= alfa * this.prms.h1.v) {
            this.debug("StructureWeirCem88v.getFlowRegime(h1=" + this.prms.h1.v
                + ",h2=" + this.prms.h2.v + ",W=" + this.W + ")=FREE");
            return structure_1.StructureFlowRegime.FREE;
        }
        else {
            alfa = this.getAlfa(this.prms.h2.v - this.W);
            if (mode === structure_1.StructureFlowMode.ORIFICE
                && this.prms.h2.v <= alfa * this.prms.h1.v + (1 - alfa) * this.W) {
                this.debug("StructureWeirCem88v.getFlowRegime(h1=" + this.prms.h1.v
                    + ",h2=" + this.prms.h2.v + ",W=" + this.W + ")=PARTIAL");
                return structure_1.StructureFlowRegime.PARTIAL;
            }
            else {
                this.debug("StructureWeirCem88v.getFlowRegime(h1=" + this.prms.h1.v
                    + ",h2=" + this.prms.h2.v + ",W=" + this.W + ")=SUBMERGED");
                return structure_1.StructureFlowRegime.SUBMERGED;
            }
        }
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = param_definition_1.ParamCalculability.DICHO;
    }
    getAlfa(h2) {
        let alfa = 1 - 0.14 * (h2) / this.W;
        alfa = Math.min(Math.max(alfa, 0.4), 0.75);
        // console.log("alfa=" + alfa);
        return alfa;
    }
    getKF(x, alfa) {
        const beta1 = -2 * alfa + 2.6;
        let KF;
        if (x > 0.2) {
            KF = 1 - Math.pow(1 - x / Math.sqrt(1 - alfa), beta1);
        }
        else {
            KF = 5 * x * (1 - Math.pow(1 - 0.2 / Math.sqrt(1 - alfa), beta1));
        }
        // console.log("KF(x="+x+")="+KF);
        return KF;
    }
}
exports.StructureGateCem88v = StructureGateCem88v;

},{"../param/param-definition":86,"../util/result":155,"./rectangular_structure":113,"./structure":115,"./structure_props":126}],118:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureGateCunge80 = void 0;
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const rectangular_structure_1 = require("./rectangular_structure");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
/**
 * Equation Cunge80
 */
class StructureGateCunge80 extends rectangular_structure_1.RectangularStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.GateCunge80;
        if (prms.W.v !== Infinity) {
            this._isZDVcalculable = false;
        }
        this.prms.W.visible = true;
        this.prms.CdCunge.visible = true;
    }
    /**
     * Calcul du débit avec l'équation Cunge80
     * @param sVarCalc Variable à calculer (doit être égale à Q ici)
     */
    CalcQ() {
        const data = this.getResultData();
        let v;
        switch (data.ENUM_StructureFlowRegime) {
            case structure_1.StructureFlowRegime.FREE:
                if (data.ENUM_StructureFlowMode === structure_1.StructureFlowMode.WEIR) {
                    const R32 = 3 * Math.sqrt(3) / 2;
                    v = this.prms.CdCunge.v * this.prms.L.v * structure_1.Structure.R2G / R32 * Math.pow(this.prms.h1.v, 1.5);
                    this.debug("StructureCunge80.Equation WEIR FREE Q=" + v);
                }
                else {
                    // Cd from Henderson, F.M., 1966. Open channel flow. MacMillan, New York.
                    // tslint:disable-next-line:variable-name
                    const Cd = StructureGateCunge80.Cc / Math.sqrt(1 + StructureGateCunge80.Cc * this.W / this.prms.h1.v);
                    v = this.prms.CdCunge.v * Cd * this.prms.L.v * structure_1.Structure.R2G
                        * this.W * Math.pow(this.prms.h1.v, 0.5);
                    this.debug("StructureCunge80.Equation ORIFICE FREE Q=" + v);
                }
                break;
            case structure_1.StructureFlowRegime.SUBMERGED:
                if (data.ENUM_StructureFlowMode === structure_1.StructureFlowMode.WEIR) {
                    v = this.prms.CdCunge.v * this.prms.L.v * structure_1.Structure.R2G * this.prms.h2.v
                        * Math.sqrt(this.prms.h1.v - this.prms.h2.v);
                    this.debug("StructureCunge80.Equation WEIR SUBMERGED Q=" + v);
                }
                else {
                    v = this.prms.CdCunge.v * this.prms.L.v * structure_1.Structure.R2G
                        * this.W * Math.sqrt(this.prms.h1.v - this.prms.h2.v);
                    this.debug("StructureCunge80.Equation ORIFICE SUBMERGED Q=" + v);
                }
        }
        return new result_1.Result(v, this, data);
    }
    getFlowRegime() {
        if (this.prms.h1.v >= 1.5 * this.prms.h2.v) {
            return structure_1.StructureFlowRegime.FREE;
        }
        else {
            return structure_1.StructureFlowRegime.SUBMERGED;
        }
    }
    getFlowMode() {
        const regime = this.getFlowRegime();
        if (regime === structure_1.StructureFlowRegime.FREE) {
            if (this.W <= 2 / 3 * this.prms.h1.v) {
                return structure_1.StructureFlowMode.ORIFICE;
            }
            else {
                return structure_1.StructureFlowMode.WEIR;
            }
        }
        else {
            if (this.W <= this.prms.h2.v) {
                return structure_1.StructureFlowMode.ORIFICE;
            }
            else {
                return structure_1.StructureFlowMode.WEIR;
            }
        }
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.CdCunge.calculability = param_definition_1.ParamCalculability.DICHO;
    }
}
exports.StructureGateCunge80 = StructureGateCunge80;
/** Contraction coefficient on underflow gates From Henderson, F.M., 1966. Open channel flow. MacMillan, New York.  */
// tslint:disable-next-line:variable-name
StructureGateCunge80.Cc = 0.611;

},{"../param/param-definition":86,"../util/result":155,"./rectangular_structure":113,"./structure":115,"./structure_props":126}],119:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureKivi = void 0;
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
const villemonte_1 = require("./villemonte");
class StructureKivi extends structure_1.Structure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.KIVI;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    CalcQ() {
        const res = new result_1.Result(0, this, this.getResultData());
        // p : pelle
        let p = this.prms.ZDV.v - this.prms.ZRAM.v;
        let h1p;
        if (p < 0.1) {
            // - p ne doit pas être inférieur à 0,10 m  (Norme NF X10-311-1983)
            res.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_STRUCTUREKIVI_PELLE_TROP_FAIBLE));
            p = 0.1;
        }
        h1p = this.prms.h1.v / p;
        if (h1p > 2.5) {
            // - h/p ne doit pas être supérieur à 2,5 (Norme NF X10-311-1983)
            res.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_STRUCTUREKIVI_HP_TROP_ELEVE));
            h1p = 2.5;
        }
        const cd = this.prms.alpha.v + this.prms.beta.v * h1p;
        let Q = cd * this.prms.L.v * structure_1.Structure.R2G * Math.pow(this.prms.h1.v, 1.5);
        if (res.resultElement.values.ENUM_StructureFlowRegime === structure_1.StructureFlowRegime.SUBMERGED) {
            Q = villemonte_1.Villemonte(this.prms.h1.v, this.prms.h2.v, 1.5) * Q;
        }
        res.resultElement.vCalc = Q;
        return res;
    }
    getFlowRegime() {
        if (this.prms.h2.v > 0) {
            return structure_1.StructureFlowRegime.SUBMERGED;
        }
        else {
            return structure_1.StructureFlowRegime.FREE;
        }
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.L.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.alpha.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.beta.calculability = param_definition_1.ParamCalculability.FREE;
        this.prms.ZRAM.calculability = param_definition_1.ParamCalculability.FREE;
    }
}
exports.StructureKivi = StructureKivi;

},{"../param/param-definition":86,"../util/message":151,"../util/result":155,"./structure":115,"./structure_props":126,"./villemonte":145}],120:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureKiviParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const structure_params_1 = require("./structure_params");
/**
 * Paramètres pour une équation de seuil rectangulaire Kindsvater-Carter & Villemonte (KIVI)
 */
class StructureKiviParams extends structure_params_1.StructureParams {
    constructor(rQ, rZDV, rZ1, rZ2, rL, rAlpha, rBeta, rZRAM) {
        super(rQ, rZDV, rZ1, rZ2, Infinity);
        this.L = new param_definition_1.ParamDefinition(this, "L", param_domain_1.ParamDomainValue.POS_NULL, "m", rL, param_definition_1.ParamFamily.WIDTHS);
        this.addParamDefinition(this.L);
        this.alpha = new param_definition_1.ParamDefinition(this, "alpha", param_domain_1.ParamDomainValue.POS, undefined, rAlpha);
        this.addParamDefinition(this.alpha);
        this.beta = new param_definition_1.ParamDefinition(this, "beta", param_domain_1.ParamDomainValue.POS_NULL, undefined, rBeta);
        this.addParamDefinition(this.beta);
        this.ZRAM = new param_definition_1.ParamDefinition(this, "ZRAM", param_domain_1.ParamDomainValue.ANY, "m", rZRAM, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZRAM);
    }
}
exports.StructureKiviParams = StructureKiviParams;

},{"../param/param-definition":86,"../param/param-domain":87,"./structure_params":125}],121:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureOrificeFree = void 0;
const param_definition_1 = require("../param/param-definition");
const structure_1 = require("../structure/structure");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const structure_props_1 = require("./structure_props");
/**
 * Equation classique orifice dénoyé
 */
class StructureOrificeFree extends structure_1.Structure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.OrificeFree;
        this._isZDVcalculable = false;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Calc(sVarCalc, rInit) {
        this.currentResult = super.Calc(sVarCalc, rInit);
        if (this._loiDebit === structure_props_1.LoiDebit.OrificeFree && this.prms.Z2.v > this.prms.Zco.v) {
            this._result.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_ORIFICE_FREE_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION, {
                Z2: this.prms.Z2.v,
                Zco: this.prms.Zco.v
            }));
        }
        return this._result;
    }
    /**
     * Calcul du débit avec l'équation classique d'un orifice dénoyé
     */
    CalcQ() {
        const data = this.getResultData();
        const v = this.prms.CdO.v * this.prms.S.v * structure_1.Structure.R2G * Math.sqrt(Math.max(0, this.prms.h1.v));
        return new result_1.Result(v, this, data);
    }
    getFlowRegime() {
        return structure_1.StructureFlowRegime.FREE;
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.ORIFICE;
    }
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.S.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.CdO.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.Zco.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.ZDV.visible = false;
    }
}
exports.StructureOrificeFree = StructureOrificeFree;

},{"../param/param-definition":86,"../structure/structure":115,"../util/message":151,"../util/result":155,"./structure_props":126}],122:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureOrificeFreeParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const structure_params_1 = require("../structure/structure_params");
class StructureOrificeFreeParams extends structure_params_1.StructureParams {
    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rCd   Coefficient de débit (-)
     * @param rS    Surface de l'orifice (m2)
     * @param rZco  Cote du centre de l'orifice (m)
     */
    constructor(rQ, rZ1, rZ2, rCd, rS, rZco) {
        super(rQ, 100, rZ1, rZ2);
        this.S = new param_definition_1.ParamDefinition(this, "S", param_domain_1.ParamDomainValue.POS_NULL, "m²", rS);
        this.addParamDefinition(this.S);
        this.CdO = new param_definition_1.ParamDefinition(this, "CdO", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 10), undefined, rCd);
        this.addParamDefinition(this.CdO);
        this.Zco = new param_definition_1.ParamDefinition(this, "Zco", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.ANY), "m", rZco);
        this.addParamDefinition(this.Zco);
        // hide params
        this.ZDV.visible = false;
    }
    /** Mise à jour de h1 */
    update_h1h2() {
        this.h1.v = this.Z1.v - this.Zco.v;
    }
}
exports.StructureOrificeFreeParams = StructureOrificeFreeParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../structure/structure_params":125}],123:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureOrificeSubmerged = void 0;
const param_definition_1 = require("../param/param-definition");
const structure_1 = require("../structure/structure");
const result_1 = require("../util/result");
const structure_props_1 = require("./structure_props");
/**
 * Equation classique orifice noyé
 */
class StructureOrificeSubmerged extends structure_1.Structure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.OrificeSubmerged;
        this._isZDVcalculable = false;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /**
     * Calcul du débit avec l'équation classique d'un orifice noyé
     * @param sVarCalc Variable à calculer (doit être égale à Q ici)
     */
    CalcQ() {
        const data = this.getResultData();
        const v = this.prms.CdO.v * this.prms.S.v * structure_1.Structure.R2G * Math.sqrt(this.prms.Z1.v - this.prms.Z2.v);
        return new result_1.Result(v, this, data);
    }
    getFlowRegime() {
        return structure_1.StructureFlowRegime.SUBMERGED;
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.ORIFICE;
    }
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.S.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.CdO.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.ZDV.visible = false;
    }
}
exports.StructureOrificeSubmerged = StructureOrificeSubmerged;

},{"../param/param-definition":86,"../structure/structure":115,"../util/result":155,"./structure_props":126}],124:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureOrificeSubmergedParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const structure_params_1 = require("../structure/structure_params");
/**
 * Parameters for rectangular structures (common for all rectangular structure equations)
 */
class StructureOrificeSubmergedParams extends structure_params_1.StructureParams {
    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rCd   Coefficient de débit (-)
     * @param rS    Surface de l'orifice (m2)
     */
    constructor(rQ, rZ1, rZ2, rCd, rS) {
        super(rQ, 100, rZ1, rZ2);
        this.S = new param_definition_1.ParamDefinition(this, "S", param_domain_1.ParamDomainValue.POS_NULL, "m²", rS);
        this.addParamDefinition(this.S);
        this.CdO = new param_definition_1.ParamDefinition(this, "CdO", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 10), undefined, rCd);
        this.addParamDefinition(this.CdO);
        // hide params
        this.ZDV.visible = false;
    }
    /**
     * Mise à jour de h1 et h2
     */
    update_h1h2() {
        // Inutile pour cette équation qui ne fait pas intervenir ces variables
    }
}
exports.StructureOrificeSubmergedParams = StructureOrificeSubmergedParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../structure/structure_params":125}],125:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const params_equation_1 = require("../param/params-equation");
/**
 * Common parameters of hydraulic structure equations
 */
class StructureParams extends params_equation_1.ParamsEquation {
    /**
     * Paramètres communs à toutes les équations de structure
     * @param rQ Débit (m3/s)
     * @param rZDV Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1 Cote de l'eau amont (m)
     * @param rZ2 Cote de l'eau aval (m)
     * @param rW Ouverture de vanne (m) (infinity pour un seuil)
     */
    constructor(rQ, rZDV, rZ1, rZ2, rW = Infinity) {
        super();
        this.Q = new param_definition_1.ParamDefinition(this, "Q", param_domain_1.ParamDomainValue.ANY, "m³/s", rQ, param_definition_1.ParamFamily.FLOWS, false);
        this.addParamDefinition(this.Q);
        this.ZDV = new param_definition_1.ParamDefinition(this, "ZDV", param_domain_1.ParamDomainValue.ANY, "m", rZDV, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZDV);
        this.Z1 = new param_definition_1.ParamDefinition(this, "Z1", param_domain_1.ParamDomainValue.ANY, "m", rZ1, undefined, false);
        this.addParamDefinition(this.Z1);
        this.Z2 = new param_definition_1.ParamDefinition(this, "Z2", param_domain_1.ParamDomainValue.ANY, "m", rZ2, undefined, false);
        this.addParamDefinition(this.Z2);
        this.h1 = new param_definition_1.ParamDefinition(this, "h1", param_domain_1.ParamDomainValue.POS_NULL, "m", Math.max(0, this.Z1.v - this.ZDV.v), undefined, false);
        this.addParamDefinition(this.h1);
        this.h2 = new param_definition_1.ParamDefinition(this, "h2", param_domain_1.ParamDomainValue.POS_NULL, "m", Math.max(0, this.Z2.v - this.ZDV.v), undefined, false);
        this.addParamDefinition(this.h2);
        this.W = new param_definition_1.ParamDefinition(this, "W", param_domain_1.ParamDomainValue.POS_NULL, undefined, rW, undefined, false);
        this.addParamDefinition(this.W);
    }
    /** Mise à jour des paramètres h1 et h2 à partir de Z1, Z2 et ZDV */
    update_h1h2() {
        if (!this.h1.visible) {
            // h1 paramètre caché utilisé dans les lois de débit
            this.h1.v = Math.max(0, this.Z1.v - this.ZDV.v);
        }
        else {
            // Else ZDV should by updated using h1 #127
            this.ZDV.v = this.Z1.v - this.h1.v;
        }
        this.h2.v = Math.max(0, this.Z2.v - this.ZDV.v);
    }
}
exports.StructureParams = StructureParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],126:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureProperties = exports.loiAdmissiblesCloisonAval = exports.loiAdmissiblesDever = exports.loiAdmissiblesCloisons = exports.loiAdmissiblesOuvrages = exports.LoiDebit = exports.StructureType = void 0;
var StructureType;
(function (StructureType) {
    StructureType[StructureType["SeuilRectangulaire"] = 0] = "SeuilRectangulaire";
    StructureType[StructureType["VanneRectangulaire"] = 1] = "VanneRectangulaire";
    StructureType[StructureType["Orifice"] = 2] = "Orifice";
    StructureType[StructureType["SeuilTriangulaire"] = 3] = "SeuilTriangulaire";
    StructureType[StructureType["SeuilTriangulaireTrunc"] = 4] = "SeuilTriangulaireTrunc";
    // VanneCirculaire,
    // VanneTrapezoidale, SeuilTrapezoidal
})(StructureType = exports.StructureType || (exports.StructureType = {}));
var LoiDebit;
(function (LoiDebit) {
    // loi de débit Déversoir / Orifice Cemagref 1988 (pour Vanne Rectangulaire)
    LoiDebit[LoiDebit["GateCem88d"] = 0] = "GateCem88d";
    // loi de débit Déversoir / Vanne de fond Cemagref 1988 (pour Vanne Rectangulaire)
    LoiDebit[LoiDebit["GateCem88v"] = 1] = "GateCem88v";
    // loi de débit Déversoir / Orifice Cemagref 1988 (pour Seuil Rectangulaire)
    LoiDebit[LoiDebit["WeirCem88d"] = 2] = "WeirCem88d";
    // loi de débit Déversoir / Vanne de fond Cemagref 1988 (pour Seuil Rectangulaire)
    LoiDebit[LoiDebit["WeirCem88v"] = 3] = "WeirCem88v";
    // loi de débit Vanne Cunge 1980
    LoiDebit[LoiDebit["GateCunge80"] = 4] = "GateCunge80";
    // loi de débit pour vanne dénoyée
    LoiDebit[LoiDebit["RectangularOrificeFree"] = 5] = "RectangularOrificeFree";
    // loi de débit pour vanne noyée
    LoiDebit[LoiDebit["RectangularOrificeSubmerged"] = 6] = "RectangularOrificeSubmerged";
    // loi de débit pour seuil dénoyé
    LoiDebit[LoiDebit["WeirFree"] = 7] = "WeirFree";
    // Loi Kindsvater-Carter et Villemonte
    LoiDebit[LoiDebit["KIVI"] = 8] = "KIVI";
    // Loi de débit seuil triangulaire (Villemonte)
    LoiDebit[LoiDebit["TriangularWeirFree"] = 9] = "TriangularWeirFree";
    // Loi de débit seuil triangulaire tronqué (Villemonte)
    LoiDebit[LoiDebit["TriangularTruncWeirFree"] = 10] = "TriangularTruncWeirFree";
    // Loi de débit fente noyée (Larinier 1992)
    LoiDebit[LoiDebit["WeirSubmergedLarinier"] = 11] = "WeirSubmergedLarinier";
    // Loi de débit fente noyée (Rajaratnam & Muralidhar 1969)
    LoiDebit[LoiDebit["WeirSubmerged"] = 12] = "WeirSubmerged";
    // Loi de débit orifice noyé
    LoiDebit[LoiDebit["OrificeSubmerged"] = 13] = "OrificeSubmerged";
    // Loi de débit orifice dénoyé
    LoiDebit[LoiDebit["OrificeFree"] = 14] = "OrificeFree";
    // Loi de seuil noyée Villemonte
    LoiDebit[LoiDebit["WeirVillemonte"] = 15] = "WeirVillemonte";
    // Vanne levante Larinier
    LoiDebit[LoiDebit["VanLevLarinier"] = 16] = "VanLevLarinier";
    // Vanne levante Villemonte
    LoiDebit[LoiDebit["VanLevVillemonte"] = 17] = "VanLevVillemonte";
    // loi de débit Seuil Cunge 1980
    LoiDebit[LoiDebit["WeirCunge80"] = 18] = "WeirCunge80";
    // Loi de débit seuil triangulaire épais (Bos, Discharge measurement structures, 1989, p.137-143)
    LoiDebit[LoiDebit["TriangularWeirBroad"] = 19] = "TriangularWeirBroad";
})(LoiDebit = exports.LoiDebit || (exports.LoiDebit = {}));
exports.loiAdmissiblesOuvrages = {
    Orifice: [
        LoiDebit.OrificeSubmerged, LoiDebit.OrificeFree
    ],
    SeuilRectangulaire: [
        LoiDebit.WeirCem88d, LoiDebit.WeirCem88v, LoiDebit.WeirSubmerged, LoiDebit.WeirSubmergedLarinier,
        LoiDebit.WeirVillemonte, LoiDebit.WeirFree, LoiDebit.KIVI, LoiDebit.WeirCunge80
    ],
    SeuilTriangulaire: [
        LoiDebit.TriangularWeirFree, LoiDebit.TriangularWeirBroad
    ],
    SeuilTriangulaireTrunc: [
        LoiDebit.TriangularTruncWeirFree
    ],
    VanneRectangulaire: [
        LoiDebit.GateCem88d, LoiDebit.GateCem88v, LoiDebit.GateCunge80, LoiDebit.RectangularOrificeFree,
        LoiDebit.RectangularOrificeSubmerged
    ]
};
exports.loiAdmissiblesCloisons = {
    Orifice: [
        LoiDebit.OrificeSubmerged
    ],
    SeuilRectangulaire: [
        LoiDebit.WeirSubmergedLarinier, LoiDebit.WeirVillemonte, LoiDebit.WeirCem88d
    ],
    SeuilTriangulaire: [
        LoiDebit.TriangularWeirFree, LoiDebit.TriangularWeirBroad
    ],
    SeuilTriangulaireTrunc: [
        LoiDebit.TriangularTruncWeirFree
    ],
    VanneRectangulaire: [
        LoiDebit.GateCem88d
    ]
};
exports.loiAdmissiblesDever = {
    SeuilRectangulaire: [
        LoiDebit.WeirFree
    ],
    SeuilTriangulaire: [
        LoiDebit.TriangularWeirFree
    ],
    SeuilTriangulaireTrunc: [
        LoiDebit.TriangularTruncWeirFree
    ]
};
exports.loiAdmissiblesCloisonAval = {
    Orifice: [
        LoiDebit.OrificeSubmerged
    ],
    SeuilRectangulaire: [
        LoiDebit.WeirSubmergedLarinier, LoiDebit.WeirVillemonte, LoiDebit.WeirCem88d
    ],
    VanneLevante: [
        LoiDebit.VanLevVillemonte, LoiDebit.VanLevLarinier
    ],
    SeuilTriangulaire: [
        LoiDebit.TriangularWeirFree, LoiDebit.TriangularWeirBroad
    ],
    SeuilTriangulaireTrunc: [
        LoiDebit.TriangularTruncWeirFree
    ],
    VanneRectangulaire: [
        LoiDebit.GateCem88d
    ]
};
class StructureProperties {
    /**
     * @return true si les valeurs de StructureType et LoiDebit sont compatibles
     */
    static isCompatibleValues(struct, loi, parentNub) {
        return parentNub.getLoisAdmissibles()[StructureType[struct]].includes(loi);
    }
    /**
     * @return la 1ère valeur de StructureType compatible avec la loi de débit, dans le contexte
     * du module de calcul parentNub @TODO la 1ère ? normalement il n'y en a qu'une !
     */
    static findCompatibleStructure(loi, parentNub) {
        const loisAdmissibles = parentNub.getLoisAdmissibles();
        for (const st in loisAdmissibles) {
            if (loisAdmissibles.hasOwnProperty(st)) {
                const lds = loisAdmissibles[st];
                for (const ld of lds) {
                    if (ld === loi) {
                        return StructureType[st];
                    }
                }
            }
        }
        return undefined;
    }
    /**
     * trouve la 1ère valeur de LoiDebit compatible avec le type de structure
     * @param struct type de structure avec laquelle la loi de débit doit être compatible
     * @param subset si non vide, recherche la loi de débit compatible dans ce tableau; sinon prend la 1ere
     */
    static findCompatibleLoiDebit(struct, subset, parentNub) {
        const sst = StructureType[struct];
        const lois = parentNub.getLoisAdmissibles();
        for (const ld of lois[sst]) {
            if (subset.length === 0 || subset.includes(ld)) {
                return ld;
            }
        }
        return undefined;
    }
}
exports.StructureProperties = StructureProperties;

},{}],127:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureRectangularOrificeFree = void 0;
const param_definition_1 = require("../param/param-definition");
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const rectangular_structure_1 = require("./rectangular_structure");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
/**
 * Equation classique orifice dénoyé ("Vanne dénoyé")
 */
class StructureRectangularOrificeFree extends rectangular_structure_1.RectangularStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.RectangularOrificeFree;
        this.prms.W.visible = true;
        this.prms.CdGR.visible = true;
    }
    Calc(sVarCalc, rInit) {
        this.currentResult = super.Calc(sVarCalc, rInit);
        if (this.prms.h2.v > 0) {
            this._result.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION, { h2: this.prms.h2.v }));
        }
        return this._result;
    }
    /**
     * Calcul du débit avec l'équation classique d'un orifice dénoyé
     * @param sVarCalc Variable à calculer (doit être égale à Q ici)
     */
    CalcQ() {
        const data = this.getResultData();
        const v = this.prms.CdGR.v * Math.min(this.prms.W.v, this.prms.h1.v) * this.prms.L.v
            * structure_1.Structure.R2G * Math.sqrt(this.prms.h1.v);
        return new result_1.Result(v, this, data);
    }
    getFlowRegime() {
        return structure_1.StructureFlowRegime.FREE;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = param_definition_1.ParamCalculability.DICHO;
    }
}
exports.StructureRectangularOrificeFree = StructureRectangularOrificeFree;

},{"../param/param-definition":86,"../util/message":151,"../util/result":155,"./rectangular_structure":113,"./structure":115,"./structure_props":126}],128:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureRectangularOrificeSubmerged = void 0;
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const rectangular_structure_1 = require("./rectangular_structure");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
/**
 * Equation classique orifice noyé ("Vanne noyé")
 */
class StructureRectangularOrificeSubmerged extends rectangular_structure_1.RectangularStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.RectangularOrificeSubmerged;
        if (prms.W.v !== Infinity) {
            this._isZDVcalculable = false;
        }
        this.prms.W.visible = true;
        this.prms.CdGRS.visible = true;
    }
    /**
     * Calcul du débit avec l'équation classique d'un orifice noyé
     * @param sVarCalc Variable à calculer (doit être égale à Q ici)
     */
    CalcQ() {
        const data = this.getResultData();
        const v = this.prms.CdGRS.v * Math.min(this.prms.W.v, this.prms.h1.v) * this.prms.L.v
            * structure_1.Structure.R2G * Math.sqrt(this.prms.h1.v - this.prms.h2.v);
        return new result_1.Result(v, this, data);
    }
    getFlowRegime() {
        return structure_1.StructureFlowRegime.SUBMERGED;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.W.calculability = param_definition_1.ParamCalculability.DICHO;
    }
}
exports.StructureRectangularOrificeSubmerged = StructureRectangularOrificeSubmerged;

},{"../param/param-definition":86,"../util/result":155,"./rectangular_structure":113,"./structure":115,"./structure_props":126}],129:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureTriangularTruncWeirFree = void 0;
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
const villemonte_1 = require("./villemonte");
/**
 * Equation classique seuil triangulaire (Villemonte)
 */
class StructureTriangularTruncWeirFree extends structure_1.Structure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.TriangularTruncWeirFree;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W)
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const data = this.getResultData();
        let Q = this.prms.CdT.v * this.prms.BT.v / (this.prms.ZT.v - this.prms.ZDV.v);
        if (this.prms.Z1.v <= this.prms.ZT.v) {
            Q = Q * Math.pow(this.prms.h1.v, 2.5);
        }
        else {
            Q = Q * (Math.pow(this.prms.h1.v, 2.5) - Math.pow(this.prms.Z1.v - this.prms.ZT.v, 2.5));
        }
        if (data.ENUM_StructureFlowRegime !== structure_1.StructureFlowRegime.FREE) {
            Q = villemonte_1.Villemonte(this.prms.h1.v, this.prms.h2.v, 2.5) * Q;
        }
        return new result_1.Result(Q, this, data);
    }
    getFlowRegime() {
        if (this.prms.Z2.v <= this.prms.ZDV.v) {
            return structure_1.StructureFlowRegime.FREE;
        }
        else {
            // La réduction de débit s'applique dès que Z2 > ZDV (Villemonte, 1946)
            if (this.prms.h2.v < 4 / 5 * this.prms.h1.v) {
                // Yc = 4 / 5 * H1 (Jameson, 1925)
                return structure_1.StructureFlowRegime.PARTIAL;
            }
            return structure_1.StructureFlowRegime.SUBMERGED;
        }
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.BT.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.ZT.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.CdT.calculability = param_definition_1.ParamCalculability.DICHO;
    }
}
exports.StructureTriangularTruncWeirFree = StructureTriangularTruncWeirFree;

},{"../param/param-definition":86,"../util/result":155,"./structure":115,"./structure_props":126,"./villemonte":145}],130:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TriangularTruncStructureParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const structure_params_1 = require("./structure_params");
/**
 * Parameters for rectangular structures (common for all rectangular structure equations)
 */
class TriangularTruncStructureParams extends structure_params_1.StructureParams {
    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZDV  Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rBT   Demi-ouverture du triangle (m)
     * @param rZT   Cote haute du triangle (m)
     * @param rCd   Coefficient de débit (-)
     * @param rW    Ouverture de la vanne (m) (Valeur par défaut +infinity pour les déversoirs)
     */
    constructor(rQ, rZDV, rZ1, rZ2, rBT, rZT, rCd, rW = Infinity) {
        super(rQ, rZDV, rZ1, rZ2, rW);
        this.BT = new param_definition_1.ParamDefinition(this, "BT", param_domain_1.ParamDomainValue.POS, "m", rBT);
        this.addParamDefinition(this.BT);
        this.ZT = new param_definition_1.ParamDefinition(this, "ZT", param_domain_1.ParamDomainValue.POS, "m", rZT, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.ZT);
        this.CdT = new param_definition_1.ParamDefinition(this, "CdT", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 10), undefined, rCd);
        this.addParamDefinition(this.CdT);
    }
}
exports.TriangularTruncStructureParams = TriangularTruncStructureParams;

},{"../param/param-definition":86,"../param/param-domain":87,"./structure_params":125}],131:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureTriangularWeir = void 0;
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
const villemonte_1 = require("./villemonte");
const structure_triangular_weir_free_1 = require("./structure_triangular_weir_free");
/**
 * Equation classique seuil triangulaire + Ennoiement Villemonte
 */
class StructureTriangularWeir extends structure_triangular_weir_free_1.StructureTriangularWeirFree {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.TriangularWeirFree; // First name of the law for backward compatibility
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W)
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const r = super.CalcQ();
        if (r.values.ENUM_StructureFlowRegime !== structure_1.StructureFlowRegime.FREE) {
            r.vCalc = villemonte_1.Villemonte(this.prms.h1.v, this.prms.h2.v, 2.5) * r.vCalc;
        }
        return r;
    }
    getFlowRegime() {
        if (this.prms.Z2.v <= this.prms.ZDV.v) {
            return structure_1.StructureFlowRegime.FREE;
        }
        else {
            // La réduction de débit s'applique dès que Z2 > ZDV (Villemonte, 1946)
            if (this.prms.h2.v < 4 / 5 * this.prms.h1.v) {
                // Yc = 4 / 5 * H1 (Jameson, 1925)
                return structure_1.StructureFlowRegime.PARTIAL;
            }
            return structure_1.StructureFlowRegime.SUBMERGED;
        }
    }
}
exports.StructureTriangularWeir = StructureTriangularWeir;

},{"./structure":115,"./structure_props":126,"./structure_triangular_weir_free":133,"./villemonte":145}],132:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureTriangularWeirBroad = void 0;
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
const structure_triangular_weir_free_1 = require("./structure_triangular_weir_free");
/**
 * Equation classique seuil triangulaire + Ennoiement Villemonte
 */
class StructureTriangularWeirBroad extends structure_triangular_weir_free_1.StructureTriangularWeirFree {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.TriangularWeirBroad; // First name of the law for backward compatibility
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W)
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const r = super.CalcQ();
        if (this.prms.h1.v > 0 && r.values.ENUM_StructureFlowRegime !== structure_1.StructureFlowRegime.FREE) {
            r.vCalc = r.vCalc * Math.sin(3.962902 * Math.pow((1 - this.prms.h2.v / this.prms.h1.v), 0.574979));
        }
        return r;
    }
    getFlowRegime() {
        if (this.prms.h2.v < 4 / 5 * this.prms.h1.v) {
            // Yc = 4 / 5 * H1 (Jameson, 1925)
            return structure_1.StructureFlowRegime.FREE;
        }
        return structure_1.StructureFlowRegime.SUBMERGED;
    }
}
exports.StructureTriangularWeirBroad = StructureTriangularWeirBroad;

},{"./structure":115,"./structure_props":126,"./structure_triangular_weir_free":133}],133:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureTriangularWeirFree = void 0;
const param_definition_1 = require("../param/param-definition");
const result_1 = require("../util/result");
const structure_1 = require("./structure");
/**
 * Equation classique seuil triangulaire + Ennoiement Villemonte
 */
class StructureTriangularWeirFree extends structure_1.Structure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W)
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const data = this.getResultData();
        const Q = this.prms.CdT.v * this.getTanFromDegrees(this.prms.alpha2.v)
            * Math.pow(this.prms.h1.v, 2.5);
        return new result_1.Result(Q, this, data);
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
    /**
     * Compute Tangent with angle in degrees
     * @param degrees Angle (degrees)
     */
    getTanFromDegrees(degrees) {
        return Math.tan(degrees * Math.PI / 180);
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.alpha2.calculability = param_definition_1.ParamCalculability.DICHO;
        this.prms.CdT.calculability = param_definition_1.ParamCalculability.DICHO;
    }
}
exports.StructureTriangularWeirFree = StructureTriangularWeirFree;

},{"../param/param-definition":86,"../util/result":155,"./structure":115}],134:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TriangularStructureParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const structure_params_1 = require("./structure_params");
/**
 * Parameters for rectangular structures (common for all rectangular structure equations)
 */
class TriangularStructureParams extends structure_params_1.StructureParams {
    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZDV  Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rAlpha2    Demi-angle au sommet du triangle (degrés)
     * @param rCd   Coefficient de débit (-)
     * @param rW    Ouverture de la vanne (m) (Valeur par défaut +infinity pour les déversoirs)
     */
    constructor(rQ, rZDV, rZ1, rZ2, rAlpha2, rCd, rW = Infinity) {
        super(rQ, rZDV, rZ1, rZ2, rW);
        this.alpha2 =
            new param_definition_1.ParamDefinition(this, "alpha2", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 90), "°", rAlpha2);
        this.addParamDefinition(this.alpha2);
        this.CdT = new param_definition_1.ParamDefinition(this, "CdT", new param_domain_1.ParamDomain(param_domain_1.ParamDomainValue.INTERVAL, 0, 10), undefined, rCd);
        this.addParamDefinition(this.CdT);
    }
}
exports.TriangularStructureParams = TriangularStructureParams;

},{"../param/param-definition":86,"../param/param-domain":87,"./structure_params":125}],135:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureVanLevLarinier = void 0;
const structure_props_1 = require("./structure_props");
const structure_weir_submerged_larinier_1 = require("./structure_weir_submerged_larinier");
class StructureVanLevLarinier extends structure_weir_submerged_larinier_1.StructureWeirSubmergedLarinier {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.VanLevLarinier;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
}
exports.StructureVanLevLarinier = StructureVanLevLarinier;

},{"./structure_props":126,"./structure_weir_submerged_larinier":143}],136:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureVanLevParams = void 0;
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
const rectangular_structure_params_1 = require("./rectangular_structure_params");
/**
 * Parameters of an automatic weir with crest elevation regulation
 */
class StructureVanLevParams extends rectangular_structure_params_1.RectangularStructureParams {
    /**
     * Constructeur d'une structure rectangulaire
     * @param rQ    Débit (m3/s)
     * @param rZDV  Cote de la crête du déversoir ou du radier de la vanne (m)
     * @param rZ1   Cote de l'eau amont (m)
     * @param rZ2   Cote de l'eau aval (m)
     * @param rL    Largeur de la vanne ou du déversoir (m)
     * @param rCd   Coefficient de débit (-)
     * @param rMinZDV Minimum weir crest elevation (m)
     * @param rMaxZDV Maximum weir crest elevation (m)
     * @param rW    Ouverture de la vanne (m) (Valeur par défaut +infinity pour les déversoirs)
     */
    constructor(rQ, rZDV, rZ1, rZ2, rL, rCd, rDH, rMinZDV, rMaxZDV, rW = Infinity) {
        super(rQ, rZDV, rZ1, rZ2, rL, rCd, rW);
        this.minZDV = new param_definition_1.ParamDefinition(this, "minZDV", param_domain_1.ParamDomainValue.ANY, "m", rMinZDV, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.minZDV);
        this.maxZDV = new param_definition_1.ParamDefinition(this, "maxZDV", param_domain_1.ParamDomainValue.ANY, "m", rMaxZDV, param_definition_1.ParamFamily.ELEVATIONS);
        this.addParamDefinition(this.maxZDV);
        this.DH = new param_definition_1.ParamDefinition(this, "DH", param_domain_1.ParamDomainValue.ANY, "m", rDH, param_definition_1.ParamFamily.BASINFALLS);
        this.addParamDefinition(this.DH);
        this.ZDV.visible = false;
    }
}
exports.StructureVanLevParams = StructureVanLevParams;

},{"../param/param-definition":86,"../param/param-domain":87,"./rectangular_structure_params":114}],137:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureVanLevVillemonte = void 0;
const structure_props_1 = require("./structure_props");
const structure_weir_villemonte_1 = require("./structure_weir_villemonte");
class StructureVanLevVillemonte extends structure_weir_villemonte_1.StructureWeirVillemonte {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.VanLevVillemonte;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
}
exports.StructureVanLevVillemonte = StructureVanLevVillemonte;

},{"./structure_props":126,"./structure_weir_villemonte":144}],138:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureWeirCem88d = void 0;
const structure_1 = require("./structure");
const structure_gate_cem88d_1 = require("./structure_gate_cem88d");
const structure_props_1 = require("./structure_props");
class StructureWeirCem88d extends structure_gate_cem88d_1.StructureGateCem88d {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.WeirCem88d;
        this._isZDVcalculable = true;
        // Gestion de l'affichage l'ouverture de vanne
        this.prms.W.visible = false;
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
}
exports.StructureWeirCem88d = StructureWeirCem88d;

},{"./structure":115,"./structure_gate_cem88d":116,"./structure_props":126}],139:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureWeirCem88v = void 0;
const structure_1 = require("./structure");
const structure_gate_cem88v_1 = require("./structure_gate_cem88v");
const structure_props_1 = require("./structure_props");
class StructureWeirCem88v extends structure_gate_cem88v_1.StructureGateCem88v {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.WeirCem88v;
        this.prms.W.visible = false;
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
}
exports.StructureWeirCem88v = StructureWeirCem88v;

},{"./structure":115,"./structure_gate_cem88v":117,"./structure_props":126}],140:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureWeirCunge80 = void 0;
const structure_1 = require("./structure");
const structure_gate_cunge80_1 = require("./structure_gate_cunge80");
const structure_props_1 = require("./structure_props");
class StructureWeirCunge80 extends structure_gate_cunge80_1.StructureGateCunge80 {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.WeirCunge80;
        this.prms.W.visible = false;
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
}
exports.StructureWeirCunge80 = StructureWeirCunge80;

},{"./structure":115,"./structure_gate_cunge80":118,"./structure_props":126}],141:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureWeirFree = void 0;
const message_1 = require("../util/message");
const result_1 = require("../util/result");
const rectangular_structure_1 = require("./rectangular_structure");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
/**
 * Equation classique seuil dénoyé
 */
class StructureWeirFree extends rectangular_structure_1.RectangularStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.WeirFree;
        this.prms.CdWR.visible = true;
    }
    Calc(sVarCalc, rInit) {
        this.currentResult = super.Calc(sVarCalc, rInit);
        // do not check h2 for derived classes (ex: StructureWeirVillemonte)
        if (this._loiDebit === structure_props_1.LoiDebit.WeirFree && this.prms.h2.v > 0) {
            this._result.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION, { h2: this.prms.h2.v }));
        }
        return this._result;
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) seuil dénoyé
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const data = this.getResultData();
        const v = this.prms.CdWR.v * this.prms.L.v * structure_1.Structure.R2G * Math.pow(this.prms.h1.v, 1.5);
        return new result_1.Result(v, this, data);
    }
    getFlowRegime() {
        return structure_1.StructureFlowRegime.FREE;
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
}
exports.StructureWeirFree = StructureWeirFree;

},{"../util/message":151,"../util/result":155,"./rectangular_structure":113,"./structure":115,"./structure_props":126}],142:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureWeirSubmerged = void 0;
const index_1 = require("../index");
const result_1 = require("../util/result");
const rectangular_structure_1 = require("./rectangular_structure");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
/**
 * Equation de la fente noyée
 * d'après Rajaratnam, N., et D. Muralidhar.
 * « Flow below deeply submerged rectangular weirs ».
 * Journal of Hydraulic Research 7, nᵒ 3 (1969): 355–374.
 */
class StructureWeirSubmerged extends rectangular_structure_1.RectangularStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.WeirSubmerged;
        this.prms.CdWS.visible = true;
    }
    Calc(sVarCalc, rInit) {
        this.currentResult = super.Calc(sVarCalc, rInit);
        const h2h1ratio = this.prms.h2.v / this.prms.h1.v;
        if (h2h1ratio < 0.8) {
            this._result.resultElement.addMessage(new index_1.Message(index_1.MessageCode.WARNING_WEIR_SUBMERSION_LOWER_THAN_08, { h1: this.prms.h1.v, h2: this.prms.h2.v }));
        }
        return this._result;
    }
    /**
     * Calcul analytique Q
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const data = this.getResultData();
        const v = this.prms.CdWS.v * this.prms.L.v * structure_1.Structure.R2G
            * this.prms.h2.v * Math.sqrt(this.prms.h1.v - this.prms.h2.v);
        return new result_1.Result(v, this, data);
    }
    getFlowRegime() {
        return structure_1.StructureFlowRegime.SUBMERGED;
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.L.calculability = index_1.ParamCalculability.DICHO;
        this.prms.CdWS.calculability = index_1.ParamCalculability.DICHO;
    }
}
exports.StructureWeirSubmerged = StructureWeirSubmerged;

},{"../index":14,"../util/result":155,"./rectangular_structure":113,"./structure":115,"./structure_props":126}],143:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureWeirSubmergedLarinier = void 0;
const index_1 = require("../index");
const result_1 = require("../util/result");
const rectangular_structure_1 = require("./rectangular_structure");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
/**
 * Equation de la fente noyé
 * d'après Larinier, M., Travade, F., Porcher, J.-P., Gosset, C., 1992.
 * Passes à poissons : expertise et conception des ouvrages de franchissement
 */
class StructureWeirSubmergedLarinier extends rectangular_structure_1.RectangularStructure {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.WeirSubmergedLarinier;
        this.prms.CdWSL.visible = true;
    }
    /**
     * paramètres castés au bon type
     */
    get prms() {
        return this._prms;
    }
    Calc(sVarCalc, rInit) {
        this.currentResult = super.Calc(sVarCalc, rInit);
        const h2h1ratio = this.prms.h2.v / this.prms.h1.v;
        if (h2h1ratio < 0.7 || h2h1ratio > 0.9) {
            this._result.resultElement.addMessage(new index_1.Message(index_1.MessageCode.WARNING_SLOT_SUBMERSION_NOT_BETWEEN_07_AND_09, { h1: this.prms.h1.v, h2: this.prms.h2.v }));
        }
        return this._result;
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) seuil dénoyé
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const data = this.getResultData();
        const v = this.prms.CdWSL.v * this.prms.L.v * structure_1.Structure.R2G
            * this.prms.h1.v * Math.sqrt(this.prms.h1.v - this.prms.h2.v);
        return new result_1.Result(v, this, data);
    }
    getFlowRegime() {
        return structure_1.StructureFlowRegime.SUBMERGED;
    }
    getFlowMode() {
        return structure_1.StructureFlowMode.WEIR;
    }
    /**
     * paramétrage de la calculabilité des paramètres
     */
    setParametersCalculability() {
        super.setParametersCalculability();
        this.prms.L.calculability = index_1.ParamCalculability.DICHO;
        this.prms.CdWSL.calculability = index_1.ParamCalculability.DICHO;
    }
}
exports.StructureWeirSubmergedLarinier = StructureWeirSubmergedLarinier;

},{"../index":14,"../util/result":155,"./rectangular_structure":113,"./structure":115,"./structure_props":126}],144:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StructureWeirVillemonte = void 0;
const message_1 = require("../util/message");
const structure_1 = require("./structure");
const structure_props_1 = require("./structure_props");
const structure_weir_free_1 = require("./structure_weir_free");
const villemonte_1 = require("./villemonte");
class StructureWeirVillemonte extends structure_weir_free_1.StructureWeirFree {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._loiDebit = structure_props_1.LoiDebit.WeirVillemonte;
    }
    Calc(sVarCalc, rInit) {
        this.currentResult = super.Calc(sVarCalc, rInit);
        if ((this.prms.h2.v / this.prms.h1.v) > 0.7) {
            this._result.resultElement.addMessage(new message_1.Message(message_1.MessageCode.WARNING_NOTCH_SUBMERSION_GREATER_THAN_07, { h1: this.prms.h1.v, h2: this.prms.h2.v }));
        }
        return this._result;
    }
    /**
     * Calcul analytique Q = f(Cd, L, h1, h2, W) seuil dénoyé
     * @param sVarCalc Variable à calculer (doit être "Q")
     */
    CalcQ() {
        const r = super.CalcQ();
        if (r.ok && r.resultElement.values.ENUM_StructureFlowRegime !== structure_1.StructureFlowRegime.FREE) {
            r.vCalc = villemonte_1.Villemonte(this.prms.h1.v, this.prms.h2.v, 1.5) * r.vCalc;
        }
        return r;
    }
    getFlowRegime() {
        if (this.prms.Z2.v <= this.prms.ZDV.v) {
            return structure_1.StructureFlowRegime.FREE;
        }
        else {
            // La réduction de débit s'applique dès que Z2 > ZDV (Villemonte, 1946)
            if (this.prms.h2.v < 2 / 3 * this.prms.h1.v) {
                // Yc = 2 / 3 * H1 (Jameson, 1925)
                return structure_1.StructureFlowRegime.PARTIAL;
            }
            return structure_1.StructureFlowRegime.SUBMERGED;
        }
    }
}
exports.StructureWeirVillemonte = StructureWeirVillemonte;

},{"../util/message":151,"./structure":115,"./structure_props":126,"./structure_weir_free":141,"./villemonte":145}],145:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Villemonte = void 0;
/**
 *
 * @param h1 hauteur d'eau amont au dessus de la crête du seuil
 * @param h2 hauteur d'eau aval au dessus de la crête du seuil
 * @param n n est l'exposant dans les relations d'écoulement dénoyé :
 *          déversoir proportionnel : n=1 déversoir rectangulaire : n=1,5
 *          déversoir parabolique : n=2 déversoir triangulaire : n=2,5
 */
function Villemonte(h1, h2, n) {
    if (h1 === 0 || h2 > h1) {
        return 0;
    }
    if (n < 1 || n > 2.5) {
        throw new Error("Villemonte n doit être compris entre 1 et 2.5");
    }
    return Math.pow(1 - Math.pow(h2 / h1, n), 0.385);
}
exports.Villemonte = Villemonte;

},{}],146:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrayReverseIterator = void 0;
/**
 * itérateur sur un tableau dans le sens inverse (depuis la fin)
 *
 * utilisation :
 *   let arr = [1,2,3];
 *   const it = new ArrayReverseIterator<Result>(arr);
 *   for (let r of it)
 *     console.log( r );   // 3 2 1
 */
class ArrayReverseIterator {
    constructor(_arr) {
        this._arr = _arr;
        this._index = this._arr === undefined ? 0 : this._arr.length - 1;
    }
    next() {
        if (this._arr !== undefined && this._index >= 0) {
            return {
                done: false,
                value: this._arr[this._index--]
            };
        }
        else {
            return {
                done: true,
                value: null
            };
        }
    }
    [Symbol.iterator]() {
        return this;
    }
}
exports.ArrayReverseIterator = ArrayReverseIterator;

},{}],147:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnumEx = void 0;
/**
 * classe d'itérateurs pour les enums
 *
 * utilisation :
 *
 *  for (const v of EnumEx.getValues(MonTypeEnum)) {
 *     console.log(v);
 *  }
 *
 *  for (const n of EnumEx.getNames(MonTypeEnum)) {
 *     console.log(n);
 *  }
 *
 *  for (const c of EnumEx.getNamesAndValues(MonTypeEnum)) {
 *     console.log(c.name);
 *     console.log(c.value);
 *  }
 */
// tslint:disable-next-line:max-line-length
// see https://stackoverflow.com/questions/21293063/how-to-programmatically-enumerate-an-enum-type-in-typescript-0-9-5#21294925
class EnumEx {
    /**
     * retourne les noms et les valeurs d'un enum
     */
    static getNamesAndValues(e) {
        return EnumEx.getNames(e).map((n) => ({ name: n, value: e[n] }));
    }
    /**
     * retourne les noms d'un enum
     */
    static getNames(e) {
        return EnumEx.getObjValues(e).filter((v) => typeof v === "string");
    }
    /**
     * retourne les valeurs d'un enum
     */
    static getValues(e) {
        return EnumEx.getObjValues(e).filter((v) => typeof v === "number");
    }
    static getObjValues(e) {
        return Object.keys(e).map((k) => e[k]);
    }
}
exports.EnumEx = EnumEx;

},{}],148:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Interval = void 0;
const message_1 = require("./message");
const base_1 = require("../base");
/**
 * Couple de valeurs ordonnées
 */
class Interval extends base_1.Debug {
    constructor(val1, val2, dbg = false) {
        super(dbg);
        this.val1 = val1;
        this.val2 = val2;
    }
    setValues(v1, v2) {
        this.val1 = v1;
        this.val2 = v2;
    }
    /** "constructeur" par copie */
    setInterval(i) {
        this.setValues(i.val1, i.val2);
    }
    get min() {
        return Math.min(this.val1, this.val2);
    }
    get max() {
        return Math.max(this.val1, this.val2);
    }
    get length() {
        return this.max - this.min;
    }
    intervalHasValue(v) {
        return this.min <= v && v <= this.max;
    }
    intersect(i) {
        const min = Math.max(this.min, i.min);
        const max = Math.min(this.max, i.max);
        let intersection = null;
        if (min <= max) {
            intersection = new Interval(min, max);
        } // else no intersection
        return intersection;
    }
    checkValue(v) {
        if (v === undefined) {
            const e = new message_1.Message(message_1.MessageCode.ERROR_INTERVAL_UNDEF);
            throw e;
        }
        if (!this.intervalHasValue(v)) {
            const e = new message_1.Message(message_1.MessageCode.ERROR_INTERVAL_OUTSIDE);
            e.extraVar.value = v;
            e.extraVar.interval = this.toString();
            throw e;
        }
    }
    toString() {
        return "[" + this.min + "," + this.max + "]";
    }
    getVal(i) {
        if (i < 1 || i > 2) {
            throw new Error("Interval getVal n'accepte que 1 ou 2  en argument");
        }
        if (i === 1) {
            return this.val1;
        }
        else {
            return this.val2;
        }
    }
    setVal(i, val) {
        if (i < 1 || i > 2) {
            throw new Error("Interval getVal n'accepte que 1 ou 2  en argument");
        }
        if (i === 1) {
            this.val1 = val;
        }
        else {
            this.val2 = val;
        }
    }
    get minIndex() {
        if (this.val1 === this.min) {
            return 1;
        }
        else {
            return 2;
        }
    }
    get maxIndex() {
        if (this.val1 === this.max) {
            return 1;
        }
        else {
            return 2;
        }
    }
}
exports.Interval = Interval;

},{"../base":3,"./message":151}],149:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cLog = void 0;
// tslint:disable-next-line:class-name
class cLog {
    constructor(parent) {
        this._messages = [];
        this.parent = parent;
    }
    /**
     * efface tous les messages
     */
    clear() {
        this._messages = [];
    }
    /**
     * insère un message en fin de liste
     */
    add(m) {
        // add pointer to the current log as message's parent, unless the message
        // already has a parent, and unless the current log has no pointer to a result
        if (m.parent === undefined && this.parent !== undefined) {
            m.parent = this;
        }
        this._messages.push(m);
    }
    /**
     * insère un log entier en fin de liste
     */
    addLog(l) {
        for (const m of l.messages) {
            this.add(m);
        }
    }
    /**
     * insère un message en début de liste
     */
    insert(m) {
        // add pointer to the current log as message's parent, unless the message
        // already has a parent, and unless the current log has no pointer to a result
        if (m.parent === undefined && this.parent !== undefined) {
            m.parent = this;
        }
        this._messages.unshift(m);
    }
    /**
     * insère un log entier en début de liste
     */
    insertLog(l) {
        for (let i = l.messages.length - 1; i >= 0; i--) {
            this.insert(l.messages[i]);
        }
    }
    get messages() {
        return this._messages;
    }
    toString() {
        return this._messages.join("\n");
    }
    /**
     * @param mc message code you're looking for
     * @returns true if log contains at least one occurrence of given message code
     */
    contains(mc) {
        for (const m of this.messages) {
            if (m.code === mc) {
                return true;
            }
        }
        return false;
    }
}
exports.cLog = cLog;

},{}],150:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MapIterator = void 0;
/**
 * itérateur sur un map string<->any
 *
 * utilisation :
 * class MaClasseAIterer implements Iterable<Machin> {
 *     private _machinMap: { [key: string]: Machin } = {};
 *     [Symbol.iterator](): Iterator<Machin> {
 *        return new MapIterator(this._machinMap);
 *     }
 * }
 * const mc = new MaClasseAIterer();
 * for ( const m of mc ) {
 *   ...
 * }
 *
 * avec IterableIterator au lieu de Iterator, on peut faire :
 * const m = {"a":0, "b":1}
 * const it = new MapIterator<number>();
 * for (let e of it) {
 *   ...
 * }
 */
class MapIterator {
    constructor(m) {
        this._lastIndex = -1; // index of last element returned; -1 if no such
        this._index = 0;
        this._map = m;
        if (this._map !== undefined) {
            this._keys = Object.keys(this._map);
        }
        else {
            this._keys = [];
        }
    }
    next() {
        const i = this._index;
        if (this._index < this._keys.length) {
            this._currentKey = this._keys[this._index++];
            this._lastIndex = i;
            return {
                done: false,
                value: this._map[this._currentKey]
            };
        }
        else {
            this._currentKey = undefined;
            this._lastIndex = undefined;
            return {
                done: true,
                value: undefined
            };
        }
    }
    get index() {
        return this._lastIndex;
    }
    get key() {
        return this._currentKey;
    }
    // interface IterableIterator
    [Symbol.iterator]() {
        return this;
    }
}
exports.MapIterator = MapIterator;

},{}],151:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Message = exports.MessageSeverity = exports.MessageCode = void 0;
var MessageCode;
(function (MessageCode) {
    /** generic error stating that something triggered a fatal error leading to an undefined vCalc */
    MessageCode[MessageCode["ERROR_SOMETHING_FAILED_IN_CHILD"] = 0] = "ERROR_SOMETHING_FAILED_IN_CHILD";
    /** abstract showing number of error messages encountered in an iterative calculation, if number is 1 (otherwise see _PLUR below) */
    MessageCode[MessageCode["WARNING_ERRORS_ABSTRACT"] = 1] = "WARNING_ERRORS_ABSTRACT";
    /** abstract showing number of error messages encountered in an iterative calculation */
    MessageCode[MessageCode["WARNING_ERRORS_ABSTRACT_PLUR"] = 2] = "WARNING_ERRORS_ABSTRACT_PLUR";
    /** calculation of Z1 in Fluvial regime has failed (upstream abscissa not present in results) */
    MessageCode[MessageCode["ERROR_BIEF_Z1_CALC_FAILED"] = 3] = "ERROR_BIEF_Z1_CALC_FAILED";
    /** calculation of Z1 in Torrential regime has failed (downstream abscissa not present in results) */
    MessageCode[MessageCode["ERROR_BIEF_Z2_CALC_FAILED"] = 4] = "ERROR_BIEF_Z2_CALC_FAILED";
    /**
     * La dichotomie n'a pas trouvé de solution dans sa recherche d'intervalle:
     * La valeur cible est trop élevée
     */
    MessageCode[MessageCode["ERROR_DICHO_TARGET_TOO_HIGH"] = 5] = "ERROR_DICHO_TARGET_TOO_HIGH";
    /**
     * La dichotomie n'a pas trouvé de solution dans sa recherche d'intervalle:
     * La valeur cible est trop basse
     */
    MessageCode[MessageCode["ERROR_DICHO_TARGET_TOO_LOW"] = 6] = "ERROR_DICHO_TARGET_TOO_LOW";
    /**
     * la dichotomie n'a pas pu trouver automatiquement d'intervalle de départ
     * car la valeur cible de la fonction n'existe pas pour des valeurs de la
     * variable dans son domaine de définition, cad il n'existe pas de solution
     */
    MessageCode[MessageCode["ERROR_DICHO_INIT_DOMAIN"] = 7] = "ERROR_DICHO_INIT_DOMAIN";
    /**
     * la dichotomie n'a pas pu converger
     */
    MessageCode[MessageCode["ERROR_DICHO_CONVERGE"] = 8] = "ERROR_DICHO_CONVERGE";
    /**
     * le pas pour la recherche de l'intervalle de départ est =0
     */
    MessageCode[MessageCode["ERROR_DICHO_NULL_STEP"] = 9] = "ERROR_DICHO_NULL_STEP";
    /**
     * l'augmentation du pas pour la recherche de l'intervalle de départ est incorrecte (=0)
     */
    MessageCode[MessageCode["ERROR_DICHO_INVALID_STEP_GROWTH"] = 10] = "ERROR_DICHO_INVALID_STEP_GROWTH";
    /**
     * impossible de déterminer le sens de variation de la fonction
     */
    MessageCode[MessageCode["ERROR_DICHO_FUNCTION_VARIATION"] = 11] = "ERROR_DICHO_FUNCTION_VARIATION";
    /**
     * impossible de résoudre l'équation en raison d'une division par zéro
     */
    MessageCode[MessageCode["ERROR_DIVISION_BY_ZERO"] = 12] = "ERROR_DIVISION_BY_ZERO";
    /**
     * la cote amont Z1 est plus basse que la cote aval Z2
     */
    MessageCode[MessageCode["ERROR_ELEVATION_ZI_LOWER_THAN_Z2"] = 13] = "ERROR_ELEVATION_ZI_LOWER_THAN_Z2";
    /**
     * Something failed when calculating upstream Nubs
     */
    MessageCode[MessageCode["ERROR_IN_CALC_CHAIN"] = 14] = "ERROR_IN_CALC_CHAIN";
    /** Jet submergé, pente trop faible: pas de solution pour calculer l'abscisse de l'impact */
    MessageCode[MessageCode["ERROR_JET_SUBMERGED_NO_SOLUTION"] = 15] = "ERROR_JET_SUBMERGED_NO_SOLUTION";
    /** At least one of the %variables% variables must be defined */
    MessageCode[MessageCode["ERROR_AT_LEAST_ONE_OF_THOSE_MUST_BE_DEFINED"] = 16] = "ERROR_AT_LEAST_ONE_OF_THOSE_MUST_BE_DEFINED";
    /** PAR - simulation : le nombre de ralentisseurs fourni ne correspond pas aux dimensions de la passe %stdNb% */
    MessageCode[MessageCode["ERROR_PAR_NB_INCONSISTENT"] = 17] = "ERROR_PAR_NB_INCONSISTENT";
    /**
     * Passe à ralentisseurs: la valeur donnée de P est plus de 10% plus petite / plus de 5%
     * plus grande que la valeur standard %stdP%
     */
    MessageCode[MessageCode["ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT"] = 18] = "ERROR_PAR_P_DEVIATES_MORE_THAN_10_5_PCT";
    /** La valeur %val% de Q sort de l'intervalle de validité [ %min%, %max% ] donné par les abaques de Q* */
    MessageCode[MessageCode["ERROR_PAR_QSTAR_OUT_OF_RANGE"] = 19] = "ERROR_PAR_QSTAR_OUT_OF_RANGE";
    /** La valeur %val% de ha sort de l'intervalle de validité [ %min%, %max% ] donné par les abaques */
    MessageCode[MessageCode["ERROR_PAR_HA_OUT_OF_RANGE"] = 20] = "ERROR_PAR_HA_OUT_OF_RANGE";
    /** Les valeurs de %var_ZR% et %var_ZD% ne correspondent pas : soit %var_ZR% devrait valoir %expectedZR%, soit %var_ZD% devrait valoir %expectedZD% */
    MessageCode[MessageCode["ERROR_PAR_ZR_ZD_MISMATCH"] = 21] = "ERROR_PAR_ZR_ZD_MISMATCH";
    /**
     * Something failed in certain steps (but not all), when calculating upstream Nubs with varying parameter
     */
    MessageCode[MessageCode["WARNING_ERROR_IN_CALC_CHAIN_STEPS"] = 22] = "WARNING_ERROR_IN_CALC_CHAIN_STEPS";
    /**
     * les bornes de l'intervalle d'un ParamDomain sont incorrectes
     */
    MessageCode[MessageCode["ERROR_PARAMDOMAIN_INTERVAL_BOUNDS"] = 23] = "ERROR_PARAMDOMAIN_INTERVAL_BOUNDS";
    /**
     * la valeur du ParamDomain est incorrecte
     */
    MessageCode[MessageCode["ERROR_PARAMDOMAIN_INVALID"] = 24] = "ERROR_PARAMDOMAIN_INVALID";
    /**
     * la calculabilité d'un ParamDefinition est non définie
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_CALC_UNDEFINED"] = 25] = "ERROR_PARAMDEF_CALC_UNDEFINED";
    /**
     * la valeur d'un ParamDefinition est non définie
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_VALUE_UNDEFINED"] = 26] = "ERROR_PARAMDEF_VALUE_UNDEFINED";
    /**
     * la valeur de la cible d'un ParamDefinition est non définie
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_LINKED_VALUE_UNDEFINED"] = 27] = "ERROR_PARAMDEF_LINKED_VALUE_UNDEFINED";
    /**
     * la valeur d'un ParamDefinition ne peut pas être changée
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_VALUE_FIXED"] = 28] = "ERROR_PARAMDEF_VALUE_FIXED";
    /**
     * la valeur d'un ParamDefinition doit être entière
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_VALUE_INTEGER"] = 29] = "ERROR_PARAMDEF_VALUE_INTEGER";
    /**
     * la valeur d'un ParamDefinition doit être > 0
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_VALUE_POS"] = 30] = "ERROR_PARAMDEF_VALUE_POS";
    /**
     * la valeur d'un ParamDefinition doit être >= 0
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_VALUE_POSNULL"] = 31] = "ERROR_PARAMDEF_VALUE_POSNULL";
    /**
     * la valeur d'un ParamDefinition doit être = 0
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_VALUE_NULL"] = 32] = "ERROR_PARAMDEF_VALUE_NULL";
    /**
     * la valeur d'un ParamDefinition est en dehors de son intervalle autorisé
     */
    MessageCode[MessageCode["ERROR_PARAMDEF_VALUE_INTERVAL"] = 33] = "ERROR_PARAMDEF_VALUE_INTERVAL";
    /**
     * la valeur passée à une méthode de la classe Interval est undefined
     */
    MessageCode[MessageCode["ERROR_INTERVAL_UNDEF"] = 34] = "ERROR_INTERVAL_UNDEF";
    /**
     * la valeur passée à une méthode de la classe Interval est hors de l'intervalle défini
     */
    MessageCode[MessageCode["ERROR_INTERVAL_OUTSIDE"] = 35] = "ERROR_INTERVAL_OUTSIDE";
    /**
     * Un seul ouvrage régulé est autorisé sur la cloison aval
     */
    MessageCode[MessageCode["ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE"] = 36] = "ERROR_CLOISON_AVAL_UN_OUVRAGE_REGULE";
    /**
     * PAB : La cote amont est plus basse que la cote aval
     */
    MessageCode[MessageCode["ERROR_PAB_Z1_LOWER_THAN_Z2"] = 37] = "ERROR_PAB_Z1_LOWER_THAN_Z2";
    /**
     * PAB : La cote amont est plus basse que la cloison amont de la passe
     */
    MessageCode[MessageCode["ERROR_PAB_Z1_LOWER_THAN_UPSTREAM_WALL"] = 38] = "ERROR_PAB_Z1_LOWER_THAN_UPSTREAM_WALL";
    /** PAB : Erreur de calcul de la cote amont de la cloison %n% */
    MessageCode[MessageCode["ERROR_PAB_CALC_Z1_CLOISON"] = 39] = "ERROR_PAB_CALC_Z1_CLOISON";
    /** PAB : Erreur de calcul de la cote amont de la cloison aval */
    MessageCode[MessageCode["ERROR_PAB_CALC_Z1_CLOISON_DW"] = 40] = "ERROR_PAB_CALC_Z1_CLOISON_DW";
    /** Aucune ligne d'eau ne peut être calculée (aucun tirant d'eau à l'amont ni nà l'aval) */
    MessageCode[MessageCode["ERROR_REMOUS_NO_WATER_LINE"] = 41] = "ERROR_REMOUS_NO_WATER_LINE";
    /** Solveur : interdiction de faire varier des paramètres dans le Nub calculé */
    MessageCode[MessageCode["ERROR_SOLVEUR_NO_VARIATED_PARAMS_ALLOWED"] = 42] = "ERROR_SOLVEUR_NO_VARIATED_PARAMS_ALLOWED";
    /** Vérificateur : la passe à vérifier contient des erreurs */
    MessageCode[MessageCode["ERROR_VERIF_ERRORS_IN_PASS"] = 43] = "ERROR_VERIF_ERRORS_IN_PASS";
    /** Vérificateur : le critère testé %var_criterion% est indéfini */
    MessageCode[MessageCode["ERROR_VERIF_MISSING_CRITERION"] = 44] = "ERROR_VERIF_MISSING_CRITERION";
    /** Vérificateur : la passe variée à vérifier contient des erreurs à l'itération %i% */
    MessageCode[MessageCode["ERROR_VERIF_VARYING_ERRORS_IN_PASS"] = 45] = "ERROR_VERIF_VARYING_ERRORS_IN_PASS";
    /** Vérificateur : les nubs Espece ont produit au moins un message de niveau erreur sur l'ensemble des résultats */
    MessageCode[MessageCode["ERROR_VERIF_KO"] = 46] = "ERROR_VERIF_KO";
    /** Vérificateur, passe variée : les nubs Espece ont produit au moins un message de niveau erreur pour chacune des itérations */
    MessageCode[MessageCode["ERROR_VERIF_VARYING_KO"] = 47] = "ERROR_VERIF_VARYING_KO";
    /** Vérificateur : le nub Espece pour le groupe %speciesGroup% a produit au moins un message de niveau erreur dans le résultat en cours */
    MessageCode[MessageCode["ERROR_VERIF_SPECIES_GROUP_KO"] = 48] = "ERROR_VERIF_SPECIES_GROUP_KO";
    /** Vérificateur : le nub Espece pour le groupe %speciesGroup% n'a pas produit de message de niveau erreur, mais en a produit de niveau avertissement, dans le résultat en cours */
    MessageCode[MessageCode["WARNING_VERIF_SPECIES_GROUP_OK_BUT"] = 49] = "WARNING_VERIF_SPECIES_GROUP_OK_BUT";
    /** Vérificateur : le nub Espece pour le groupe %speciesGroup% n'a produit aucun message de niveau erreur */
    MessageCode[MessageCode["INFO_VERIF_SPECIES_GROUP_OK"] = 50] = "INFO_VERIF_SPECIES_GROUP_OK";
    /** Vérificateur : le nub Espece personnalisé %uid% a produit au moins un message de niveau erreur dans le résultat en cours */
    MessageCode[MessageCode["ERROR_VERIF_SPECIES_NUB_KO"] = 51] = "ERROR_VERIF_SPECIES_NUB_KO";
    /** Vérificateur : le nub Espece personnalisé %uid% n'a pas produit de message de niveau erreur, mais en a produit de niveau avertissement, dans le résultat en cours */
    MessageCode[MessageCode["WARNING_VERIF_SPECIES_NUB_OK_BUT"] = 52] = "WARNING_VERIF_SPECIES_NUB_OK_BUT";
    /** Vérificateur : le nub Espece personnalisé %uid% n'a produit aucun message de niveau erreur */
    MessageCode[MessageCode["INFO_VERIF_SPECIES_NUB_OK"] = 53] = "INFO_VERIF_SPECIES_NUB_OK";
    /** Vérificateur, passe à macrorugosités : vitesse max. %V% trop élevée (maximum: %maxV%) */
    MessageCode[MessageCode["ERROR_VERIF_MR_VMAX"] = 54] = "ERROR_VERIF_MR_VMAX";
    /** Vérificateur, passe à macrorugosités : Blocs submergés */
    MessageCode[MessageCode["ERROR_VERIF_MR_SUBMERGED"] = 55] = "ERROR_VERIF_MR_SUBMERGED";
    /** Vérificateur, passe à macrorugosités : puissance dissipée %PV% trop élevée (maximum: %maxPV%) */
    MessageCode[MessageCode["ERROR_VERIF_MR_PVMAX"] = 56] = "ERROR_VERIF_MR_PVMAX";
    /** Vérificateur, passe à macrorugosités : tirant d'eau %Y% insuffisant (minimum: %minY%) */
    MessageCode[MessageCode["ERROR_VERIF_MR_YMIN"] = 57] = "ERROR_VERIF_MR_YMIN";
    /** Vérificateur, passe à macrorugosités complexe : au moins un radier doit être franchissable */
    MessageCode[MessageCode["ERROR_VERIF_MRC_AT_LEAST_ONE_APRON"] = 58] = "ERROR_VERIF_MRC_AT_LEAST_ONE_APRON";
    /** Vérificateur, passe à macrorugosités complexe : la largeur franchissable %width% est inférieure à la largeur d'un motif de blocs %patternWidth% */
    MessageCode[MessageCode["ERROR_VERIF_MRC_CROSSABLE_WIDTH"] = 59] = "ERROR_VERIF_MRC_CROSSABLE_WIDTH";
    /** Vérificateur, passe à macrorugosités complexe : vitesse max. %V% trop élevée (maximum: %maxV%) dans le radier %N% */
    MessageCode[MessageCode["WARNING_VERIF_MRC_VMAX_APRON_N"] = 60] = "WARNING_VERIF_MRC_VMAX_APRON_N";
    /** Vérificateur, passe à macrorugosités complexe : Blocs submergés dans le radier %N% */
    MessageCode[MessageCode["WARNING_VERIF_MRC_SUBMERGED_APRON_N"] = 61] = "WARNING_VERIF_MRC_SUBMERGED_APRON_N";
    /** Vérificateur, passe à macrorugosités complexe : tirant d'eau %Y% insuffisant (minimum: %minY%) dans le radier %N% */
    MessageCode[MessageCode["WARNING_VERIF_MRC_YMIN_APRON_N"] = 62] = "WARNING_VERIF_MRC_YMIN_APRON_N";
    /** Vérificateur, passe à macrorugosités complexe : la largeur franchissable est %width% */
    MessageCode[MessageCode["INFO_VERIF_MRC_CROSSABLE_WIDTH"] = 63] = "INFO_VERIF_MRC_CROSSABLE_WIDTH";
    /** Vérificateur : aucun jeu de contraintes pour le couple espèce / type de passe */
    MessageCode[MessageCode["ERROR_VERIF_NO_PRESET"] = 64] = "ERROR_VERIF_NO_PRESET";
    /** Vérificateur, passe à bassins : jet plongeant non supporté */
    MessageCode[MessageCode["ERROR_VERIF_PAB_DIVING_JET_NOT_SUPPORTED"] = 65] = "ERROR_VERIF_PAB_DIVING_JET_NOT_SUPPORTED";
    /** Vérificateur, passe à bassins : jet plongeant non supporté */
    MessageCode[MessageCode["WARNING_VERIF_PAB_DIVING_JET_NOT_SUPPORTED"] = 66] = "WARNING_VERIF_PAB_DIVING_JET_NOT_SUPPORTED";
    /** Vérificateur, passe à bassins : chute %DH% trop importante pour les deux types de jets (maximum: %maxDHS% et %maxDHP%) */
    MessageCode[MessageCode["ERROR_VERIF_PAB_DHMAX"] = 67] = "ERROR_VERIF_PAB_DHMAX";
    /** Vérificateur, passe à bassins : chute %DH% trop importante pour le type de jet %jetType% (maximum: %maxDH%) */
    MessageCode[MessageCode["ERROR_VERIF_PAB_DHMAX_JET"] = 68] = "ERROR_VERIF_PAB_DHMAX_JET";
    /** Vérificateur, passe à bassins : chute %DH% trop importante pour le type de jet %jetType% (maximum: %maxDH%), mais la cloison est franchissable grâce à l'autre type de jet */
    MessageCode[MessageCode["WARNING_VERIF_PAB_DHMAX_JET"] = 69] = "WARNING_VERIF_PAB_DHMAX_JET";
    /** Vérificateur, passe à bassins : largeur de l'échancrure ou de la fente %L% insuffisante (minimum: %minB%) */
    MessageCode[MessageCode["ERROR_VERIF_PAB_BMIN"] = 70] = "ERROR_VERIF_PAB_BMIN";
    /** Vérificateur, passe à bassins : largeur de l'échancrure ou de la fente %L% insuffisante (minimum: %minB%) */
    MessageCode[MessageCode["WARNING_VERIF_PAB_BMIN"] = 71] = "WARNING_VERIF_PAB_BMIN";
    /** Vérificateur, passe à bassins : surface de l'orifice %S% insuffisante (minimum: %minS%) */
    MessageCode[MessageCode["ERROR_VERIF_PAB_SMIN"] = 72] = "ERROR_VERIF_PAB_SMIN";
    /** Vérificateur, passe à bassins : surface de l'orifice %S% insuffisante (minimum: %minS%) */
    MessageCode[MessageCode["WARNING_VERIF_PAB_SMIN"] = 73] = "WARNING_VERIF_PAB_SMIN";
    /** Vérificateur, passe à bassins : longueur de bassin %LB% insuffisante pour les deux types de jet (minimum: %minLBS% et %minLBP%) */
    MessageCode[MessageCode["ERROR_VERIF_PAB_LMIN"] = 74] = "ERROR_VERIF_PAB_LMIN";
    /** Vérificateur, passe à bassins : longueur de bassin %LB% insuffisante pour le type de jet %jetType% (minimum: %minLB%) */
    MessageCode[MessageCode["ERROR_VERIF_PAB_LMIN_JET"] = 75] = "ERROR_VERIF_PAB_LMIN_JET";
    /** Vérificateur, passe à bassins : longueur de bassin %LB% insuffisante pour le type de jet %jetType% (minimum: %minLB%) */
    MessageCode[MessageCode["WARNING_VERIF_PAB_LMIN_JET"] = 76] = "WARNING_VERIF_PAB_LMIN_JET";
    /** Vérificateur, passe à bassins : charge sur l'échancrure %h1% insuffisante (minimum: %minH%) */
    MessageCode[MessageCode["WARNING_VERIF_PAB_HMIN"] = 77] = "WARNING_VERIF_PAB_HMIN";
    /** Vérificateur, passe à bassins : profondeur de bassin %PB% insuffisante pour les deux types de jet (minimum: %minPBS% et %minPBP%) */
    MessageCode[MessageCode["ERROR_VERIF_PAB_YMOY"] = 78] = "ERROR_VERIF_PAB_YMOY";
    /** Vérificateur, passe à bassins : profondeur de bassin %PB% insuffisante pour le type de jet %jetType% (minimum: %minPB%) */
    MessageCode[MessageCode["ERROR_VERIF_PAB_YMOY_JET"] = 79] = "ERROR_VERIF_PAB_YMOY_JET";
    /** Vérificateur, passe à bassins : profondeur de bassin %PB% insuffisante pour le type de jet %jetType% (minimum: %minPB%) */
    MessageCode[MessageCode["WARNING_VERIF_PAB_YMOY_JET"] = 80] = "WARNING_VERIF_PAB_YMOY_JET";
    /** Vérificateur, passe à bassins, jet plongeant : profondeur de bassin %PB% inférieure à 2x la chute %DH% */
    MessageCode[MessageCode["ERROR_VERIF_PAB_YMOY_2_DH"] = 81] = "ERROR_VERIF_PAB_YMOY_2_DH";
    /** Vérificateur, passe à bassins, jet plongeant : profondeur de bassin %PB% inférieure à 2x la chute %DH% */
    MessageCode[MessageCode["WARNING_VERIF_PAB_YMOY_2_DH"] = 82] = "WARNING_VERIF_PAB_YMOY_2_DH";
    /** Vérificateur, passe à bassins : les poissons ne sont pas censés passer par les ouvrages de type "orifice" (cloison %NC%, ouvrage %NS%) */
    MessageCode[MessageCode["WARNING_VERIF_PAB_ORIFICE"] = 83] = "WARNING_VERIF_PAB_ORIFICE";
    /** Vérificateur, passe à bassins : la puissance dissipée %PV% est trop élevée (maximum : %maxPV%) sur la cloison %N% */
    MessageCode[MessageCode["ERROR_VERIF_PAB_PVMAX"] = 84] = "ERROR_VERIF_PAB_PVMAX";
    /** Vérificateur, passe à bassins : aucun ouvrage de la cloison %N% n'est franchissable */
    MessageCode[MessageCode["ERROR_VERIF_PAB_WALL_NOT_CROSSABLE"] = 85] = "ERROR_VERIF_PAB_WALL_NOT_CROSSABLE";
    /** Vérificateur, passe à bassins : aucun ouvrage de la cloison aval n'est franchissable */
    MessageCode[MessageCode["ERROR_VERIF_PAB_DW_NOT_CROSSABLE"] = 86] = "ERROR_VERIF_PAB_DW_NOT_CROSSABLE";
    /** Vérificateur, passe à ralentisseurs : présence d'une chute en bas de passe */
    MessageCode[MessageCode["ERROR_VERIF_PAR_DH"] = 87] = "ERROR_VERIF_PAR_DH";
    /** Vérificateur, passe à ralentisseurs : pente %S% trop forte (maximum : %maxS%) */
    MessageCode[MessageCode["ERROR_VERIF_PAR_SLOPE"] = 88] = "ERROR_VERIF_PAR_SLOPE";
    /** Vérificateur, passe à ralentisseurs : tirant d'eau %h% insuffisant (minimum: %minY%) */
    MessageCode[MessageCode["ERROR_VERIF_PAR_YMIN"] = 89] = "ERROR_VERIF_PAR_YMIN";
    /** Vérificateur, passe à bassins : la puissance dissipée %PV% est très élevée (recommandée : %maxPV%) sur la cloison %N% */
    MessageCode[MessageCode["WARNING_VERIF_PAB_PVMAX"] = 90] = "WARNING_VERIF_PAB_PVMAX";
    /** Vérificateur, passe à ralentisseurs : les groupes d'espèces 3a, 3b et 7b sont déconseillés */
    MessageCode[MessageCode["WARNING_VERIF_PAR_SPECIES_GROUP"] = 91] = "WARNING_VERIF_PAR_SPECIES_GROUP";
    /**
     * Dever : La cote du lit de la rivière ne peut pas être supérieure à la cote de l'eau
     */
    MessageCode[MessageCode["WARNING_DEVER_ZR_SUP_Z1"] = 92] = "WARNING_DEVER_ZR_SUP_Z1";
    /**
     * courbes de remous : Arrêt du calcul : hauteur critique atteinte à l'abscisse x
     */
    MessageCode[MessageCode["WARNING_REMOUS_ARRET_CRITIQUE"] = 93] = "WARNING_REMOUS_ARRET_CRITIQUE";
    /** Baqssin d'un PréBarrage : description (numéro d'ordre %order%) */
    MessageCode[MessageCode["INFO_PB_BASSIN_DESCRIPTION"] = 94] = "INFO_PB_BASSIN_DESCRIPTION";
    /** Cloison d'un PréBarrage : description (bassin amont %ub%, bassin aval %db%) */
    MessageCode[MessageCode["INFO_PB_CLOISON_DESCRIPTION"] = 95] = "INFO_PB_CLOISON_DESCRIPTION";
    /**
     * courbe de remous : Condition limite aval >= Hauteur critique : calcul de la partie fluviale à partir de l'aval
     */
    MessageCode[MessageCode["INFO_REMOUS_CALCUL_FLUVIAL"] = 96] = "INFO_REMOUS_CALCUL_FLUVIAL";
    /**
     * courbe de remous : Condition limite amont <= Hauteur critique :
     * calcul de la partie torrentielle à partir de l'amont
     */
    MessageCode[MessageCode["INFO_REMOUS_CALCUL_TORRENTIEL"] = 97] = "INFO_REMOUS_CALCUL_TORRENTIEL";
    /**
     * courbe de remous : ressaut hydraulique détecté à l'amont/aval de l'abscisse x
     */
    MessageCode[MessageCode["INFO_REMOUS_RESSAUT_DEHORS"] = 98] = "INFO_REMOUS_RESSAUT_DEHORS";
    /**
     * courbe de remous : Largeur au niveau des berges
     */
    MessageCode[MessageCode["INFO_REMOUS_LARGEUR_BERGE"] = 99] = "INFO_REMOUS_LARGEUR_BERGE";
    /**
     * courbe de remous : Tirant d'eau critique
     */
    MessageCode[MessageCode["INFO_REMOUS_H_CRITIQUE"] = 100] = "INFO_REMOUS_H_CRITIQUE";
    /**
     * courbe de remous : Tirant d'eau normal
     */
    MessageCode[MessageCode["INFO_REMOUS_H_NORMALE"] = 101] = "INFO_REMOUS_H_NORMALE";
    /**
     * courbe de remous : Ressaut hydraulique détecté entre les abscisses Xmin et Xmax m
     */
    MessageCode[MessageCode["INFO_REMOUS_RESSAUT_HYDRO"] = 102] = "INFO_REMOUS_RESSAUT_HYDRO";
    /** Verificateur : tout s'est bien passé (aucun autre message d'avertissement ni d'erreur) */
    MessageCode[MessageCode["INFO_VERIF_OK"] = 103] = "INFO_VERIF_OK";
    /** Verificateur, passe variée : tout s'est bien passé à toutes les itérations (aucun message d'erreur sur aucune des itérations) */
    MessageCode[MessageCode["INFO_VERIF_VARYING_OK"] = 104] = "INFO_VERIF_VARYING_OK";
    /** Verificateur : tout s'est bien passé mais il y a des messages d'avertissement */
    MessageCode[MessageCode["WARNING_VERIF_OK_BUT"] = 105] = "WARNING_VERIF_OK_BUT";
    /** Verificateur, passe variée : au moins une itération "passe" et au moins une "ne passe pas" */
    MessageCode[MessageCode["WARNING_VERIF_VARYING_OK_BUT"] = 106] = "WARNING_VERIF_VARYING_OK_BUT";
    /**
     * courbe de remous : La pente de la ligne d'eau est trop forte à l'abscisse x m
     */
    MessageCode[MessageCode["ERROR_REMOUS_PENTE_FORTE"] = 107] = "ERROR_REMOUS_PENTE_FORTE";
    /**
     * courbe de remous : Condition limite aval < Hauteur critique : pas de calcul possible depuis l'aval
     */
    MessageCode[MessageCode["WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL"] = 108] = "WARNING_REMOUS_PAS_CALCUL_DEPUIS_AVAL";
    /**
     * courbe de remous : Condition limite amont > Hauteur critique : pas de calcul possible depuis l'amont
     */
    MessageCode[MessageCode["WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT"] = 109] = "WARNING_REMOUS_PAS_CALCUL_DEPUIS_AMONT";
    /**
     * courbe de remous : pas de calcul possible, ni depuis l'amont ni depuis l'aval
     */
    MessageCode[MessageCode["ERROR_REMOUS_PAS_CALCUL"] = 110] = "ERROR_REMOUS_PAS_CALCUL";
    /** RegimeUniforme : impossible de calculer avec uen conduite en charge (section circulaire) */
    MessageCode[MessageCode["ERROR_RU_CIRC_LEVEL_TOO_HIGH"] = 111] = "ERROR_RU_CIRC_LEVEL_TOO_HIGH";
    /**
     * section : Non convergence du calcul de la hauteur critique (Méthode de Newton)
     */
    MessageCode[MessageCode["ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCRITIQUE"] = 112] = "ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCRITIQUE";
    /**
     * section : Non convergence du calcul de la hauteur normale (Méthode de Newton)
     */
    MessageCode[MessageCode["ERROR_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE"] = 113] = "ERROR_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE";
    /** équivalent de l'erreur ci-dessus, pour la courbe de remous */
    MessageCode[MessageCode["WARNING_YN_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE"] = 114] = "WARNING_YN_SECTION_NON_CONVERGENCE_NEWTON_HNORMALE";
    /**
     * section : Non convergence du calcul de la hauteur conjuguée (Méthode de Newton)
     */
    MessageCode[MessageCode["ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCONJUG"] = 115] = "ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCONJUG";
    /**
     * section : Non convergence du calcul de la hauteur correspondante (Méthode de Newton)
     */
    MessageCode[MessageCode["ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR"] = 116] = "ERROR_SECTION_NON_CONVERGENCE_NEWTON_HCOR";
    /**
     * section : La pente est négative ou nulle, la hauteur normale est infinie
     */
    MessageCode[MessageCode["ERROR_SECTION_PENTE_NEG_NULLE_HNORMALE_INF"] = 117] = "ERROR_SECTION_PENTE_NEG_NULLE_HNORMALE_INF";
    /** équivalent de l'erreur ci-dessus, pour la courbe de remous */
    MessageCode[MessageCode["WARNING_YN_SECTION_PENTE_NEG_NULLE_HNORMALE_INF"] = 118] = "WARNING_YN_SECTION_PENTE_NEG_NULLE_HNORMALE_INF";
    /**
     * section : calcul impossible à cause d'un périmètre nul
     */
    MessageCode[MessageCode["ERROR_SECTION_PERIMETRE_NUL"] = 119] = "ERROR_SECTION_PERIMETRE_NUL";
    /**
     * section : calcul impossible à cause d'un rayon nul
     */
    MessageCode[MessageCode["ERROR_SECTION_RAYON_NUL"] = 120] = "ERROR_SECTION_RAYON_NUL";
    /**
     * section : calcul impossible à cause d'une surface nulle
     */
    MessageCode[MessageCode["ERROR_SECTION_SURFACE_NULLE"] = 121] = "ERROR_SECTION_SURFACE_NULLE";
    /**
     * newton : pas de convergence
     */
    MessageCode[MessageCode["ERROR_NEWTON_NON_CONVERGENCE"] = 122] = "ERROR_NEWTON_NON_CONVERGENCE";
    /**
     * newton : dérivée nulle
     */
    MessageCode[MessageCode["ERROR_NEWTON_DERIVEE_NULLE"] = 123] = "ERROR_NEWTON_DERIVEE_NULLE";
    /**
     * Le paramètre "Cote de radier" ne peut pas être calculé avec cette loi de débit
     */
    MessageCode[MessageCode["ERROR_STRUCTURE_ZDV_PAS_CALCULABLE"] = 124] = "ERROR_STRUCTURE_ZDV_PAS_CALCULABLE";
    /**
     * Le débit passant par les autres ouvrages est trop important
     */
    MessageCode[MessageCode["ERROR_STRUCTURE_Q_TROP_ELEVE"] = 125] = "ERROR_STRUCTURE_Q_TROP_ELEVE";
    /**
     * Les cotes amont aval sont égales et le débit n'est pas nul
     */
    MessageCode[MessageCode["ERROR_STRUCTURE_Z_EGAUX_Q_NON_NUL"] = 126] = "ERROR_STRUCTURE_Z_EGAUX_Q_NON_NUL";
    /**
     * Il faut au moins un ouvrage dans une structure
     */
    MessageCode[MessageCode["ERROR_STRUCTURE_AU_MOINS_UNE"] = 127] = "ERROR_STRUCTURE_AU_MOINS_UNE";
    /** On essaye d'appliquer une puissance non entière à un nombre négatif */
    MessageCode[MessageCode["ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER"] = 128] = "ERROR_NON_INTEGER_POWER_ON_NEGATIVE_NUMBER";
    /** abstract showing number of warning messages encountered in an iterative calculation */
    MessageCode[MessageCode["WARNING_WARNINGS_ABSTRACT"] = 129] = "WARNING_WARNINGS_ABSTRACT";
    /** Déversoirs: la cote de déversoir est en dessous de la cote de fond du lit */
    MessageCode[MessageCode["WARNING_DEVER_ZDV_INF_ZR"] = 130] = "WARNING_DEVER_ZDV_INF_ZR";
    /** La cote de fond aval est plus élevée que la code de l'eau aval */
    MessageCode[MessageCode["WARNING_DOWNSTREAM_BOTTOM_HIGHER_THAN_WATER"] = 131] = "WARNING_DOWNSTREAM_BOTTOM_HIGHER_THAN_WATER";
    /** La cote de fond amont est plus élevée que la code de l'eau amont */
    MessageCode[MessageCode["WARNING_UPSTREAM_BOTTOM_HIGHER_THAN_WATER"] = 132] = "WARNING_UPSTREAM_BOTTOM_HIGHER_THAN_WATER";
    /**
     * La cote de l'eau aval est plus élevée que la cote du seuil (ennoiement possible)
     */
    MessageCode[MessageCode["WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION"] = 133] = "WARNING_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION";
    /**
     * Lechapt-Calmon : la vitesse est en dehors de l'intervalle [0.4, 2]
     */
    MessageCode[MessageCode["WARNING_LECHAPT_CALMON_SPEED_OUTSIDE_04_2"] = 134] = "WARNING_LECHAPT_CALMON_SPEED_OUTSIDE_04_2";
    /**
     * La cote de l'eau aval est plus élevée que la cote du centre de l'orifice (ennoiement possible)
     */
    MessageCode[MessageCode["WARNING_ORIFICE_FREE_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION"] = 135] = "WARNING_ORIFICE_FREE_DOWNSTREAM_ELEVATION_POSSIBLE_SUBMERSION";
    /**
     * Grille orientée, préconisation pour le guidage des poissons : α ≤ 45°
     */
    MessageCode[MessageCode["WARNING_GRILLE_ALPHA_GREATER_THAN_45"] = 136] = "WARNING_GRILLE_ALPHA_GREATER_THAN_45";
    /**
     * Grille inclinée, préconisation pour le guidage des poissons : β ≤ 26°
     */
    MessageCode[MessageCode["WARNING_GRILLE_BETA_GREATER_THAN_26"] = 137] = "WARNING_GRILLE_BETA_GREATER_THAN_26";
    /** L'obstruction totale (saisie) est inférieure à l'obstruction due aux barreaux seulement (calculée) */
    MessageCode[MessageCode["WARNING_GRILLE_O_LOWER_THAN_OB"] = 138] = "WARNING_GRILLE_O_LOWER_THAN_OB";
    /**
     * Préconisation pour éviter le placage des poissons sur le plan de grille (barrière physique),
     * ou leur passage prématuré au travers (barrière comportementale) : VN ≤ 0.5 m/s
     */
    MessageCode[MessageCode["WARNING_GRILLE_VN_GREATER_THAN_05"] = 139] = "WARNING_GRILLE_VN_GREATER_THAN_05";
    /** La cote de départ du jet est plus basse que la code de l'eau */
    MessageCode[MessageCode["WARNING_JET_START_SUBMERGED"] = 140] = "WARNING_JET_START_SUBMERGED";
    /** La cote de départ du jet est plus basse que la code de fond */
    MessageCode[MessageCode["WARNING_JET_START_ELEVATION_UNDERGROUND"] = 141] = "WARNING_JET_START_ELEVATION_UNDERGROUND";
    /** La cote de l'eau est plus basse ou égale à la cote de fond */
    MessageCode[MessageCode["WARNING_JET_WATER_ELEVATION_UNDERGROUND"] = 142] = "WARNING_JET_WATER_ELEVATION_UNDERGROUND";
    /** La longueur du résultat varié est limitée par un résultat varié lié trop court */
    MessageCode[MessageCode["WARNING_VARIATED_LENGTH_LIMITED_BY_LINKED_RESULT"] = 143] = "WARNING_VARIATED_LENGTH_LIMITED_BY_LINKED_RESULT";
    /** Macrorugo : la rampe ne contient pas au moins un motif de plot */
    MessageCode[MessageCode["WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH"] = 144] = "WARNING_RAMP_WIDTH_LOWER_THAN_PATTERN_WIDTH";
    /** Macrorugo : la largeur de la rampe devrait être un multiple de la demie-largeur d'un motif de plot */
    MessageCode[MessageCode["WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH"] = 145] = "WARNING_RAMP_WIDTH_NOT_MULTIPLE_OF_HALF_PATTERN_WIDTH";
    /** MacroRugo : concentration des blocs hors 8-20% */
    MessageCode[MessageCode["WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS"] = 146] = "WARNING_MACRORUGO_CONCENTRATION_OUT_OF_BOUNDS";
    /** section : le tirant d'eau dépasse la hauteur de berge */
    MessageCode[MessageCode["WARNING_SECTION_OVERFLOW"] = 147] = "WARNING_SECTION_OVERFLOW";
    /** section : le tirant d'eau dépasse la hauteur de berge entre les abscisses %xa% et %xb% */
    MessageCode[MessageCode["WARNING_SECTION_OVERFLOW_ABSC"] = 148] = "WARNING_SECTION_OVERFLOW_ABSC";
    /**
     * StructureKivi : La pelle du seuil doit mesurer au moins 0,1 m. Le coefficient béta est forcé à 0.
     */
    MessageCode[MessageCode["WARNING_STRUCTUREKIVI_PELLE_TROP_FAIBLE"] = 149] = "WARNING_STRUCTUREKIVI_PELLE_TROP_FAIBLE";
    /**
     * StructureKivi : h/p ne doit pas être supérieur à 2,5. h/p est forcé à 2,5.
     */
    MessageCode[MessageCode["WARNING_STRUCTUREKIVI_HP_TROP_ELEVE"] = 150] = "WARNING_STRUCTUREKIVI_HP_TROP_ELEVE";
    /** Cloisons: la pelle de l'ouvrage est en dessous du radier */
    MessageCode[MessageCode["WARNING_NEGATIVE_SILL"] = 151] = "WARNING_NEGATIVE_SILL";
    /**
     * La formule de l'échancrure n'est pas conseillée pour un ennoiement supérieur à 0.7
     */
    MessageCode[MessageCode["WARNING_NOTCH_SUBMERSION_GREATER_THAN_07"] = 152] = "WARNING_NOTCH_SUBMERSION_GREATER_THAN_07";
    /**
     * La formule de la fente n'est pas conseillée pour un ennoiement inférieur à 0.7 et supérieur à 0.9
     */
    MessageCode[MessageCode["WARNING_SLOT_SUBMERSION_NOT_BETWEEN_07_AND_09"] = 153] = "WARNING_SLOT_SUBMERSION_NOT_BETWEEN_07_AND_09";
    /** PAR : la largeur est plus grande que la valeur d'alerte %max% */
    MessageCode[MessageCode["WARNING_PAR_L"] = 154] = "WARNING_PAR_L";
    /** PAR : la largeur est en dehors des valeurs admissibles %min% et %max% (comme ERROR_PAR_L), mais pas d'erreur fatale */
    MessageCode[MessageCode["WARNING_ERROR_PAR_L"] = 155] = "WARNING_ERROR_PAR_L";
    /** PAR : la pente est en dehors des valeurs standard (%min%, facultatif) et %max% */
    MessageCode[MessageCode["WARNING_PAR_S"] = 156] = "WARNING_PAR_S";
    /** PAR : la largeur est en dehors des valeurs admissibles %min% et %max% */
    MessageCode[MessageCode["ERROR_PAR_L"] = 157] = "ERROR_PAR_L";
    /** PAR : la pente est en dehors des valeurs admissibles %min% et %max% */
    MessageCode[MessageCode["ERROR_PAR_S"] = 158] = "ERROR_PAR_S";
    /** PAR superactive / chevrons : la hauteur est en dehors des valeurs admissibles %min% et %max% */
    MessageCode[MessageCode["ERROR_PAR_A"] = 159] = "ERROR_PAR_A";
    /** PAR superactive / chevrons : la hauteur dépasse la valeur d'alerte %max% */
    MessageCode[MessageCode["WARNING_PAR_A"] = 160] = "WARNING_PAR_A";
    /** PAR superactive / chevrons : le nombre de motifs a été arrondi à 0.5 près, à la valeur %val% */
    MessageCode[MessageCode["WARNING_PAR_N_ROUNDED_TO_05"] = 161] = "WARNING_PAR_N_ROUNDED_TO_05";
    /** PAR chevrons : le nombre de bandes dépasse deux fois le nombre de motifs %max% */
    MessageCode[MessageCode["ERROR_PAR_M_GREATER_THAN_2_N"] = 162] = "ERROR_PAR_M_GREATER_THAN_2_N";
    /** PAR chevrons : le nombre de bandes a été arrondi à 1 près, à la valeur %val% */
    MessageCode[MessageCode["WARNING_PAR_M_ROUNDED_TO_1"] = 163] = "WARNING_PAR_M_ROUNDED_TO_1";
    /** PAR :  La cote de l'eau aval doit être supérieure ou égale à la hauteur d'eau dans la passe au niveau du dernier ralentisseur aval %ZDB% */
    MessageCode[MessageCode["WARNING_PAR_NOT_SUBMERGED"] = 164] = "WARNING_PAR_NOT_SUBMERGED";
    /**
     * La formule du seuil noyé n'est pas conseillé pour un ennoiement inférieur à 0.8
     */
    MessageCode[MessageCode["WARNING_WEIR_SUBMERSION_LOWER_THAN_08"] = 165] = "WARNING_WEIR_SUBMERSION_LOWER_THAN_08";
    /**
     * Vanne levante : ZDV > ZDV max
     */
    MessageCode[MessageCode["WARNING_VANLEV_ZDV_SUP_MAX"] = 166] = "WARNING_VANLEV_ZDV_SUP_MAX";
    /**
     * Vanne levante : ZDV < ZDV min
     */
    MessageCode[MessageCode["WARNING_VANLEV_ZDV_INF_MIN"] = 167] = "WARNING_VANLEV_ZDV_INF_MIN";
    /**
     * Des problèmes ont été rencontrés durant le calcul
     */
    MessageCode[MessageCode["WARNING_PROBLEMS_ENCOUNTERED"] = 168] = "WARNING_PROBLEMS_ENCOUNTERED";
    /**
     * La valeur du paramètre a été arrondie à l'entier
     */
    MessageCode[MessageCode["WARNING_VALUE_ROUNDED_TO_INTEGER"] = 169] = "WARNING_VALUE_ROUNDED_TO_INTEGER";
    /** %name% n°%position% : */
    MessageCode[MessageCode["INFO_PARENT_PREFIX"] = 170] = "INFO_PARENT_PREFIX";
    /** %name%%position% */
    MessageCode[MessageCode["INFO_PARENT_PREFIX_SHORT"] = 171] = "INFO_PARENT_PREFIX_SHORT";
    /** downwall : */
    MessageCode[MessageCode["INFO_PARENT_PREFIX_DOWNWALL"] = 172] = "INFO_PARENT_PREFIX_DOWNWALL";
    /**
     * Pré-barrage : non convergence du calcul
     */
    MessageCode[MessageCode["WARNING_PREBARRAGE_NON_CONVERGENCE"] = 173] = "WARNING_PREBARRAGE_NON_CONVERGENCE";
    /** Pré-barrage : cote de l'eau aval supérieure à la cote de l'eau amont */
    MessageCode[MessageCode["ERROR_PREBARRAGE_Z2_SUP_Z1"] = 174] = "ERROR_PREBARRAGE_Z2_SUP_Z1";
    /** Pré-barrage : cote de fond du bassin %n% supérieure à la cote de l'eau amont */
    MessageCode[MessageCode["WARNING_PREBARRAGE_BASSIN_ZF_SUP_Z1"] = 175] = "WARNING_PREBARRAGE_BASSIN_ZF_SUP_Z1";
    /** Pré-barrage : cote de radier de l'ouvrage %ns% inférieure à la cote de fond du bassin amont de la cloison %cub%-%cdb% */
    MessageCode[MessageCode["ERROR_PREBARRAGE_STRUCTURE_ZDV_INF_ZF"] = 176] = "ERROR_PREBARRAGE_STRUCTURE_ZDV_INF_ZF";
})(MessageCode = exports.MessageCode || (exports.MessageCode = {}));
/**
 * niveau de criticité du message
 */
var MessageSeverity;
(function (MessageSeverity) {
    MessageSeverity[MessageSeverity["ERROR"] = 0] = "ERROR";
    MessageSeverity[MessageSeverity["WARNING"] = 1] = "WARNING";
    MessageSeverity[MessageSeverity["INFO"] = 2] = "INFO";
})(MessageSeverity = exports.MessageSeverity || (exports.MessageSeverity = {}));
/**
 * Résultat de calcul comprenant la valeur du résultat et des calculs annexes (flag, calculs intermédiaires...)
 */
class Message {
    constructor(c, extraVar = {}) {
        this._code = c;
        this.extraVar = extraVar;
    }
    get code() { return this._code; }
    /**
     * retourne le niveau de criticité (erreur, warning, info) du message
     */
    getSeverity() {
        const m = MessageCode[this._code];
        const prefix = m.split("_")[0];
        switch (prefix) {
            case "ERROR":
                return MessageSeverity.ERROR;
            case "WARNING":
                return MessageSeverity.WARNING;
            case "INFO":
                return MessageSeverity.INFO;
        }
        throw new Error("Message.getSeverity() : valeur de code '" + this._code + "' invalide");
    }
    toString() {
        return MessageCode[this._code] + " " + JSON.stringify(this.extraVar);
    }
}
exports.Message = Message;

},{}],152:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NumberArrayIterator = void 0;
/**
 * itérateur sur les valeurs prises par un tableau
 */
class NumberArrayIterator {
    constructor(_arr) {
        this._arr = _arr;
        this._it = this._arr[Symbol.iterator](); // crée un itérateur à partir d'un tableau
        this._index = 0;
    }
    count() {
        return this._arr.length;
    }
    get hasNext() {
        return this._index < this._arr.length;
    }
    next() {
        this._index++;
        const res = this._it.next();
        if (!res.done) {
            this._current = res.value;
        }
        return res;
    }
    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    nextValue() {
        return this.next();
    }
    get currentValue() {
        return this._current;
    }
    // interface IterableIterator
    [Symbol.iterator]() {
        return this;
    }
}
exports.NumberArrayIterator = NumberArrayIterator;

},{}],153:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NumberArrayReverseIterator = void 0;
const array_reverse_iterator_1 = require("../util/array_reverse_iterator");
/**
 * itérateur sur les valeurs prises par un tableau (parcourues depuis la fin)
 */
class NumberArrayReverseIterator extends array_reverse_iterator_1.ArrayReverseIterator {
    constructor(arr) {
        super(arr);
        this._count = 0;
    }
    count() {
        return this._arr.length;
    }
    get hasNext() {
        return this._count < super._arr.length;
    }
    next() {
        this._count++;
        const res = super.next();
        if (!res.done) {
            this._current = res.value;
        }
        return res;
    }
    /** trick method - use .next() instead, unless you are explicitely in jalhyd#222 case */
    nextValue() {
        return this.next();
    }
    get currentValue() {
        return this._current;
    }
    // interface IterableIterator
    [Symbol.iterator]() {
        return this;
    }
}
exports.NumberArrayReverseIterator = NumberArrayReverseIterator;

},{"../util/array_reverse_iterator":146}],154:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.asObservable = exports.isObservable = exports.Observable = void 0;
class Observable {
    constructor() {
        this._observers = [];
    }
    /**
     * ajoute un observateur à la liste
     */
    addObserver(o) {
        if (this._observers.indexOf(o) === -1) {
            this._observers.push(o);
        }
    }
    /**
     * supprime un observateur de la liste
     */
    removeObserver(o) {
        this._observers = this._observers.filter((a) => a !== o);
    }
    /**
     * notifie un événement aux observateurs
     */
    notifyObservers(data, sender) {
        if (sender === undefined) {
            sender = this;
        }
        for (const o of this._observers) {
            o.update(sender, data);
        }
    }
    getObservers() {
        return this._observers;
    }
}
exports.Observable = Observable;
/**
 * @return true si l'objet passé implémente IObservable
 * @param o objet à tester
 */
function isObservable(o) {
    if (o === undefined) {
        return false;
    }
    const ob = o;
    return typeof ob.addObserver === "function";
}
exports.isObservable = isObservable;
/**
 * cast d'un objet implémentant IObservable
 * @param o objet à convertir
 */
function asObservable(o) {
    if (isObservable(o)) {
        return o;
    }
    return undefined;
}
exports.asObservable = asObservable;

},{}],155:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Result = void 0;
const jalhyd_object_1 = require("../jalhyd_object");
const log_1 = require("./log");
const message_1 = require("./message");
const resultelement_1 = require("./resultelement");
/**
 * Result of a calculation.
 * May contain several ResultElement, if one or more parameters were varying.
 */
class Result extends jalhyd_object_1.JalhydObject {
    /**
     * @param v value of calculated parameter, or result element, or (usually error) Message
     * @param sourceNub Nub that generated this result
     * @param extraValues map of result values by symbol, to add to the first result element
     */
    constructor(v, sourceNub, extraValues) {
        super();
        this._globalLog = new log_1.cLog(this);
        this._resultElements = [];
        this._sourceNub = sourceNub;
        // try to store main result symbol
        if (this._sourceNub) {
            if (this._sourceNub.calculatedParam) {
                this.symbol = this._sourceNub.calculatedParam.symbol;
            }
        }
        // detect first argument
        if (typeof (v) === "number") {
            this.addResultElement(new resultelement_1.ResultElement());
            this.resultElement.vCalc = v;
        }
        else if (v instanceof message_1.Message) {
            this.addResultElement(new resultelement_1.ResultElement(v));
        }
        else if (v instanceof resultelement_1.ResultElement) {
            this.resultElement = v;
        }
        if (extraValues !== undefined) { // for setter
            this.resultElement.mergeValues(extraValues);
        }
    }
    get symbol() {
        return this._symbol;
    }
    set symbol(s) {
        this._symbol = s;
        // hack to update "vCalc"
        if (this.resultElement) {
            this.resultElement.updateVCalcSymbol(s);
        }
    }
    get sourceNub() {
        return this._sourceNub;
    }
    /** Use with caution, for Solveur for ex. */
    updateSourceNub(sn) {
        this._sourceNub = sn;
    }
    get globalLog() {
        return this._globalLog;
    }
    get resultElements() {
        return this._resultElements;
    }
    /**
     * @return the most recent result element
     */
    get resultElement() {
        /* if (this._resultElements.length === 0) {
            throw new Error("Result.resultElement() : there are no result elements");
        } */
        return this._resultElements[this._resultElements.length - 1];
    }
    /**
     * sets the most recent result element
     */
    set resultElement(r) {
        if (this._resultElements.length === 0) {
            this._resultElements.push(r);
        }
        else {
            this._resultElements[this._resultElements.length - 1] = r;
        }
        r.parent = this;
    }
    addResultElement(r) {
        this._resultElements.push(r);
        r.parent = this;
    }
    /**
     * @return true if there is at least 1 result element
     */
    hasResultElements() {
        return this._resultElements.length > 0;
    }
    /**
     * @return true if there are at least 2 result elements
     */
    hasMultipleValues() {
        return this.resultElements.length > 1;
    }
    /**
     * Returns the list of calculated values for the parameter in CALC mode;
     * if no parameter is variated, the list will contain only 1 value
     */
    getCalculatedValues() {
        return this.resultElements.map((re) => {
            return re.vCalc;
        });
    }
    /**
     * @returns proxy to .resultElement.ok (false if there is no result element)
     */
    get ok() {
        return this._resultElements.length > 0 && this.resultElement.ok;
    }
    // -------------- log related methods
    /** adds a message to the global log */
    addMessage(m) {
        this._globalLog.add(m);
    }
    /** merges the given log with the global log */
    addLog(l) {
        this._globalLog.addLog(l);
    }
    /**
     * @return true if global log has at least one message, whatever its level
     */
    hasGlobalLog() {
        return this.globalLog.messages.length > 0;
    }
    /**
     * @return true if union of global log and result elements logs has at least one
     *         message, whatever its level
     */
    hasLog() {
        if (this.hasGlobalLog()) {
            return true;
        }
        else {
            // result elements logs
            for (const r of this._resultElements) {
                if (r.hasLog()) {
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * @returns true if globalLog has at least one message with level ERROR
     */
    hasGlobalError() {
        for (const m of this._globalLog.messages) {
            if (m.getSeverity() === message_1.MessageSeverity.ERROR) {
                return true;
            }
        }
        return false;
    }
    /**
     * @returns true if globalLog has at least one message with level WARNING
     * @WARNING this is NOT the famous hasGlobalWarming() Al-Gore-ithm !
     */
    hasGlobalWarning() {
        for (const m of this._globalLog.messages) {
            if (m.getSeverity() === message_1.MessageSeverity.WARNING) {
                return true;
            }
        }
        return false;
    }
    /**
     * @returns true if at least one of the log messages (general log or result elements logs)
     *          has an error-level message, i.e. true if at least one calculation has failed
     */
    hasErrorMessages() {
        // global log
        if (this.hasGlobalError()) {
            return true;
        }
        // result elements logs
        for (const r of this._resultElements) {
            if (r.hasErrorMessages()) {
                return true;
            }
        }
        return false;
    }
    /**
     * @returns true if at least one of the log messages (general log or result elements logs)
     *          has a warning-level message
     */
    hasWarningMessages() {
        // global log
        if (this.hasGlobalWarning()) {
            return true;
        }
        // result elements logs
        for (const r of this._resultElements) {
            if (r.hasWarningMessages()) {
                return true;
            }
        }
        return false;
    }
    /**
     * @returns true if every result element log contains an error (i.e. no calculation has succeeded);
     *          does not inspect global log
     */
    get hasOnlyErrors() {
        for (const r of this._resultElements) {
            if (!r.hasErrorMessages()) {
                return false;
            }
        }
        return true;
    }
    // -------------- shortcuts to most recent result element
    /**
     * @return value of the calculated parameter, for the most recent result element
     */
    get vCalc() {
        return this.resultElement.vCalc;
    }
    /**
     * sets the value of the calculated parameter, for the most recent result element
     */
    set vCalc(r) {
        this.resultElement.vCalc = r;
    }
    /**
     * @returns log from the most recent result element
     * @TODO ambiguous / counter-intuitive : should be renamed
     */
    get log() {
        return this.resultElement.log;
    }
    // set of all results, from most recent result element
    get values() {
        return this.resultElement.values;
    }
    // set of all results except vCalc, from most recent result element
    get extraResults() {
        return this.resultElement.extraResults;
    }
    // value of a result given its symbol, from most recent result element
    getValue(name) {
        return this.resultElement.getValue(name);
    }
    /**
     * Removes all values that are not vCalc, from all result elements
     */
    removeExtraResults() {
        for (const re of this.resultElements) {
            re.removeExtraResults();
        }
    }
}
exports.Result = Result;

},{"../jalhyd_object":15,"./log":149,"./message":151,"./resultelement":156}],156:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResultElement = void 0;
const log_1 = require("./log");
const message_1 = require("./message");
/**
 * Calculation result for one iteration.
 * May include value of the Nub's calculated parameter (if any), and/or other
 * values (previously known as extraResults)
 */
class ResultElement {
    /**
     * @param v value of calculated parameter, or key-values map, or (usually error) Message
     */
    constructor(v) {
        this.log = new log_1.cLog();
        this._values = {};
        this._vCalcSymbol = ""; // default retrocompatible pseudo-symbol for cases like  `new ResultElement(42);`
        // detect first argument
        if (typeof v === "number") {
            this.vCalc = v;
        }
        else if (v instanceof message_1.Message) {
            this.addMessage(v);
        }
        else if (v !== undefined) {
            // assuming key-value map
            this._values = v;
            // assuming 1st value is vCalc
            if (Object.keys(v).length > 0) {
                this._vCalcSymbol = Object.keys(v)[0];
            }
        }
    }
    get parent() {
        return this._parent;
    }
    set parent(r) {
        this._parent = r;
        this.log.parent = this.parent;
    }
    /**
     * @returns value of the calculated parameter (main result); might be undefined,
     *          for ex. with SectionParametree
     */
    get vCalc() {
        return this._values[this._vCalcSymbol];
    }
    /**
     * sets value of the calculated parameter (main result)
     */
    set vCalc(r) {
        this.setVCalc(r);
    }
    get vCalcSymbol() {
        return this._vCalcSymbol;
    }
    setVCalc(r, keepSymbol = false) {
        if (!keepSymbol && this.parent && this.parent.symbol) {
            this._vCalcSymbol = this.parent.symbol;
        }
        this._values[this._vCalcSymbol] = r;
    }
    /**
     * Hack to update the key of vCalc, when Result.symbol is updated
     * after Equation() returned a ResultElement
     */
    updateVCalcSymbol(s) {
        if (!this.hasValues) {
            throw new Error("updateVCalcSymbol() : values map is empty");
        }
        const tmp = this.vCalc;
        delete this._values[this._vCalcSymbol];
        this._vCalcSymbol = s;
        this.setVCalc(tmp, true);
    }
    /**
     * Sets vCalcSymbol back to the parent Nub's calculated parameter symbol
     */
    resetVCalcSymbol() {
        this.updateVCalcSymbol(this.parent.sourceNub.calculatedParam.symbol);
    }
    /**
     * Adds given values map to the local values map; in case of key conflict,
     * precedence goes to the given value
     */
    mergeValues(values) {
        this._values = Object.assign(Object.assign({}, this._values), values);
    }
    /**
     * Returns an array of all the keys in the local values map,
     * where vCalcSymbol is always in first position (used by
     * GUI to iterate on displayable results)
     */
    get keys() {
        const keys = Object.keys(this._values);
        //  make sure vCalc symbol is always first
        if (this._vCalcSymbol) { // sometimes empty (ex: SectionParametree)
            const index = keys.indexOf(this._vCalcSymbol);
            if (index > -1) {
                keys.splice(index, 1);
            }
            keys.unshift(this._vCalcSymbol);
        }
        return keys;
    }
    /** Returns the whole key-value map */
    get values() {
        return this._values;
    }
    /**
     * @returns the result value associated to the given symbol, or undefined if the given
     *          symbol was not found in the local values map
     */
    getValue(symbol) {
        return this._values[symbol];
    }
    get firstValue() {
        return this._values[Object.keys(this._values)[0]];
    }
    count() {
        return Object.keys(this._values).length;
    }
    /**
     * @returns true if local values map is not empty, and local log has no error-level
     *          message (log still might have non-error level messages)
     */
    get ok() {
        return this.hasValues() && !this.hasErrorMessages();
    }
    /**
     * @returns true if local values map has at least one key-value pair
     */
    hasValues() {
        return Object.keys(this._values).length > 0;
    }
    /**
     * Adds a key-value pair to the values map
     */
    addExtraResult(name, value) {
        this._values[name] = value;
    }
    /**
     * Returns a copy of the local key-values map, without vCalc (only "extra" results)
     */
    get extraResults() {
        const er = JSON.parse(JSON.stringify(this._values)); // clone
        // remove vCalc
        delete er[this._vCalcSymbol];
        return er;
    }
    /**
     * Removes all values that are not vCalc
     */
    removeExtraResults() {
        for (const k in this._values) {
            if (k !== this._vCalcSymbol) {
                delete this._values[k];
            }
        }
    }
    toString() {
        if (this.vCalc !== undefined) {
            return String(this.vCalc);
        }
        return JSON.stringify(this);
    }
    // -------------- log related methods
    /** adds a message to the global log */
    addMessage(m) {
        this.log.add(m);
    }
    /** @returns true if current log has at least one message */
    hasLog() {
        return this.log.messages.length > 0;
    }
    /**
     * @returns true if at least one of the log messages has an error level,
     *          i.e. if calculation has failed
     */
    hasErrorMessages() {
        for (const m of this.log.messages) {
            if (m.getSeverity() === message_1.MessageSeverity.ERROR) {
                return true;
            }
        }
        return false;
    }
    /** @returns true if at least one of the log messages has a warning level */
    hasWarningMessages() {
        for (const m of this.log.messages) {
            if (m.getSeverity() === message_1.MessageSeverity.WARNING) {
                return true;
            }
        }
        return false;
    }
}
exports.ResultElement = ResultElement;

},{"./log":149,"./message":151}],157:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SearchInterval = void 0;
const interval_1 = require("./interval");
const message_1 = require("./message");
class SearchInterval extends interval_1.Interval {
    constructor(d, rInit, s, dbg = false) {
        super(rInit, rInit + s, dbg);
        if (s === 0) {
            const e = new message_1.Message(message_1.MessageCode.ERROR_DICHO_NULL_STEP);
            throw e;
        }
        this._initialValue = rInit;
        this._initialStep = s;
        this._step = this._initialStep;
        this._dicho = d;
    }
    debug(s) {
        if (this.DBG) {
            super.debug("SearchInterval: " + s);
        }
    }
    reInit(rInit) {
        if (rInit !== undefined) {
            this.val1 = rInit;
        }
        else {
            this.val1 = this._initialValue;
        }
        this._step = this._initialStep;
        this.val2 = this.val1 + this._step;
    }
    growStep(k) {
        if (k === 0) {
            const e = new message_1.Message(message_1.MessageCode.ERROR_DICHO_INVALID_STEP_GROWTH);
            throw e;
        }
        this._step *= k;
    }
    get step() {
        return this._step;
    }
    get targets() {
        if (this._targets === undefined) {
            this._targets = new interval_1.Interval(undefined, undefined);
        }
        return this._targets;
    }
    next() {
        if (this._step > 0) {
            this.val1 = this.val2;
            this.val2 += this._step;
            this.targets.setValues(this.targets.val2, undefined);
        }
        else {
            this.val2 = this.val1;
            this.val1 += this._step;
            this.targets.setValues(undefined, this.targets.val1);
        }
        this.updateTargets(false); // false => this.reInit() if calculation fails
    }
    checkDirection() {
        if (this._signDeltaTargets) { // !== undefined and !== 0
            if (Math.sign(this.targets.val1 - this.targets.val2) !== this._signDeltaTargets) {
                this.debug(this.toString() + " = [" + this.targets.val1 + "," + this.targets.val2 + "] sign changed!");
                // We have gone too far, let's start again the search from here
                if (this._step > 0) {
                    this.val2 = this.val1;
                }
                else {
                    this.val1 = this.val2;
                }
                this._step = -1 * Math.sign(this._step) * this._initialStep;
                this.next();
            }
        }
        this._signDeltaTargets = Math.sign(this.targets.val1 - this.targets.val2);
        this.debug(this.toString() + " = [" + this.targets.val1 + "," + this.targets.val2 + "]" + (this._signDeltaTargets > 0) ? ">0" : "<0");
        this.debug("_signDeltaTargets=" + this._signDeltaTargets);
    }
    hasTargetValue(targ) {
        this.updateTargets();
        return this.targets.intervalHasValue(targ);
    }
    toString() {
        return super.toString() + " step=" + this._step;
    }
    updateTargets(bFatal = true) {
        try {
            let t1 = this.targets.val1;
            if (t1 === undefined || isNaN(t1)) {
                t1 = this._dicho.CalculX(this.val1);
                this._lastGoodValue = this.val1;
                this.debug(`updateTargets 1: f(${this.val1})=${t1}`);
            }
            let t2 = this.targets.val2;
            if (t2 === undefined || isNaN(t2)) {
                t2 = this._dicho.CalculX(this.val2);
                this._lastGoodValue = this.val2;
                this.debug(`updateTargets 2: f(${this.val2})=${t2}`);
            }
            this.targets.setValues(t1, t2);
        }
        catch (e) {
            if (bFatal) {
                throw e;
            }
            else {
                this.reInit(this._lastGoodValue);
            }
        }
    }
}
exports.SearchInterval = SearchInterval;

},{"./interval":148,"./message":151}],158:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
;

},{}],159:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DivingJetSupport = void 0;
/** Diving jet support for Espece */
var DivingJetSupport;
(function (DivingJetSupport) {
    DivingJetSupport[DivingJetSupport["NOT_SUPPORTED"] = 0] = "NOT_SUPPORTED";
    DivingJetSupport[DivingJetSupport["SUPPORTED"] = 1] = "SUPPORTED";
})(DivingJetSupport = exports.DivingJetSupport || (exports.DivingJetSupport = {}));

},{}],160:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Espece = void 0;
const nub_1 = require("../nub");
const compute_node_1 = require("../compute-node");
const param_definition_1 = require("../param/param-definition");
const fish_species_1 = require("./fish_species");
const result_1 = require("../util/result");
const structure_1 = require("../structure/structure");
const message_1 = require("../util/message");
const par_1 = require("../par/par");
const macrorugo_1 = require("../macrorugo/macrorugo");
const resultelement_1 = require("../util/resultelement");
const structure_props_1 = require("../structure/structure_props");
const base_1 = require("../base");
const diving_jet_support_1 = require("./diving-jet-support");
/**
 * Settings for a given fish species (or custom settings), to verify crossing capacities on fish passes.
 * Meant to be used with Verificateur.
 */
class Espece extends nub_1.Nub {
    constructor(prms, dbg = false) {
        super(prms, dbg);
        this._calcType = compute_node_1.CalculatorType.Espece;
        this._props.addObserver(this);
        // Diving jets in PAB are not supported by default
        this.divingJetSupported = diving_jet_support_1.DivingJetSupport.NOT_SUPPORTED;
        // defaults to CUSTOM mode, for GUI instanciation
        this.species = fish_species_1.FishSpecies.SPECIES_CUSTOM;
        this.indexToCheck = 0;
        this.calculatedParam = this.prms.OK;
        this.resetDefaultCalculatedParam();
        // presets
        // Source: Informations sur la Continuité Ecologique (ICE), Onema 2014
        this.presets = {};
        // PAB
        this.presets[compute_node_1.CalculatorType.Pab] = {};
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_1] = {
            DHMaxS: 0.35,
            BMin: 0.3,
            PMinS: 1,
            LMinS: 2.5,
            DHMaxP: 0.75,
            PMinP: 1,
            HMin: 0.3,
            LMinP: 2,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: diving_jet_support_1.DivingJetSupport.SUPPORTED
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_2] = {
            DHMaxS: 0.35,
            BMin: 0.2,
            PMinS: 1,
            LMinS: 1.75,
            DHMaxP: 0.6,
            PMinP: 0.75,
            HMin: 0.2,
            LMinP: 1.25,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: diving_jet_support_1.DivingJetSupport.SUPPORTED
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_3a] =
            this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_3b] = {
                DHMaxS: 0.3,
                BMin: 0.4,
                PMinS: 1,
                HMin: 0.4,
                LMinS: 3.5,
                PVMaxPrec: 150,
                PVMaxLim: 200
            };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_3c] = {
            DHMaxS: 0.3,
            BMin: 0.15,
            PMinS: 1,
            HMin: 0.15,
            LMinS: 1.25,
            PVMaxPrec: 200,
            PVMaxLim: 250
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_4a] = {
            DHMaxS: 0.35,
            BMin: 0.2,
            PMinS: 1,
            LMinS: 1.75,
            DHMaxP: 0.4,
            PMinP: 0.75,
            HMin: 0.2,
            LMinP: 1.25,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: diving_jet_support_1.DivingJetSupport.SUPPORTED
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_4b] = {
            DHMaxS: 0.3,
            BMin: 0.15,
            PMinS: 0.75,
            LMinS: 1,
            DHMaxP: 0.3,
            PMinP: 0.75,
            HMin: 0.2,
            LMinP: 1,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: diving_jet_support_1.DivingJetSupport.SUPPORTED
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_5] = {
            DHMaxS: 0.3,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5,
            PVMaxPrec: 150,
            PVMaxLim: 200
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_6] = {
            DHMaxS: 0.3,
            BMin: 0.2,
            PMinS: 0.75,
            LMinS: 1.75,
            DHMaxP: 0.3,
            PMinP: 0.75,
            HMin: 0.2,
            LMinP: 1,
            PVMaxPrec: 200,
            PVMaxLim: 250,
            DivingJetSupported: diving_jet_support_1.DivingJetSupport.SUPPORTED
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_7a] = {
            DHMaxS: 0.3,
            BMin: 0.25,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2,
            PVMaxPrec: 150,
            PVMaxLim: 200
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_7b] = {
            DHMaxS: 0.3,
            BMin: 0.15,
            PMinS: 0.75,
            HMin: 0.15,
            LMinS: 1.25,
            PVMaxPrec: 130,
            PVMaxLim: 150
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_8a] = {
            DHMaxS: 0.25,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_8b] = {
            DHMaxS: 0.25,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5,
            PVMaxPrec: 150,
            PVMaxLim: 200
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_8c] = {
            DHMaxS: 0.25,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5,
            PVMaxPrec: 130,
            PVMaxLim: 150
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_8d] = {
            DHMaxS: 0.25,
            BMin: 0.3,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2.5,
            PVMaxPrec: 150,
            PVMaxLim: 200
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_9a] = {
            DHMaxS: 0.25,
            BMin: 0.25,
            PMinS: 0.75,
            HMin: 0.2,
            LMinS: 2,
            PVMaxPrec: 130,
            PVMaxLim: 150
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_9b] = {
            DHMaxS: 0.2,
            BMin: 0.15,
            PMinS: 0.5,
            HMin: 0.2,
            LMinS: 1.25,
            PVMaxPrec: 130,
            PVMaxLim: 150
        };
        this.presets[compute_node_1.CalculatorType.Pab][fish_species_1.FishSpecies.SPECIES_10] = {
            DHMaxS: 0.2,
            BMin: 0.15,
            PMinS: 0.5,
            HMin: 0.2,
            LMinS: 1.25,
            PVMaxPrec: 100,
            PVMaxLim: 150
        };
        // PAR Simulation
        this.presets[compute_node_1.CalculatorType.ParSimulation] = {};
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_1] = {
            YMinSB: 0.2,
            YMinPB: 0.3
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_2] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_3a] = {
            YMinSB: 0.2,
            YMinPB: 0.3
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_3b] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_3c] = {
            YMinSB: 0.1,
            YMinPB: 0.1
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_4a] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_4b] = {
            YMinSB: 0.1,
            YMinPB: 0.2
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_5] = {
            YMinSB: 0.2,
            YMinPB: 0.3
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_6] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_7a] = {
            YMinSB: 0.15,
            YMinPB: 0.25
        };
        this.presets[compute_node_1.CalculatorType.ParSimulation][fish_species_1.FishSpecies.SPECIES_7b] = {
            YMinSB: 0.1,
            YMinPB: 0.1
        };
        // MacroRugo
        this.presets[compute_node_1.CalculatorType.MacroRugo] = {};
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_1] = {
            YMin: 0.4,
            VeMax: 2.5
        };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_2] = {
            YMin: 0.3,
            VeMax: 2.5
        };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_3a] =
            this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_3b] = {
                YMin: 0.4,
                VeMax: 2
            };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_3c] = {
            YMin: 0.15,
            VeMax: 2
        };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_4a] = {
            YMin: 0.3,
            VeMax: 2
        };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_4b] = {
            YMin: 0.2,
            VeMax: 2
        };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_5] =
            this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_6] =
                this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_7a] = {
                    YMin: 0.3,
                    VeMax: 2
                };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_7b] = {
            YMin: 0.15,
            VeMax: 2
        };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_8a] =
            this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_8b] =
                this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_8c] =
                    this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_8d] = {
                        YMin: 0.3,
                        VeMax: 1.5
                    };
        this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_9a] =
            this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_9b] =
                this.presets[compute_node_1.CalculatorType.MacroRugo][fish_species_1.FishSpecies.SPECIES_10] = {
                    YMin: 0.2,
                    VeMax: 1.5
                };
    }
    get prms() {
        return this._prms;
    }
    get species() {
        return this._props.getPropValue("species");
    }
    /** Changing the fish species will load adequate predefined values */
    set species(s) {
        this.properties.setPropValue("species", s);
    }
    set passToCheck(p) {
        this._passToCheck = p;
        this.loadPredefinedParametersValues();
    }
    get divingJetSupported() {
        return this.properties.getPropValue("divingJetSupported");
    }
    set divingJetSupported(i) {
        this.properties.setPropValue("divingJetSupported", i);
    }
    /**
     * Check that all given criteria are defined before controlling their values
     * @param symbols list of symbols of criteria that have to be defined
     * @return undefined if all criteria are present, a Result with appropriate error
     *      log messages otherwise
     */
    checkCriteriaPresence(symbols) {
        const r = new result_1.Result();
        r.addResultElement(new resultelement_1.ResultElement());
        let error = false;
        for (const s of symbols) {
            if (this.getParameter(s).singleValue === undefined) {
                error = true;
                const m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_MISSING_CRITERION);
                m.extraVar.var_criterion = s;
                r.log.add(m);
            }
        }
        if (error) {
            return r;
        }
        else {
            return undefined; // everything OK
        }
    }
    /** Returns 1 if the fish can go through the pass, 0 if it cannot */
    Equation() {
        // default result is 1 (OK)
        const res = new result_1.Result(1, this);
        const sp = this.species;
        // detect pass characteristics and species
        let pt = this._passToCheck.calcType;
        // MacroRugoCompound is just a repetition of the MacroRugo check
        if (pt === compute_node_1.CalculatorType.MacroRugoCompound) {
            pt = compute_node_1.CalculatorType.MacroRugo;
        }
        // get result to examine
        const r = this._passToCheck.result.resultElements[this.indexToCheck];
        if (r === undefined) {
            throw new Error(`Espece.Equation(): pass to check has no resultElements[${this.indexToCheck}]`);
        }
        // is there a valid preset for the pass type / species couple ?
        if (sp !== fish_species_1.FishSpecies.SPECIES_CUSTOM && this.presets[pt][sp] === undefined) {
            res.vCalc = 0;
            res.log.add(new message_1.Message(message_1.MessageCode.ERROR_VERIF_NO_PRESET));
            return res;
        }
        else {
            // normal processing
            switch (this._passToCheck.calcType) { // not pt, to detect MacroRugoCompound
                case compute_node_1.CalculatorType.Pab:
                    const passB = this._passToCheck;
                    // Check criteria presence (depends on jet types)
                    let hasAtLeastOneSurfaceJet = false;
                    let hasAtLeastOneDivingJet = false;
                    for (const wall of passB.children) {
                        hasAtLeastOneSurfaceJet = hasAtLeastOneSurfaceJet || this.hasJet(wall, structure_1.StructureJetType.SURFACE);
                        hasAtLeastOneDivingJet = hasAtLeastOneDivingJet || this.hasJet(wall, structure_1.StructureJetType.PLONGEANT);
                    }
                    hasAtLeastOneDivingJet = hasAtLeastOneDivingJet || this.hasJet(passB.downWall, structure_1.StructureJetType.PLONGEANT);
                    let cpRetPAB;
                    let criteriaToCheck = [/* "PVMaxPrec", "PVMaxLim", */ "HMin", "BMin"];
                    if (hasAtLeastOneSurfaceJet) {
                        criteriaToCheck = criteriaToCheck.concat(["DHMaxS", "PMinS", "LMinS"]);
                    }
                    if (hasAtLeastOneDivingJet && this.divingJetSupported === diving_jet_support_1.DivingJetSupport.SUPPORTED) {
                        criteriaToCheck = criteriaToCheck.concat(["DHMaxP", "PMinP", "LMinP"]);
                    }
                    if (criteriaToCheck.length > 0) {
                        cpRetPAB = this.checkCriteriaPresence(criteriaToCheck);
                        if (cpRetPAB !== undefined) {
                            return cpRetPAB;
                        }
                    }
                    // A -- walls
                    let cloisonNumber = 1;
                    for (const wall of passB.children) {
                        const iRes = wall.result.resultElements[this.indexToCheck];
                        const hasSJet = this.hasJet(wall, structure_1.StructureJetType.SURFACE);
                        const hasPJet = this.hasJet(wall, structure_1.StructureJetType.PLONGEANT);
                        // Wall crossability array, one element for each device, by default every device is crossable;
                        // marked only when device is receiving a Warning, to avoid errors redundancy
                        const wca = [];
                        wca.length = wall.structures.length;
                        wca.fill(true);
                        // 0. diving jet support
                        this.checkDivingJetSupport(wall, res, wca);
                        // 1. falls
                        this.checkFalls(wall, iRes, res, wca, hasSJet, hasPJet);
                        // 2. weirs and slots witdh
                        this.checkWeirsAndSlotsWidth(wall, res, wca);
                        // 3. basins depth
                        this.checkBasinDepth(wall, iRes, res, wca, hasSJet, hasPJet);
                        // diving jets only: depth < 2x fall
                        if (hasPJet) {
                            if (base_1.isLowerThan(iRes.values.YMOY, (2 * iRes.values.DH), 1e-3)) {
                                let m;
                                // diving jet fails, but surface jet exists
                                if (hasSJet) {
                                    m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_YMOY_2_DH);
                                    const failingDevices = this.getDevicesHavingJet(wall, structure_1.StructureJetType.PLONGEANT);
                                    for (const idx of failingDevices) {
                                        wca[idx] = false;
                                    }
                                }
                                else {
                                    m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_YMOY_2_DH);
                                    res.vCalc = 0;
                                }
                                ;
                                m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                                m.extraVar.PB = iRes.values.YMOY;
                                m.extraVar.DH = iRes.values.DH;
                                res.log.add(m);
                            }
                        }
                        // 4. head on weirs
                        this.checkHeadOnWeirs(wall, res, wca);
                        // 5. basins length
                        this.checkBasinLength(wall, res, wca, hasSJet, hasPJet);
                        // 6. dissipated power
                        /* this.checkDissipatedPowerPAB(wall, res); */
                        // 7. orifices
                        this.checkOrifices(wall, res, wca);
                        // check crossability
                        let ok = false;
                        for (const devC of wca) {
                            ok = ok || devC;
                        }
                        if (!ok) {
                            const m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_WALL_NOT_CROSSABLE);
                            m.extraVar.N = String(cloisonNumber); // keep the "+1" for readability
                            res.log.add(m);
                        }
                        cloisonNumber++;
                    }
                    // B -- downwall
                    const dw = passB.downWall;
                    const iResDw = dw.result.resultElements[this.indexToCheck];
                    const hasSJetDw = this.hasJet(dw, structure_1.StructureJetType.SURFACE);
                    const hasPJetDw = this.hasJet(dw, structure_1.StructureJetType.PLONGEANT);
                    // Downwall crossability array, one element for each device, by default every device is crossable;
                    // marked only when device is receiving a Warning, to avoid errors redundancy
                    const dwca = [];
                    dwca.length = dw.structures.length;
                    dwca.fill(true);
                    // 0. diving jet support
                    this.checkDivingJetSupport(dw, res, dwca);
                    // 1. falls
                    this.checkFalls(dw, iResDw, res, dwca, hasSJetDw, hasPJetDw);
                    // 2. weirs and slots witdh
                    this.checkWeirsAndSlotsWidth(dw, res, dwca);
                    // 3. head on weirs
                    this.checkHeadOnWeirs(dw, res, dwca);
                    // 4. orifices
                    this.checkOrifices(dw, res, dwca);
                    // check crossability
                    let okDw = false;
                    for (const devC of dwca) {
                        okDw = okDw || devC;
                    }
                    if (!okDw) {
                        const m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_DW_NOT_CROSSABLE);
                        res.log.add(m);
                    }
                    break;
                case compute_node_1.CalculatorType.ParSimulation:
                    const passR = this._passToCheck;
                    // Check criteria presence, depending on baffle type
                    let parCriteria = [];
                    if ([par_1.ParType.PLANE, par_1.ParType.FATOU].includes(passR.parType)) {
                        parCriteria = ["YMinPB"];
                    }
                    else if ([par_1.ParType.SUPERACTIVE, par_1.ParType.CHEVRON].includes(passR.parType)) {
                        parCriteria = ["YMinSB"];
                    }
                    if (parCriteria.length > 0) {
                        const cpRetPAR = this.checkCriteriaPresence(parCriteria);
                        if (cpRetPAR !== undefined) {
                            return cpRetPAR;
                        }
                    }
                    // 1. species groups 3a, 3b, 7b are discouraged
                    if ([fish_species_1.FishSpecies.SPECIES_3a, fish_species_1.FishSpecies.SPECIES_3b, fish_species_1.FishSpecies.SPECIES_7b].includes(this.species)) {
                        res.log.add(new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAR_SPECIES_GROUP));
                    }
                    // 2. water level
                    let minY = 0; // if this.prms.YMin(P|S)B is undefined, "skip test"
                    if ([par_1.ParType.PLANE, par_1.ParType.FATOU].includes(passR.parType)) {
                        minY = this.prms.YMinPB.singleValue;
                    }
                    else if ([par_1.ParType.SUPERACTIVE, par_1.ParType.CHEVRON].includes(passR.parType)) {
                        minY = this.prms.YMinSB.singleValue;
                    }
                    if (base_1.isLowerThan(r.values.h, minY, 1e-3)) {
                        res.vCalc = 0;
                        const m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAR_YMIN);
                        m.extraVar.h = r.values.h;
                        m.extraVar.minY = minY;
                        res.log.add(m);
                    }
                    break;
                case compute_node_1.CalculatorType.MacroRugoCompound:
                    const passMRC = this._passToCheck;
                    // Check criteria presence
                    const cpRetMRC = this.checkCriteriaPresence(["YMin", "VeMax"]);
                    if (cpRetMRC !== undefined) {
                        return cpRetMRC;
                    }
                    let atLeastOneOK = false;
                    let apronNumber = 1;
                    const consecutiveWidths = [];
                    let previousApronFailed = true;
                    // loop on aprons with MacroRugo check; at least one is required
                    for (const mr of passMRC.children) {
                        const mrRes = this.checkMacroRugo(mr, mr.result.resultElements[this.indexToCheck], res, apronNumber);
                        if (mrRes === 1) {
                            atLeastOneOK = true;
                            if (previousApronFailed) {
                                consecutiveWidths.push(0);
                                previousApronFailed = false;
                            }
                            consecutiveWidths[consecutiveWidths.length - 1] += mr.prms.B.v;
                        }
                        else {
                            previousApronFailed = true;
                        }
                        apronNumber++;
                    }
                    if (!atLeastOneOK) {
                        res.vCalc = 0;
                        res.log.add(new message_1.Message(message_1.MessageCode.ERROR_VERIF_MRC_AT_LEAST_ONE_APRON));
                    }
                    else {
                        // Check max. crossable width
                        const maxCrossableWidth = Math.max(...consecutiveWidths);
                        const patternWidth = passMRC.prms.PBD.v / Math.sqrt(passMRC.prms.C.v);
                        if (base_1.isLowerThan(maxCrossableWidth, patternWidth, 1e-3)) {
                            res.vCalc = 0;
                            const m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_MRC_CROSSABLE_WIDTH);
                            m.extraVar.width = maxCrossableWidth;
                            m.extraVar.patternWidth = patternWidth;
                            res.log.add(m);
                        }
                        else {
                            const m = new message_1.Message(message_1.MessageCode.INFO_VERIF_MRC_CROSSABLE_WIDTH);
                            m.extraVar.width = maxCrossableWidth;
                            res.log.add(m);
                        }
                    }
                    break;
                case compute_node_1.CalculatorType.MacroRugo:
                    // Check criteria presence
                    const cpRetMR = this.checkCriteriaPresence(["YMin", "VeMax"]);
                    if (cpRetMR !== undefined) {
                        return cpRetMR;
                    }
                    res.vCalc = this.checkMacroRugo(this._passToCheck, r, res);
                    break;
                default:
                    // should never happen
                    throw new Error(`Espece.Equation(): undefined pass type ${compute_node_1.CalculatorType[this._passToCheck.calcType]}`);
            }
        }
        return res;
    }
    update(sender, data) {
        if (data.action === "propertyChange") {
            if (data.name === "species") {
                this.loadPredefinedParametersValues();
                // no parameter visibility update: GUI will never show anything but SPECIES_CUSTOM mode,
                // where all parameters are visible
            }
        }
    }
    /**
     * Checks the diving jet support on every structure of the given wall of a Pab
     * @param wall the current wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     */
    checkDivingJetSupport(wall, res, ca) {
        // if diving jets are not supported by the current species
        if (this.divingJetSupported === diving_jet_support_1.DivingJetSupport.NOT_SUPPORTED) {
            const devices = this.getDevicesHavingJet(wall, structure_1.StructureJetType.PLONGEANT);
            // mark every device having a diving jet as not crossable
            for (const idx of devices) {
                ca[idx] = false;
            }
            // if every device is failing, add an error for the whole wall
            if (devices.length === wall.structures.length) {
                let m;
                m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_DIVING_JET_NOT_SUPPORTED);
                m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                res.log.add(m);
            }
            else {
                // add a warning for every device having a diving jet
                for (const idx of devices) {
                    let m;
                    m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_DIVING_JET_NOT_SUPPORTED);
                    m.parent = wall.structures[idx].result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                    res.log.add(m);
                }
            }
        }
    }
    /**
     * Checks the falls on every structure of the given wall of a Pab
     * @param wall the current wall to check
     * @param iRes the current result element to check on the wall result
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     * @param hasSJet true if current wall has at least one device with a surface jet (or an Orifice)
     * @param hasPJet true if current wall has at least one device with a diving jet
     */
    checkFalls(wall, iRes, res, ca, hasSJet, hasPJet) {
        const fallSJetFails = (hasSJet && this.prms.DHMaxS.singleValue !== undefined && base_1.isGreaterThan(iRes.values.DH, this.prms.DHMaxS.singleValue, 1e-3));
        const fallPJetFails = (this.divingJetSupported === diving_jet_support_1.DivingJetSupport.SUPPORTED && hasPJet && this.prms.DHMaxP.singleValue !== undefined && base_1.isGreaterThan(iRes.values.DH, this.prms.DHMaxP.singleValue, 1e-3));
        // error cases
        if (fallSJetFails || fallPJetFails) {
            let m;
            // everything fails: error
            if (fallSJetFails && fallPJetFails) {
                m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_DHMAX);
                m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                res.vCalc = 0;
                m.extraVar.maxDHS = this.prms.DHMaxS.singleValue;
                m.extraVar.maxDHP = this.prms.DHMaxP.singleValue;
            }
            else { // only one type of jet fails, the other type may be present or not
                const failingJetType = fallSJetFails ? structure_1.StructureJetType.SURFACE : structure_1.StructureJetType.PLONGEANT;
                // one jet fails, but other jet is present (and succeeds)
                if ((fallSJetFails && hasPJet) || (fallPJetFails && hasSJet)) {
                    m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_DHMAX_JET);
                    m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                    const failingDevices = this.getDevicesHavingJet(wall, failingJetType);
                    for (const idx of failingDevices) {
                        ca[idx] = false;
                    }
                }
                else { // one jet fails and there is no other jet
                    m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_DHMAX_JET);
                    m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                    res.vCalc = 0;
                }
                m.extraVar.maxDH = fallSJetFails ? this.prms.DHMaxS.singleValue : this.prms.DHMaxP.singleValue;
                m.extraVar.jetType = String(failingJetType);
            }
            m.extraVar.DH = iRes.values.DH;
            res.log.add(m);
        } // else everything ok
    }
    /**
     * Checks the basin depth on every structure of the given wall of a Pab
     * @param wall the current wall to check
     * @param iRes the current result element to check on the wall result
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     * @param hasSJet true if current wall has at least one device with a surface jet (or an Orifice)
     * @param hasPJet true if current wall has at least one device with a diving jet
     */
    checkBasinDepth(wall, iRes, res, ca, hasSJet, hasPJet) {
        const fallSJetFails = (hasSJet && this.prms.PMinS.singleValue !== undefined && base_1.isLowerThan(iRes.values.YMOY, this.prms.PMinS.singleValue, 1e-3));
        const fallPJetFails = (this.divingJetSupported === diving_jet_support_1.DivingJetSupport.SUPPORTED && hasPJet && this.prms.PMinP.singleValue !== undefined && base_1.isLowerThan(iRes.values.YMOY, this.prms.PMinP.singleValue, 1e-3));
        // error cases
        if (fallSJetFails || fallPJetFails) {
            let m;
            // everything fails: error
            if (fallSJetFails && fallPJetFails) {
                m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_YMOY);
                m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                res.vCalc = 0;
                m.extraVar.minPBS = this.prms.PMinS.singleValue;
                m.extraVar.minPBP = this.prms.PMinP.singleValue;
            }
            else { // only one type of jet fails, the other type may be present or not
                const failingJetType = fallSJetFails ? structure_1.StructureJetType.SURFACE : structure_1.StructureJetType.PLONGEANT;
                // other jet is present (and succeeds)
                if ((fallSJetFails && hasPJet) || (fallPJetFails && hasSJet)) {
                    m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_YMOY_JET);
                    m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                    const failingDevices = this.getDevicesHavingJet(wall, failingJetType);
                    for (const idx of failingDevices) {
                        ca[idx] = false;
                    }
                }
                else { // there is no other jet
                    m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_YMOY_JET);
                    m.parent = iRes.log; // trick to prefix message with Nub position inside parent
                    res.vCalc = 0;
                }
                m.extraVar.minPB = fallSJetFails ? this.prms.PMinS.singleValue : this.prms.PMinP.singleValue;
                m.extraVar.jetType = String(failingJetType);
            }
            m.extraVar.PB = iRes.values.YMOY;
            res.log.add(m);
        } // else everything ok
    }
    /**
     * Checks the basin length on every structure of the given wall of a Pab
     * @param wall the current wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     * @param hasSJet true if current wall has at least one device with a surface jet (or an Orifice)
     * @param hasPJet true if current wall has at least one device with a diving jet
     */
    checkBasinLength(wall, res, ca, hasSJet, hasPJet) {
        const fallSJetFails = (hasSJet && this.prms.LMinS.singleValue !== undefined && base_1.isLowerThan(wall.prms.LB.singleValue, this.prms.LMinS.singleValue, 1e-3));
        const fallPJetFails = (this.divingJetSupported === diving_jet_support_1.DivingJetSupport.SUPPORTED && hasPJet && this.prms.LMinP.singleValue !== undefined && base_1.isLowerThan(wall.prms.LB.singleValue, this.prms.LMinP.singleValue, 1e-3));
        // error cases
        if (fallSJetFails || fallPJetFails) {
            let m;
            // everything fails: error
            if (fallSJetFails && fallPJetFails) {
                m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_LMIN);
                m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                res.vCalc = 0;
                m.extraVar.minLBS = this.prms.LMinS.singleValue;
                m.extraVar.minLBP = this.prms.LMinP.singleValue;
                ca.fill(false); // no device is crossable
            }
            else { // only one type of jet fails, the other type may be present or not
                const failingJetType = fallSJetFails ? structure_1.StructureJetType.SURFACE : structure_1.StructureJetType.PLONGEANT;
                // other jet is present (and succeeds)
                if ((fallSJetFails && hasPJet) || (fallPJetFails && hasSJet)) {
                    m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_LMIN_JET);
                    m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                    const failingDevices = this.getDevicesHavingJet(wall, failingJetType);
                    for (const idx of failingDevices) {
                        ca[idx] = false;
                    }
                }
                else { // there is no other jet
                    m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_LMIN_JET);
                    m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                    res.vCalc = 0;
                }
                m.extraVar.minLB = fallSJetFails ? this.prms.LMinS.singleValue : this.prms.LMinP.singleValue;
                m.extraVar.jetType = String(failingJetType);
            }
            m.extraVar.LB = wall.prms.LB.singleValue;
            res.log.add(m);
        } // else everything ok
    }
    /**
     * Checks the weirs and slots widths on every structure of the given wall of a Pab
     * @param wall the Pab wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     */
    checkWeirsAndSlotsWidth(wall, res, ca) {
        let structureNumber = 1;
        for (const device of wall.structures) {
            const hasOtherDevicesThanWeirs = (this.hasJet(wall, [structure_1.StructureJetType.PLONGEANT])
                || (this.getDevicesHavingLaw(wall, [structure_props_1.LoiDebit.WeirVillemonte, structure_props_1.LoiDebit.WeirSubmergedLarinier]).length < wall.structures.length));
            const hasOtherDevicesThanOrifice = (this.hasJet(wall, [structure_1.StructureJetType.PLONGEANT])
                || (this.getDevicesHavingLaw(wall, structure_props_1.LoiDebit.OrificeSubmerged).length < wall.structures.length));
            const iRes = device.result.resultElements[this.indexToCheck];
            const jt = iRes.values.ENUM_StructureJetType;
            if (jt !== structure_1.StructureJetType.PLONGEANT) {
                // Surface jets: for "Villemonte 1947" ("échancrure") or "Fente noyée (Larinier 1992)" ("fente") only
                if ([structure_props_1.LoiDebit.WeirVillemonte, structure_props_1.LoiDebit.WeirSubmergedLarinier].includes(device.loiDebit)) {
                    const structParams = device.prms;
                    if (this.prms.BMin.singleValue !== undefined && base_1.isLowerThan(structParams.L.v, this.prms.BMin.singleValue, 1e-3)) {
                        let m;
                        if (hasOtherDevicesThanWeirs) {
                            ca[structureNumber - 1] = false; // device is not crossable
                            m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_BMIN);
                            m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                        }
                        else {
                            res.vCalc = 0;
                            m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_BMIN);
                            m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                        }
                        m.extraVar.L = structParams.L.v;
                        m.extraVar.minB = this.prms.BMin.singleValue;
                        res.log.add(m);
                    }
                }
                else if (device.loiDebit === structure_props_1.LoiDebit.OrificeSubmerged) {
                    const structParams = device.prms;
                    // Orifices (no jet)
                    if (this.prms.BMin.singleValue !== undefined && base_1.isLowerThan(structParams.S.v, Math.pow(this.prms.BMin.singleValue, 2), 1e-3)) {
                        let m;
                        if (hasOtherDevicesThanOrifice) {
                            ca[structureNumber - 1] = false; // device is not crossable
                            m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_SMIN);
                            m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                        }
                        else {
                            res.vCalc = 0;
                            m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_SMIN);
                            m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                        }
                        m.extraVar.S = structParams.S.v;
                        m.extraVar.minS = Math.pow(this.prms.BMin.singleValue, 2);
                        res.log.add(m);
                    }
                }
            }
            structureNumber++;
        }
    }
    /**
     * Checks the head on weirs on every structure of the given wall of a Pab
     * @param wall the Pab wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     */
    checkHeadOnWeirs(wall, res, ca) {
        let structureNumber = 1;
        for (const device of wall.structures) {
            let zdv = device.prms.ZDV.singleValue;
            // if device is a regulated weir, get calculated ZDV at current iteration from parent downWall
            if (structure_props_1.loiAdmissiblesCloisonAval.VanneLevante.includes(device.properties.getPropValue("loiDebit"))) {
                zdv = device.parent.result.resultElements[this.indexToCheck].values.ZDV;
            }
            // do not use device.prms.h1.v, that contains only the latest calculated value if pass is variyng
            const h1 = wall.result.resultElements[this.indexToCheck].vCalc - zdv;
            /* console.log(
                `wall ${wall.findPositionInParent() + 1} / device ${structureNumber} : h1=${h1}, ZDV=${zdv}, class=${device.constructor.name}`
            ); */
            if (this.prms.HMin.singleValue !== undefined && base_1.isLowerThan(h1, this.prms.HMin.singleValue, 1e-3)) {
                let m;
                ca[structureNumber - 1] = false; // device is not crossable
                m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_HMIN);
                m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                m.extraVar.h1 = Math.max(0, h1);
                m.extraVar.minH = this.prms.HMin.singleValue;
                res.log.add(m);
            }
            structureNumber++;
        }
    }
    /**
     * Checks the dissipated power on the given wall of a Pab
     * @param wall the Pab wall to check
     * @param res the current result to add logs to, and set value
     */
    checkDissipatedPowerPAB(wall, res) {
        const pv = wall.result.resultElements[this.indexToCheck].values.PV;
        if (this.prms.PVMaxLim.singleValue !== undefined && base_1.isGreaterThan(pv, this.prms.PVMaxLim.singleValue, 1e-3)) {
            res.vCalc = 0;
            const m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAB_PVMAX);
            m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
            m.extraVar.PV = pv;
            m.extraVar.maxPV = this.prms.PVMaxLim.singleValue;
            res.log.add(m);
        }
        else if (this.prms.PVMaxPrec.singleValue !== undefined && base_1.isGreaterThan(pv, this.prms.PVMaxPrec.singleValue, 1e-3)) {
            res.vCalc = 0;
            const m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_PVMAX);
            m.parent = wall.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
            m.extraVar.PV = pv;
            m.extraVar.maxPV = this.prms.PVMaxPrec.singleValue;
            res.log.add(m);
        }
    }
    /**
     * Marks orifices as not supposed to be crossable, with a warning (jalhyd#249)
     * @param wall the Pab wall to check
     * @param res the current result to add logs to, and set value
     * @param ca crossability array; mark non-crossable devices with "false" value at their index
     */
    checkOrifices(wall, res, ca) {
        let structureNumber = 1;
        for (const device of wall.structures) {
            // a structure is considered an orifice if its flow mode is "Orifice"
            if (device.result.resultElements[this.indexToCheck].values.ENUM_StructureFlowMode === structure_1.StructureFlowMode.ORIFICE) {
                let m;
                ca[structureNumber - 1] = false; // device is not crossable
                m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_PAB_ORIFICE);
                m.parent = device.result.resultElements[this.indexToCheck].log; // trick to prefix message with Nub position inside parent
                res.log.add(m);
            }
            structureNumber++;
        }
    }
    /**
     * Returns true if given wall has at least one device with a jet type among given jetTypes
     */
    hasJet(wall, jetTypes) {
        if (!Array.isArray(jetTypes)) {
            jetTypes = [jetTypes];
        }
        for (const device of wall.structures) {
            const deviceJetType = device.result.resultElements[this.indexToCheck].values.ENUM_StructureJetType;
            if (jetTypes.includes(deviceJetType)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Returns indexes of the devices having given jet type, on the given wall
     * @param wall wall to check devices on
     * @param t jet type
     */
    getDevicesHavingJet(wall, t) {
        const idxs = [];
        for (let i = 0; i < wall.structures.length; i++) {
            const deviceJetType = wall.structures[i].result.resultElements[this.indexToCheck].values.ENUM_StructureJetType;
            if (deviceJetType === t) {
                idxs.push(i);
            }
        }
        return idxs;
    }
    /**
     * Returns indexes of the devices having given discharge law, on the given wall
     * @param wall wall to check devices on
     * @param l discharge law
     */
    getDevicesHavingLaw(wall, l) {
        const idxs = [];
        if (!Array.isArray(l)) {
            l = [l];
        }
        for (let i = 0; i < wall.structures.length; i++) {
            if (l.includes(wall.structures[i].loiDebit)) {
                idxs.push(i);
            }
        }
        return idxs;
    }
    /**
     * Checks a MacroRugo pass (factorized to use when checking MacroRugoCompound)
     * @param pass the MacroRugo pass to check
     * @param passResult the current result of the pass to check
     * @param verifResult the result of the check, to add logs to
     * @param apronNumber if defined, means that we're checking an apron of a MacroRugoCompound
     */
    checkMacroRugo(pass, passResult, verifResult, apronNumber) {
        let val = 1;
        // 1. water level
        let yValue;
        if (pass.prms.Y.isCalculated) {
            yValue = passResult.vCalc;
        }
        else {
            if (pass.prms.Y.hasMultipleValues) {
                yValue = pass.prms.Y.getInferredValuesList()[this.indexToCheck]; // @TODO extend if necessary ?
            }
            else {
                // fixed Y
                yValue = pass.prms.Y.V;
            }
        }
        if (this.prms.YMin.singleValue !== undefined && base_1.isLowerThan(yValue, this.prms.YMin.singleValue, 1e-3)) {
            val = 0;
            let m;
            if (apronNumber !== undefined) {
                m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_MRC_YMIN_APRON_N);
                m.parent = passResult.log; // trick to prefix message with Nub position inside parent
            }
            else {
                m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_MR_YMIN);
            }
            m.extraVar.Y = yValue;
            m.extraVar.minY = this.prms.YMin.singleValue;
            verifResult.log.add(m);
        }
        // 2. velocity or submerged
        if (passResult.values.ENUM_MacroRugoFlowType === macrorugo_1.MacroRugoFlowType.SUBMERGED) {
            val = 0;
            if (apronNumber !== undefined) {
                const m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_MRC_SUBMERGED_APRON_N);
                m.parent = passResult.log; // trick to prefix message with Nub position inside parent
                verifResult.log.add(m);
            }
            else {
                verifResult.log.add(new message_1.Message(message_1.MessageCode.ERROR_VERIF_MR_SUBMERGED));
            }
        }
        else if (passResult.values.ENUM_MacroRugoFlowType === macrorugo_1.MacroRugoFlowType.SUBMERGED || this.prms.VeMax.singleValue !== undefined && base_1.isGreaterThan(passResult.values.Vmax, this.prms.VeMax.singleValue, 1e-3)) {
            val = 0;
            let m;
            if (apronNumber !== undefined) {
                m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_MRC_VMAX_APRON_N);
                m.parent = passResult.log; // trick to prefix message with Nub position inside parent
            }
            else {
                m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_MR_VMAX);
            }
            m.extraVar.V = passResult.values.Vmax;
            m.extraVar.maxV = this.prms.VeMax.singleValue;
            verifResult.log.add(m);
        }
        return val;
    }
    /**
     * Load ICE tables values into parameters, depending on species
     * and pass to check
     */
    loadPredefinedParametersValues() {
        if (this.species !== fish_species_1.FishSpecies.SPECIES_CUSTOM && this._passToCheck !== undefined) {
            const sp = this.species;
            let pt = this._passToCheck.calcType;
            // MacroRugoCompound is just a repetition of the MacroRugo check
            if (pt === compute_node_1.CalculatorType.MacroRugoCompound) {
                pt = compute_node_1.CalculatorType.MacroRugo;
            }
            // type de jet pour les PAB
            if (this.presets[pt] !== undefined
                && this.presets[pt][sp] !== undefined
                && this.presets[pt][sp].DivingJetSupported !== undefined) {
                this.divingJetSupported = this.presets[pt][sp].DivingJetSupported;
            }
            else {
                this.divingJetSupported = diving_jet_support_1.DivingJetSupport.NOT_SUPPORTED;
            }
            this.debug(`load predefined values for species=${fish_species_1.FishSpecies[sp].substring(8)} and pass class=${compute_node_1.CalculatorType[pt]}`);
            for (const p of this.prms) {
                if (p.symbol !== "OK") {
                    const preset = this.presets[pt];
                    if (preset[sp] !== undefined) {
                        // if (preset[sp][p.symbol] !== undefined) console.log(`-> setting ${p.symbol} to ${preset[sp][p.symbol]}`);
                        p.singleValue = preset[sp][p.symbol];
                    }
                }
            }
        }
    }
    /**
     * Load ICE tables values into parameters, for a given species
     * (used in GUI for loading predefined species)
     */
    loadPredefinedSpecies(sp) {
        let found;
        // for each param
        for (const p of this.prms) {
            if (p.symbol !== "OK") {
                // reset current value
                p.singleValue = undefined;
                // whatever the pass type (criteria are supposed to apply to 1 pass type only)
                found = false;
                for (const pt of Object.keys(this.presets)) {
                    const preset = this.presets[pt];
                    if (!found && preset !== undefined && preset[sp] !== undefined && preset[sp][p.symbol] !== undefined) {
                        found = true;
                        // if (preset[sp][p.symbol] !== undefined) console.log(`-> setting ${p.symbol} to ${preset[sp][p.symbol]}`);
                        // apply preset
                        p.singleValue = preset[sp][p.symbol];
                    }
                }
                // type de jet pour les PAB
                if (this.presets[compute_node_1.CalculatorType.Pab] !== undefined
                    && this.presets[compute_node_1.CalculatorType.Pab][sp] !== undefined
                    && this.presets[compute_node_1.CalculatorType.Pab][sp].DivingJetSupported !== undefined) {
                    this.divingJetSupported = this.presets[compute_node_1.CalculatorType.Pab][sp].DivingJetSupported;
                }
                else {
                    this.divingJetSupported = diving_jet_support_1.DivingJetSupport.NOT_SUPPORTED;
                }
            }
        }
    }
    /** Parameters other than OK are just settings here, nothing can vary or be calculated */
    setParametersCalculability() {
        for (const p of this._prms) {
            p.calculability = param_definition_1.ParamCalculability.FIXED;
        }
        this.prms.OK.calculability = param_definition_1.ParamCalculability.EQUATION;
    }
}
exports.Espece = Espece;

},{"../base":3,"../compute-node":5,"../macrorugo/macrorugo":20,"../nub":33,"../par/par":74,"../param/param-definition":86,"../structure/structure":115,"../structure/structure_props":126,"../util/message":151,"../util/result":155,"../util/resultelement":156,"./diving-jet-support":159,"./fish_species":162}],161:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EspeceParams = void 0;
const params_equation_1 = require("../param/params-equation");
const param_definition_1 = require("../param/param-definition");
const param_domain_1 = require("../param/param-domain");
class EspeceParams extends params_equation_1.ParamsEquation {
    constructor(rDHMaxS, rDHMaxP, rBMin, rPMinS, rPMinP, rLMinS, rLMinP, rHMin, rYMin, rVeMax, rYMinSB, rYMinPB, rPVMaxPrec, rPVMaxLim) {
        super();
        this._OK = new param_definition_1.ParamDefinition(this, "OK", param_domain_1.ParamDomainValue.POS, "");
        this._DHMaxS = new param_definition_1.ParamDefinition(this, "DHMaxS", param_domain_1.ParamDomainValue.POS, "m", rDHMaxS);
        this._DHMaxP = new param_definition_1.ParamDefinition(this, "DHMaxP", param_domain_1.ParamDomainValue.POS, "m", rDHMaxP);
        this._BMin = new param_definition_1.ParamDefinition(this, "BMin", param_domain_1.ParamDomainValue.POS, "m", rBMin);
        this._PMinS = new param_definition_1.ParamDefinition(this, "PMinS", param_domain_1.ParamDomainValue.POS, "m", rPMinS);
        this._PMinP = new param_definition_1.ParamDefinition(this, "PMinP", param_domain_1.ParamDomainValue.POS, "m", rPMinP);
        this._LMinS = new param_definition_1.ParamDefinition(this, "LMinS", param_domain_1.ParamDomainValue.POS, "m", rLMinS);
        this._LMinP = new param_definition_1.ParamDefinition(this, "LMinP", param_domain_1.ParamDomainValue.POS, "m", rLMinP);
        this._HMin = new param_definition_1.ParamDefinition(this, "HMin", param_domain_1.ParamDomainValue.POS, "m", rHMin);
        this._YMin = new param_definition_1.ParamDefinition(this, "YMin", param_domain_1.ParamDomainValue.POS, "m", rYMin);
        this._VeMax = new param_definition_1.ParamDefinition(this, "VeMax", param_domain_1.ParamDomainValue.POS, "m", rVeMax);
        this._YMinSB = new param_definition_1.ParamDefinition(this, "YMinSB", param_domain_1.ParamDomainValue.POS, "m", rYMinSB);
        this._YMinPB = new param_definition_1.ParamDefinition(this, "YMinPB", param_domain_1.ParamDomainValue.POS, "m", rYMinPB);
        this._PVMaxPrec = new param_definition_1.ParamDefinition(this, "PVMaxPrec", param_domain_1.ParamDomainValue.POS, "W/m³", rPVMaxPrec);
        this._PVMaxLim = new param_definition_1.ParamDefinition(this, "PVMaxLim", param_domain_1.ParamDomainValue.POS, "W/m³", rPVMaxLim);
        this.addParamDefinition(this._OK);
        this.addParamDefinition(this._DHMaxS);
        this.addParamDefinition(this._DHMaxP);
        this.addParamDefinition(this._BMin);
        this.addParamDefinition(this._PMinS);
        this.addParamDefinition(this._PMinP);
        this.addParamDefinition(this._LMinS);
        this.addParamDefinition(this._LMinP);
        this.addParamDefinition(this._HMin);
        this.addParamDefinition(this._YMin);
        this.addParamDefinition(this._VeMax);
        this.addParamDefinition(this._YMinSB);
        this.addParamDefinition(this._YMinPB);
        this.addParamDefinition(this._PVMaxPrec);
        this.addParamDefinition(this._PVMaxLim);
    }
    get OK() {
        return this._OK;
    }
    get DHMaxS() {
        return this._DHMaxS;
    }
    get DHMaxP() {
        return this._DHMaxP;
    }
    get BMin() {
        return this._BMin;
    }
    get PMinS() {
        return this._PMinS;
    }
    get PMinP() {
        return this._PMinP;
    }
    get LMinS() {
        return this._LMinS;
    }
    get LMinP() {
        return this._LMinP;
    }
    get HMin() {
        return this._HMin;
    }
    get YMin() {
        return this._YMin;
    }
    get VeMax() {
        return this._VeMax;
    }
    get YMinSB() {
        return this._YMinSB;
    }
    get YMinPB() {
        return this._YMinPB;
    }
    get PVMaxPrec() {
        return this._PVMaxPrec;
    }
    get PVMaxLim() {
        return this._PVMaxLim;
    }
}
exports.EspeceParams = EspeceParams;

},{"../param/param-definition":86,"../param/param-domain":87,"../param/params-equation":92}],162:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FishSpecies = void 0;
/**
 * Predefined crossing capabilities criteria for different fish species
 * Source: Informations sur la Continuité Ecologique (ICE), Onema 2014
 * @see Espece
 */
var FishSpecies;
(function (FishSpecies) {
    FishSpecies[FishSpecies["SPECIES_CUSTOM"] = 0] = "SPECIES_CUSTOM";
    FishSpecies[FishSpecies["SPECIES_1"] = 1] = "SPECIES_1";
    FishSpecies[FishSpecies["SPECIES_2"] = 2] = "SPECIES_2";
    FishSpecies[FishSpecies["SPECIES_3a"] = 3] = "SPECIES_3a";
    FishSpecies[FishSpecies["SPECIES_3b"] = 4] = "SPECIES_3b";
    FishSpecies[FishSpecies["SPECIES_3c"] = 5] = "SPECIES_3c";
    FishSpecies[FishSpecies["SPECIES_4a"] = 6] = "SPECIES_4a";
    FishSpecies[FishSpecies["SPECIES_4b"] = 7] = "SPECIES_4b";
    FishSpecies[FishSpecies["SPECIES_5"] = 8] = "SPECIES_5";
    FishSpecies[FishSpecies["SPECIES_6"] = 9] = "SPECIES_6";
    FishSpecies[FishSpecies["SPECIES_7a"] = 10] = "SPECIES_7a";
    FishSpecies[FishSpecies["SPECIES_7b"] = 11] = "SPECIES_7b";
    FishSpecies[FishSpecies["SPECIES_8a"] = 12] = "SPECIES_8a";
    FishSpecies[FishSpecies["SPECIES_8b"] = 13] = "SPECIES_8b";
    FishSpecies[FishSpecies["SPECIES_8c"] = 14] = "SPECIES_8c";
    FishSpecies[FishSpecies["SPECIES_8d"] = 15] = "SPECIES_8d";
    FishSpecies[FishSpecies["SPECIES_9a"] = 16] = "SPECIES_9a";
    FishSpecies[FishSpecies["SPECIES_9b"] = 17] = "SPECIES_9b";
    FishSpecies[FishSpecies["SPECIES_10"] = 18] = "SPECIES_10";
    // SPECIES_11a,    // Anguille européenne [jaune]
    // SPECIES_11b     // Anguille européenne [civelle]
})(FishSpecies = exports.FishSpecies || (exports.FishSpecies = {}));

},{}],163:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Verificateur = void 0;
const nub_1 = require("../nub");
const compute_node_1 = require("../compute-node");
const session_1 = require("../session");
const result_1 = require("../util/result");
const espece_1 = require("./espece");
const fish_species_1 = require("./fish_species");
const espece_params_1 = require("./espece_params");
const message_1 = require("../util/message");
const verificateur_params_1 = require("./verificateur_params");
const par_simulation_1 = require("../par/par_simulation");
const base_1 = require("../base");
const par_1 = require("../par/par");
class Verificateur extends nub_1.Nub {
    constructor(dbg = false) {
        super(new verificateur_params_1.VerificateurParams(), dbg);
        /** Index of the reseult element to verify, when the pass to verify is variating */
        this._variatingIndex = 0;
        this._calcType = compute_node_1.CalculatorType.Verificateur;
        // UID of Session Nub to verify
        this.nubToVerify = undefined;
        // List of fish species to check the pass against
        this.speciesList = [];
        this._species = [];
        // no calculated param
    }
    get species() {
        return this._species;
    }
    get prms() {
        return this._prms;
    }
    /** Contains either string keys from FishSpecies enum, or UIDs of Espece Session Nubs */
    get speciesList() {
        return this._props.getPropValue("speciesList");
    }
    set speciesList(l) {
        this.properties.setPropValue("speciesList", l);
        // (re)create Espece instances
        this.initSpecies();
    }
    /** finds the Nub to verify by its UID */
    get nubToVerify() {
        let nub;
        const nubUID = this._props.getPropValue("nubToVerify");
        if (nubUID !== undefined && nubUID !== "") {
            nub = session_1.Session.getInstance().findNubByUid(nubUID);
            // silent fail if (nub === undefined)
        }
        return nub;
    }
    /** defines the Nub to verify by setting property "nubToVerify" to the UID of the given Nub */
    set nubToVerify(n) {
        let uid = ""; // empty value
        if (n !== undefined) {
            uid = n.uid;
        }
        this.properties.setPropValue("nubToVerify", uid);
    }
    /**
     * Adds a new empty ResultElement to the current Result object, so that
     * computation result is stored into it, via set currentResult(); does
     * the same for all Espece Nubs
     */
    initNewResultElement() {
        super.initNewResultElement();
        // do the same for Espece
        for (const c of this.species) {
            c.initNewResultElement();
        }
    }
    /**
     * Sets this._result to a new empty Result, before starting a new CalcSerie();
     * does the same for all Espece Nubs
     */
    reinitResult() {
        super.reinitResult();
        // do the same for Espece
        for (const c of this.species) {
            c.reinitResult();
        }
    }
    CalcSerie(rInit) {
        // calc pass systematically
        this.nubToVerify.CalcSerie();
        // check presence of valid result
        if (!this.nubToVerify.result || !this.nubToVerify.result.ok) {
            this._result = new result_1.Result(undefined, this);
            this._result.addMessage(new message_1.Message(message_1.MessageCode.ERROR_VERIF_ERRORS_IN_PASS));
            return this._result;
        }
        // (re)create Espece instances
        this.initSpecies();
        this.progress = 0;
        this.resetResult();
        // reinit Result and children Results
        this.reinitResult();
        const passIsVarying = this.nubToVerify.resultHasMultipleValues();
        if (!passIsVarying) { // pass to verify is not varying
            // prepare a new slot to store results
            this._variatingIndex = 0;
            this.initNewResultElement();
            this.doCalc(undefined, rInit);
            this.progress = 100;
            // global error messages
            if (this._result.hasErrorMessages()) {
                this._result.addMessage(new message_1.Message(message_1.MessageCode.ERROR_VERIF_KO));
            }
            else if (this._result.hasWarningMessages()) {
                this._result.addMessage(new message_1.Message(message_1.MessageCode.WARNING_VERIF_OK_BUT));
            }
            else { // if no error log (everything went ok), add a success message
                this._result.addMessage(new message_1.Message(message_1.MessageCode.INFO_VERIF_OK));
            }
        }
        else { // pass to verify is varying
            const size = this.nubToVerify.result.resultElements.length;
            let progressStep;
            const remainingProgress = 100 - this.progress;
            progressStep = remainingProgress / size;
            let stageErrorsCount = 0;
            // iterate over pass to verify
            for (let l = 0; l < size; l++) {
                // prepare a new slot to store results
                this._variatingIndex = l;
                this.initNewResultElement();
                // don't calculate if this iteration has errors
                if (!this.nubToVerify.result.resultElements[this._variatingIndex].ok) {
                    const m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_VARYING_ERRORS_IN_PASS);
                    m.extraVar.i = String(l + 1);
                    this._result.resultElement.addMessage(m);
                    stageErrorsCount++;
                }
                else {
                    // calculate
                    this.doCalc(undefined); // résultat dans this.currentResult (resultElement le plus récent)
                    if (!this._result.resultElement.ok) {
                        stageErrorsCount++;
                    }
                }
                // update progress
                this.progress += progressStep;
            }
            // round progress to 100%
            this.progress = 100;
            // global error messages
            if (stageErrorsCount === size) {
                this._result.addMessage(new message_1.Message(message_1.MessageCode.ERROR_VERIF_VARYING_KO));
            }
            else if (stageErrorsCount > 0) {
                this._result.addMessage(new message_1.Message(message_1.MessageCode.WARNING_VERIF_VARYING_OK_BUT));
            }
            else {
                this._result.addMessage(new message_1.Message(message_1.MessageCode.INFO_VERIF_VARYING_OK));
            }
        }
        this.notifyResultUpdated();
        return this._result;
    }
    Calc() {
        this.currentResult = this.Equation();
        return this.result;
    }
    /**
     * Returns 1 if everything is OK, 0 as soon as a species check fails
     */
    Equation() {
        let v = 1;
        const r = new result_1.Result(v, this);
        // species-independent check: PAR
        if (this.nubToVerify instanceof par_simulation_1.ParSimulation) {
            // 1. slope
            const maxS = [par_1.ParType.PLANE, par_1.ParType.FATOU].includes(this.nubToVerify.parType) ? 0.2 : 0.16;
            if (base_1.isGreaterThan(this.nubToVerify.prms.S.v, maxS)) {
                const m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAR_SLOPE);
                m.extraVar.S = this.nubToVerify.prms.S.v;
                m.extraVar.maxS = maxS;
                r.resultElement.addMessage(m);
                r.vCalc = 0;
                return r;
            }
            // 2. downstream fall
            if (base_1.isGreaterThan(this.nubToVerify.result.resultElements[this._variatingIndex].values.DH, 0, 1e-3)) {
                r.log.add(new message_1.Message(message_1.MessageCode.ERROR_VERIF_PAR_DH));
                r.vCalc = 0;
                return r;
            }
        }
        let spIndex = 0;
        for (const speciesNub of this._species) {
            // give the pass pointer
            speciesNub.passToCheck = this.nubToVerify;
            // give the index to check if pass is variating
            speciesNub.indexToCheck = this._variatingIndex;
            const subRes = speciesNub.Calc();
            let m;
            const isCustomSpecies = !(this.speciesList[spIndex] in fish_species_1.FishSpecies);
            // if at least one error log in Espece nub, add one at Verificateur level, so that .ok is false
            // else add a success info message
            if (isCustomSpecies) {
                if (subRes.resultElements[this._variatingIndex].hasErrorMessages()) {
                    m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_SPECIES_NUB_KO);
                }
                else if (subRes.resultElements[this._variatingIndex].hasWarningMessages()) {
                    m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_SPECIES_NUB_OK_BUT);
                }
                else {
                    m = new message_1.Message(message_1.MessageCode.INFO_VERIF_SPECIES_NUB_OK);
                }
                m.extraVar.uid = this.speciesList[spIndex];
            }
            else {
                if (subRes.resultElements[this._variatingIndex].hasErrorMessages()) {
                    m = new message_1.Message(message_1.MessageCode.ERROR_VERIF_SPECIES_GROUP_KO);
                }
                else if (subRes.resultElements[this._variatingIndex].hasWarningMessages()) {
                    m = new message_1.Message(message_1.MessageCode.WARNING_VERIF_SPECIES_GROUP_OK_BUT);
                }
                else {
                    m = new message_1.Message(message_1.MessageCode.INFO_VERIF_SPECIES_GROUP_OK);
                }
                m.extraVar.speciesGroup = this.speciesList[spIndex];
            }
            r.resultElement.addMessage(m);
            // @TODO remove vCalc as no parameter is calculated ?
            if (subRes.vCalc === 0) {
                v = 0;
            }
            spIndex++;
        }
        r.vCalc = v;
        return r;
    }
    /**
     * Creates an instance of Espece for each element of this.speciesList, and places
     * it in this.species so that their results are kept and their logs are accessible
     * after calculation has ended
     */
    initSpecies() {
        this._species = [];
        for (const s of this.speciesList) {
            this._species.push(this.getEspeceFromString(s));
        }
    }
    /**
     * Retrieves an Espece from a given string, which should be either
     * the UID of an Espece Session Nub, or the label of an element
     * of FishSpecies enum
     * @param s
     */
    getEspeceFromString(s) {
        let e;
        // is it a predefined species ?
        if (Object.keys(fish_species_1.FishSpecies).includes(s)) {
            if (s === "SPECIES_CUSTOM") {
                throw new Error("Verificateur.getEspeceFromString(): cannot automatically instantiate SPECIES_CUSTOM");
            }
            // instanciate out of Session
            e = new espece_1.Espece(new espece_params_1.EspeceParams(1, 1));
            e.species = fish_species_1.FishSpecies[s];
        }
        else {
            // is it a session Nub ?
            e = session_1.Session.getInstance().findNubByUid(s);
            if (e === undefined) {
                throw new Error(`Verificateur.getEspeceFromString(): could not find Nub ${s} in the session`);
            }
        }
        return e;
    }
    // tslint:disable-next-line:no-empty
    setParametersCalculability() { }
    // no calculated param
    findCalculatedParameter() {
        return undefined;
    }
}
exports.Verificateur = Verificateur;

},{"../base":3,"../compute-node":5,"../nub":33,"../par/par":74,"../par/par_simulation":76,"../session":104,"../util/message":151,"../util/result":155,"./espece":160,"./espece_params":161,"./fish_species":162,"./verificateur_params":164}],164:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerificateurParams = void 0;
const params_equation_1 = require("../param/params-equation");
class VerificateurParams extends params_equation_1.ParamsEquation {
}
exports.VerificateurParams = VerificateurParams;

},{"../param/params-equation":92}]},{},[1]);
