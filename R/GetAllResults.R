#' Retrieve results of a nub
#'
#' @param sv8 V8 context including the Cassiopée's code (See [Cassiopee]), a session (See [LoadSession]), and a calculated nub (See [CalcNub])
#' @param uid [character] identifying the calculated nub
#'
#' @return [data.frame] with the results of the calculation calculated variable for all the variables (columns) and variation of the parameters (rows).
#' @export
GetAllResults <- function(sv8, uid) {
    sNub <- paste0("jalhyd.Session.getInstance().findNubByUid(\"", uid, "\")")
    # Get Result from parent Nub
    dfRes <- GetNubResults(sv8, sNub)
    # Get Result from children Nub and bind results
    nChildren <- sv8$get(paste0(sNub, ".getChildren().length"))
    if(nChildren > 0) {
        for(iChild in 1:nChildren) {
            dfChild <- GetNubResults(sv8, paste0(sNub, ".getChildren()[", iChild - 1, "]"))
            names(dfChild) <- paste0("child[", iChild, "].", names(dfChild))
        }
        dfRes <- cbind(dfRes, dfChild)
    }
    return(dfRes)
}

GetNubResults <- function(sv8, sNub) {
    nRes <- sv8$get(paste0(sNub, ".result.resultElements.length"))
    elements <- 0:(nRes-1)
    lRes <- lapply(elements, function(x) {
        sv8$get(paste0(sNub, ".result.resultElements[", x, "].values"))
    })
    df <- do.call(data.frame,
                 lapply(names(lRes[[1]]),
                             function(t) sapply(1:length(lRes),
                                                function(j) lRes[[j]][[t]])))
    colnames(df) <- names(lRes[[1]])
    return(df)
}
