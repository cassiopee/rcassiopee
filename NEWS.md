# rcassiopee 0.1.2

* Added a `NEWS.md` file to track changes to the package.

# rcassiopee 0.1.1 - 2020-10-07

* dependencies: update to jalhyd 4.13.1
* fix: error in `setParams`

# rcassiopee 0.1.0 - 2020-08-05

* Initial version with load session, run nub and get result (#3), et parameters setting (#2) capabilities
